package com.haier.cellarette.libutils;

public class CommonUtils {
    public static final String APP_VERSION_CODE = "当前版本号";//

    public static String TXTID_BEAN = "TXTID_BEAN";// 绘本列表保存到本地跳转TAG
    public static String TIPS_WUSHUJU = "暂无数据";//
    public static String TIPS_WUWANG = "请确认网络是否可用";//

    public static final String HUIBEN_CATEIDS = "HUIBEN_CATEIDS";
    public static final String HUIBEN_IDS_sourceType = "sourceType";
    public static final String HUIBEN_IDS_ZONG = "bookId";
    public static final String HUIBEN_IDS = "id";
    public static final String HUIBEN_TITLES = "name";
    public static final String HUIBEN_XMLY = "喜马拉雅听书数据";
    public static final String HUIBEN_TSGIF = "听书gif";
    public static final String HUIBEN_TAG1 = "热门绘本";//
    public static final String HUIBEN_TAG2 = "我的书架";//
    public static final String HUIBEN_TAG3 = "历史记录";//
    public static final String HUIBEN_TAG4 = "分类";//
    public static final String HUIBEN_TAG5 = "国学经典";//
    public static final String F1_TAG = "刷新首页我的书架";//
    public static final String LISTENBOOK_TAG1 = "后台播放";//
    public static final String LISTENBOOK_TAG21 = "后台播放的书id_zong";//
    public static final String LISTENBOOK_TAG22 = "后台播放的书id";//
    public static final String listen_action = "听书floatbuttn";//
    public static final String READBOOK_AUTOBUTTON = "自动阅读绘本";//

    public static final String index_action = "qiehuan_fragment";

    // 听书bufen
    public static final String LB_IDS1 = "id1";// 总id
    public static final String LB_IDS2 = "id2";// item id
    public static final String LB_NAME1 = "name1";
    public static final String LB_NAME2 = "name2";
    public static final String LB_TYPE = "type";
    public static final String LB_TYPE1 = "type1";
    public static final String LB_broadcastreceiver = "LB_broadcastreceiver";// 监听第三方来电音乐变化
    public static final String RB_broadcastreceiver = "RB_broadcastreceiver";// 监听第三方来电音乐变化

    //
    public static final String HIOS_ID = "id";
    public static final String HIOS_NAME = "name";
    public static final String HIOS_TYPE = "type";


    public static final String USER_TEL = "用户的手机号";//
    public static final String USER_TOKEN = "用户的TOKEN";//
    public static final String USER_NAME = "用户的昵称";//
    public static final String USER_IMG = "用户的头像";//
    public static final String USER_FORCE_LOGIN = "是否强制登录";//
    public static final String USER_FORCE_CHILD = "是否强制填写宝宝信息";//
    public static final String START_COUNT = "启动音乐";//
    public static final String img_file_url = ConstantsUtils.file_url_img;
    public static final String img_file_name = "uploadimg" + ".jpg";
    public static final String img_file_name2 = "shareimg" + ".jpg";

    public static final String HUIBEN_PAYSUCCESS_TAG = "支付成功后跳转tag";// -1 1 支付VIP成功弹窗 2 支付绘本成功弹窗
    public static final int HUIBEN_PAYSUCCESS_TAG1 = 1111;// 从绘本页面购买VIP
    public static final int HUIBEN_PAYSUCCESS_TAG2 = 2222;// 从绘本页面单本购买

    public static final int HXTK_PAYSUCCESS_TAG3 = 3333;//支付
    public static final String HUIBEN_PAYSUCCESS = "支付成功的弹窗";// -1 1 支付VIP成功弹窗 2 支付绘本成功弹窗

    public static final String WECHAT_USER_NAME = "微信用户的昵称";//
    public static final String WECHAT_USER_IMG = "微信用户的头像";//


    public static final String TAG_MEDALBEAN1 = "MedalBean";// 勋章bean
    public static final String TAG_MEDALID1 = "MedalId";// 勋章id

    public static final String TAG_ADCOMMON_ID = "id1";// 广告id
    public static final String ID_SCTD = "上次听到";//

    public static final String BANJI_ID = "banji_id";//
    public static final String INTENT_FROM = "INTENT_FROM";//
    public static final String MMKV_KEY1 = "MMKV_KEY1";//
    public static final String MMKV_SEX = "用户性别";//
    public static final String MMKV_IMG = "用户头像";//
    public static final String MMKV_TEL = "用户电话";//
    public static final String MMKV_NAME = "用户昵称";//
    public static final String MMKV_ADDRESS_NAME = "用户地址昵称";//
    public static final String MMKV_ADDRESS_TEL = "用户地址电话";//
    public static final String MMKV_ADDRESS = "用户地址";//
    public static final String MMKV_ADDRESS_DETAIL = "用户详细地址";//
    public static final String MMKV_ADDRESS_SHENG = "用户地址省";//
    public static final String MMKV_ADDRESS_SHI = "用户地址市";//
    public static final String MMKV_ADDRESS_QU = "用户地址区";//
    public static final String MMKV_ADDRESS_ALL = "用户地址拼接";//
    public static final String MMKV_CLASS = "用户年级";//
    public static final String MMKV_TOKEN = "用户token";//
    public static final String MMKV_forceLogin = "是否强制登录";// 1 强 0非

    public static final String MMKV_startBgImg = "开机背景";//
    public static final String MMKV_aboutUs = "首页->我的->关于我们";//
    public static final String MMKV_how2Teach = "首页如何上课";//
    public static final String MMKV_aboutHxEdu = "首页关于合象";//
    public static final String MMKV_startSound = "开机声音";//
    public static final String MMKV_serviceProtocol = "用户服务协议";//
    public static final String MMKV_privacyPolicy = "隐私政策";//
    public static final String MMKV_hxEduWechatImg = "合象课堂公众号图片";//
    public static final String MMKV_hxEduWechatNum = "合象课堂公众号二维码号";//
    public static final String MMKV_contactInfo = "联系人信息";//
    public static final String MMKV_admission = "入学通知书";//图片
    public static final String MMKV_couponstips = "优惠券说明";//

//    HxEduWechatImg("hxEduWechatImg", "合象课堂公众号二维码图片"),
//    AboutUs("aboutUs", "关于我们"),
//    AboutHxEdu("aboutHxEdu", "关于合象"),
//    How2Teach("how2Teach", "如何上课"),
//    ServiceProtocol("serviceProtocol", "用户服务协议"),
//    PrivacyPolicy("privacyPolicy", "隐私政策"),
//    ForceLogin("forceLogin", "强制登陆"),
//    StartSound("startSound", "启动声音"),
//    StartBgImg("startBgImg", "启动图片"),
//    ContactInfo("contactInfo", "联系人信息"),
//    AppLoginBg("appLoginBg", "登录背景图"),
//    Admission("admission", "入学通知书")
}
