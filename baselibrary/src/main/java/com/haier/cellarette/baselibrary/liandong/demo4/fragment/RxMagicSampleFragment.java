package com.haier.cellarette.baselibrary.liandong.demo4.fragment;
/*
 * Copyright (c) 2018-2019. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.haier.cellarette.baselibrary.R;
import com.kunminx.linkage.LinkageRecyclerView;
import com.kunminx.linkage.adapter.viewholder.LinkagePrimaryViewHolder;
import com.kunminx.linkage.adapter.viewholder.LinkageSecondaryFooterViewHolder;
import com.kunminx.linkage.adapter.viewholder.LinkageSecondaryHeaderViewHolder;
import com.kunminx.linkage.adapter.viewholder.LinkageSecondaryViewHolder;
import com.kunminx.linkage.bean.BaseGroupedItem;
import com.kunminx.linkage.bean.DefaultGroupedItem;
import com.kunminx.linkage.defaults.DefaultLinkagePrimaryAdapterConfig;
import com.kunminx.linkage.defaults.DefaultLinkageSecondaryAdapterConfig;

import java.util.List;

/**
 * Create by KunMinX at 19/5/8
 */
public class RxMagicSampleFragment extends Fragment {

    private LinkageRecyclerView linkage;
    private MaterialButton btn_preview;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_liandong4_fragment_rxmagic, container, false);
        linkage = view.findViewById(R.id.linkage);
        btn_preview = view.findViewById(R.id.btn_preview);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLinkageDatas(linkage);
    }

    private void initLinkageDatas(LinkageRecyclerView linkage) {
        Gson gson = new Gson();
        List<DefaultGroupedItem> items = gson.fromJson(getString(R.string.operators_json),
                new TypeToken<List<DefaultGroupedItem>>() {
                }.getType());

        linkage.init(items);
        linkage.setDefaultOnItemBindListener(new DefaultLinkagePrimaryAdapterConfig.OnPrimaryItemClickListner() {
            @Override
            public void onItemClick(LinkagePrimaryViewHolder holder, View view, String title) {
                Snackbar.make(view, title, Snackbar.LENGTH_SHORT).show();
            }
        }, new DefaultLinkagePrimaryAdapterConfig.OnPrimaryItemBindListener() {
            @Override
            public void onBindViewHolder(LinkagePrimaryViewHolder primaryHolder, String title) {

            }
        }, new DefaultLinkageSecondaryAdapterConfig.OnSecondaryItemBindListener() {
            @Override
            public void onBindViewHolder(LinkageSecondaryViewHolder secondaryHolder, BaseGroupedItem<DefaultGroupedItem.ItemInfo> item) {
//                Snackbar.make(secondaryHolder.itemView, item.info.getTitle(), Snackbar.LENGTH_SHORT).show();
            }
        }, new DefaultLinkageSecondaryAdapterConfig.OnSecondaryHeaderBindListener() {
            @Override
            public void onBindHeaderViewHolder(LinkageSecondaryHeaderViewHolder headerHolder, BaseGroupedItem<DefaultGroupedItem.ItemInfo> item) {

            }
        }, new DefaultLinkageSecondaryAdapterConfig.OnSecondaryFooterBindListener() {
            @Override
            public void onBindFooterViewHolder(LinkageSecondaryFooterViewHolder footerHolder, BaseGroupedItem<DefaultGroupedItem.ItemInfo> item) {

            }
        });
    }
}
