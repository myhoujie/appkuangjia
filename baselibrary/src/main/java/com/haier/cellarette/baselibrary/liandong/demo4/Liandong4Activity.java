package com.haier.cellarette.baselibrary.liandong.demo4;

/*
 * Copyright (c) 2018-2019. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.R;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.BottomSheetSampleFragment;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.ElemeSampleFragment;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.GreatPhoneSampleFragment;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.RxMagicSampleFragment;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.SwitchSampleFragment;
import com.haier.cellarette.baselibrary.liandong.demo4.fragment.YoumiStoreSampleFragment;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerSlide;

import java.util.ArrayList;

/**
 * Create by KunMinX at 19/4/27
 */
public class Liandong4Activity extends AppCompatActivity {

    private String[] mFragmentTitles;
    private String[] mFragmentPaths;
    private Fragment[] mFragments;

    //    private ActivityMainBinding mBinding;
    private Toolbar toolbar;
    private ViewPagerSlide view_pager;
    private TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_liandong4);
        setContentView(R.layout.activity_liandong4);
        toolbar = findViewById(R.id.toolbar);
        view_pager = findViewById(R.id.view_pager);
        tabs = findViewById(R.id.tabs);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        mFragmentTitles = getResources().getStringArray(R.array.fragments);
        mFragmentPaths = getResources().getStringArray(R.array.fragments_full_path);
        mFragments = new Fragment[mFragmentTitles.length];
        ArrayList<String> titleDatas = new ArrayList<>();
        titleDatas.add("BottomSheetSampleFragment");
//        titleDatas.add("DialogSampleFragment");
        titleDatas.add("ElemeSampleFragment");
        titleDatas.add("GreatPhoneSampleFragment");
        titleDatas.add("RxMagicSampleFragment");
        titleDatas.add("SwitchSampleFragment");
        titleDatas.add("YoumiStoreSampleFragment");
        ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();
        fragmentList.add(new BottomSheetSampleFragment());
//        fragmentList.add(new DialogSampleFragment());
        fragmentList.add(new ElemeSampleFragment());
        fragmentList.add(new GreatPhoneSampleFragment());
        fragmentList.add(new RxMagicSampleFragment());
        fragmentList.add(new SwitchSampleFragment());
        fragmentList.add(new YoumiStoreSampleFragment());
        MyViewPageAdapter myViewPageAdapter = new MyViewPageAdapter(getSupportFragmentManager(), titleDatas, fragmentList);
        view_pager.setScroll(true);
        view_pager.setOffscreenPageLimit(4);
        view_pager.setAdapter(myViewPageAdapter);
        tabs.setupWithViewPager(view_pager);
        tabs.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TabUtils.tabSelect(tabs, tab);
                String tag = (String) tab.getTag();

            }
        });
        TabUtils.setIndicatorWidth(tabs, 60);
//        mBinding.viewPager.setAdapter(new PagerAdapter(this) {
//            @NonNull
//            @Override
//            public Fragment createFragment(int position) {
//                return Liandong4Activity.this.createFragment(position);
//            }
//
//            @Override
//            public int getItemCount() {
//                return mFragmentTitles.length;
//            }
//        });

//        new TabLayoutMediator(mBinding.tabs, mBinding.viewPager, (tab, position) -> {
//            tab.setText(mFragmentTitles[position].replace("SampleFragment", ""));
//        }).attach();
    }

    public class MyViewPageAdapter extends FragmentPagerAdapter {
        private ArrayList<String> titleList;
        private ArrayList<Fragment> fragmentList;

        public MyViewPageAdapter(FragmentManager fm, ArrayList<String> titleList, ArrayList<Fragment> fragmentList) {
            super(fm);
            this.titleList = titleList;
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }
    }

    private Fragment createFragment(Integer index) {
        if (mFragments[index] != null) {
            return mFragments[index];
        }
        String name = mFragmentPaths[index];
        Fragment fragment = null;
        try {
            fragment = (Fragment) Class.forName(name).newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        mFragments[index] = fragment;
        return mFragments[index];
    }
}
