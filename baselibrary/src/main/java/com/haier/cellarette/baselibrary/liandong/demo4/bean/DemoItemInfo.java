package com.haier.cellarette.baselibrary.liandong.demo4.bean;

import com.kunminx.linkage.bean.BaseGroupedItem;

public class DemoItemInfo extends BaseGroupedItem.ItemInfo {
    private String content;
    private String imgUrl;
    private String cost;
    private String cost2 = "";
    private String cost3 = "";

    public DemoItemInfo(String title, String group, String content) {
        super(title, group);
        this.content = content;
    }

    public DemoItemInfo(String title, String group, String content, String imgUrl) {
        this(title, group, content);
        this.imgUrl = imgUrl;
    }

    public DemoItemInfo(String title, String group, String content, String imgUrl, String cost, String cost2, String cost3) {
        this(title, group, content, imgUrl);
        this.cost = cost;
        this.cost2 = cost2;
        this.cost3 = cost3;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCost2() {
        return cost2;
    }

    public void setCost2(String cost2) {
        this.cost2 = cost2;
    }

    public String getCost3() {
        return cost3;
    }

    public void setCost3(String cost3) {
        this.cost3 = cost3;
    }
}