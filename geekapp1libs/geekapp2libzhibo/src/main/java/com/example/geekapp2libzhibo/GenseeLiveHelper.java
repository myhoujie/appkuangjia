package com.example.geekapp2libzhibo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.gensee.app.ApplicationUtil;
import com.gensee.app.ConfigApp;
import com.gensee.app.GLiveSharePreferences;
import com.gensee.common.GenseeConfig;
import com.gensee.common.RTSharedPref;
import com.gensee.common.ServiceType;
import com.gensee.entity.InitParam;
import com.gensee.entity.JoinParams;
import com.gensee.entity.LoginResEntity;
import com.gensee.glive.DestopStartUpActivity;
import com.gensee.glive.vod.VodActivity;
import com.gensee.holder.join.Join;
import com.gensee.holder.qa.impl.QaSharePerference;
import com.gensee.room.RtSdk;
import com.gensee.util.GenseeUtils;

import java.util.UUID;


/**
 * GenseeLiveActivity 测试启动，实际是构造InitParam和GSFastConfig
 * 分别如下
 *
 * @param isPublishMode 是否是发布端，true=发布端，false=观看端
 * @param domain        设置域名
 *                      <p>
 *                      若一个url为http://test.gensee.com/site/training   域名是“test.gensee.com”</p>
 * @param number        直播或点播编号<p>设置对应编号，如果是点播则是点播编号，是直播便是直播编号。
 *                      请注意不要将id理解为编号。
 *                      作用等价于id，但不是id。有id可以不用编号，有编号可以不用id</p>
 * @param loginAccount  站点登录账号
 *                      <p> 设置站点认证账号 即登录站点的账号
 * @param loginPwd      站点登录密码
 *                      <p> 设置站点认证密码 即登录站点的密码
 *                      可选，如果后台设置直播需要登录或点播需要登录，那么登录密码要正确  且帐号同时也必须填写正确 </p>
 * @param nickName      昵称
 *                      <p>设置昵称  用于直播间显示或统计   一定要填写</p>
 * @param joinPwd       直播口令
 *                      <p>设置口令 即直播的保护密码
 *                      可选 如果后台设置了保护密码 请填写对应的口令</p>
 * @param k             第三方认证K值
 * @param serviceType   设置服务类型   webcast站点对应 WEBCAST   training 对应 TRAINING
 */

public class GenseeLiveHelper {
    public static void startLive(final Context context) {
        JoinParams joinParams = new JoinParams();
        joinParams.setAppServiceType(ConfigApp.AppServiceType.TRAINING);
        joinParams.setJoinType(JoinParams.JOIN_LIVE_TYPE);
        joinParams.setNeedWatchWord(true);
        joinParams.setPubMode(JoinParams.PUB_MODE_DEF);

        joinParams.setDomain("sairobo.gensee.com");
        joinParams.setLiveId("HUHCnFXWRe");
        joinParams.setName("qwerdf");
        joinParams.setNumber("90539913");
        joinParams.setPwd("222111");
        live(context,joinParams);
    }

    public static void startLiveSelect(final Context context) {
        context.startActivity(new Intent(context, DestopStartUpActivity.class));
    }

    public static void startVod(final Context context) {
        vod(context);
    }


    private static void live(final Context context,JoinParams joinParams) {

        //跳转直播
        final Join join = new Join();
        join.setOnJoinListener(new Join.OnJoinListerner() {
            @Override
            public void needLogin(String subject, String number, int appServiceType, String domain, String webcastId, boolean needWatchWord) {
                Log.d("setOnJoinListener", "needLogin");
            }

            @Override
            public void needWatchWord(String subject, String sNumber, int appServiceType, String domain, String webcastId) {
                Log.d("setOnJoinListener", "needWatchWord");

            }

            @Override
            public void join(String lunachCode, LoginResEntity entity) {
                Log.d("setOnJoinListener", "join");
                join.login(context, lunachCode, entity);

            }

            @Override
            public void onError(int errorCode) {
                Log.d("setOnJoinListener", "onError" + errorCode);

            }

            @Override
            public void needNickName(String subject, String sNumber, int appServiceType, String domain, String webcastId) {
                Log.d("setOnJoinListener", "needLogin");

            }

            @Override
            public boolean getLiveEnterFlag() {
                return false;
            }
        });
        join.initWithGensee(joinParams, context);
    }

    private static void vod(Context context) {
//        //		跳转点播
        InitParam onlineParam = new InitParam();
        onlineParam.setDomain("sairobo.gensee.com");
        onlineParam.setJoinPwd("735008");
        onlineParam.setNickName("sss");
        onlineParam.setNumber("30164924");

        onlineParam.setDeviceType(256);
        onlineParam.setDownload(false);
        onlineParam.setK("");
        onlineParam.setLoginAccount("");
        onlineParam.setLoginPwd("");

        onlineParam.setServiceType(ServiceType.TRAINING);
        onlineParam.setUserData("");
        onlineParam.setUserId(0);

        Intent intent = new Intent();
        intent.putExtra(ConfigApp.VOD_JOIN_PARAM, onlineParam);
        intent.setClass(context, VodActivity.class);
        context.startActivity(intent);
    }

}
