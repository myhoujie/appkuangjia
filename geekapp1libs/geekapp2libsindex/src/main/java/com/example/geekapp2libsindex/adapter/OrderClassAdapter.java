package com.example.geekapp2libsindex.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.RefundPop;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan;
import com.fanli.biz3hxktdetail.bean.LiveSonOrderBean;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;

public class OrderClassAdapter extends BaseQuickAdapter<LiveSonOrderBean, BaseViewHolder> {

    public OrderClassAdapter() {
        super(R.layout.adapter_order_class);
    }

    @Override
    protected void convert(BaseViewHolder helper, final LiveSonOrderBean courseBean) {
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView priceTv = helper.itemView.findViewById(R.id.priceTv);
        TextView dateTv = helper.itemView.findViewById(R.id.dateTv);
        TextView teacherTv = helper.itemView.findViewById(R.id.teacherTv);
        TextView btnTv = helper.itemView.findViewById(R.id.btnTv);
        View cl = helper.itemView.findViewById(R.id.cl);

        if (getData().size() == 1 && helper.getAdapterPosition() == 0) {
            cl.setPadding(cl.getPaddingLeft(), 0, cl.getPaddingRight(), cl.getPaddingBottom());
        }

        //标题
        String title = "";
        boolean hasSubject = courseBean.getSubjectNames() != null && courseBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : courseBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + courseBean.getOrderName());
        if (hasSubject) {
            for (int i = 0; i < courseBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(mContext, TextUtils.equals(courseBean.getClassAttribute(), "experience") ? Color.parseColor("#666666") : Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        priceTv.setText("￥ " + courseBean.getAmountCourse());
        dateTv.setText(courseBean.getCourseTimeRange());
        teacherTv.setText(courseBean.getLecturerName());

        //订单状态(待支付unpaid,已支付paid,已取消cancel,已退款refund)
        if (TextUtils.equals(courseBean.getOrderStatus(), "paid") || TextUtils.equals(courseBean.getOrderStatus(), "refund")) {
            if (TextUtils.equals(courseBean.getOrderStatus(), "paid")) {
                btnTv.setText("入学通知书");
                btnTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.NoticeDetailActivity");
                        intent.putExtra("orderId", courseBean.getId());
                        mContext.startActivity(intent);
                    }
                });
            } else {
                btnTv.setText("已退款");
                btnTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showRefundPopView();
                    }
                });
            }
            btnTv.setVisibility(View.VISIBLE);
        } else {
            btnTv.setVisibility(View.INVISIBLE);
        }
    }

    private BasePopupView refundPopView;

    private void showRefundPopView() {
        if (refundPopView == null) {
            refundPopView = new XPopup.Builder(mContext)
                    .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                    .asCustom(new RefundPop(mContext));
        }
        refundPopView.show();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (refundPopView != null && refundPopView.isShow()) {
            refundPopView.dismiss();
        }
    }
}
