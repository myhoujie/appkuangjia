package com.example.geekapp2libsindex.rili;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HMonthSheduleBean;
import com.example.biz3hxktindex.bean.HlistItemTodayBean;
import com.example.biz3hxktindex.bean.HlistItemTodayBean1;
import com.example.biz3hxktindex.presenter.HMonthShedulePresenter;
import com.example.biz3hxktindex.presenter.HlistItemByDayPresenter;
import com.example.biz3hxktindex.view.HMonthSheduleView;
import com.example.biz3hxktindex.view.HlistItemTodayView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.RiliActFragment2Adapter;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.necer.calendar.BaseCalendar;
import com.necer.calendar.Miui10Calendar;
import com.necer.enumeration.SelectedModel;
import com.necer.listener.OnCalendarChangedListener;
import com.necer.listener.OnCalendarMultipleChangedListener;
import com.necer.painter.InnerPainter;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RiliActFragment1 extends SlbBaseIndexFragment implements HMonthSheduleView, HlistItemTodayView {

    private TextView tv1;
    private ImageView iv1;
    private ImageView iv2;
    private Miui10Calendar monthCalendar;
    private XRecyclerView recycler_view1;
    private String ids;
    private LocalDate localDate1;
    private EmptyViewNew1 emptyview1_order;
    private RiliActFragment2Adapter riliActFragment2Adapter;
    private List<HlistItemTodayBean1> mList1;
    private boolean is_first;
    private int i = 0;
    private HMonthShedulePresenter presenter1;
    private HlistItemByDayPresenter presenter2;


    @Override
    public void onDestroyView() {
        presenter1.onDestory();
        presenter2.onDestory();
        super.onDestroyView();
    }

    @Override
    public void call(Object value) {
        ids = (String) value;

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_riliactfragment1;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        rootView.findViewById(R.id.tv1).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SendToFragment("demo1的fragment1页面");
//            }
//        });
        tv1 = rootView.findViewById(R.id.tv1);
        iv1 = rootView.findViewById(R.id.iv1);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 前
                monthCalendar.toLastPager();
//                if (localDate1 != null) {
//                    set_month_point(TimeUtils.string2Millis(localDate1.toString() + "", "yyyy-MM-dd"));
//                }
            }
        });
        iv2 = rootView.findViewById(R.id.iv2);
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 后
                monthCalendar.toNextPager();
//                if (localDate1 != null) {
//                    set_month_point(TimeUtils.string2Millis(localDate1.toString() + "", "yyyy-MM-dd"));
//                }
            }
        });
        monthCalendar = rootView.findViewById(R.id.monthCalendar);
        //
        monthCalendar.setSelectedMode(SelectedModel.SINGLE_UNSELECTED);
        monthCalendar.setDefaultSelectFitst(false);
        //
        monthCalendar.post(new Runnable() {
            @Override
            public void run() {
//                String aaaa = TimeUtils.millis2String(System.currentTimeMillis(), "yyyy-MM-dd");
//                monthCalendar.jumpDate(aaaa);
                monthCalendar.toToday();
            }
        });
        monthCalendar.setOnCalendarChangedListener(new OnCalendarChangedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onCalendarChange(BaseCalendar baseCalendar, int year, int month, LocalDate localDate) {
//                tv1.setText(year + "年" + month + "月" + "   当前页面选中 " + localDate);
                tv1.setText(year + "年" + month + "月");
                localDate1 = localDate;
                Log.d("ssssssss", "setOnCalendarChangedListener:::" + year + "年" + month + "月" + "   当前页面选中 " + localDate);
                //
                i++;
                if (is_first && i > 2) {
                    if (localDate != null) {
                        SendToFragment(TimeUtils.string2Millis(localDate.toString(), "yyyy-MM-dd") + "");
                        //
                        retryData(TimeUtils.string2Millis(localDate.toString(), "yyyy-MM-dd"));
                    } else {
                        //
                        retryData2(TimeUtils.string2Millis(year + "-" + month, "yyyy-MM"));
                    }
                } else {
                    is_first = true;
                }
            }
        });
        monthCalendar.setOnCalendarMultipleChangedListener(new OnCalendarMultipleChangedListener() {
            @Override
            public void onCalendarChange(BaseCalendar baseCalendar, int year, int month, List<LocalDate> currectSelectList, List<LocalDate> allSelectList) {
                tv1.setText(year + "年" + month + "月" + " 当前页面选中 " + currectSelectList.size() + "个  总共选中" + allSelectList.size() + "个");

                Log.d("ssssssss", year + "年" + month + "月");
                Log.d("ssssssss", "当前页面选中：：" + currectSelectList);
                Log.d("ssssssss", "全部选中：：" + allSelectList);

            }
        });
        //
        emptyview1_order = rootView.findViewById(R.id.emptyview1_order);
        recycler_view1 = rootView.findViewById(R.id.recycler_view1);
        //
        emptyview1_order.bind(recycler_view1);
        emptyview1_order.notices("今日无课程", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview1_order.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                //重试
                donetwork();
            }
        });
        //
        recycler_view1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        riliActFragment2Adapter = new RiliActFragment2Adapter();
        recycler_view1.setAdapter(riliActFragment2Adapter);
        riliActFragment2Adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HlistItemTodayBean1 bean1 = (HlistItemTodayBean1) adapter.getData().get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                intent.putExtra("courseId", bean1.getLiveCourseId() + "");
                intent.putExtra("toSectionPosition", bean1.getOrders());
                startActivity(intent);
            }
        });
        //
        presenter1 = new HMonthShedulePresenter();
        presenter1.onCreate(this);
        presenter2 = new HlistItemByDayPresenter();
        presenter2.onCreate(this);
        donetwork();
    }

    private void donetwork() {
        emptyview1_order.loading();
        retryData(System.currentTimeMillis());
    }

    private void retryData(long timers) {
        set_month_point(timers);
        set_day_list(timers + "");
    }

    private void retryData2(long timers) {
        set_month_point(timers);
//        set_day_list(timers + "");
    }

    private void set_day_list(String times) {
        presenter2.get_listItemByDay(times);
    }

    private void set_month_point(long times) {
        String aaaa = TimeUtils.millis2String(times, "yyyyMM");
        presenter1.get_MonthShedule(aaaa);
    }

    /**
     * 页面传值操作部分
     *
     * @param id1
     */
    private void SendToFragment(String id1) {
        //举例
//        IndexFoodFragmentUpdateIds iff = new IndexFoodFragmentUpdateIds();
//        iff.setFood_definition_id(id1);
//        iff.setFood_name(id2);
        if (getActivity() != null && getActivity() instanceof RiliAct) {
            ((RiliAct) getActivity()).callFragment(id1, RiliActFragment2.class.getName());
        }
    }

    @Override
    public void OnMonthSheduleSuccess(HMonthSheduleBean hMonthSheduleBean) {
        //
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.CHINA);
//        String dates1 = TimeUtils.millis2String(1569910001000L, dateFormat);
//        String dates2 = TimeUtils.millis2String(1570687601000L, dateFormat);
//        List<String> pointList = new ArrayList<>();
//        pointList.add(dates1);
//        pointList.add(dates2);
//        List<String> pointList = Arrays.asList("2019-9-01", "2019-9-19", "2019-9-20", "2019-09-23", "2019-10-01");
        List<String> pointList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        for (String bean : hMonthSheduleBean.getTimeStamps()) {
            pointList.add(TimeUtils.millis2String(Long.valueOf(bean), dateFormat));
        }
        InnerPainter innerPainter = (InnerPainter) monthCalendar.getCalendarPainter();
        innerPainter.setPointList(pointList);
    }

    @Override
    public void OnMonthSheduleNodata(String s) {

    }

    @Override
    public void OnMonthSheduleFail(String s) {

    }

    @Override
    public void OnlistItemTodaySuccess(HlistItemTodayBean hlistItemTodayBean) {
        //
        emptyview1_order.success();
        //
        mList1 = new ArrayList<>();
        mList1 = hlistItemTodayBean.getItems();
        riliActFragment2Adapter.setNewData(mList1);
        if (mList1.size() == 0) {
            emptyview1_order.nodata();
        } else {
            emptyview1_order.success();
        }
    }

    @Override
    public void OnlistItemTodayNodata(String s) {
        emptyview1_order.nodata();
    }

    @Override
    public void OnlistItemTodayFail(String s) {
        emptyview1_order.nodata();
    }
}
