package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.ClassDetailServerAdapter;
import com.fanli.biz3hxktdetail.bean.CourseServiceBean;
import com.lxj.xpopup.core.BottomPopupView;

import java.util.List;

public class ClassDetailServerPopup extends BottomPopupView {
    private RecyclerView rv;
    private ClassDetailServerAdapter adapter;
    private List<CourseServiceBean> serviceBeanList;

    public ClassDetailServerPopup(@NonNull Context context, List<CourseServiceBean> serviceBeanList) {
        super(context);
        this.serviceBeanList = serviceBeanList;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_classdetail_server;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ClassDetailServerAdapter();
        rv.setAdapter(adapter);
        adapter.setNewData(serviceBeanList);
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

}
