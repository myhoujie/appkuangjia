package com.example.geekapp2libsindex.fragment.demo.f1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geekapp2libsindex.R;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {
 
    private Context context;
    private int i = 0;
 
    public StudentAdapter(Context context) {
        this.context = context;
    }
 
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
 
        View view = LayoutInflater.from(context).inflate(R.layout.demo_f1_yulan_item1, parent, false);
        StudentAdapter.ViewHolder holder = new StudentAdapter.ViewHolder(view);
 
        return holder;
    }
 
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name.setText("学生+" + i);
    }
 
    @Override
    public int getItemCount() {
        return 20;
    }
 
    public void setData(int position) {
        this.i = position;
    }
 
 
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
 
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.f1_tv1);
        }
    }
}
