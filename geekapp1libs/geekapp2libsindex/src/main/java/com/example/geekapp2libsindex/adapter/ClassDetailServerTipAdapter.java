package com.example.geekapp2libsindex.adapter;

import android.widget.TextView;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.CourseServiceBean;

public class ClassDetailServerTipAdapter extends BaseQuickAdapter<CourseServiceBean, BaseViewHolder> {

    private float minTextSize;
    private int position = -1;

    public ClassDetailServerTipAdapter() {
        super(R.layout.adapter_classdetail_server_tip);
    }

    @Override
    protected void convert(final BaseViewHolder helper, CourseServiceBean item) {
        final TextView textTv = helper.itemView.findViewById(R.id.textTv);
        textTv.setText(item.getName());
//        if (position != -1 && minTextSize == 0 && helper.getAdapterPosition() == position) {
//            helper.itemView.post(new Runnable() {
//                @Override
//                public void run() {
//                    adjustHeightTextSize(textTv, helper.itemView.getMeasuredWidth());
//                }
//            });
//        }
//        System.out.println("position:" + helper.getAdapterPosition() + ",width:" + helper.itemView.getMeasuredWidth());
//        if (minTextSize > 0) {
//            textTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, minTextSize);
//        }
    }

    //
//    private void adjustHeightTextSize(TextView textView, float maxWidth) {
//        String text = textView.getText().toString();
//        float textSize = textView.getTextSize();
//        TextPaint textPaintClone = new TextPaint(textView.getPaint());
//        System.out.println("maxWidth:" + maxWidth);
////        maxWidth -= DisplayUtil.dip2px(mContext, 14);
//        while (textPaintClone.measureText(text) > maxWidth) {
//            textSize--;
//            textPaintClone.setTextSize(textSize);
//        }
////        System.out.println("textSize:" + textSize);
//        minTextSize = textSize;
//        notifyDataSetChanged();
//    }
//
    public void setTextSizeByPosition(int position) {
        this.position = position;
    }
}
