package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.lxj.xpopup.core.CenterPopupView;

public class TwoBtnPop extends CenterPopupView {
    private String content;
    private String title;
    private String left;
    private String right;
    private OnBtnListener onBtnListener;

    public TwoBtnPop(@NonNull Context context, String title, String content, String left, String right, OnBtnListener onBtnListener) {
        super(context);
        this.content = content;
        this.title = title;
        this.left = left;
        this.right = right;
        this.onBtnListener = onBtnListener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_two_btn;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView contentTv = findViewById(R.id.contentTv);
        TextView titleTv = findViewById(R.id.titleTv);
        TextView leftTv = findViewById(R.id.leftTv);
        TextView rightTv = findViewById(R.id.rightTv);

        contentTv.setText(content);
        titleTv.setText(title);
        leftTv.setText(left);
        rightTv.setText(right);

        leftTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBtnListener != null) {
                    onBtnListener.onLeft();
                }
            }
        });
        rightTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBtnListener != null) {
                    onBtnListener.onRight();
                }
            }
        });
    }

    public interface OnBtnListener {
        void onLeft();

        void onRight();
    }
}