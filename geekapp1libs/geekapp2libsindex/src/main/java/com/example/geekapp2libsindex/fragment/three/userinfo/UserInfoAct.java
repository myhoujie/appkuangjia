package com.example.geekapp2libsindex.fragment.three.userinfo;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.aigestudio.wheelpicker.WheelPicker;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCategoryBean1;
import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.example.biz3hxktindex.presenter.HCategoryPresenter;
import com.example.biz3hxktindex.presenter.HTouxiangPresenter;
import com.example.biz3hxktindex.presenter.HUserInfoPresenter;
import com.example.biz3hxktindex.presenter.HUserInfoupdatePresenter;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.biz3hxktindex.view.HTouxiangView;
import com.example.biz3hxktindex.view.HUserInfoView;
import com.example.biz3hxktindex.view.HUserInfoupdateView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.NamePopup;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.uploadimg.ClipImageActivity;
import com.example.slbappcomm.uploadimg.UploadImgUtils;
import com.example.slbappcomm.uploadimg.view.UploadImgGetFileUtil;
import com.example.slbappcomm.utils.LoginImgUtils;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.SlbLoginUtil2;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.umeng.analytics.MobclickAgent;
import com.zhy.base.fileprovider.FileProvider7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.shaohui.bottomdialog.BottomDialog;

public class UserInfoAct extends SlbBaseActivity implements View.OnClickListener, HUserInfoView, HTouxiangView, HCategoryView, HUserInfoupdateView {

    private ImageView ivBack;
    private TextView tvCenterContent;

    private RelativeLayout rl1;
    private GlideImageView iv1;
    private RelativeLayout rl2;
    private TextView tv1;
    private RelativeLayout rl3;
    private TextView tv2;
    private RelativeLayout rl4;
    private TextView tv3;
    private RelativeLayout rl5;
    private TextView tv4;

    //
    private String name;
    private String nianji;
    private String sex;
    private String address;
    private String swChildId;
    private String swUserId;

    //
    private BasePopupView loadingPopup;
    private BottomDialog bottomDialog;

    private HUserInfoPresenter presenter;
    private HTouxiangPresenter presenterImg;
    private HCategoryPresenter hCategoryPresenter;
    private HUserInfoupdatePresenter hUserInfoupdatePresenter;

    private MessageReceiverIndex mMessageReceiver;


    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("UserInfoAct".equals(intent.getAction())) {
                    MyLogUtil.e("---geekyun--", intent.getStringExtra("UserInfoAct"));
                    tv4.setText(intent.getStringExtra("UserInfoAct"));
                    address = intent.getStringExtra("UserInfoAct");
                    hUserInfoupdatePresenter.get_myaddressupdate(sex, nianji, name, swChildId, swUserId, address);
                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        if (bottomDialog != null) {
            bottomDialog.setViewListener(null);
            bottomDialog.dismiss();
            bottomDialog = null;
        }
        if (UploadImgUtils.getInstance(getApplicationContext()).getPopupWindow() != null) {
            UploadImgUtils.getInstance(getApplicationContext()).diss();
        }
        presenter.onDestory();
        presenterImg.onDestory();
        hCategoryPresenter.onDestory();
        hUserInfoupdatePresenter.onDestory();
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
//        retryData();
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_userinfo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        // 获取资料bufen
        presenter = new HUserInfoPresenter();
        presenter.onCreate(this);
        // 上传头像bufen
        presenterImg = new HTouxiangPresenter();
        presenterImg.onCreate(this);
        // 年级bufen
        hCategoryPresenter = new HCategoryPresenter();
        hCategoryPresenter.onCreate(this);
        // 更新资料bufen
        hUserInfoupdatePresenter = new HUserInfoupdatePresenter();
        hUserInfoupdatePresenter.onCreate(this);
        //
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("UserInfoAct");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("编辑资料");
        name = "";
        nianji = "";
        sex = "";
        address = "";
        swChildId = "";
        swUserId = "";
        retryData();
    }

    private void retryData() {
        presenter.get_userinfo();

    }


    private void onclick() {
        ivBack.setOnClickListener(this);
        iv1.setOnClickListener(this);
        rl1.setOnClickListener(this);
        rl2.setOnClickListener(this);
        rl3.setOnClickListener(this);
        rl4.setOnClickListener(this);
        rl5.setOnClickListener(this);
    }

    private void findview() {
        ivBack = findViewById(R.id.iv_back1_order);
        tvCenterContent = findViewById(R.id.tv_center_content);
        rl1 = findViewById(R.id.rl1);
        iv1 = findViewById(R.id.iv1);
        rl2 = findViewById(R.id.rl2);
        tv1 = findViewById(R.id.tv1);
        rl3 = findViewById(R.id.rl3);
        tv2 = findViewById(R.id.tv2);
        rl4 = findViewById(R.id.rl4);
        tv3 = findViewById(R.id.tv3);
        rl5 = findViewById(R.id.rl5);
        tv4 = findViewById(R.id.tv4);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.iv1 || view.getId() == R.id.rl1) {
            // 头像
            MobclickAgent.onEvent(UserInfoAct.this, "minepage_avatar");
            if (!SlbLoginUtil2.get().isUserLogin()) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SlbLoginActivity"));
                return;
            }
            UploadImgUtils.getInstance(getApplicationContext()).
                    uploadHeadImage(UserInfoAct.this,
                            new File(CommonUtils.img_file_url, CommonUtils.img_file_name)
                            , new UploadImgUtils.OnPopBackListener() {
                                @Override
                                public void onClick1(PopupWindow popupWindow) {
                                    //权限判断
                                    if (ContextCompat.checkSelfPermission(UserInfoAct.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(UserInfoAct.this, UploadImgGetFileUtil.needPermissions, UploadImgUtils.WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                                    } else {
                                        if (FileUtils.createFileByDeleteOldFile(CommonUtils.img_file_url + CommonUtils.img_file_name)) {
                                            gotoCamera(new File(CommonUtils.img_file_url, CommonUtils.img_file_name));
                                        }
                                    }
                                    popupWindow.dismiss();
                                }

                                @Override
                                public void onClick2(PopupWindow popupWindow) {
                                    //权限判断
                                    if (ContextCompat.checkSelfPermission(UserInfoAct.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(UserInfoAct.this, UploadImgGetFileUtil.needPermissions, UploadImgUtils.READ_EXTERNAL_STORAGE_REQUEST_CODE);
                                    } else {
                                        if (FileUtils.createFileByDeleteOldFile(CommonUtils.img_file_url + CommonUtils.img_file_name)) {
                                            gotoPhoto();
                                        }
                                    }
                                    popupWindow.dismiss();
                                }

                                @Override
                                public void onClick3(PopupWindow popupWindow) {
                                    popupWindow.dismiss();
                                }
                            });

        } else if (view.getId() == R.id.rl2) {
            // 学员姓名
            loadingPopup = new XPopup.Builder(UserInfoAct.this)
                    .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                    .autoOpenSoftInput(true)
                    .asCustom(new NamePopup(UserInfoAct.this, new NamePopup.OnDismiss() {
                        @Override
                        public void OnDismiss(final String content) {
//                                loadingPopup.dismiss();
                            loadingPopup.postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                            if (loadingPopup.isShow())
                                    loadingPopup.dismissWith(new Runnable() {
                                        @Override
                                        public void run() {
                                            tv1.setText(content);
                                            name = content;
                                            hUserInfoupdatePresenter.get_myaddressupdate(sex, nianji, name, swChildId, swUserId, address);
                                        }
                                    });
                                }
                            }, 100);
                        }
                    }, tv1.getText().toString().trim()))
                    .show();
        } else if (view.getId() == R.id.rl3) {
            // 年级
            hCategoryPresenter.get_category("grade", "0");

        } else if (view.getId() == R.id.rl4) {
            // 性别
            bottomDialog = BottomDialog.create(getSupportFragmentManager());
            bottomDialog.setViewListener(new BottomDialog.ViewListener() {
                private String content = "";

                @Override
                public void bindView(View v) {
                    // // You can do any of the necessary the operation with the view
                    TextView tv111 = v.findViewById(R.id.tv1);
                    TextView tv222 = v.findViewById(R.id.tv2);
                    final WheelPicker wheelPicker = v.findViewById(R.id.wheelview11);
                    //
                    List<String> data = new ArrayList<>();
                    data.add(getResources().getString(R.string.slb_userinfo_tip1));
                    data.add(getResources().getString(R.string.slb_userinfo_tip2));
                    wheelPicker.setData(data);
                    //
                    tv111.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottomDialog.setViewListener(null);
                            bottomDialog.dismiss();
                        }
                    });
                    //
                    tv222.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottomDialog.setViewListener(null);
                            bottomDialog.dismiss();
                            if (TextUtils.isEmpty(content)) {
                                content = wheelPicker.getData().get(0).toString();
                                tv3.setText(content);
                            } else {
                                tv3.setText(content);
                            }
                            // 男 女
                            if (TextUtils.equals(content, getResources().getString(R.string.slb_userinfo_tip1))) {
                                sex = "1";
                            } else {
                                sex = "2";
                            }
                            hUserInfoupdatePresenter.get_myaddressupdate(sex, nianji, name, swChildId, swUserId, address);
                        }
                    });
                    //
                    wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(WheelPicker picker, Object data, int position) {
                            content = data.toString();
//                            tv3.setText(content);
                        }
                    });
                }
            })
                    .setLayoutRes(R.layout.pop_hxkt_sex)
                    .setDimAmount(0.2f)            // Dialog window dim amount(can change window background color）, range：0 to 1，default is : 0.2f
                    .setCancelOutside(true)     // click the external area whether is closed, default is : true
                    .setTag("BottomDialog_nianji1")     // setting the DialogFragment tag
                    .show();
//            new XPopup.Builder(UserInfoAct.this)
////                        .enableDrag(false)
//                    .asCenterList("", new String[]{"男", "女"},
//                            new OnSelectListener() {
//                                @Override
//                                public void onSelect(int position, String text) {
////                                    Toasty.normal(UserInfoAct.this, text).show();
//                                    tv3.setText(text);
//                                }
//                            })
//                    .show();
        } else if (view.getId() == R.id.rl5) {
            // 收货地址
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouhuodizhiAct"));
        } else {

        }
    }


    /**
     * 跳转到相册
     */
    public void gotoPhoto() {
        Log.d("evan", "*****************打开图库********************");
        //跳转到调用系统图库
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "请选择图片"), UploadImgUtils.REQUEST_PICK);
    }

    /**
     * 跳转到照相机 File file = new File(ConstantsUtils.file_url, "uploadimg" + ".jpg")
     */
    public void gotoCamera(File file) {
        Log.d("evan", "*****************打开相机********************");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            Uri fileUri1 = FileProvider7.getUriForFile(this, file);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri1);
            startActivityForResult(takePictureIntent, UploadImgUtils.REQUEST_CAPTURE);
        }
    }

    /**
     * 打开截图界面 File file = new File(ConstantsUtils.file_url, "uploadimg" + ".jpg")  Uri.fromFile(file)
     */
    public void gotoClipActivity(Uri uri) {
        if (uri == null) {
            return;
        }
        Intent intent = new Intent();
        intent.setClass(this, ClipImageActivity.class);
        intent.putExtra("type", 2);
        intent.putExtra("img_file_url", CommonUtils.img_file_url);
        intent.putExtra("img_file_name", CommonUtils.img_file_name);
        intent.setData(uri);
        startActivityForResult(intent, UploadImgUtils.REQUEST_CROP_PHOTO);
    }

    /**
     * 外部存储权限申请返回
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == UploadImgUtils.WRITE_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
//                Fragment4UploadImgUtils.getInstance(getActivity()).gotoCamera(getActivity(), new File(img_file_url, img_file_name));
                if (FileUtils.createFileByDeleteOldFile(CommonUtils.img_file_url + CommonUtils.img_file_name)) {
                    gotoCamera(new File(CommonUtils.img_file_url, CommonUtils.img_file_name));
                }
            }
        } else if (requestCode == UploadImgUtils.READ_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
//                Fragment4UploadImgUtils.getInstance(getActivity()).gotoPhoto(getActivity());
                if (FileUtils.createFileByDeleteOldFile(CommonUtils.img_file_url + CommonUtils.img_file_name)) {
                    gotoPhoto();
                }
            }
        }
    }

    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UploadImgUtils.REQUEST_CAPTURE: //调用系统相机返回
                if (resultCode == RESULT_OK) {
//                    Fragment4UploadImgUtils.getInstance(getActivity()).gotoClipActivity(getActivity(),2, Uri.fromFile(new File(CommonUtils.img_file_url, CommonUtils.img_file_name)));
                    gotoClipActivity(Uri.fromFile(new File(CommonUtils.img_file_url, CommonUtils.img_file_name)));
                }
                break;
            case UploadImgUtils.REQUEST_PICK:  //调用系统相册返回
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
//                    Fragment4UploadImgUtils.getInstance(getActivity()).gotoClipActivity(getActivity(),2, uri);
                    gotoClipActivity(uri);
                }
                break;
            case UploadImgUtils.REQUEST_CROP_PHOTO:  //剪切图片返回
                if (resultCode == RESULT_OK) {
                    final Uri uri = data.getData();
                    if (uri == null) {
                        return;
                    }
                    String cropImagePath = UploadImgUtils.getInstance(getApplicationContext()).getRealFilePathFromUri(UserInfoAct.this, uri);
                    Bitmap bitMap = BitmapFactory.decodeFile(cropImagePath);

//                    iv1.loadImage(cropImagePath, R.drawable.ic_def_loading);

//                    if (TextUtils.isEmpty(cropImagePath)) {
//                        return;
//                    }
//                    if (bitMap == null) {
//                        return;
//                    }
//                    ivCenter.setImageBitmap(bitMap);
                    //此处后面可以将bitMap转为二进制上传后台网络
                    //......
//                    Toasty.normal(BaseApp.get(), "上传到服务器").show();
//                    presenterImg.getUploadUserImgData2(DeviceUtils.getAndroidID(), SPUtils.getInstance().getString(CommonUtils.USER_TOKEN), BitmapToBase64Utils.bitmapToBase64(bitMap));
                    File file = new File(CommonUtils.img_file_url + CommonUtils.img_file_name);
                    MyLogUtil.e("ssssssssssss1", cropImagePath);
                    MyLogUtil.e("ssssssssssss2", file.toString());
                    presenterImg.get_touxiang2(new File(cropImagePath));

                }
                break;
        }
    }


    /**
     * 业务逻辑部分
     */


    // 上传头像bufen
    @Override
    public void OnTouxiangSuccess(HTouxiangBean hTouxiangBean) {
//        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_IMG, hTouxiangBean.getUrl());
//        LoginImgUtils.getInstance(getApplicationContext()).get_head_icon_new(iv1, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_SEX));
        retryData();
    }

    @Override
    public void OnTouxiangNodata(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnTouxiangFail(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnUserInfoSuccess(HUserInfoBean hUserInfoBean) {
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_SEX, hUserInfoBean.getChildGender());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_IMG, hUserInfoBean.getChildAvatar());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_NAME, hUserInfoBean.getNikeName());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_TEL, hUserInfoBean.getPhone());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_CLASS, hUserInfoBean.getChildGradeName());

        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_NAME, hUserInfoBean.getNikeName());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_TEL, hUserInfoBean.getPhone());
        //

        name = hUserInfoBean.getNikeName();
        nianji = hUserInfoBean.getChildGrade();
        sex = hUserInfoBean.getChildGender();
        address = hUserInfoBean.getAddressDtl();
        swChildId = hUserInfoBean.getSwChildId();
        swUserId = hUserInfoBean.getSwUserId();
        LoginImgUtils.getInstance(getApplicationContext()).get_head_icon_new(iv1, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_SEX));
        tv1.setText(hUserInfoBean.getNikeName());
        tv2.setText(hUserInfoBean.getChildGradeName());
        if (TextUtils.equals(hUserInfoBean.getChildGender(), "1")) {
            tv3.setText(getResources().getString(R.string.slb_userinfo_tip1));
        } else {
            tv3.setText(getResources().getString(R.string.slb_userinfo_tip2));
        }
        sex = hUserInfoBean.getChildGender();
        tv4.setText(hUserInfoBean.getAddressDtl());
    }

    @Override
    public void OnUserInfoNodata(String s) {
//        ToastUtils.showLong(s);
    }

    @Override
    public void OnUserInfoFail(String s) {
//        ToastUtils.showLong(s);
    }

    @Override
    public void OnCategorySuccess(final HCategoryBean hCategoryBean) {
        final List<String> data = new ArrayList<>();
        for (HCategoryBean1 bean1 : hCategoryBean.getList()) {
            data.add(bean1.getName());
        }
        final BottomDialog bottomDialog = BottomDialog.create(getSupportFragmentManager());
        bottomDialog.setViewListener(new BottomDialog.ViewListener() {
            private String content = "";

            @Override
            public void bindView(View v) {
                // // You can do any of the necessary the operation with the view
                TextView tv111 = v.findViewById(R.id.tv1);
                TextView tv222 = v.findViewById(R.id.tv2);
                final WheelPicker wheelPicker = v.findViewById(R.id.wheelview11);
                wheelPicker.setData(data);
                //
                tv111.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomDialog.setViewListener(null);
                        bottomDialog.dismiss();
                    }
                });
                //
                tv222.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomDialog.setViewListener(null);
                        bottomDialog.dismiss();
                        if (TextUtils.isEmpty(content)) {
                            content = wheelPicker.getData().get(0).toString();
                            tv2.setText(content);
                        } else {
                            tv2.setText(content);
                        }
                        //更新资料
                        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
                            if (TextUtils.equals(content, hCategoryBean.getList().get(i).getName())) {
                                nianji = hCategoryBean.getList().get(i).getCode();
                            }
                        }
                        hUserInfoupdatePresenter.get_myaddressupdate(sex, nianji, name, swChildId, swUserId, address);
                    }
                });
                //
                wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(WheelPicker picker, Object data, int position) {
                        content = data.toString();
                    }
                });
            }
        }).setLayoutRes(R.layout.pop_hxkt_banji)
                .setDimAmount(0.2f)            // Dialog window dim amount(can change window background color）, range：0 to 1，default is : 0.2f
                .setCancelOutside(true)     // click the external area whether is closed, default is : true
                .setTag("BottomDialog_nianji1")     // setting the DialogFragment tag
                .show();
    }

    @Override
    public void OnCategoryNodata(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnCategoryFail(String s) {
        ToastUtils.showLong(s);
    }


    @Override
    public void OnUserInfoUpdateSuccess(String s) {
        ToastUtils.showLong(s);
        retryData();
    }

    @Override
    public void OnUserInfoUpdateNodata(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnUserInfoUpdateFail(String s) {
        ToastUtils.showLong(s);
    }


}
