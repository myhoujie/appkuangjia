package com.example.geekapp2libsindex.adapter;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.HalfCircleDecorView;
import com.fanli.biz3hxktdetail.bean.Coupon;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CouponSelectAdapter extends BaseQuickAdapter<Coupon, BaseViewHolder> {
    private int lastSelectIndex = -1;
    private Set<Integer> unfoldSet;

    public CouponSelectAdapter() {
        super(R.layout.adapter_coupon_select2);
        unfoldSet = new HashSet<>();
    }

    @Override
    protected void convert(final BaseViewHolder helper, Coupon item) {
        final int position = helper.getAdapterPosition();
        HalfCircleDecorView halfCdv = helper.itemView.findViewById(R.id.halfCdv);
        TextView unitTv = helper.itemView.findViewById(R.id.unitTv);
        TextView moneyTv = helper.itemView.findViewById(R.id.moneyTv);
        TextView useMoneyTv = helper.itemView.findViewById(R.id.useMoneyTv);
        View ll_right = helper.itemView.findViewById(R.id.ll_right);
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView timeRangeTv = helper.itemView.findViewById(R.id.timeRangeTv);
        ImageView checkedIv = helper.itemView.findViewById(R.id.checkedIv);
        TextView simpleTipTv = helper.itemView.findViewById(R.id.simpleTipTv);
        final View ll_detail = helper.itemView.findViewById(R.id.ll_detail);
        final TextView detailTv = helper.itemView.findViewById(R.id.detailTv);
        final View arrowIv = helper.itemView.findViewById(R.id.arrowIv);
        moneyTv.setText(item.getAmount());
        timeRangeTv.setText(item.getUseTimeRange());
        useMoneyTv.setText(item.getCouponTypeDesc());
        titleTv.setText(item.getCouponName());
        simpleTipTv.setText(item.getCouponRangeDesc());
        final String detail = "仅限指定课程使用\n" + item.getCouponRangeDesc().replaceAll(",", "    ");
        detailTv.setText(detail);
        if (TextUtils.equals("1", item.getCanSelected())) {//可用
            helper.itemView.setEnabled(true);
            halfCdv.setEnabled(true);
            ll_right.setEnabled(true);
            unitTv.setTextColor(Color.parseColor("#FF701B"));
            moneyTv.setTextColor(Color.parseColor("#FF701B"));
            useMoneyTv.setTextColor(Color.parseColor("#FF701B"));
            titleTv.setTextColor(Color.parseColor("#333333"));
            checkedIv.setVisibility(View.VISIBLE);
            checkedIv.setImageResource(R.drawable.icon_not_checked);

            if (position == lastSelectIndex) {//被选中
                checkedIv.setImageResource(R.drawable.icon_checked);
            } else {
                checkedIv.setImageResource(R.drawable.icon_not_checked);
            }
        } else {
            helper.itemView.setEnabled(false);
            halfCdv.setEnabled(false);
            ll_right.setEnabled(false);
            unitTv.setTextColor(Color.parseColor("#C0C0C0"));
            moneyTv.setTextColor(Color.parseColor("#C0C0C0"));
            useMoneyTv.setTextColor(Color.parseColor("#C0C0C0"));
            titleTv.setTextColor(Color.parseColor("#999999"));
            checkedIv.setVisibility(View.INVISIBLE);
        }

        if (TextUtils.equals(item.getStrShrink(), "1")) {
            simpleTipTv.setVisibility(View.GONE);
            ll_detail.setVisibility(View.VISIBLE);

            if (unfoldSet.contains(position)) {
                detailTv.setVisibility(View.VISIBLE);
                arrowIv.setRotation(180);
                unfoldSet.add(position);
            } else {
                detailTv.setVisibility(View.GONE);
                arrowIv.setRotation(0);
                unfoldSet.remove(position);
            }
        } else {
            simpleTipTv.setVisibility(View.VISIBLE);
            ll_detail.setVisibility(View.GONE);
        }
        ll_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unfoldSet.contains(position)) {//已展开
                    detailTv.setVisibility(View.GONE);
                    arrowIv.setRotation(0);
                    unfoldSet.remove(position);
                } else {//未展开
                    detailTv.setVisibility(View.VISIBLE);
                    arrowIv.setRotation(180);
                    unfoldSet.add(position);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            ImageView checkedIv = holder.itemView.findViewById(R.id.checkedIv);
            String payload = payloads.get(0).toString();
            if (TextUtils.equals("cancelSelect", payload)) {
                checkedIv.setImageResource(R.drawable.icon_not_checked);
            } else if (TextUtils.equals("select", payload)) {
                checkedIv.setImageResource(R.drawable.icon_checked);
            }
        }
    }

    public void setSelectCouponId(int selectPosition, String checkedLiveCouponInfoId) {
        if (selectPosition == -1) {//之前没有选择使用优惠券
            if (!TextUtils.isEmpty(checkedLiveCouponInfoId) && getData().size() > 0) {//第一次进来，匹配最大券
                for (int i = 0; i < getData().size(); i++) {
                    Coupon coupon = getData().get(i);
                    if (TextUtils.equals(coupon.getLiveCouponInfoId(), checkedLiveCouponInfoId)) {
                        lastSelectIndex = i;
                        notifyItemChanged(selectPosition, "select");
                        break;
                    }
                }
            } else {//选择不使用优惠券
                if (lastSelectIndex > -1 && lastSelectIndex < getData().size()) {
                    notifyItemChanged(lastSelectIndex, "cancelSelect");
                }
                lastSelectIndex = -1;
            }
        } else {//切换可用优惠券
            if (selectPosition != lastSelectIndex) {
                if (lastSelectIndex > -1 && lastSelectIndex < getData().size()) {
                    notifyItemChanged(lastSelectIndex, "cancelSelect");
                }
                if (selectPosition > -1 && selectPosition < getData().size()) {
                    notifyItemChanged(selectPosition, "select");
                }
                lastSelectIndex = selectPosition;
            }
        }
    }

    public void setSelectCouponId(int selectPosition) {
        if (selectPosition == lastSelectIndex) {
            return;
        }
        if (lastSelectIndex > -1 && lastSelectIndex < getData().size()) {
            notifyItemChanged(lastSelectIndex, "cancelSelect");
        }
        if (selectPosition > -1 && selectPosition < getData().size()) {
            notifyItemChanged(selectPosition, "select");
        }
        lastSelectIndex = selectPosition;
    }
}
