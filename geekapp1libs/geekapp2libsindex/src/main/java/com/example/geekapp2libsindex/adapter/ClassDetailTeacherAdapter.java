package com.example.geekapp2libsindex.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.Lecturer;
import com.geek.libglide47.base.CircleImageView;

public class ClassDetailTeacherAdapter extends BaseQuickAdapter<Lecturer, BaseViewHolder> {
    private Context context;
    private boolean showArrow;

    public ClassDetailTeacherAdapter(Context context) {
        this(context, true);
    }

    public ClassDetailTeacherAdapter(Context context, boolean showArrow) {
        super(R.layout.adapter_classdetail_teacher);
        this.context = context;
        this.showArrow = showArrow;
    }

    @Override
    protected void convert(BaseViewHolder helper, final Lecturer item) {
        CircleImageView headIv = helper.itemView.findViewById(R.id.headIv);

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.default_head_teather)
                .error(R.drawable.default_head_teather)
                .fallback(R.drawable.default_head_teather); //url为空的时候,显示的图片;
        Glide.with(context).load(item.getAvatar()).apply(options).into(headIv);
        TextView nameTv = helper.itemView.findViewById(R.id.nameTv);
        TextView tipTv = helper.itemView.findViewById(R.id.tipTv);
        nameTv.setText(item.getNikeName());
        tipTv.setText(item.getTeacherType());
        ImageView arrowIv = helper.itemView.findViewById(R.id.arrowIv);
        if (TextUtils.equals(item.getTeacherTypeCode(), "1") && showArrow) {
            arrowIv.setVisibility(View.VISIBLE);
            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.TeacherActivity");
                    intent.putExtra("teacherId", item.getSysUserId());
                    context.startActivity(intent);
                }
            });
        } else {
            arrowIv.setVisibility(View.INVISIBLE);
            helper.itemView.setOnClickListener(null);
        }
    }


}
