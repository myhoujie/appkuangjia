package com.example.geekapp2libsindex.rili;

import androidx.collection.SparseArrayCompat;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseFragment;


/**
 * Created by shining on 2017/2/27 0027.
 */

public class RiliActFragmentFactory {
    private static SparseArrayCompat<Class<? extends SlbBaseFragment>> sIndexFragments = new SparseArrayCompat<>();

    static {

        sIndexFragments.put(R.id.riliact1_page_0_item_0, RiliActFragment1.class);//模块日历
//        sIndexFragments.put(R.id.riliact1_page_0_item_1, RiliActFragment2.class);//模块时间轴列表

    }

    public static Class<? extends SlbBaseFragment> get(int id) {
        if (sIndexFragments.indexOfKey(id) < 0) {
            throw new UnsupportedOperationException("cannot find fragment by " + id);
        }
        return sIndexFragments.get(id);
    }

    public static SparseArrayCompat<Class<? extends SlbBaseFragment>> get() {
        return sIndexFragments;
    }
}
