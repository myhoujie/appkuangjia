package com.example.geekapp2libsindex.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HlistItemTodayBean1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan2;

public class RiliActFragment2Adapter extends BaseQuickAdapter<HlistItemTodayBean1, BaseViewHolder> {

    public RiliActFragment2Adapter() {
        super(R.layout.recycleview_hxkt_riliactfragment2_item1);
    }

    @Override
    protected void convert(BaseViewHolder helper, HlistItemTodayBean1 item) {
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        ImageView iv2 = helper.itemView.findViewById(R.id.iv2);
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        if (helper.getAdapterPosition() == getItemCount() - 1) {
            iv2.setVisibility(View.GONE);
        } else {
            iv2.setVisibility(View.VISIBLE);
        }
        set_flowlayout(tv1, item);
        if (TextUtils.equals(item.getItemStatus(), "1")) {
            tv2.setTextColor(ContextCompat.getColor(mContext, R.color.color_333333));
            tv2.setText("直播中");
            select_useful(tv2, R.drawable.hxkticon_play, true);
        } else if (TextUtils.equals(item.getItemStatus(), "0")) {
            tv2.setTextColor(ContextCompat.getColor(mContext, R.color.colorFF4E08));
            tv2.setText("未开始");
            select_useful(tv2, R.drawable.hxkticon_play, false);
        } else if (TextUtils.equals(item.getItemStatus(), "2")) {
            tv2.setTextColor(ContextCompat.getColor(mContext, R.color.color_333333));
            tv2.setText("进行中");
            select_useful(tv2, R.drawable.hxkticon_play, false);
        } else if (TextUtils.equals(item.getItemStatus(), "3")) {
            tv2.setTextColor(ContextCompat.getColor(mContext, R.color.colorCCCCCC));
            tv2.setText("已结束");
            select_useful(tv2, R.drawable.hxkticon_play, false);
        }
        tv3.setText(item.getItemTitle());
        tv4.setText(item.getItemTimeRange());
    }

    private void select_useful(TextView tv1, int drawabless, boolean is_dra) {
        if (!is_dra) {
            tv1.setCompoundDrawables(null, null, null, null);
        } else {
            Drawable drawable = mContext.getResources().getDrawable(drawabless);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(),
                    drawable.getMinimumHeight());
            tv1.setCompoundDrawables(drawable, null, null, null);
        }
    }

    private void set_flowlayout(TextView tv1, HlistItemTodayBean1 bean) {
        //标题
        String title = "";
        boolean hasSubject = bean.getSubjectNames() != null && bean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : bean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + bean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < bean.getSubjectNames().size(); i++) {
                String colorString = "#F78451";
                if (TextUtils.equals("system", bean.getClassAttribute())) {
                    colorString = "#F78451";
                } else if (TextUtils.equals("experience", bean.getClassAttribute())) {
                    colorString = "#666666";
                }
                spannableString.setSpan(new RoundBackgroundColorSpan2(mContext, Color.parseColor(colorString), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        tv1.setText(spannableString);
    }

}
