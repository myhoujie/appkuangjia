package com.example.geekapp2libsindex.common;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerSlide;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public abstract class SlbBaseOrderAct extends SlbBaseActivity {

    //
    protected ImageView ivBack;
    protected TextView tvCenterContent;
    protected TextView tv_right;
    protected EmptyViewNew1 emptyview;
    //
    protected LinearLayout ll_refresh1;
    protected TabLayout tablayoutMy;
    protected ViewPagerSlide viewpager_my1_order;
    //    private List<CommonClassAdapterType> mList1;
    protected OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    //
    protected String current_id;
    protected int scrolly = 0;
    protected boolean enscrolly;

    public int getScrolly() {
        return scrolly;
    }

    public void setScrolly(int scrolly) {
        this.scrolly = scrolly;
    }

    public boolean isEnscrolly() {
        return enscrolly;
    }

    public void setEnscrolly(boolean enscrolly) {
        this.enscrolly = enscrolly;
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void updateUI(final Integer scrolly) {
        setScrolly(scrolly);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void updateUI(final Boolean scrolly) {
        setEnscrolly(scrolly);
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
//        tvCenterContent.setText("我的订单");
        emptyview.loading();
//        is_zhu = true;
        donetwork1();
        retryData();
//        current_id = id1;
    }

    protected abstract void donetwork1();

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
        // ceshi
//        ((ShouyeActivity)getActivity()).callFragment("传值3",OneFragment3.class.getName());
        retryData1();
    }

    protected abstract void retryData1();

    protected void init_tablayout(final List<OneBean1> mlist, List<Fragment> mFragmentList) {
        //
        if (mlist == null || mlist.size() == 0) {
            return;
        }
//        List<Fragment> mFragmentList = new ArrayList<>();
//        for (int i = 0; i < mlist.size(); i++) {
//            Bundle bundle = new Bundle();
//            bundle.putString("id", mlist.get(i).getTab_id());
//            OrderCommonFragment2 fragment1 = FragmentHelper.newFragment(OrderCommonFragment2.class, bundle);
//            mFragmentList.add(fragment1);
//        }
        List<String> titlesString = new ArrayList<>();
        for (OneBean1 bean1 : mlist) {
            titlesString.add(bean1.getTab_name());
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(getSupportFragmentManager(), SlbBaseOrderAct.this, titlesString, mFragmentList);
        viewpager_my1_order.setAdapter(orderFragmentPagerAdapter);
        viewpager_my1_order.setOffscreenPageLimit(3);
        viewpager_my1_order.setScroll(true);
        tablayoutMy.setupWithViewPager(viewpager_my1_order);
//        tablayoutMy.removeAllTabs();
//        for (OneBean1 item : mlist) {
//            tablayoutMy.addTab(tablayoutMy.newTab()
//                    .setTag(item.getTab_id()).setText(item.getTab_name()));
//        }
        tablayoutMy.clearAnimation();
        tablayoutMy.post(new Runnable() {
            @Override
            public void run() {
                TextView textView = new TextView(SlbBaseOrderAct.this);
                float selectedSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 16, getResources().getDisplayMetrics());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, selectedSize);
                textView.setTextColor(getResources().getColor(R.color.color_333333));// color_999999
                textView.setText(tablayoutMy.getTabAt(0).getText());
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                tablayoutMy.getTabAt(0).setCustomView(textView);
            }
        });
        tablayoutMy.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                TabUtils.tabSelect(tablayoutMy, tab);
                TextView textView = new TextView(SlbBaseOrderAct.this);
                float selectedSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 16, getResources().getDisplayMetrics());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, selectedSize);
                textView.setTextColor(getResources().getColor(R.color.color_333333));// color_999999
                textView.setText(tab.getText());
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                tab.setCustomView(textView);
                //
                String tag = mlist.get(tab.getPosition()).getTab_id();
                current_id = tag;
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == tag) {
                    return;
                }
                MyLogUtil.e("---geekyun----", current_id);
//                show_viewpager_current(false, current_id);
//                viewpager_my1_order.setCurrentItem(tablayoutMy.getSelectedTabPosition(), false);
//                Intent msgIntent = new Intent();
//                msgIntent.setAction("OrderAct");
//                // fragment传值 刷新
//                OrderCommonItem2Bean bean = new OrderCommonItem2Bean("0", current_id, true);
////                EventBus.getDefault().post(bean);
//                msgIntent.putExtra("OrderAct1", bean);
//                LocalBroadcastManagers.getInstance(SlbBaseOrderAct.this).sendBroadcast(msgIntent);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setCustomView(null);
            }
        });
        TabUtils.setIndicatorWidth(tablayoutMy, 40);
        //
//        show_viewpager_current(true, current_id);
        viewpager_my1_order.setCurrentItem(0, false);
    }

    protected void show_viewpager_current(boolean is_first, String current_id) {
        if (is_first) {
            viewpager_my1_order.setCurrentItem(0, true);
        }
        // fragment传值
//        OrderCommonItem2Bean bean = new OrderCommonItem2Bean("0", current_id, true);
//        EventBus.getDefault().post(bean);
    }

    private void onclick() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //
        emptyview.bind(ll_refresh1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 主布局
                emptyview.loading();
                retryData();
            }
        });
        onclick1();

    }

    protected abstract void onclick1();

    private void findview() {
        ivBack = findViewById(R.id.iv_back1_order);
        tvCenterContent = findViewById(R.id.tv_center_content);
        tv_right = findViewById(R.id.tv_right);
        emptyview = findViewById(R.id.emptyview1_order);
        ll_refresh1 = findViewById(R.id.ll_refresh1_order);
        tablayoutMy = findViewById(R.id.tab1_order);
        viewpager_my1_order = findViewById(R.id.viewpager_my1_order);

        //
        findview1();
//        emptyview1.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");

    }

    protected abstract void findview1();

    protected void OnOrderSuccess() {
        emptyview.success();
    }

    protected void OnOrderNodata() {
        emptyview.nodata();
    }

    protected void OnOrderFail() {
        emptyview.errorNet();
    }

}
