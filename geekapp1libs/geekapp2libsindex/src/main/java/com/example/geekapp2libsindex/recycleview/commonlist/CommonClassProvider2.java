package com.example.geekapp2libsindex.recycleview.commonlist;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.biz3hxktindex.bean.HClasscategoryBean2;
import com.example.biz3hxktindex.bean.HClasscategoryBean3;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan2;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.baselibrary.flowlayout.FlowLayout;
import com.haier.cellarette.baselibrary.flowlayout.TagAdapter;
import com.haier.cellarette.baselibrary.flowlayout.TagFlowLayout;
import com.haier.cellarette.baselibrary.widget.LxLinearLayout;

import java.util.List;

public class CommonClassProvider2 extends BaseItemProvider<CommonClassAdapterType, BaseViewHolder> {

    @Override
    public int viewType() {
        return CommonClassAdapter.STYLE_TWO;
    }

    @Override
    public int layout() {
        return R.layout.recycleview_hxkt_classprovider_item2;
    }

    @Override
    public void convert(BaseViewHolder helper, CommonClassAdapterType data, int position) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TagFlowLayout tagflowlayout10 = helper.itemView.findViewById(R.id.tagflowlayout10);
        TagFlowLayout tagflowlayout1 = helper.itemView.findViewById(R.id.tagflowlayout1);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        View view1 = helper.itemView.findViewById(R.id.view1);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        TextView tv6 = helper.itemView.findViewById(R.id.tv6);
        TextView tv7 = helper.itemView.findViewById(R.id.tv7);
        TextView tv_money1 = helper.itemView.findViewById(R.id.tv_money1);
        LxLinearLayout sCardView = helper.itemView.findViewById(R.id.scardview1);
        sCardView.setTouch(true);
        LxLinearLayout ll1 = helper.itemView.findViewById(R.id.ll1);
//        ll1.setTouch(true);
//        helper.setText(R.id.tv1, data.getmBean().getText1());
//        get_flowlayout0(tagflowlayout10, data.getmBean().getText1_list());
//        helper.setText(R.id.tv2, mContext.getResources().getString(R.string.slb_tips1, data.getmBean().getText2()));
        set_flowlayout(tv2, data.getmBean());


        helper.setText(R.id.tv3, data.getmBean().getCourseTimeRange());
        helper.setText(R.id.tv4, data.getmBean().getItemCount() + "课节");
        helper.setText(R.id.tv5, data.getmBean().getSignUpCount() + "人已报名");
//        helper.setText(R.id.tv6, data.getmBean().getText6());

        //中间划横线
        tv6.getPaint().setFlags(tv6.getPaint().getFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tv6.setText("¥" + data.getmBean().getPriceMarked());

        if (TextUtils.equals("0", data.getmBean().getPricePayable()) ||
                TextUtils.equals("0.0", data.getmBean().getPricePayable()) ||
                TextUtils.equals("0.00", data.getmBean().getPricePayable())) {
            tv_money1.setVisibility(View.GONE);
            tv6.setVisibility(View.GONE);
            helper.setText(R.id.tv7, "免费");
        } else {
            tv_money1.setVisibility(View.VISIBLE);
            tv6.setVisibility(View.VISIBLE);
            helper.setText(R.id.tv7, data.getmBean().getPricePayable());
        }
        if(Double.parseDouble(data.getmBean().getPriceMarked())==0){
            tv6.setVisibility(View.INVISIBLE);
        }else{
            tv6.setVisibility(View.VISIBLE);
        }

        get_flowlayout(tagflowlayout1, data.getmBean().getTeacherList());

//        helper.addOnClickListener(R.id.scardview1);
//        helper.addOnClickListener(R.id.ll1);
    }

    private void set_flowlayout(TextView tv1, HClasscategoryBean2 bean) {
        //标题
        String title = "";
        boolean hasSubject = bean.getSubjectNames() != null && bean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : bean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + bean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < bean.getSubjectNames().size(); i++) {
                String colorString = "#F78451";
                if (TextUtils.equals("system", bean.getClassAttribute())) {
                    colorString = "#F78451";
                } else if (TextUtils.equals("experience", bean.getClassAttribute())) {
                    colorString = "#666666";
                }
                spannableString.setSpan(new RoundBackgroundColorSpan2(mContext, Color.parseColor(colorString), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        tv1.setText(spannableString);
    }

    private void get_flowlayout0(final TagFlowLayout tagflowlayout1, List<CommonClassClassCommonbean3> list) {
        //mFlowLayout.setMaxSelectCount(3);
        final LayoutInflater mInflater = LayoutInflater.from(mContext);
        tagflowlayout1.setAdapter(new TagAdapter(list) {
            @Override
            public View getView(FlowLayout parent, int position, Object bean) {
                View view = mInflater.inflate(R.layout.recycleview_hxkt_class_img_item2,
                        tagflowlayout1, false);
                TextView tv1 = view.findViewById(R.id.tv1);
                CommonClassClassCommonbean3 bean1 = (CommonClassClassCommonbean3) bean;
                //
                GradientDrawable drawable = new GradientDrawable();
                //设置圆角大小
                drawable.setCornerRadius(5);
                //设置边缘线的宽以及颜色
                drawable.setStroke(1, Color.parseColor(bean1.getText1()));
                //设置shape背景色
                drawable.setColor(Color.parseColor(bean1.getText1()));
                //设置到TextView中
                tv1.setBackground(drawable);
                tv1.setPadding(10, 5, 10, 5);
                tv1.setTextColor(Color.parseColor("#ffffff"));
                tv1.setText(bean1.getText2());
                return view;
            }
        });

    }

    private void get_flowlayout(final TagFlowLayout tagflowlayout1, List<HClasscategoryBean3> getList) {
        //mFlowLayout.setMaxSelectCount(3);
        final LayoutInflater mInflater = LayoutInflater.from(mContext);
        tagflowlayout1.setAdapter(new TagAdapter(getList) {
            @Override
            public View getView(FlowLayout parent, int position, Object bean) {
                View view = mInflater.inflate(R.layout.recycleview_hxkt_class_img_item1,
                        tagflowlayout1, false);
                GlideImageView iv1 = view.findViewById(R.id.iv1);
                TextView tv1 = view.findViewById(R.id.tv1);
                TextView tv2 = view.findViewById(R.id.tv2);
                HClasscategoryBean3 bean1 = (HClasscategoryBean3) bean;
                iv1.loadImage(bean1.getAvatar(), R.drawable.head_zanwu1);
                tv1.setText(bean1.getNikeName());
                tv2.setText(bean1.getTeacherType());
                return view;
            }
        });
    }

}
