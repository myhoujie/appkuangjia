package com.example.geekapp2libsindex.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HClasscategoryBean3;
import com.example.biz3hxktindex.bean.HStudycategoryBean1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan2;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.baselibrary.flowlayout.FlowLayout;
import com.haier.cellarette.baselibrary.flowlayout.TagAdapter;
import com.haier.cellarette.baselibrary.flowlayout.TagFlowLayout;
import com.haier.cellarette.baselibrary.widget.LxLinearLayout;
import com.haier.cellarette.baselibrary.widget.LxRelativeLayout;

import java.util.List;

public class Studydapter extends BaseQuickAdapter<HStudycategoryBean1, BaseViewHolder> {

    public Studydapter() {
        super(R.layout.recycleview_hxkt_studyprovider_item2);
    }

    @Override
    protected void convert(BaseViewHolder helper, HStudycategoryBean1 item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        TagFlowLayout tagflowlayout10 = helper.itemView.findViewById(R.id.tagflowlayout10);
        TagFlowLayout tagflowlayout1 = helper.itemView.findViewById(R.id.tagflowlayout1);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        TextView tv6 = helper.itemView.findViewById(R.id.tv6);
        LxRelativeLayout sCardView = helper.itemView.findViewById(R.id.scardview1);
        sCardView.setTouch(true);
        LxLinearLayout ll1 = helper.itemView.findViewById(R.id.ll1);
        //
//        get_flowlayout0(tagflowlayout10, item.getSubjectNames());
//        helper.setText(R.id.tv2, mContext.getResources().getString(R.string.slb_tips1, item.getCourseName()));
        set_flowlayout(tv2, item);
        tv3.setText(item.getCourseTimeRange());
        tv4.setText(item.getItemCount() + "课节");
        if (item.getStatusDesc().contains("直播")) {
            tv5.setTextColor(ContextCompat.getColor(mContext, R.color.color_333333));
            select_useful(tv5, R.drawable.hxkticon_play, true);
        } else if (item.getStatusDesc().contains("未开始")) {
            tv5.setTextColor(ContextCompat.getColor(mContext, R.color.colorFF4E08));
            select_useful(tv5, R.drawable.hxkticon_play, false);
        } else if (item.getStatusDesc().contains("进行")) {
            tv5.setTextColor(ContextCompat.getColor(mContext, R.color.color_333333));
            select_useful(tv5, R.drawable.hxkticon_play, false);
        } else if (item.getStatusDesc().contains("已结束")) {
            tv5.setTextColor(ContextCompat.getColor(mContext, R.color.colorCCCCCC));
            select_useful(tv5, R.drawable.hxkticon_play, false);
        }
        tv5.setText(item.getStatusDesc());
        if (TextUtils.isEmpty(item.getNextTime())) {
            tv6.setVisibility(View.GONE);
        } else {
            tv6.setVisibility(View.VISIBLE);
            tv6.setText(item.getNextTime());
        }
        //
        get_flowlayout(tagflowlayout1, item.getTeacherList());
//        helper.addOnClickListener(R.id.scardview1);
//        helper.addOnClickListener(R.id.ll1);
    }

    private void select_useful(TextView tv1, int drawabless, boolean is_dra) {
        if (!is_dra) {
            tv1.setCompoundDrawables(null, null, null, null);
        } else {
            Drawable drawable = mContext.getResources().getDrawable(drawabless);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(),
                    drawable.getMinimumHeight());
            tv1.setCompoundDrawables(drawable, null, null, null);
        }
    }


    private void set_flowlayout(TextView tv1, HStudycategoryBean1 bean) {
        //标题
        String title = "";
        boolean hasSubject = bean.getSubjectNames() != null && bean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : bean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + bean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < bean.getSubjectNames().size(); i++) {
                String colorString = "#F78451";
                if (TextUtils.equals("system", bean.getClassAttribute())) {
                    colorString = "#F78451";
                } else if (TextUtils.equals("experience", bean.getClassAttribute())) {
                    colorString = "#666666";
                }
                spannableString.setSpan(new RoundBackgroundColorSpan2(mContext, Color.parseColor(colorString), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        tv1.setText(spannableString);
    }

    private void get_flowlayout0(final TagFlowLayout tagflowlayout1, List<String> list) {
        //mFlowLayout.setMaxSelectCount(3);
        final LayoutInflater mInflater = LayoutInflater.from(mContext);
        tagflowlayout1.setAdapter(new TagAdapter(list) {
            @Override
            public View getView(FlowLayout parent, int position, Object bean) {
                View view = mInflater.inflate(R.layout.recycleview_hxkt_class_img_item2,
                        tagflowlayout1, false);
                TextView tv1 = view.findViewById(R.id.tv1);
                String bean1 = (String) bean;
                //
                GradientDrawable drawable = new GradientDrawable();
                //设置圆角大小
                drawable.setCornerRadius(5);
                //设置边缘线的宽以及颜色
                drawable.setStroke(1, Color.parseColor("#F78451"));
                //设置shape背景色
                drawable.setColor(Color.parseColor("#F78451"));
                //设置到TextView中
                tv1.setBackground(drawable);
                tv1.setPadding(10, 5, 10, 5);
                tv1.setTextColor(Color.parseColor("#ffffff"));
                tv1.setText(bean1);
                return view;
            }
        });
    }

    private void get_flowlayout(final TagFlowLayout tagflowlayout1, List<HClasscategoryBean3> mlist1) {
        //mFlowLayout.setMaxSelectCount(3);
        final LayoutInflater mInflater = LayoutInflater.from(mContext);
        tagflowlayout1.setAdapter(new TagAdapter(mlist1) {
            @Override
            public View getView(FlowLayout parent, int position, Object bean) {
                View view = mInflater.inflate(R.layout.recycleview_hxkt_class_img_item1,
                        tagflowlayout1, false);
                GlideImageView iv1 = view.findViewById(R.id.iv1);
                TextView tv1 = view.findViewById(R.id.tv1);
                TextView tv2 = view.findViewById(R.id.tv2);
                HClasscategoryBean3 bean1 = (HClasscategoryBean3) bean;
                iv1.loadImage(bean1.getAvatar(), R.drawable.default_head_teather);
                tv1.setText(bean1.getNikeName());
                tv2.setText(bean1.getTeacherType());
                return view;
            }
        });
    }


}
