package com.example.geekapp2libsindex.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.ClassSectionBean;

public class ClassSection2Adapter extends BaseQuickAdapter<ClassSectionBean, BaseViewHolder> {
    private int toSectionPosition = -1;
    private int pos = -1;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public ClassSection2Adapter() {
        super(R.layout.adapter_classdetail_section2);
//        setHasStableIds(true);
    }

    public ClassSection2Adapter(int toSectionPosition) {
        this();
        this.toSectionPosition = toSectionPosition;
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassSectionBean item) {
        final ConstraintLayout c11 = helper.itemView.findViewById(R.id.c1);
        TextView noTv = helper.itemView.findViewById(R.id.noTv);
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView dateTv = helper.itemView.findViewById(R.id.dateTv);
        ImageView playIv = helper.itemView.findViewById(R.id.playIv);
        View rightArrowIv = helper.itemView.findViewById(R.id.rightArrowIv);
        View ll_living = helper.itemView.findViewById(R.id.ll_living);
        ImageView playingIv = helper.itemView.findViewById(R.id.playingIv);
        TextView nameTv = helper.itemView.findViewById(R.id.nameTv);

        helper.addOnClickListener(R.id.ll_huifang);
        helper.addOnClickListener(R.id.ll_lianxi);
        helper.addOnClickListener(R.id.ll_baogao);

        if (helper.getAdapterPosition() + 1 == toSectionPosition) {
            if (on_huxi != null) {
                on_huxi.OnFinish(c11);
            }
            c11.setBackgroundColor(Color.parseColor("#F7F7F7"));
        } else {
            c11.setBackgroundColor(Color.WHITE);
        }
        //播放状态(0, "未开始"), (1, "直播中"), (2, "看回放"), (3, "已结束");
        if (TextUtils.equals(item.getItemStatus(), "1")) {
            titleTv.setTextColor(mContext.getResources().getColor(R.color.blue55AAFF));
            dateTv.setTextColor(mContext.getResources().getColor(R.color.blue55AAFF));
            nameTv.setTextColor(mContext.getResources().getColor(R.color.blue55AAFF));
            ll_living.setVisibility(View.VISIBLE);
            playIv.setVisibility(View.VISIBLE);
            rightArrowIv.setVisibility(View.INVISIBLE);
            load(playingIv);
        } else {
            titleTv.setTextColor(mContext.getResources().getColor(R.color.color_333333));
            dateTv.setTextColor(mContext.getResources().getColor(R.color.color_666));
            nameTv.setTextColor(mContext.getResources().getColor(R.color.color_666));
            ll_living.setVisibility(View.GONE);
            playIv.setVisibility(View.GONE);
            rightArrowIv.setVisibility(View.VISIBLE);
        }

//        noTv.setText((helper.getAdapterPosition() + 1) + "");
        noTv.setText(item.getOrders());
        titleTv.setText(item.getItemTitle());
        String date;
        if (TextUtils.equals("1", item.getTodayFlag())) {
            date = "今天";
        } else {
            date = item.getDayChn();
        }
        dateTv.setText(date + "  " + item.getWeekChn() + "  " + item.getItemTimeRangeNoDay() + "  " + item.getLecturerName());
//        nameTv.setText(item.getLecturerName());
//        load(playingIv);
    }

    public interface On_huxi {
        void OnFinish(ConstraintLayout cl);
    }

    public On_huxi on_huxi;

    public On_huxi getOn_huxi() {
        return on_huxi;
    }

    public void setOn_huxi(On_huxi on_huxi) {
        this.on_huxi = on_huxi;
    }

    private void load(ImageView image) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);

        Glide.with(mContext)
                .load(R.drawable.playing)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .apply(options)
                //.thumbnail(Glide.with(this).load(R.mipmap.ic_launcher))
                .into(image);
    }

}
