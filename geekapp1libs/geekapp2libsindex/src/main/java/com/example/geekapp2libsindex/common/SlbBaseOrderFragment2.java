package com.example.geekapp2libsindex.common;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public abstract class SlbBaseOrderFragment2 extends SlbBaseIndexFragment {

    //
    protected SmartRefreshLayout refreshLayout1;
    protected ClassicsHeader smartHeader1;
    //
    protected static final int PAGE_SIZE = 10;
    protected int mNextRequestPage = 1;
    protected int mPage;
    protected static boolean mFirstPageNoMore;
    protected static boolean mFirstError = true;
    protected EmptyViewNew1 emptyview1;
    protected LinearLayout ll_refresh1;
    protected XRecyclerView recyclerView1;


    @Override
    public void net_con_none() {
        emptyview1.errorNet();
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        findview(rootView);
        onclick();
//        donetwork();
    }

    private int mLastItemVisible;
    private int firstVisibleItemPosition = 0;

    public void setData(BaseQuickAdapter mAdapter, boolean isRefresh, List data) {
        mNextRequestPage++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            mAdapter.setNewData(data);
//            recyclerView1.scrollToPosition(0);
        } else {
            if (size > 0) {
                mAdapter.addData(data);
            }
        }
//        if (mAdapter1.getData().size() > 12) {
//            mAdapter1.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了12");
//            return;
//        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            mAdapter.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了");
        } else {
            mAdapter.loadMoreComplete();
        }

    }

    /**
     * 加载更多bufen
     */
    public void loadMore() {
        retryData();
    }

    /**
     * 第一次进来加载bufen
     */
    @Override
    public void loadData() {
        super.loadData();
        donetwork();
    }

    public void donetwork() {
        donetwork1();
        retryData();
    }

    public void donetwork1_init(BaseQuickAdapter mAdapter1) {
        mNextRequestPage = 1;
        mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
    }

    protected abstract void donetwork1();

    private void retryData() {
        retryData1();
    }

    protected abstract void retryData1();

    private void onclick() {
        smartHeader1.setEnableLastTime(false);
//        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(final RefreshLayout refreshLayout) {
//                retryData();
//            }
//        });
//        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
//            @Override
//            public void retry() {
//                // 分布局
//                emptyview1.loading();
//                retryData();
//            }
//        });
        recyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                MyLogUtil.e("ssssssssssss", "" + newState);

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                dx > 0 时为手指向左滚动,列表滚动显示右面的内容
//                dx < 0 时为手指向右滚动,列表滚动显示左面的内容
//                dy > 0 时为手指向上滚动,列表滚动显示下面的内容
//                dy < 0 时为手指向下滚动,列表滚动显示上面的内容
                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                mLastItemVisible = layoutManager.findLastVisibleItemPosition();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                MyLogUtil.e("---geekyun1---", firstVisibleItemPosition + "");
                // 传值给act
//                EventBus.getDefault().post(new Integer(firstVisibleItemPosition));
                //
                b = recyclerView.canScrollVertically(-1);
                MyLogUtil.e("---geekyun2---", b + "");
                EventBus.getDefault().post(new Boolean(b));
            }
        });
        onclick1();

    }

    private boolean b;

    /**
     * 底部点击
     */
    protected void getCate(OrderCommonItem2Bean tablayoutId) {

        if (!tablayoutId.isText2()) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        if (!b) {
            if (refreshLayout1 != null) {
                refreshLayout1.autoRefresh();
            }
        } else {
            recyclerView1.scrollToPosition(0);
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    protected void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    protected abstract void onclick1();

    private void findview(View rootView) {
        ll_refresh1 = rootView.findViewById(R.id.ll_refresh1_order);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);
        smartHeader1 = rootView.findViewById(R.id.smart_header1_order);
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
        recyclerView1 = rootView.findViewById(R.id.recycler_view1_order);
        //
        findview1();

    }

    protected abstract void findview1();

    protected void OnOrderSuccess() {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    protected void OnOrderNodata() {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(true);
    }

    protected void OnOrderFail() {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

}
