package com.example.geekapp2libsindex.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class PaySuccess implements Parcelable {
    private String id;

    public PaySuccess() {
    }

    public PaySuccess(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
    }

    protected PaySuccess(Parcel in) {
        this.id = in.readString();
    }

    public static final Creator<PaySuccess> CREATOR = new Creator<PaySuccess>() {
        @Override
        public PaySuccess createFromParcel(Parcel source) {
            return new PaySuccess(source);
        }

        @Override
        public PaySuccess[] newArray(int size) {
            return new PaySuccess[size];
        }
    };
}
