package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.lxj.xpopup.core.CenterPopupView;
import com.umeng.analytics.MobclickAgent;

public class NoticePopup extends CenterPopupView {
    private Context context;
    private String orderId;

    public NoticePopup(@NonNull Context context, String orderId) {
        super(context);
        this.context = context;
        this.orderId = orderId;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_notice;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(context, "successpage_notice_closebutton");
                dismiss();
            }
        });
        findViewById(R.id.toDetailTv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(context, "successpage_noticeclick");
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.NoticeDetailActivity");
                intent.putExtra("orderId", orderId);
                context.startActivity(intent);
                dismiss();
            }
        });
    }

//    @Override
//    protected int getMaxHeight() {
//        return (int) (XPopupUtils.getWindowHeight(getContext()) * .6f);
//    }
}
