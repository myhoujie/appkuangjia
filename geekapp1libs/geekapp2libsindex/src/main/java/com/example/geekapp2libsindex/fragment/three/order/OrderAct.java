package com.example.geekapp2libsindex.fragment.three.order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.presenter.HCategoryPresenter;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.common.SlbBaseOrderAct;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;
import com.haier.cellarette.libwebview.hois2.HiosHelper;

import java.util.ArrayList;
import java.util.List;

public class OrderAct extends SlbBaseOrderAct implements View.OnClickListener, HCategoryView {

    private List<HCategoryBean> mListbanner1;
    private HCategoryPresenter presenter1;


    @Override
    protected void onDestroy() {
        presenter1.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_order1;
    }

    @Override
    protected void donetwork1() {
        tvCenterContent.setText("我的订单");
        presenter1 = new HCategoryPresenter();
        presenter1.onCreate(this);

    }

    @Override
    protected void retryData1() {
        // 接口
        emptyview.loading();
        mListbanner1 = new ArrayList<>();
        presenter1.get_category("miniOrderStatus", "1");
    }

    @Override
    protected void onclick1() {
        tvCenterContent.setOnClickListener(this);
        tv_right.setOnClickListener(this);
        emptyview.notices("暂无订单，去选课中心看看吧…", "没有网络了,检查一下吧", "正在加载....", "");
    }


    @Override
    protected void findview1() {
        //
//        skeletonScreen = Skeleton.bind(ll_refresh1)
//                .load(R.layout.skeleton_view_layout)
//                .shimmer(true)
//                .angle(20)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .show();
        tv_right.setVisibility(View.GONE);
        tv_right.setText("说明");

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_center_content) {
            OrderCommonItem2Bean bean = null;
            //
            Intent msgIntent = new Intent();
            msgIntent.setAction("OrderAct");
//            if (getScrolly() == 0) {
            if (!isEnscrolly()) {
                //
//                retryData1();
                // fragment传值 刷新
                bean = new OrderCommonItem2Bean("0", current_id, true);
//                EventBus.getDefault().post(bean);
                msgIntent.putExtra("OrderAct1", bean);
                LocalBroadcastManagers.getInstance(OrderAct.this).sendBroadcast(msgIntent);
            } else {
                // fragment传值 滚动
                bean = new OrderCommonItem2Bean("1", current_id, true);
                msgIntent.putExtra("OrderAct1", bean);
                LocalBroadcastManagers.getInstance(OrderAct.this).sendBroadcast(msgIntent);
            }
        } else if (view.getId() == R.id.tv_right) {
            // 用户协议
            HiosHelper.resolveAd(OrderAct.this, this, "http://pc.jiuzhidao.com/portal/page/index/id/9.html");
        } else {

        }
    }

    @Override
    public void OnCategorySuccess(HCategoryBean hCategoryBean) {
        OnOrderSuccess();
        List<OneBean1> mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
        }
        // viewpager
        current_id = mDataTablayout.get(0).getTab_id();
//        init_tablayout(mDataTablayout);
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mDataTablayout.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mDataTablayout.get(i).getTab_id());
            OrderCommonFragment2 fragment1 = FragmentHelper.newFragment(OrderCommonFragment2.class, bundle);
            mFragmentList.add(fragment1);
        }
        init_tablayout(mDataTablayout, mFragmentList);
        //
//        skeletonScreen.hide();
    }

    @Override
    public void OnCategoryNodata(String s) {
        OnOrderNodata();

    }

    @Override
    public void OnCategoryFail(String s) {
        OnOrderFail();

    }
}
