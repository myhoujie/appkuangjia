package com.example.geekapp2libsindex.classdetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.listener.InitSuccessListener;
import com.example.geekapp2libsindex.widget.CustomWebView;
import com.example.slbappcomm.base.SlbBaseFragment;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;

import java.util.HashMap;

public class ClassDetailFragment extends SlbBaseFragment implements OnClassDetailListener {
    private ImageView videoCoverIv;
    //    private TextView detailTv;
    private CustomWebView detailWv;
    private View ll_video;
    private View ll_play;
    private InitSuccessListener initSuccessListener;

    public static ClassDetailFragment newInstance() {
        ClassDetailFragment fragment = new ClassDetailFragment();
        return fragment;
    }

    public void setInitSuccessListener(InitSuccessListener initSuccessListener) {
        this.initSuccessListener = initSuccessListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_calssdetail;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        videoCoverIv = rootView.findViewById(R.id.videoCoverIv);
        detailWv = rootView.findViewById(R.id.detailWv);
        ll_video = rootView.findViewById(R.id.ll_video);
        ll_play = rootView.findViewById(R.id.ll_play);
//        RichText.initCacheDir(getContext());
        if (initSuccessListener != null) {
            initSuccessListener.onSuccess();
        }
    }


    @Override
    public void onDetailReceive(final CourseBean courseBean) {
        if (TextUtils.isEmpty(courseBean.getCourseVideo())) {
            ll_video.setVisibility(View.GONE);
        } else {
            ll_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent("hs.act.slbapp.JiaoZiVideoActivity");
                    intent.putExtra("videourl", courseBean.getCourseVideo());
                    intent.putExtra("goodsname", courseBean.getCourseName());
                    startActivity(intent);
                }
            });
            videoCoverIv.setImageBitmap(getBitmapFormUrl(courseBean.getCourseVideo()));
            ll_video.setVisibility(View.VISIBLE);
        }
//        RichText.fromHtml(courseBean.getCourseDetail()).autoPlay(true).into(detailTv);
//        RichText.fromHtml(courseBean.getCourseDetail()).into(detailTv);
        detailWv.loadRichText(courseBean.getCourseDetail());
    }

    @Override
    public void onTeacherReceive(TeacherBean teacherBean) {

    }

    @Override
    public void onDestroy() {
//        RichText.clear(this);
//        RichText.recycle();
        detailWv.onDestroy();
        super.onDestroy();
    }

    public static Bitmap getBitmapFormUrl(String url) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                retriever.setDataSource(url, new HashMap<String, String>());
            } else {
                retriever.setDataSource(url);
            }
        /*getFrameAtTime()--->在setDataSource()之后调用此方法。 如果可能，该方法在任何时间位置找到代表性的帧，
         并将其作为位图返回。这对于生成输入数据源的缩略图很有用。**/
            bitmap = retriever.getFrameAtTime();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return bitmap;

    }
}