package com.example.geekapp2libsindex.fragment.demo.f1;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.fragment.one.OneBean1;

public class F1LeftAdapter extends BaseQuickAdapter<OneBean1, BaseViewHolder> {
    private Context context;

    public F1LeftAdapter(Context context) {
        super(R.layout.demo_f1_yulan_item1);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OneBean1 item) {
        TextView f1_tv1 = helper.itemView.findViewById(R.id.f1_tv1);
        if (item.isEnable()) {
            f1_tv1.setTextColor(Color.parseColor("#ff0000"));
        } else {
            f1_tv1.setTextColor(Color.parseColor("#000000"));
        }
        f1_tv1.setText(item.getTab_name());
    }


}
