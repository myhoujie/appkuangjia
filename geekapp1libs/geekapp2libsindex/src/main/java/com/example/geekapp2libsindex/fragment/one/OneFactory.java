package com.example.geekapp2libsindex.fragment.one;//package com.example.geekapp2libsindex.fragment.one;
//
//import androidx.collection.SparseArrayCompat;
//import androidx.fragment.app.Fragment;
//
//import com.example.geekapp2libsindex.R;
//
//
//public class OneFactory {
//
//    /**
//     * viewpager页大小
//     */
//    public static int PAGE_COUNT = 2;
//    /**
//     * viewpager每页的itemview id
//     */
//    public static String PAGE_LAYOUT_ID = "activity_fragment2_layout_pager_item_";
//    /**
//     * 默认显示第几页
//     */
//    public static int DEFAULT_PAGE_INDEX = 0;
//
//    private static SparseArrayCompat<Class<? extends Fragment>> sIndexFragments = new SparseArrayCompat<>();
//
//    static {
//
//        sIndexFragments.put(R.id.fragment2_pager_index_0_0, OneFragment1.class);//模块1
//        sIndexFragments.put(R.id.fragment2_pager_index_1_0, OneFragment2.class);//模块2
//
//    }
//
//    public static Class<? extends Fragment> get(int id) {
//        if (sIndexFragments.indexOfKey(id) < 0) {
//            throw new UnsupportedOperationException("cannot find fragment by " + id);
//        }
//        return sIndexFragments.get(id);
//    }
//
//    public static SparseArrayCompat<Class<? extends Fragment>> get() {
//        return sIndexFragments;
//    }
//
//}
