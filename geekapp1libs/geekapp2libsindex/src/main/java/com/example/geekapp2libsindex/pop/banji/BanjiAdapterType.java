package com.example.geekapp2libsindex.pop.banji;

import com.example.biz3hxktindex.bean.HGradecategoryBean2;

import java.io.Serializable;
import java.util.List;

public class BanjiAdapterType implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int style1 = 1;
    public static final int style2 = 2;

    public int type;
    private HGradecategoryBean2 mBean;

    public BanjiAdapterType(int type) {
        this.type = type;
    }

    public BanjiAdapterType(int type, HGradecategoryBean2 mBean) {
        this.type = type;
        this.mBean = mBean;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public HGradecategoryBean2 getmBean() {
        return mBean;
    }

    public void setmBean(HGradecategoryBean2 mBean) {
        this.mBean = mBean;
    }

    public void set_select(List<HGradecategoryBean2> list, int pos) {
        for (int i = 0; i < list.size(); i++) {
            HGradecategoryBean2 bean = list.get(i);
            bean.setText3(false);
        }
        list.get(pos).setText3(true);
    }

}
