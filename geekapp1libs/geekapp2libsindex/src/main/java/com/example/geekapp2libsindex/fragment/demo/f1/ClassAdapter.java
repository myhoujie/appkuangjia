package com.example.geekapp2libsindex.fragment.demo.f1;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geekapp2libsindex.R;

import java.util.ArrayList;
import java.util.List;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ViewHolder> {
 
    private Context context;
    private OnItemClickListener mOnItemClickListener = null;
    private List<Boolean> isClicks;//控件是否被点击,默认为false，如果被点击，改变值，控件根据值改变自身颜色
 
    public ClassAdapter(Context context) {
 
        this.context = context;
        isClicks = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            isClicks.add(false);
            if (i == 0) {
                isClicks.set(0, true);
            }
        }
 
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.demo_f1_yulan_item1, parent, false);
        ClassAdapter.ViewHolder holder = new ClassAdapter.ViewHolder(view);
 
        return holder;
    }
 
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
 
        holder.itemView.setTag(holder.tv_class_name);
 
 
        if (isClicks.get(position)) {
            holder.tv_class_name.setTextColor(Color.parseColor("#ff0000"));
        } else {
            holder.tv_class_name.setTextColor(Color.parseColor("#000000"));
        }
 
        holder.tv_class_name.setText("年级" + position);
 
        // 如果设置了回调，则设置点击事件
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < isClicks.size(); i++) {
                        isClicks.set(i, false);
                    }
                    isClicks.set(position, true);
                    notifyDataSetChanged();
                    mOnItemClickListener.onItemClick(holder.itemView, position);
                }
            });
        }
    }
 
    @Override
    public int getItemCount() {
        if (isClicks == null)
            return 0;
        return isClicks.size();
    }
 
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }
 
    public interface OnItemClickListener {
 
        void onItemClick(View view, int position);
 
    }
 
    public class ViewHolder extends RecyclerView.ViewHolder {
 
        private TextView tv_class_name;
 
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_class_name = itemView.findViewById(R.id.f1_tv1);
        }
    }
}
