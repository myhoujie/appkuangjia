package com.example.geekapp2libsindex.common;

import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.SparseArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HMyorderBean1;
import com.example.biz3hxktindex.bean.HMyorderBeanNew1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan2;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.widget.LxRelativeLayout;

public class OrderCommonAdapter3 extends BaseQuickAdapter<HMyorderBeanNew1, BaseViewHolder> {

    public OrderCommonAdapter3() {
        super(R.layout.recycleview_hxkt_order_item2);
    }

    @Override
    protected void convert(BaseViewHolder helper, HMyorderBeanNew1 item) {
        TextView tv10 = helper.itemView.findViewById(R.id.tv10);
        TextView tv11 = helper.itemView.findViewById(R.id.tv11);
        TextView tv12 = helper.itemView.findViewById(R.id.tv12);
        TextView tv44 = helper.itemView.findViewById(R.id.tv44);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        TextView tv6 = helper.itemView.findViewById(R.id.tv6);
        TextView tv777 = helper.itemView.findViewById(R.id.tv777);
        TextView tv77 = helper.itemView.findViewById(R.id.tv77);
        TextView tv88 = helper.itemView.findViewById(R.id.tv88);
        TextView tv99 = helper.itemView.findViewById(R.id.tv99);
        LxRelativeLayout ll1 = helper.itemView.findViewById(R.id.ll1);
        ll1.setTouch(true);
        XRecyclerView recyclerView1 = helper.itemView.findViewById(R.id.recycler_view1);
        //
        if (TextUtils.isEmpty(item.getPkgTitle())) {
            tv12.setVisibility(View.GONE);
        } else {
            tv12.setVisibility(View.VISIBLE);
        }
        tv12.setText(item.getPkgTitle());
        if (item.getOrderStatus().contains("unpaid")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_FF8865, mContext.getTheme()));
            } else {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_FF8865));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_999999, mContext.getTheme()));
            } else {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_999999));
            }
        }
        // unpaid-待支付,paid-已支付,cancel-已取消,refund-已退款
        tv11.setText(item.getOrderStatusName());
//        if (item.getOrderStatus().contains("unpaid")) {
//            tv11.setText("待支付");
//        } else if (item.getOrderStatus().contains("paid")) {
//            tv11.setText("已支付");
//        } else if (item.getOrderStatus().contains("cancel")) {
//            tv11.setText("已取消");
//        } else if (item.getOrderStatus().contains("refund")) {
//            tv11.setText("已退款");
//        }
        tv44.setText("共" + item.getCourseNum() + "个课程");
        if (item.getOrderStatus().contains("unpaid") || item.getOrderStatus().contains("cancel")) {
            tv5.setText("应付：");
            tv6.setText(item.getAmountPayable());
        } else {
            tv5.setText("实付：");
            tv6.setText(item.getAmountFinal());
        }
        if (TextUtils.isEmpty(item.getButtonCancel())) {
            tv777.setVisibility(View.GONE);
        } else {
            tv777.setVisibility(View.VISIBLE);
            tv777.setText(item.getButtonCancel());
        }
        if (TextUtils.isEmpty(item.getButtonDelete())) {
            tv77.setVisibility(View.GONE);
        } else {
            tv77.setVisibility(View.VISIBLE);
            tv77.setText(item.getButtonDelete());
        }
        if (TextUtils.isEmpty(item.getButtonPay())) {
            tv88.setVisibility(View.GONE);
        } else {
            tv88.setVisibility(View.VISIBLE);
            tv88.setText(item.getButtonPay());
        }
        if (TextUtils.isEmpty(item.getButtonInfo())) {
            tv99.setVisibility(View.GONE);
        } else {
            tv99.setVisibility(View.VISIBLE);
            tv99.setText(item.getButtonInfo());
        }
        CountDownTimer countDownTimer = timer_list.get(tv10.hashCode());
        if (countDownTimer != null) {
            //将复用的倒计时清除
            countDownTimer.cancel();
            countDownTimer = null;
        }
        if (item.getCurrentCountDownSecond() == 0) {
            item.setCurrentCountDownSecond(System.currentTimeMillis() + Long.valueOf(item.getCountDownSecond()));
        }

        if (item.getCurrentCountDownSecond() - System.currentTimeMillis() > 0) {
            startTime(item.getCurrentCountDownSecond() - System.currentTimeMillis(), tv10, item, helper.getAdapterPosition());
        } else {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.CHINA);
//            String dates = TimeUtils.millis2String(item.getCountDownSecond(), dateFormat);
            tv10.setText(item.getOrderCreateTime());
        }

        if (item.getSons().size() > 0) {
            recyclerView1.setLayoutManager(new GridLayoutManager(mContext, 1, RecyclerView.VERTICAL, false));
            recyclerView1.setHasFixedSize(true);
            recyclerView1.setNestedScrollingEnabled(false);
            recyclerView1.setFocusable(false);
            RecycleListItemAdapter mAdapter = new RecycleListItemAdapter();
            mAdapter.setNewData(item.getSons());
            recyclerView1.setAdapter(mAdapter);
        } else {

        }
        helper.addOnClickListener(R.id.tv77);
        helper.addOnClickListener(R.id.tv777);
        helper.addOnClickListener(R.id.tv88);
        helper.addOnClickListener(R.id.tv99);
    }

    private void set_flowlayout(TextView tv1, HMyorderBean1 bean) {
        //标题
        String title = "";
        boolean hasSubject = bean.getSubjectNames() != null && bean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : bean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + bean.getOrderName());
        if (hasSubject) {
            for (int i = 0; i < bean.getSubjectNames().size(); i++) {
                String colorString = "#F78451";
                if (TextUtils.equals("system", bean.getClassAttribute())) {
                    colorString = "#F78451";
                } else if (TextUtils.equals("experience", bean.getClassAttribute())) {
                    colorString = "#666666";
                }
                spannableString.setSpan(new RoundBackgroundColorSpan2(mContext, Color.parseColor(colorString), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        tv1.setText(spannableString);
    }

    private SparseArray<CountDownTimer> timer_list = new SparseArray<>();

    public void cancle_timer_all() {
        for (int i = 0, length = timer_list.size(); i < length; i++) {
            CountDownTimer cdt = timer_list.get(timer_list.keyAt(i));
            if (cdt != null) {
                cdt.cancel();
            }
        }
    }

    /**
     * 从x开始倒计时
     *
     * @param x
     */
    public void startTime(long x, final TextView tv1, final HMyorderBeanNew1 item, final int pos) {
        CountDownTimer timer = new CountDownTimer(x, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int remainTime = (int) (millisUntilFinished / 1000L);// 182
                int hour = remainTime / 60;
                int second = remainTime % 60;
                String text = hour + "分" + second + "秒";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text)));
                }
                item.setCurrentCountDownSecond(System.currentTimeMillis() + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                remove_pos(item, pos);
                this.cancel();
            }
        };
        timer.start();
        timer_list.put(tv1.hashCode(), timer);
    }

    public void remove_pos(HMyorderBeanNew1 item, int pos) {
        if (pos < 0 || pos >= getItemCount()) {
            return;
        }
        //
        getData().remove(item);
        notifyItemRemoved(pos);
    }

    class RecycleListItemAdapter extends BaseQuickAdapter<HMyorderBean1, BaseViewHolder> {

        public RecycleListItemAdapter() {
            super(R.layout.recycleview_hxkt_order_item22);
        }

        @Override
        protected void convert(BaseViewHolder helper, HMyorderBean1 item) {
            View view1 = helper.itemView.findViewById(R.id.view1);
            TextView tv1 = helper.itemView.findViewById(R.id.tv1);
            TextView tv2 = helper.itemView.findViewById(R.id.tv2);
            TextView tv3 = helper.itemView.findViewById(R.id.tv3);
            TextView tv4 = helper.itemView.findViewById(R.id.tv4);
            TextView tv7 = helper.itemView.findViewById(R.id.tv7);
            LinearLayout ll1 = helper.itemView.findViewById(R.id.ll1);
            set_flowlayout(tv1, item);
            tv2.setText("¥" + item.getAmountPayable());
//            if (TextUtils.isEmpty(item.getButtonInfo())) {
//                tv7.setText(item.getButtonInfo());
//            } else if (TextUtils.isEmpty(item.getButtonPay())) {
//                tv7.setText(item.getButtonPay());
//            } else if (TextUtils.isEmpty(item.getButtonCancel())) {
//                tv7.setText(item.getButtonCancel());
//            }
            tv7.setText(item.getOrderStatusName());
            if (TextUtils.isEmpty(item.getCourseTimeRange())) {
                tv3.setText("--");
            } else {
                tv3.setText(item.getCourseTimeRange());
            }
            tv4.setText(item.getLecturerName());
            if (getData().size() == 1) {
                view1.setVisibility(View.GONE);
            } else {
                view1.setVisibility(View.VISIBLE);
            }
//            helper.addOnClickListener(R.id.ll1);
        }
    }

}