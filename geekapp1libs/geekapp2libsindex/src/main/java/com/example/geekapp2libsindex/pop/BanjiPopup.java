package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HGradecategoryBean2;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapter;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapterType;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lxj.xpopup.core.CenterPopupView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BanjiPopup extends CenterPopupView {

    public static final String TAG_CONTENT = "全部";
    public Context context;
    public List<BanjiAdapterType> mList1;

    public void set_select(BaseQuickAdapter mAdapter1, int pos) {
        List<BanjiAdapterType> list = mAdapter1.getData();
        for (int i = 0; i < list.size(); i++) {
            BanjiAdapterType bean = list.get(i);
            bean.getmBean().setText3(false);
        }
        list.get(pos).getmBean().setText3(true);
//        SPUtils.getInstance().put(CommonUtils.BANJI_ID, list.get(pos).getmBean().getText2());
        MmkvUtils.getInstance().set_common_json2(CommonUtils.BANJI_ID, list.get(pos).getmBean());
        mAdapter1.setNewData(list);
    }

    public void get_select(BaseQuickAdapter mAdapter1, List<BanjiAdapterType> mList1, HGradecategoryBean2 banjiCommonbean) {
//            List<BanjiAdapterType> list = mAdapter1.getData();
        for (int i = 0; i < mList1.size(); i++) {
            BanjiAdapterType bean = mList1.get(i);
            if (TextUtils.equals(bean.getmBean().getCode(), banjiCommonbean.getCode())) {
                bean.getmBean().setText3(true);
            } else {
                bean.getmBean().setText3(false);
            }
        }
        mAdapter1.setNewData(mList1);
    }

    public void set_tv1_background(TextView tv1, boolean is_press) {
        if (is_press) {
//            tv1.setPressed(true);
            tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_press);
            tv1.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
//            tv1.setPressed(false);
            tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_enpress);
            tv1.setTextColor(ContextCompat.getColor(context, R.color.color2F3D57));
        }
    }

    public BanjiPopup(@NonNull Context context, Ondismiss ondismiss, List<BanjiAdapterType> mList1) {
        super(context);
        this.context = context;
        this.ondismiss = ondismiss;
        this.mList1 = new ArrayList<>();
        this.mList1 = mList1;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_hxkt_nianji;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        XRecyclerView recyclerView1 = findViewById(R.id.recycler_view1);
        final TextView tv1 = findViewById(R.id.tv1);
        tv1.setText(TAG_CONTENT);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView1.setFocusable(false);
        recyclerView1.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView1.addItemDecoration(new SpaceItemDecoration(context));
        //
        final BanjiAdapter mAdapter1 = new BanjiAdapter(mList1);
        mAdapter1.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
//                int type = mList1.get(position).type;
                int type = Objects.requireNonNull(mAdapter1.getItem(position)).type;
                if (type == BanjiAdapterType.style1) {
                    return 3;
                } else if (type == BanjiAdapterType.style2) {
                    return 1;
                } else {
                    return 3;
                }
            }
        });
        recyclerView1.setAdapter(mAdapter1);

        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                BanjiAdapterType bean = (BanjiAdapterType) adapter.getData().get(position);
                int i = view.getId();
//                    Toasty.normal(getActivity(), bean.getmBean().getText2()).show();
                MyLogUtil.e("--geekyun---", bean.getmBean().getName());
//                tv1.setPressed(false);
                set_select(adapter, position);
                if (ondismiss != null) {
                    ondismiss.OnDismiss(bean.getmBean().getName());
                }
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 全部
//                SPUtils.getInstance().put(CommonUtils.BANJI_ID, "");
                MmkvUtils.getInstance().set_common_json2(CommonUtils.BANJI_ID, new HGradecategoryBean2("", TAG_CONTENT, 1, true));
                set_tv1_background(tv1, true);
                List<BanjiAdapterType> list = mAdapter1.getData();
                get_select(mAdapter1, list, MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class));
                if (ondismiss != null) {
                    ondismiss.OnDismiss(TAG_CONTENT);
                }
//                loadingPopup.dismiss();
            }
        });
//            mAdapter1.setNewData(mList1);
        get_select(mAdapter1, mList1, MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class));
        // 初始化
        HGradecategoryBean2 banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class);
        if (banjiCommonbean == null || TextUtils.equals(banjiCommonbean.getName(), TAG_CONTENT)) {
//            tv1.setText(BanjiPopup.TAG_CONTENT);
            set_tv1_background(tv1, true);
            //
            HGradecategoryBean2 banjiCommonbean1 = new HGradecategoryBean2("", BanjiPopup.TAG_CONTENT, 1, true);
            MmkvUtils.getInstance().set_common_json2(CommonUtils.BANJI_ID, banjiCommonbean1);
        } else {
//            tv1.setText(SPUtils.getInstance().getString(CommonUtils.BANJI_ID));
            set_tv1_background(tv1, false);
        }


    }

    protected void onShow() {
        super.onShow();
    }

//        @Override
//        protected int getMaxHeight() {
//            return 200;
//        }
//
    //返回0表示让宽度撑满window，或者你可以返回一个任意宽度
//        @Override
//        protected int getMaxWidth() {
//            return 1200;
//        }

    public Ondismiss ondismiss;

    public Ondismiss getOndismiss() {
        return ondismiss;
    }

    public void setOndismiss(Ondismiss ondismiss) {
        this.ondismiss = ondismiss;
    }

    public interface Ondismiss {
        void OnDismiss(String content);
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 5);
            tbSpace = DisplayUtil.dip2px(context, 2);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = outRect.left = lrSpace;
            outRect.bottom = tbSpace;
        }
    }
}
