package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.lxj.xpopup.core.CenterPopupView;

public class NamePopup extends CenterPopupView {

    private String text;

    public NamePopup(@NonNull Context context, OnDismiss ondismiss, String text) {
        super(context);
        this.ondismiss = ondismiss;
        this.text = text;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_hxkt_name;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        final EditText ed1 = findViewById(R.id.ed1);
        final TextView tv1 = findViewById(R.id.tv1);
        ed1.setText(text);
        ed1.setSelection(ed1.getText().length());
        tv1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ondismiss != null) {
                    ondismiss.OnDismiss(ed1.getText().toString().trim());
                }
            }
        });

    }

    protected void onShow() {
        super.onShow();
    }

    public OnDismiss ondismiss;

    public OnDismiss getOndismiss() {
        return ondismiss;
    }

    public void setOndismiss(OnDismiss ondismiss) {
        this.ondismiss = ondismiss;
    }

    public interface OnDismiss {
        void OnDismiss(String content);
    }

//        @Override
//        protected int getMaxHeight() {
//            return 200;
//        }
//
    //返回0表示让宽度撑满window，或者你可以返回一个任意宽度
//        @Override
//        protected int getMaxWidth() {
//            return 1200;
//        }
}