package com.example.geekapp2libsindex.fragment.demo.f1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;

import java.util.ArrayList;
import java.util.List;


public class F1_yulan1 extends SlbBaseIndexFragment {

    private String ids;
    private String tablayoutId;
    private ImageView iv_back1_order;
    private TextView tv_center_content;
    //
    private XRecyclerView mRvClass;
    private XRecyclerView mRvStudent;
    private F1LeftAdapter classAdapter;
    private F1RightAdapter studentAdapter;
    private List<OneBean1> mDataTablayoutLeft;
    private List<OneBean1> mDataTablayoutRight;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f1_yulan1;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        iv_back1_order.setVisibility(View.GONE);
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("实时监控");
        mRvClass = rootView.findViewById(R.id.rv_class);
        mRvStudent = rootView.findViewById(R.id.rv_student);
        initAdapter();
        donetwork();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toasty.normal(getActivity(), "测试bugly->热更新成功修复").show();
            }
        }, 3000);
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRvClass.setLayoutManager(linearLayoutManager);
        classAdapter = new F1LeftAdapter(getActivity());
        mRvClass.setAdapter(classAdapter);
        //设置默认的选取状态
        mRvClass.setScrollingTouchSlop(0);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity());
        mRvStudent.setLayoutManager(linearLayoutManager1);
        studentAdapter = new F1RightAdapter(getActivity());
        mRvStudent.setAdapter(studentAdapter);

        /**
         * 左侧列表的事件处理
         * */
        classAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                mRvClass.setScrollingTouchSlop(position);
//                studentAdapter.setData(position);
                set_footer_change(bean1);
                studentAdapter.setNewData(setData1(bean1.getTab_id()));
                mRvStudent.scrollToPosition(0);
            }
        });
        studentAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.LiandongDemoAct");
                intent.putExtra("courseId", bean1.getTab_id() + "");
                startActivity(intent);
            }
        });
    }

    private void initList() {
        for (int i = 0; i < classAdapter.getData().size(); i++) {
            OneBean1 item = classAdapter.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            classAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
        //
        mDataTablayoutLeft = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            if (i == 0) {
                mDataTablayoutLeft.add(new OneBean1("geek" + i, "测试" + i, true));
            } else {
                mDataTablayoutLeft.add(new OneBean1("geek" + i, "测试" + i, false));
            }
        }
        setData1(mDataTablayoutLeft.get(0).getTab_id());
        //
        classAdapter.setNewData(mDataTablayoutLeft);
        studentAdapter.setNewData(mDataTablayoutRight);
    }

    private List<OneBean1> setData1(String pos) {
        mDataTablayoutRight = new ArrayList<>();
        for (int i = 0; i < 14; i++) {
            if (i == 0) {
                mDataTablayoutRight.add(new OneBean1("1", "测试1→" + pos, false));
            } else {
                mDataTablayoutRight.add(new OneBean1("2", "测试2→" + pos, false));
            }
        }
        return mDataTablayoutRight;
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */


}
