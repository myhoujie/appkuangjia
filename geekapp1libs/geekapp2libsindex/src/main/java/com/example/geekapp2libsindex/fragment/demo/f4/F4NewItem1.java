package com.example.geekapp2libsindex.fragment.demo.f4;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.bean.HfeedBackReqBean;
import com.example.biz3hxktindex.presenter.HTouxiangsPresenter;
import com.example.biz3hxktindex.presenter.HfeedBackPresenter;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.example.biz3hxktindex.view.HfeedBackView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.fragment.demo.LocationService;
import com.example.slbappcomm.uploadimg3.FullyGridLayoutManager;
import com.example.slbappcomm.uploadimg3.GridImageAdapter;
import com.haier.cellarette.baselibrary.common.BaseAppManager;
import com.haier.cellarette.baselibrary.statusbar.StatusBarUtil;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libutils.utilslib.data.LocationUtils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.permissions.RxPermissions;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.lzy.imagepicker.util.Utils;
import com.lzy.imagepicker.view.GridSpacingItemDecoration;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.internal.CustomAdapt;

import static android.app.Activity.RESULT_OK;

public class F4NewItem1 extends Fragment implements HfeedBackView, HTouxiangsView, CustomAdapt {

    private EditText contentEt;
    private EditText f1_edt1;
    private EditText f1_edt2;
    private TextView f1_tv1;
    private TextView numTv;
    private TextView submitTv;
    private HfeedBackPresenter feedBackPresenter;
    private HTouxiangsPresenter uploadPresenter;
    private List<LocalMedia> selectList;
    private int themeId;
    private int chooseMode = PictureMimeType.ofImage();
    private int maxSelectNum = 3;
    private int mCurrentPosition;

    private ImageView cameraIv;
    private ImageView f4_iv1;
    private RecyclerView recyclerView;
    private GridImageAdapter adapter;
    private BasePopupView loadingPopup;

    private String page;
    private String text;
    private String ids;
    private LocationService mLocationService;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        AutoSize.autoConvertDensity(getActivity(), 375, true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mUrl = getArguments().getString(ARG_PARAM1);
//            mType = getArguments().getInt(ARG_TRANSPORT_MODE);
//            sendOption = getArguments().getInt(ARG_SEND_OPTION);
//            mRR = getArguments().getParcelable(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.demo_f4_new_item1, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        BaseAppManager.getInstance().add(getActivity());
        StatusBarUtil.setColor(getActivity(), ContextCompat.getColor(getActivity(), R.color.blue357CD4), 0);
//        StatusBarUtil.setLightMode(getActivity());
//        getActivity().bindService(new Intent(getActivity(), LocationService.class), conn, Context.BIND_AUTO_CREATE);
        findview(view);
        onclick();
        donetwork();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    selectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
                    for (LocalMedia media : selectList) {
                        Log.i("ssss", "压缩---->" + media.getCompressPath());
                        Log.i("ssss", "原图---->" + media.getPath());
                        Log.i("ssss", "裁剪---->" + media.getCutPath());
                    }
                    adapter.setList(selectList);
                    adapter.notifyDataSetChanged();
                    set_vis_layout();
                    break;
            }
        }
    }

//    @Override
//    public void call(Object value) {
//        ids = (String) value;
//        Toasty.normal(getActivity(), ids).show();
//    }

    @Override
    public void onDestroyView() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        getActivity().unbindService(conn);
        uploadPresenter.onDestory();
        feedBackPresenter.onDestory();
        super.onDestroyView();
    }

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {

        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mLocationService = ((LocationService.LocationBinder) service).getService();
            mLocationService.setOnGetLocationListener(new LocationService.OnGetLocationListener() {
                @Override
                public void getLocation(final String lastLatitude, final String lastLongitude, final String latitude, final String longitude, final String country, final String locality, final String street) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            f1_edt1.setText("" + longitude);
                            f1_edt2.setText("" + latitude);
                            contentEt.setText("" + country + street + locality);
//                            tvAboutLocation.setText(new SpanUtils()
//                                    .appendLine("lastLatitude: " + lastLatitude)
//                                    .appendLine("lastLongitude: " + lastLongitude)
//                                    .appendLine("latitude: " + latitude)
//                                    .appendLine("longitude: " + longitude)
//                                    .appendLine("getCountryName: " + country )
//                                    .appendLine("getLocality: " + locality)
//                                    .appendLine( "getStreet: " + street)
//                                    .create()
//                            );

                        }
                    });
                }
            });
        }
    };

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 375;
    }

    private void findview(View rootView) {
        selectList = new ArrayList<>();
        themeId = R.style.picture_white_style;

        ImageView iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        iv_back1_order.setVisibility(View.GONE);
        TextView tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("信息上传");
        f4_iv1 = rootView.findViewById(R.id.f4_iv1);
        contentEt = rootView.findViewById(R.id.contentEt);
        f1_edt1 = rootView.findViewById(R.id.f1_edt1);
        f1_edt2 = rootView.findViewById(R.id.f1_edt2);
        f1_tv1 = rootView.findViewById(R.id.f1_tv1);
        numTv = rootView.findViewById(R.id.numTv);
        submitTv = rootView.findViewById(R.id.submitTv);
        submitTv.setEnabled(false);
        cameraIv = rootView.findViewById(R.id.cameraIv);
        numTv = rootView.findViewById(R.id.numTv);

        recyclerView = rootView.findViewById(R.id.recyclerView);
        FullyGridLayoutManager manager = new FullyGridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, Utils.dp2px(getActivity(), 20), false));
        adapter = new GridImageAdapter(getActivity(), onAddPicClickListener);
        adapter.setList(selectList);
        adapter.setSelectMax(maxSelectNum);
        recyclerView.setAdapter(adapter);
    }

    private void donetwork() {
        feedBackPresenter = new HfeedBackPresenter();
        feedBackPresenter.onCreate(this);

        uploadPresenter = new HTouxiangsPresenter();
        uploadPresenter.onCreate(this);
    }

    private int MAX_TEXT = 100;
    private TextWatcher mTextWatcher = new TextWatcher() {
        private CharSequence temp;
        private int editStart;
        private int editEnd;

        @Override
        public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            temp = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            numTv.setText(s.length() + "/100字");
            setSubmitTvStatus();
        }

        @Override
        public void afterTextChanged(Editable s) {
            editStart = contentEt.getSelectionStart();
            editEnd = contentEt.getSelectionEnd();
            if (temp.length() > MAX_TEXT) {
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                contentEt.setText(s);
                contentEt.setSelection(tempSelection);
            }
        }
    };

    private void onclick() {
        KeyboardVisibilityEvent.setEventListener(getActivity(), new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                // some code depending on keyboard visiblity status
                if (isOpen) {
                    // 此处为得到焦点时的处理内容
                    submitTv.setVisibility(View.GONE);
                } else {
                    // 此处为失去焦点时的处理内容
                    submitTv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            submitTv.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                }
            }
        });
        submitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectList.size() > 0) {
                    List<File> list = new ArrayList<>();
                    for (int i = 0; i < selectList.size(); i++) {
                        if (selectList.get(i).isCompressed()) {
                            File file = new File(selectList.get(i).getCompressPath());
                            list.add(file);
                        } else {
                            File file = new File(selectList.get(i).getPath());
                            list.add(file);
                        }
                    }
                    loadingPopup = new XPopup.Builder(getActivity())
                            .dismissOnBackPressed(true)
                            .dismissOnTouchOutside(false)
                            .asLoading("上传中...")
                            .show();
                    uploadPresenter.get_touxiang3(list);
                } else {
                    feedback(new ArrayList<String>());
                }
            }
        });
        f4_iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 位置
//                getActivity().bindService(new Intent(getActivity(), LocationService.class), conn, Context.BIND_AUTO_CREATE);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                isSuccess = LocationUtils.register(0, 0, mOnLocationChangeListener);
            }
        });
        cameraIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_pop();
            }
        });
        contentEt.addTextChangedListener(mTextWatcher);
//        contactEt.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                setSubmitTvStatus();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        adapter.setOnItemClickListener(new GridImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (selectList.size() > 0) {
                    LocalMedia media = selectList.get(position);
                    String pictureType = media.getPictureType();
                    int mediaType = PictureMimeType.pictureToVideo(pictureType);
                    switch (mediaType) {
                        case 1:
                            // 预览图片 可自定长按保存路径
                            //PictureSelector.create(MainActivity.this).themeStyle(themeId).externalPicturePreview(position, "/custom_file", selectList);
                            PictureSelector.create(getActivity()).themeStyle(themeId).openExternalPreview(position, selectList);
                            break;
                        case 2:
                            // 预览视频
                            PictureSelector.create(getActivity()).externalPictureVideo(media.getPath());
                            break;
                        case 3:
                            // 预览音频
                            PictureSelector.create(getActivity()).externalPictureAudio(media.getPath());
                            break;
                    }
                }
            }
        });
        adapter.setOnItemClickListener2(new GridImageAdapter.OnItemClickListener2() {
            @Override
            public void onItemClick2(int position, View v) {
                mCurrentPosition = position;
                showDeleteDialog();
            }
        });
    }

    private LocationService.OnGetLocationListener mOnGetLocationListener;
    private String lastLatitude = "";
    private String lastLongitude = "";
    private String latitude = "";
    private String longitude = "";
    private String country = "";
    private String locality = "";
    private String street = "";
    private boolean isSuccess;

    private LocationUtils.OnLocationChangeListener mOnLocationChangeListener = new LocationUtils.OnLocationChangeListener() {
        @Override
        public void getLastKnownLocation(Location location) {
            lastLatitude = String.valueOf(location.getLatitude());
            lastLongitude = String.valueOf(location.getLongitude());
//            if (mOnGetLocationListener != null) {
//                mOnGetLocationListener.getLocation(lastLatitude, lastLongitude, latitude, longitude, country, locality, street);
//            }
            if (isSuccess) {
//                    ToastUtils.showShort("init success");
                f1_edt1.setText("" + longitude);
                f1_edt1.setSelection(f1_edt1.getText().length());
                f1_edt2.setText("" + latitude);
                f1_edt1.setSelection(f1_edt1.getText().length());
                contentEt.setText("" + country + street + locality);
            }
        }

        @Override
        public void onLocationChanged(final Location location) {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
//            if (mOnGetLocationListener != null) {
//                mOnGetLocationListener.getLocation(lastLatitude, lastLongitude, latitude, longitude, country, locality, street);
//            }
            country = LocationUtils.getCountryName(Double.parseDouble(latitude), Double.parseDouble(longitude));
            locality = LocationUtils.getLocality(Double.parseDouble(latitude), Double.parseDouble(longitude));
            street = LocationUtils.getStreet(Double.parseDouble(latitude), Double.parseDouble(longitude));
//            if (mOnGetLocationListener != null) {
//                mOnGetLocationListener.getLocation(lastLatitude, lastLongitude, latitude, longitude, country, locality, street);
//            }
            if (isSuccess) {
//                    ToastUtils.showShort("init success");
                f1_edt1.setText("" + longitude);
                f1_edt1.setSelection(f1_edt1.getText().length());
                f1_edt2.setText("" + latitude);
                f1_edt2.setSelection(f1_edt2.getText().length());
                contentEt.setText("" + country + street + locality);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    private void feedback(List<String> urls) {
        String phone = f1_tv1.getText().toString().trim();
//        if (!TextUtils.isEmpty(phone) && !RegexUtils.isMobileSimple(phone)) {
        if (!TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("请选择");
            return;
        }
        HfeedBackReqBean bean = new HfeedBackReqBean(BanbenUtils.getInstance().getVersion(), phone, contentEt.getText().toString().trim(), BanbenUtils.getInstance().getImei(), "android", urls);
        feedBackPresenter.get_feedBack(bean);
    }

    private void set_pop() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(this)
                .openGallery(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(maxSelectNum)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(false)// 是否可预览视频
                .enablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .compressSavePath(getPath())//压缩图片保存地址
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(true)// 是否显示gif图片
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                //.isDragFrame(false)// 是否可拖动裁剪框(固定)
//                        .videoMaxSecond(15)
//                        .videoMinSecond(10)
                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                //.rotateEnabled(true) // 裁剪是否可旋转图片
                //.scaleEnabled(true)// 裁剪是否可放大缩小图片
                //.videoQuality()// 视频录制质量 0 or 1
                //.videoSecond()//显示多少秒以内的视频or音频也可适用
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    /**
     * 是否删除此张图片
     */
    private void showDeleteDialog() {
        new XPopup.Builder(getActivity())
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onCreated() {
                        Log.e("tag", "弹窗创建了");
                    }

                    @Override
                    public void onShow() {
                        Log.e("tag", "onShow");
                    }

                    @Override
                    public void onDismiss() {
                        Log.e("tag", "onDismiss");
                    }

                    //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                    @Override
                    public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                        return true;
                    }
                }).asConfirm("提示", "要删除这张照片吗？",
                "取消", "确定",
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        //移除当前图片刷新界面
                        selectList.remove(mCurrentPosition);
                        adapter.notifyItemRemoved(mCurrentPosition);
                        adapter.notifyItemRangeChanged(mCurrentPosition, selectList.size());
                        set_vis_layout();

                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                }, false)
                .show();
    }

    private void set_vis_layout() {
        if (selectList.size() == 0) {
            cameraIv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            submitTv.setVisibility(View.GONE);
        } else {
            cameraIv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        setSubmitTvStatus();
    }

    private void setSubmitTvStatus() {
        if (contentEt.getText().toString().trim().length() > 0) {
            submitTv.setEnabled(true);
        } else {
            submitTv.setEnabled(false);
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        donetwork();
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

//    /**
//     * fragment间通讯bufen
//     *
//     * @param value 要传递的值
//     * @param tag   要通知的fragment的tag
//     */
//    public void callFragment(Object value, String... tag) {
//        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
//        Fragment fragment;
//        for (String item : tag) {
//            if (TextUtils.isEmpty(item)) {
//                continue;
//            }
//
//            fragment = fm.findFragmentByTag(item);
//            if (fragment instanceof SlbBaseIndexFragment) {
//                ((SlbBaseIndexFragment) fragment).call(value);
//            }
//        }
//    }


    private void set_clear_img_lujing() {
        // 清空图片缓存，包括裁剪、压缩后的图片 注意:必须要在上传完成后调用 必须要获取权限
        RxPermissions permissions = new RxPermissions(getActivity());
        permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PictureFileUtils.deleteCacheDirFile(getActivity());
                } else {
                    Toast.makeText(getActivity(),
                            getString(R.string.picture_jurisdiction), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }

    /**
     * 自定义压缩存储地址
     *
     * @return
     */
    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        if (file.mkdirs()) {
            return path;
        }
        return path;
    }

    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            set_pop();
        }
    };

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */
    @Override
    public void OnTouxiangsSuccess(HTouxiangBean1 hTouxiangBean1) {
        String imgStr = hTouxiangBean1.getUrls();
        List<String> list1 = new ArrayList<>();
        if (!TextUtils.isEmpty(imgStr)) {
            String[] str = imgStr.split(",");
            list1 = Arrays.asList(str);
        }
        feedback(list1);
    }

    @Override
    public void OnTouxiangsNodata(final String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        ToastUtils.showLong(s);
    }

    @Override
    public void OnTouxiangsFail(final String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        ToastUtils.showLong(s);
    }

    @Override
    public void OnfeedBackSuccess(String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        Toasty.normal(getActivity(), "反馈成功").show();
        set_clear_img_lujing();
//        finish();
    }

    @Override
    public void OnfeedBackNodata(String s) {
        Toasty.normal(getActivity(), "反馈失败，请稍后重试~").show();
    }

    @Override
    public void OnfeedBackFail(String s) {
        Toasty.normal(getActivity(), "反馈失败，请稍后重试~").show();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }
}
