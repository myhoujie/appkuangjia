package com.example.geekapp2libsindex.fragment.three.order;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.presenter.HLunbotuPresenter;
import com.example.biz3hxktindex.view.HLunbotuView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.common.OrderCommonAdapter;
import com.example.geekapp2libsindex.common.SlbBaseOrderFragment;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.example.geekapp2libsindex.domain.OrderCommonItemBean;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class OrderCommonFragment1 extends SlbBaseOrderFragment implements HLunbotuView {

    private List<HLunbotuBean1> mListbanner1;
    private HLunbotuPresenter presenter1;
    //
    private OrderCommonAdapter mAdapter1;
    private List<OrderCommonItemBean> mList1;
    private String tablayoutId;

    @Override
    public void call(Object value) {
        tablayoutId = (String) value;
        Toasty.normal(getActivity(), tablayoutId).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        if (presenter1 != null) {
            presenter1.onDestory();
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_order1_common2;
    }

    @Override
    protected void donetwork1() {
        mListbanner1 = new ArrayList<>();
        presenter1 = new HLunbotuPresenter();
        presenter1.onCreate(this);
    }

    @Override
    protected void retryData1() {
        if (presenter1 == null) {
            mListbanner1 = new ArrayList<>();
            presenter1 = new HLunbotuPresenter();
            presenter1.onCreate(this);
        }
        presenter1.get_lunbotu();
    }

    @Override
    protected void onclick1() {
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OrderCommonItemBean bean = (OrderCommonItemBean) adapter.getData().get(position);
                if (view.getId() == R.id.tv0) {
                    return;
                }
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun---", bean.getText_id());
                //订单详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderDetailActivity");
//                intent.putExtra("orderType", bean.getOrderType());
                intent.putExtra("orderId", bean.getText_id());
                startActivity(intent);
            }
        });
        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                OrderCommonItemBean bean = (OrderCommonItemBean) adapter.getData().get(position);
                int i = view.getId();
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun1---", bean.getText_id());
                //订单详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderDetailActivity");
                intent.putExtra("orderId", bean.getText_id());
//                intent.putExtra("orderType", bean.getOrderType());
                startActivity(intent);

            }
        });
    }

    @Override
    protected void findview1() {

        //
        emptyview1.notices("暂无订单，去选课中心看看吧…", "没有网络了,检查一下吧", "正在加载....", "");
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        mList1 = new ArrayList<>();
        mAdapter1 = new OrderCommonAdapter(R.layout.recycleview_hxkt_order_item1);
        recyclerView1.setAdapter(mAdapter1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void updateUI(final OrderCommonItem2Bean tablayoutId) {
        MyLogUtil.e("--geekyun-updateUI-tttttt", tablayoutId.getId());
        if (TextUtils.equals(tablayoutId.getId(), "0")) {
            // 刷新
            retryData1();
        }
        if (TextUtils.equals(tablayoutId.getId(), "1")) {
            // 滚动到顶部
            getCate(tablayoutId);
        }
    }

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    public List<OrderCommonItemBean> getSampleData(int lenth) {
        List<OrderCommonItemBean> list = new ArrayList<>();
        for (int i = 0; i < lenth; i++) {
            OrderCommonItemBean status = new OrderCommonItemBean();
            status.setText_id(i + "");
            status.setText_content("https://avatars1.githubusercontent.com/u/7698209?v=3&s=460");
            list.add(status);
        }
        return list;
    }


    @Override
    public void OnLunbotuSuccess(HLunbotuBean hLunbotuBean) {
        OnOrderSuccess();
        mList1 = new ArrayList<>();
        mList1 = getSampleData(10);
        mAdapter1.setNewData(mList1);
        mAdapter1.notifyDataSetChanged();
    }

    @Override
    public void OnLunbotuNodata(String s) {
        OnOrderNodata();

    }

    @Override
    public void OnLunbotuFail(String s) {
        OnOrderFail();

    }
}
