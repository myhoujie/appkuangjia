package com.example.geekapp2libsindex.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import com.geek.libglide47.base.util.DisplayUtil;

public class RoundBackgroundColorSpan2 extends ReplacementSpan {
    private int bgColor;
    private int textColor;
    private Context context;

    public RoundBackgroundColorSpan2(Context context, int bgColor, int textColor) {
        super();
        this.bgColor = bgColor;
        this.textColor = textColor;
        this.context = context;
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        //这个地方返回的与其余字体的间距
        return ((int) paint.measureText(text, start, end) + DisplayUtil.dip2px(context, 15));
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        int color1 = paint.getColor();
        paint.setColor(this.bgColor);
        System.out.println(top + "," + y + "," + bottom);
        //rx  ry 控制背景圆角的大小
        RectF rect = new RectF(x + DisplayUtil.dip2px(context, 0), top + DisplayUtil.dip2px(context, 3), x + DisplayUtil.dip2px(context, 20), top + DisplayUtil.dip2px(context, 23));
        canvas.drawRoundRect(rect, DisplayUtil.dip2px(context, 5), DisplayUtil.dip2px(context, 5), paint);
        paint.setColor(this.textColor);
        canvas.drawText(text, start, end, x + (DisplayUtil.dip2px(context, 20) - (int) paint.measureText(text, start, end)) / 2, y - 5, paint);
        paint.setColor(color1);
    }
}
