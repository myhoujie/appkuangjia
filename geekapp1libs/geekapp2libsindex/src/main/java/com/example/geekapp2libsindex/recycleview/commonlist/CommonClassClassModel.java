package com.example.geekapp2libsindex.recycleview.commonlist;

import java.io.Serializable;
import java.util.List;

public class CommonClassClassModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<CommonClassClassModel2> list;

    public CommonClassClassModel() {
    }

    public CommonClassClassModel(List<CommonClassClassModel2> list) {
        this.list = list;
    }

    public List<CommonClassClassModel2> getList() {
        return list;
    }

    public void setList(List<CommonClassClassModel2> list) {
        this.list = list;
    }
}
