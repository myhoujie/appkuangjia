package com.example.geekapp2libsindex.pay;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.NoticePopup;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.fanli.biz3hxktdetail.bean.LiveSonOrderBean;
import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.fanli.biz3hxktdetail.presenter.OrderOrCourseDetailPresenter;
import com.fanli.biz3hxktdetail.view.OrderOrCourseDetailView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupAnimation;
import com.umeng.analytics.MobclickAgent;

public class SignUpSuccessActivity extends SlbBaseActivity implements OrderOrCourseDetailView {
    private TextView toOrderTv;
    private TextView toDetailTv;
    private ImageView bottomIv;
    private String liveCourseId;
    private String liveOrderId;
    private String orderType;
    private OrderOrCourseDetailPresenter orderOrCourseDetailPresenter;
    private LiveSonOrderBean liveSonOrderBean;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_signup_success;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        if (TextUtils.equals("1", getIntent().getStringExtra("chargeFlag"))) {//收费系统课
            MobclickAgent.onEvent(this, "courseselection_systemclass_successpage");
        } else {//免费系统课
            MobclickAgent.onEvent(this, "courseselection_systemfreeclass_successpage");
        }
        liveCourseId = getIntent().getStringExtra("liveCourseId");
        liveOrderId = getIntent().getStringExtra("liveOrderId");
        orderType = getIntent().getStringExtra("orderType");

        orderOrCourseDetailPresenter = new OrderOrCourseDetailPresenter();
        orderOrCourseDetailPresenter.onCreate(this);

        findview();
        onclick();

        orderOrCourseDetailPresenter.get_orderDetail(liveOrderId, orderType);
//        showNoticePop();
    }

    private void showNoticePop(String liveOrderId) {
        new XPopup.Builder(this)
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .autoOpenSoftInput(true)
                .asCustom(new NoticePopup(this, liveOrderId)).show();
    }

    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("报名成功");
        toOrderTv = findViewById(R.id.toOrderTv);
        toDetailTv = findViewById(R.id.toDetailTv);
        bottomIv = findViewById(R.id.bottomIv);
    }

    private void onclick() {
        bottomIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.NoticeDetailActivity");
                intent.putExtra("orderId", liveSonOrderBean == null ? "" : liveSonOrderBean.getId());
                startActivity(intent);
            }
        });
        toOrderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderDetailActivity");
                intent.putExtra("orderId", liveOrderId);
                intent.putExtra("orderType", orderType);
                startActivity(intent);
            }
        });
        toDetailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(SignUpSuccessActivity.this, "successpage_noticebannerclick");
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                intent.putExtra("courseId", liveSonOrderBean == null ? "" : liveSonOrderBean.getLiveCourseId());
                startActivity(intent);
            }
        });
    }

    @Override
    public void back(View v) {
        MobclickAgent.onEvent(this, "courseselection_successpage_closebutton");
        Intent msgIntent = new Intent();
        msgIntent.setAction("OrderAct");
        msgIntent.putExtra("id", liveOrderId);
        LocalBroadcastManagers.getInstance(getApplicationContext()).sendBroadcast(msgIntent);
        super.back(v);
    }

    @Override
    public void OrderOrCourseDetailSuccess(OrderOrCourseDetailResponseBean orderOrCourseDetailResponseBean) {
        if (orderOrCourseDetailResponseBean.getLivePOrder() != null && !ListUtil.isEmpty(orderOrCourseDetailResponseBean.getLivePOrder().getSons())) {
            liveSonOrderBean = orderOrCourseDetailResponseBean.getLivePOrder().getSons().get(0);
            showNoticePop(liveSonOrderBean.getId());
        }

    }

    @Override
    public void OrderOrCourseDetailNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OrderOrCourseDetailFail(String s) {
        Toasty.normal(this, s).show();
    }
}
