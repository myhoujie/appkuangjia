package com.example.geekapp2libsindex.widget;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.haier.cellarette.libutils.ConstantsUtils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cc.shinichi.library.ImagePreview;
import cc.shinichi.library.bean.ImageInfo;

public class PicHelper {
    private static Set<OnPicOperateListener> onPicOperateListenerSet = new HashSet<>();
    private int defaultThemeId = R.style.picture_white_style;
    private int defaultChooseMode = PictureMimeType.ofImage();
    private String defaultPath;
    private static PicHelper instance;
    private List<LocalMedia> selectList;

    private PicHelper() {
        defaultPath = getPath();
    }

    public static PicHelper getInstance(OnPicOperateListener onPicOperateListener) {
        if (instance == null) {
            synchronized (PicHelper.class) {
                if (instance == null) {
                    instance = new PicHelper();
                }
            }
        }
        if (onPicOperateListener != null) {
            onPicOperateListenerSet.add(onPicOperateListener);
        }
        return instance;
    }

    public void addOnPicOperateListener(OnPicOperateListener onPicOperateListener) {
        if (onPicOperateListener != null) {
            onPicOperateListenerSet.add(onPicOperateListener);
        }
    }

    public static PicHelper getInstance() {
        return getInstance(null);
    }

    /**
     * 自定义压缩存储地址
     *
     * @return
     */
    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        if (file.mkdirs()) {
            return path;
        }
        return path;
    }

    public void toSelectPic(Activity activity) {
        toSelectPic(activity, defaultThemeId, defaultChooseMode, defaultPath,true);
    }
    public void toSelectPic(Activity activity ,boolean isCamera) {
        toSelectPic(activity, defaultThemeId, defaultChooseMode, defaultPath,isCamera);
    }
    public void toSelectPic(Activity activity, int themeId, int chooseMode, String path,boolean isCamera) {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(activity)
                .openGallery(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(false)// 是否可预览视频
                .enablePreviewAudio(false) // 是否可播放音频
                .isCamera(isCamera)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .compressSavePath(path)//压缩图片保存地址
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(true)// 是否显示gif图片
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(getSelectedList())// 是否传入已选图片
                //.isDragFrame(false)// 是否可拖动裁剪框(固定)
//                        .videoMaxSecond(15)
//                        .videoMinSecond(10)
                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                //.rotateEnabled(true) // 裁剪是否可旋转图片
                //.scaleEnabled(true)// 裁剪是否可放大缩小图片
                //.videoQuality()// 视频录制质量 0 or 1
                //.videoSecond()//显示多少秒以内的视频or音频也可适用
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }
    public void toOpenCamera(Activity activity) {
        toOpenCamera(activity, defaultThemeId, defaultChooseMode, defaultPath);
    }
    public void toOpenCamera(Activity activity, int themeId, int chooseMode, String path) {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(activity)
                .openCamera(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .previewImage(true)// 是否可预览图片
                .previewVideo(false)// 是否可预览视频
                .enablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .compressSavePath(path)//压缩图片保存地址
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(true)// 是否显示gif图片
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }
    public void getBackSelectedList(Intent data) {
        // 图片选择结果回调
        List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
        this.selectList = selectList;
        if (onPicOperateListenerSet.size() > 0) {
            for (OnPicOperateListener onPicOperateListener : onPicOperateListenerSet) {
                onPicOperateListener.onPicSelect(selectList);
            }
        }
    }

    public void previewLocal(Activity activity, int position) {
        PictureSelector.create(activity).themeStyle(R.style.picture_white_style).openExternalPreview(position, selectList);
    }

    public void previewUrl(Activity activity, int position, List<ImageInfo> imageInfoList) {
        ImagePreview
                .getInstance()
                .setContext(activity)
                .setIndex(position)
                .setImageInfoList(imageInfoList)
                .setShowDownButton(true)
                .setLoadStrategy(ImagePreview.LoadStrategy.Default)
                .setFolderName(ConstantsUtils.gen_img)
                .setScaleLevel(1, 3, 8)
                .setZoomTransitionDuration(500)
                .setShowCloseButton(true)
                .setEnableDragClose(true)// 是否启用上拉/下拉关闭，默认不启用
                .setEnableClickClose(true)// 是否启用点击图片关闭，默认启用
                .start();
    }

    public List<LocalMedia> getSelectedList() {
        return selectList;
    }

    public interface OnPicOperateListener {
        void onPicSelect(List<LocalMedia> selectList);
    }

    public void release() {
        onPicOperateListenerSet.clear();
        if (!ListUtil.isEmpty(selectList)) {
            selectList.clear();
        }
    }

    public void release(OnPicOperateListener onPicOperateListener) {
        onPicOperateListenerSet.remove(onPicOperateListener);
    }
}
