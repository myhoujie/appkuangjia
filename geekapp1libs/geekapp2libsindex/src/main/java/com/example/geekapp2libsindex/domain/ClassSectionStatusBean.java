package com.example.geekapp2libsindex.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class ClassSectionStatusBean implements Parcelable {
    private int position;
    private String exerciseStatus;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.position);
        dest.writeString(this.exerciseStatus);
    }

    public ClassSectionStatusBean() {
    }

    protected ClassSectionStatusBean(Parcel in) {
        this.position = in.readInt();
        this.exerciseStatus = in.readString();
    }

    public static final Parcelable.Creator<ClassSectionStatusBean> CREATOR = new Parcelable.Creator<ClassSectionStatusBean>() {
        @Override
        public ClassSectionStatusBean createFromParcel(Parcel source) {
            return new ClassSectionStatusBean(source);
        }

        @Override
        public ClassSectionStatusBean[] newArray(int size) {
            return new ClassSectionStatusBean[size];
        }
    };
}
