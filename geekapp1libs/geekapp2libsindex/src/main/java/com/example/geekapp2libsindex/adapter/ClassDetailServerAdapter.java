package com.example.geekapp2libsindex.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.CourseServiceBean;

public class ClassDetailServerAdapter extends BaseQuickAdapter<CourseServiceBean, BaseViewHolder> {

    public ClassDetailServerAdapter() {
        super(R.layout.adapter_classdetail_server);
    }

    @Override
    protected void convert(BaseViewHolder helper, CourseServiceBean item) {
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView contentTv = helper.itemView.findViewById(R.id.contentTv);
        titleTv.setText(item.getName());
        contentTv.setText(item.getDirection());
    }
}
