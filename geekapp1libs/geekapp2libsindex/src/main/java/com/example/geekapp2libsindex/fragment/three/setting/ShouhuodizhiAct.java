package com.example.geekapp2libsindex.fragment.three.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HMyaddressBean;
import com.example.biz3hxktindex.presenter.HMyaddressPresenter;
import com.example.biz3hxktindex.presenter.HMyaddressupdatePresenter;
import com.example.biz3hxktindex.view.HMyaddressView;
import com.example.biz3hxktindex.view.HMyaddressupdateView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.style.cityjd.JDCityConfig;
import com.lljjcoder.style.cityjd.JDCityPicker;

public class ShouhuodizhiAct extends SlbBaseActivity implements View.OnClickListener, HMyaddressView, HMyaddressupdateView {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private RelativeLayout rl1;
    private EditText ed1;
    private RelativeLayout rl2;
    private EditText ed2;
    private RelativeLayout rl3;
    private TextView ed3;
    private EditText ed4;
    private TextView tv1;
    private LinearLayout ll1;
    private String content;
    private String sheng;
    private String shi;
    private String qu;
    private String swUserId;
    private int intent_from;

    private HMyaddressPresenter hMyaddressPresenter;
    private HMyaddressupdatePresenter hMyaddressupdatePresenter;

    @Override
    protected void onDestroy() {
        hMyaddressPresenter.onDestory();
        hMyaddressupdatePresenter.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shdz;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("收货地址");
        intent_from = getIntent().getIntExtra(CommonUtils.INTENT_FROM, 0);// 0 编辑资料 1 订单详情
        //
        ed1.setText("");
        ed2.setText("");
        ed3.setText("");
        ed4.setText("");
        sheng = "";
        shi = "";
        qu = "";
        swUserId = "";
        // 获取
        hMyaddressPresenter = new HMyaddressPresenter();
        hMyaddressPresenter.onCreate(this);
        hMyaddressPresenter.get_myaddress();
        // 更新
        hMyaddressupdatePresenter = new HMyaddressupdatePresenter();
        hMyaddressupdatePresenter.onCreate(this);

    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        tv1.setOnClickListener(this);
        ed3.setOnClickListener(this);
        ed2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void findview() {
        ivBack = (ImageView) findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) findViewById(R.id.tv_center_content);
        rl1 = (RelativeLayout) findViewById(R.id.rl1);
        ed1 = (EditText) findViewById(R.id.ed1);
        rl2 = (RelativeLayout) findViewById(R.id.rl2);
        ed2 = (EditText) findViewById(R.id.ed2);
        rl3 = (RelativeLayout) findViewById(R.id.rl3);
        ed3 = (TextView) findViewById(R.id.ed3);
        ed4 = (EditText) findViewById(R.id.ed4);
        tv1 = (TextView) findViewById(R.id.tv1);
        ll1 = (LinearLayout) findViewById(R.id.ll1);

    }

    private boolean is_yanzheng() {
        if (TextUtils.isEmpty(ed1.getText().toString().trim())) {
            ToastUtils.showLong("请填写收货人");
            return false;
        }
        if (TextUtils.isEmpty(ed2.getText().toString().trim())) {
            ToastUtils.showLong("请填写收货人电话");
            return false;
        }
//        String aaaa = ed2.getText().toString().trim().replace(" ", "");
        if (!RegexUtils.isMobileSimple(ed2.getText().toString().trim())) {
            ToastUtils.showLong("手机号格式不正确");
            return false;
        }
        MyLogUtil.e("ssssssss", ed2.getText().toString().trim());
        if (TextUtils.isEmpty(ed3.getText().toString().trim())) {
            ToastUtils.showLong("请填写所在地区");
            return false;
        }
        if (TextUtils.isEmpty(ed4.getText().toString().trim())) {
            ToastUtils.showLong("请填写所在地区的详细地址");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.ed3) {
            // 地区
            //
            KeyboardUtils.hideSoftInput(ShouhuodizhiAct.this);
            ll1.clearFocus();
            //
            JDCityPicker cityPicker = new JDCityPicker();
            JDCityConfig jdCityConfig = new JDCityConfig.Builder().build();
            jdCityConfig.setShowType(JDCityConfig.ShowType.PRO_CITY_DIS);
            cityPicker.init(ShouhuodizhiAct.this);
            cityPicker.setConfig(jdCityConfig);
            cityPicker.setOnCityItemClickListener(new OnCityItemClickListener() {
                @Override
                public void onSelected(ProvinceBean province, CityBean city, DistrictBean district) {
                    MyLogUtil.e("---geekyun--", "城市选择结果：\n" + province.getName() + "(" + province.getId() + ")\n"
                            + city.getName() + "(" + city.getId() + ")\n"
                            + district.getName() + "(" + district.getId() + ")");
                    content = province.getName() + city.getName() + district.getName();
                    sheng = province.getName();
                    shi = city.getName();
                    qu = district.getName();
                    ed3.setText(content);
                    //
//                    MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHENG, sheng);
//                    MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHI, shi);
//                    MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_QU, qu);
                }

                @Override
                public void onCancel() {
                }
            });
            cityPicker.showCityPicker();
        } else if (view.getId() == R.id.tv1) {
            // 保存
            if (!is_yanzheng()) {
                return;
            }
            //
//            if (TextUtils.isEmpty(shi)) {
//                onBackPressed();
//                return;
//            }
//            if (TextUtils.isEmpty(sheng)) {
//                sheng = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHENG);
//                shi = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHI);
//                qu = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_QU);
//            }
            hMyaddressupdatePresenter.get_myaddressupdate(ed4.getText().toString().trim(), shi, qu, sheng, swUserId, ed1.getText().toString().trim(), ed2.getText().toString().trim());
        } else {

        }
    }


    @Override
    public void OnMyaddressSuccess(HMyaddressBean bean) {
        ed1.setText(bean.getUserName());
        ed2.setText(bean.getUserPhone());
        ed3.setText(bean.getProvince() + bean.getCity() + bean.getDistrict());
        ed4.setText(bean.getAddressDtl());
        swUserId = bean.getSwUserId();
        sheng = bean.getProvince();
        shi = bean.getCity();
        qu = bean.getDistrict();
        getWindow().getDecorView().clearFocus();
    }

    @Override
    public void OnMyaddressNodata(String s) {

    }

    @Override
    public void OnMyaddressFail(String s) {

    }

    @Override
    public void OnMyaddressupdateSuccess(String s) {
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_NAME, ed1.getText().toString().trim());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_TEL, ed2.getText().toString().trim());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS, ed3.getText().toString().trim());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_DETAIL, ed4.getText().toString().trim());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHENG, sheng);
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHI, shi);
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_QU, qu);
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_ALL, ed3.getText().toString().trim() + ed4.getText().toString().trim());
        if (intent_from == 0) {
            // 编辑资料
            Intent msgIntent = new Intent();
            msgIntent.setAction("UserInfoAct");
            if (TextUtils.isEmpty(content)) {
                content = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHENG) +
                        MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHI) +
                        MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_QU);
            }
            msgIntent.putExtra("UserInfoAct", content + ed4.getText().toString().trim());
            LocalBroadcastManagers.getInstance(this).sendBroadcast(msgIntent);
        } else if (intent_from == 1) {
            // 订单详情
//                Address1Bean bean = new Address1Bean();
//                bean.setText1(ed1.getText().toString().trim());
//                bean.setText2(ed2.getText().toString().trim());
//                bean.setText3(content + ed4.getText().toString().trim());
//                MmkvUtils.getInstance().set_common_json2(CommonUtils.MMKV_KEY1, bean);
            // 订单详情
//                Address1Bean bean1 =  MmkvUtils.getInstance(ShouhuodizhiAct.this).get_common_json(CommonUtils.MMKV_KEY1,Address1Bean.class);
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouhuodizhiAct");
//                intent.putExtra(CommonUtils.INTENT_FROM,1);
//                startActivity(intent);
        }
        finish();
    }

    @Override
    public void OnMyaddressupdateNodata(String s) {

    }

    @Override
    public void OnMyaddressupdateFail(String s) {

    }
}
