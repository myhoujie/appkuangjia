package com.example.geekapp2libsindex.classdetail;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.ClassScheduleAdapter;
import com.example.slbappcomm.base.SlbBaseFragment;
import com.fanli.biz3hxktdetail.bean.ScheduleBean;
import com.fanli.biz3hxktdetail.bean.ScheduleResponseBean;
import com.fanli.biz3hxktdetail.presenter.SchedulePresenter;
import com.fanli.biz3hxktdetail.view.SchdeuleView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;

import java.util.List;


public class ClassScheduleFragment extends SlbBaseFragment implements SchdeuleView {
    private RecyclerView scheduleRv;
    private ClassScheduleAdapter scheduleAdapter;
    //    private SmartRefreshLayout refreshLayout;
    // 分页
    private static final int PAGE_SIZE = 10;
    private int mNextRequestPage = 1;
    private String courseId;
    private SchedulePresenter presenter;
    private EmptyViewNew1 emptyView;

    public static ClassScheduleFragment newInstance(String courseId) {
        ClassScheduleFragment fragment = new ClassScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putString("courseId", courseId);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_teacher_session;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        refreshLayout = rootView.findViewById(R.id.smartRefreshLayout);
        scheduleRv = rootView.findViewById(R.id.scheduleRv);
        emptyView = rootView.findViewById(R.id.emptyview1);
        emptyView.bind(scheduleRv);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                getData();
            }
        });

        scheduleAdapter = new ClassScheduleAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        scheduleRv.setNestedScrollingEnabled(true);
        scheduleRv.setLayoutManager(linearLayoutManager);
        scheduleRv.setAdapter(scheduleAdapter);

//        refreshLayout.setEnableRefresh(false);
//        refreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
//            @Override
//            public void onLoadmore(RefreshLayout refreshlayout) {
//
//            }
//        });


        courseId = getArguments().getString("courseId");
        presenter = new SchedulePresenter();
        presenter.onCreate(this);

        scheduleAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData();
            }
        }, scheduleRv);

        getData();
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        super.onDestroy();
    }

    private void getData() {
        presenter.getCourseItemPage(courseId, mNextRequestPage, PAGE_SIZE);
    }

    @Override
    public void OnSuccess(ScheduleResponseBean courseResponseBean) {
//        refreshLayout.finishLoadmore();
        List<ScheduleBean> classSectionBeanList = courseResponseBean.getList();
        if (mNextRequestPage == 1) {
            if (classSectionBeanList.size() == 0) {
                emptyView.nodata();
            } else {
                scheduleAdapter.setNewData(classSectionBeanList);
            }
        } else {
            scheduleAdapter.addData(classSectionBeanList);
            emptyView.success();
        }
        mNextRequestPage++;
        if (classSectionBeanList.size() < PAGE_SIZE) {
//            refreshLayout.setEnableLoadmore(false);
            scheduleAdapter.setEnableLoadMore(false);
        } else {
            scheduleAdapter.loadMoreComplete();
        }
    }

    @Override
    public void OnNodata(String s) {
        if (mNextRequestPage == 1) {
            emptyView.nodata();
        } else {
//            scheduleAdapter.loadMoreFail();
            Toasty.normal(getContext(), s).show();
        }
    }

    @Override
    public void OnFail(String s) {
        if (mNextRequestPage == 1) {
            emptyView.errorNet();
        } else {
            scheduleAdapter.loadMoreFail();
//            Toasty.normal(getContext(), s).show();
        }
    }
}