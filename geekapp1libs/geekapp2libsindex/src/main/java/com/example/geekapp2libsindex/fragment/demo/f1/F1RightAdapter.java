package com.example.geekapp2libsindex.fragment.demo.f1;

import android.content.Context;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.fragment.one.OneBean1;

public class F1RightAdapter extends BaseQuickAdapter<OneBean1, BaseViewHolder> {
    private Context context;

    public F1RightAdapter(Context context) {
        super(R.layout.demo_f1_yulan_item1);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OneBean1 item) {
        TextView f1_tv1 = helper.itemView.findViewById(R.id.f1_tv1);
        f1_tv1.setText(item.getTab_name());
    }


}
