package com.example.geekapp2libsindex.fragment.one;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCategoryBean1;
import com.example.biz3hxktindex.bean.HGradecategoryBean;
import com.example.biz3hxktindex.bean.HGradecategoryBean1;
import com.example.biz3hxktindex.bean.HGradecategoryBean2;
import com.example.biz3hxktindex.presenter.HCategoryPresenter;
import com.example.biz3hxktindex.presenter.HGradecategoryPresenter;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.biz3hxktindex.view.HGradecategoryView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.BanjiPopup;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapterType;
import com.example.geekapp2libsindex.widget.CollapsingToolbarLayoutState;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerChangeAdapter;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerSlide;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.internal.CustomAdapt;

public class OneFragment1New extends SlbBaseIndexFragment implements HGradecategoryView, HCategoryView, CustomAdapt {

    private String ids;
    private LinearLayout ll_qb1;
    private TextView tv1;
    private BasePopupView loadingPopup;
    private TabLayout tablayoutMy;
    private ViewPagerSlide mViewPager;
    private String id1 = "1";//tablayout
    private String id2 = "2";
    private String id3 = "3";
    private String id4 = "4";
    private String current_id = id1;
    private EmptyViewNew1 emptyview;
    //    private EmptyViewNew1 emptyview1;
    private NestedScrollView scrollView1;
    private CoordinatorLayout ll_view1;
    private AppBarLayout app_bar;
    private CollapsingToolbarLayoutState state;
    //    private LinearLayout ll_view1;
    private int mOffset = 0;
    private int mScrollY = 0;
    private int scrolly = 0;
    private boolean is_dingbu = true;
    //
    private HGradecategoryPresenter hGradecategoryPresenter;
    private HCategoryPresenter hCategoryPresenter;
    private List<BanjiAdapterType> mList1;
    private boolean once;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
    }

    @Override
    public void onDestroyView() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        hCategoryPresenter.onDestory();
        hGradecategoryPresenter.onDestory();
        super.onDestroyView();
    }

    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 375;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one1;
    }

    @Override
    protected void setup(View rootView, Bundle savedInstanceState) {
        AutoSize.autoConvertDensity(getActivity(), 375, true);
        super.setup(rootView, savedInstanceState);
        ll_qb1 = rootView.findViewById(R.id.ll_qb1);
        tv1 = rootView.findViewById(R.id.tv1);
        // 初始化
//        ExpandViewRect.expandViewTouchDelegate(tv1, 30, 30, 30, 30);
        HGradecategoryBean2 banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class);
        if (banjiCommonbean == null || banjiCommonbean.getName() == null || TextUtils.equals(banjiCommonbean.getName(), BanjiPopup.TAG_CONTENT)) {
//            tv1.setText(BanjiPopup.TAG_CONTENT);
            tv1.setText("年级");
            //
            HGradecategoryBean2 banjiCommonbean1 = new HGradecategoryBean2("", BanjiPopup.TAG_CONTENT, 1, true);
            MmkvUtils.getInstance().set_common_json2(CommonUtils.BANJI_ID, banjiCommonbean1);
        } else {
            tv1.setText(banjiCommonbean.getName());
        }

        ll_qb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 年级
                hGradecategoryPresenter.get_gradecategory();
            }
        });
        tablayoutMy = rootView.findViewById(R.id.tab);
        mViewPager = rootView.findViewById(R.id.viewpager_my);
        emptyview = rootView.findViewById(R.id.emptyview1);
//        emptyview1 = rootView.findViewById(R.id.emptyview11);
//        scrollView1 = rootView.findViewById(R.id.scroll_view1);
        ll_view1 = rootView.findViewById(R.id.ll_view1);
        app_bar = rootView.findViewById(R.id.appbarlayout1);
        //
        initsmart();

        emptyview.notices("课程正在准备中哦…", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview.bind(mViewPager).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 主布局
                is_zhu = true;
                retryData();
            }
        });
//        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
//            @Override
//            public void retry() {
//                // 分布局
//                is_zhu = false;
//                retryData();
//            }
//        });
        //
//        init_viewpager();
        //
        hGradecategoryPresenter = new HGradecategoryPresenter();
        hGradecategoryPresenter.onCreate(this);
        hCategoryPresenter = new HCategoryPresenter();
        hCategoryPresenter.onCreate(this);
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        is_zhu = true;
        retryData();
    }

    private void retryData() {
//        if (is_zhu) {
//            if (emptyview == null) {
//                return;
//            }
//            HCategoryBean bean = MmkvUtils.getInstance().get_common_json("index1", HCategoryBean.class);
//            if (bean != null) {
//                set_list();
//            } else {
//                emptyview.loading();
//            }
//        } else {
////            refreshLayout1.autoLoadmore(3000);
////            emptyview1.loading();
//        }
        if (hCategoryPresenter == null) {
            return;
        }
//        hCategoryPresenter.get_category("subject", "0");
        HGradecategoryBean2 bean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class);
        if (bean != null && bean.getCode() != null) {
            hCategoryPresenter.get_category2(bean.getCode());
        }
    }

    private void initsmart() {
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                //计算进度百分比
                float percent = Float.valueOf(Math.abs(verticalOffset)) / Float.valueOf(appBarLayout.getTotalScrollRange());
                //根据百分比做你想做的
                if (verticalOffset == 0) {
                    if (state != CollapsingToolbarLayoutState.EXPANDED) {
                        state = CollapsingToolbarLayoutState.EXPANDED;//修改状态标记为展开
//                        collapsingToolbarLayout.setTitle("EXPANDED");//设置title为EXPANDED
                        MyLogUtil.e("--geekyun---", "1");
                        is_dingbu = true;
                    }
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (state != CollapsingToolbarLayoutState.COLLAPSED) {
//                        collapsingToolbarLayout.setTitle("");//设置title不显示
//                        iv_icon2.setVisibility(View.VISIBLE);//隐藏播放按钮
                        state = CollapsingToolbarLayoutState.COLLAPSED;//修改状态标记为折叠
                        MyLogUtil.e("--geekyun---", "2");
                        is_dingbu = false;
                    }
                } else {
                    if (state != CollapsingToolbarLayoutState.INTERNEDIATE) {
                        if (state == CollapsingToolbarLayoutState.COLLAPSED) {
//                            iv_icon2.setVisibility(View.GONE);//由折叠变为中间状态时隐藏播放按钮
                            MyLogUtil.e("--geekyun---", "3");
                        }
//                        collapsingToolbarLayout.setTitle("INTERNEDIATE");//设置title为INTERNEDIATE
                        state = CollapsingToolbarLayoutState.INTERNEDIATE;//修改状态标记为中间
                        MyLogUtil.e("--geekyun---", "4");
                        is_dingbu = false;
                    }
                }
            }
        });

//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
//        if (scrolly == 0) {
        if (is_dingbu) {
//            if (refreshLayout1 != null) {
//                refreshLayout1.autoRefresh();
//            }
            donetwork();
        } else {
//            scrollView1.smoothScrollTo(0, 0);
            CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) app_bar.getLayoutParams()).getBehavior();
            if (behavior instanceof AppBarLayout.Behavior) {
                AppBarLayout.Behavior appBarLayoutBehavior = (AppBarLayout.Behavior) behavior;
                int topAndBottomOffset = appBarLayoutBehavior.getTopAndBottomOffset();
                if (topAndBottomOffset != 0) {
                    appBarLayoutBehavior.setTopAndBottomOffset(0);
                }
            }
            //
            Intent msgIntent = new Intent();
            if (mDataTablayout != null && mDataTablayout.size() > 0 && TextUtils.equals(current_id, mDataTablayout.get(0).getTab_id())) {
                msgIntent.setAction("OneFragment1ToOneFragment11");
            } else {
                msgIntent.setAction("OneFragment12");
            }
            LocalBroadcastManagers.getInstance(getActivity()).sendBroadcast(msgIntent);
            is_dingbu = true;
        }

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    private void init_tablayout() {
        tablayoutMy.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                TabUtils.tabSelect(tablayoutMy, tab);
                String tag = (String) tab.getTag();
                current_id = tag;
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
                if (!once) {
                    once = true;
                    return;
                }
                if (TextUtils.isEmpty(tag)) {
                    return;
                }
                MyLogUtil.e("-进了几次tablayout--geekyun-1-", "进了几次tablayout");
                changeView(tablayoutMy.getSelectedTabPosition());
                showFragment(current_id, is_zhu);
            }
        });
        TabUtils.setIndicatorWidth(tablayoutMy, 40);
    }

    private void init_viewpager() {
        mViewPager.setScroll(true);
        //给ViewPager添加事件监听
        mViewPager.addOnPageChangeListener(new ViewPagerChangeAdapter() {
            @Override
            public void onPageSelected(int position) {
                // 此处不能做统计数据bufen

            }
        });
    }

    private List<OneBean1> mDataTablayout;


    private void setdata_viewpager(final List<OneBean1> list, int pos) {
        //
        // ToastUtils.showLong(count+",,,,"+pos);
        init_viewpager();

        // 使用HierarchyChangeListener的目的是防止在viewpager的itemview还没有准备好就去inflateFragment
        // 带来的问题
//        mViewPager.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
//            @Override
//            public void onChildViewAdded(View parent, View child) {
//                if (((ViewGroup) parent).getChildCount() < count) {
//                    return;
//                }
//
//                FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
////                SparseArrayCompat<Class<? extends Fragment>> array = new SparseArrayCompat<>();//一个版本模式bufen
//                IdentityHashMap<Integer,Class<? extends Fragment>> array = new IdentityHashMap<>();//一个版本模式bufen
//
//                int size = count;
//                Fragment item;
//                for (int i = 0; i < size; i++) {
//                    if (i == 0){
//                        array.put(new Integer(R.id.fragment_one_pager_index_0_0), OneFragment11.class);//模块1
//                    }else {
//                        array.put(new Integer(R.id.fragment_one_pager_index_1_0), OneFragment12.class);//模块2
//                    }
//                }
//                for(Map.Entry<Integer, Class<? extends Fragment>> entry : array.entrySet()) {
//                    System.out.print(entry.getKey() +"    ");
//                    System.out.println(entry.getValue());
//                    MyLogUtil.e("--geekyun--",entry.getValue()+"");
//                    item = FragmentHelper.newFragment(entry.getValue(), null);
//                    ft.replace(entry.getKey(), item, item.getClass().getName());
//                }
////                for (int i = 0; i < size; i++) {
////                    MyLogUtil.e("--geekyun--",array.(i)+"");
////                    item = FragmentHelper.newFragment(array.valueAt(i), null);
////                    ft.replace(array.keyAt(i), item, item.getClass().getName());
////                }
//                ft.commitAllowingStateLoss();
//            }
//
//            @Override
//            public void onChildViewRemoved(View parent, View child) {
//
//            }
//        });
        mViewPager.setOffscreenPageLimit(3);
//        mViewPager.setAdapter(new OnePagerAdapterNew(getActivity(),count));
        final ArrayList<Fragment> mFragments = new ArrayList<>();
        final FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        for (int i = 0; i < list.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", list.get(i).getTab_id());
            if (i == 0) {
                OneFragment11 fragment11 = FragmentHelper.newFragment(OneFragment11.class, bundle);
                mFragments.add(fragment11);

            } else {
                OneFragment12 fragment12 = FragmentHelper.newFragment(OneFragment12.class, bundle);
                mFragments.add(fragment12);

            }

        }
        mViewPager.setAdapter(new FragmentStatePagerAdapterCompat(getActivity().getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }
//
//            @Override
//            public Fragment instantiateItem(ViewGroup container, int position) {
//                Fragment fragment = (Fragment) super.instantiateItem(container,
//                        position);
//                fm.beginTransaction().show(fragment).commit();
//                return fragment;
//            }
//
//            @Override
//            public void destroyItem(ViewGroup container, int position, Object object) {
//                // super.destroyItem(container, position, object);
//                Fragment fragment = (Fragment) object;
//                fm.beginTransaction().hide(fragment).commit();
//            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

        });
        mViewPager.setCurrentItem(pos);//设置当前显示标签页为第一页

    }


    private void showFragment(String tag, boolean is_zhu) {
        //
        final Intent msgIntent = new Intent();
        if (tablayoutMy.getSelectedTabPosition() == 0) {
            callFragment(tag, OneFragment11.class.getName());
            msgIntent.setAction("OneFragment1ToOneFragment11_1");
            msgIntent.putExtra("OneFragment1ToOneFragment11_1", is_zhu);
            msgIntent.putExtra("OneFragment1ToOneFragment11_1", tag);
        } else {
            callFragment(tag, OneFragment12.class.getName());
            msgIntent.setAction("OneFragment12_1");
            msgIntent.putExtra("OneFragment12_1", is_zhu);
            msgIntent.putExtra("OneFragment12_1", tag);
        }
        //
//        LocalBroadcastManagers.getInstance(getActivity()).sendBroadcast(msgIntent);
    }


    /**
     * fragment间通讯bufen
     *
     * @param value 要传递的值
     * @param tag   要通知的fragment的tag
     */
    public void callFragment(Object value, String... tag) {
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        Fragment fragment;
        for (String item : tag) {
            if (TextUtils.isEmpty(item)) {
                continue;
            }

            fragment = fm.findFragmentByTag(item);
            if (fragment instanceof SlbBaseIndexFragment) {
                ((SlbBaseIndexFragment) fragment).call(value);
            }
        }
    }


    //手动设置ViewPager要显示的视图
    public void changeView(int desTab) {
        mViewPager.setCurrentItem(desTab, false);
    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    private boolean is_zhu;// ceshi


    @Override
    public void OnGradecategorySuccess(HGradecategoryBean hGradecategoryBean) {
        //
        mList1 = new ArrayList<>();
//        String gson_url = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).getJson(getActivity(), "jsonbean/banjibean.json");
//        BanjiModel bean_gson = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).JsonToObject(gson_url, BanjiModel.class);
        //
//        List<HGradecategoryBean1> bean1 = hGradecategoryBean.getGradeModels();
//        BanjiModel bean_gson = new BanjiModel();
//        List<BanjiModel2> bean_list1 = new ArrayList<>();
//        for (int i1 = 0; i1 < bean1.size(); i1++) {
//            BanjiModel2 banjiModel2 = new BanjiModel2();
//            banjiModel2.setText1(bean1.get(i1).getStageName());
//            List<BanjiCommonbean> bean_list2 = new ArrayList<>();
//            for (int i2 = 0; i2 < bean1.get(i1).getGrades().size(); i2++) {
//                BanjiCommonbean banjiCommonbean = new BanjiCommonbean();
//                banjiCommonbean.setText1(bean1.get(i1).getGrades().get(i2).getCode());
//                banjiCommonbean.setText2(bean1.get(i1).getGrades().get(i2).getName());
//                banjiCommonbean.setText3(false);
//                banjiCommonbean.setText4(bean1.get(i1).getGrades().get(i2).getEnable());
//                bean_list2.add(banjiCommonbean);
//            }
//            banjiModel2.setList(bean_list2);
//            bean_list1.add(banjiModel2);
//        }
//        bean_gson.setList(bean_list1);
        //
        for (int i = 0; i < hGradecategoryBean.getGradeModels().size(); i++) {
            HGradecategoryBean1 bean = hGradecategoryBean.getGradeModels().get(i);
            for (int j = 0; j < bean.getGrades().size(); j++) {
                if (j == 0) {
                    HGradecategoryBean2 banjiCommonbean = new HGradecategoryBean2();
                    banjiCommonbean.setName(bean.getStageName());
                    banjiCommonbean.setCode("-1");
                    banjiCommonbean.setEnable(-1);
                    banjiCommonbean.setText3(false);
                    mList1.add(new BanjiAdapterType(BanjiAdapterType.style1, banjiCommonbean));
                }
                mList1.add(new BanjiAdapterType(BanjiAdapterType.style2, bean.getGrades().get(j)));
            }
        }
        loadingPopup = new XPopup.Builder(getContext())
                .popupAnimation(PopupAnimation.ScaleAlphaFromLeftTop)
                .asCustom(new BanjiPopup(Objects.requireNonNull(getActivity()), new BanjiPopup.Ondismiss() {
                    @Override
                    public void OnDismiss(final String content) {
//                                loadingPopup.dismiss();
                        loadingPopup.postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                            if (loadingPopup.isShow())
                                loadingPopup.dismissWith(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv1.setText(TextUtils.equals(content, BanjiPopup.TAG_CONTENT) ? "年级" : content);
                                        is_zhu = true;
                                        retryData();
                                    }
                                });
                            }
                        }, 100);
                    }
                }, mList1))
                .show();
    }

    @Override
    public void OnGradecategoryNodata(String s) {

    }

    @Override
    public void OnGradecategoryFail(String s) {

    }

    @Override
    public void OnCategorySuccess(HCategoryBean hCategoryBean) {
        emptyview.success();
        MmkvUtils.getInstance().set_common_json2("index1", hCategoryBean);
        set_list();

    }

    @Override
    public void OnCategoryNodata(String s) {
        emptyview.nodata();
    }

    @Override
    public void OnCategoryFail(String s) {
        HCategoryBean bean = MmkvUtils.getInstance().get_common_json("index1", HCategoryBean.class);
        if (bean != null) {
            set_list();
        } else {
            emptyview.errorNet();
        }
    }

    private ViewSkeletonScreen skeletonScreen;

    private void set_list() {
        //
//        skeletonScreen = Skeleton.bind(mViewPager)
//                .load(R.layout.skeleton_view_layout)
//                .shimmer(true)
//                .angle(20)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .show();
        //setdata_tablayout();
        HCategoryBean bean = MmkvUtils.getInstance().get_common_json("index1", HCategoryBean.class);
        mDataTablayout = new ArrayList<>();
        for (HCategoryBean1 bean1 : bean.getList()) {
            mDataTablayout.add(new OneBean1(bean1.getCode(), bean1.getName(), false));
        }
//        mDataTablayout.add(new OneBean1(id1, "精选",false));
//        mDataTablayout.add(new OneBean1(id2, "数学1111",false));
//        mDataTablayout.add(new OneBean1(id3, "语文",false));
//        mDataTablayout.add(new OneBean1(id4, "英语222",false));
        //
        current_id = mDataTablayout.get(0).getTab_id();
        setdata_viewpager(mDataTablayout, OneFactory1New.DEFAULT_PAGE_INDEX);
        tablayoutMy.setupWithViewPager(mViewPager);
        tablayoutMy.removeAllTabs();
        for (OneBean1 item : mDataTablayout) {
            tablayoutMy.addTab(tablayoutMy.newTab()
                    .setTag(item.getTab_id()).setText(item.getTab_name()));
        }
        tablayoutMy.clearAnimation();
        tablayoutMy.setVisibility(View.VISIBLE);
        init_tablayout();
        showFragment(current_id, is_zhu);
        //
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                skeletonScreen.hide();
//            }
//        }, 1500);
    }
}
