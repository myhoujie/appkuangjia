package com.example.geekapp2libsindex.coupons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.example.biz3hxktindex.bean.HCouponsListBean;
import com.example.biz3hxktindex.bean.HCouponsListBean1;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.presenter.HCouponsListPresenter;
import com.example.biz3hxktindex.view.HCouponsListView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.common.SlbBaseOrderFragment2;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


public class CouponsCommonFragment extends SlbBaseOrderFragment2 implements HCouponsListView {

    private List<HLunbotuBean1> mListbanner1;
    private HCouponsListPresenter presenter1;
    //
    private CouponsCommon1Adapter mAdapter1;
    private List<HCouponsListBean1> mList1;
    private String tablayoutId;

    @Override
    public void call(Object value) {
        tablayoutId = (String) value;
        Toasty.normal(getActivity(), tablayoutId).show();
        //
//        retryData();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("id");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_coupons1_common2;
    }

    @Override
    protected void donetwork1() {
        emptyview1.loading();
        donetwork1_init(mAdapter1);
        //
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OrderAct");
        filter.addAction("OrderAct1");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
    }

    @Override
    protected void retryData1() {
        if (presenter1 == null) {
            mListbanner1 = new ArrayList<>();
            presenter1 = new HCouponsListPresenter();
            presenter1.onCreate(this);
        }
        String limit = PAGE_SIZE + "";
        String page = mNextRequestPage + "";
        String orderStatus = tablayoutId;
        presenter1.get_coupons_list(orderStatus, limit, page);
    }

    @Override
    protected void onclick1() {
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                donetwork1_init(mAdapter1);
                mAdapter1.setNewData(null);
                retryData1();
            }
        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                donetwork1_init(mAdapter1);
                mAdapter1.setNewData(null);
                retryData1();
            }
        });
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HCouponsListBean1 bean = (HCouponsListBean1) adapter.getData().get(position);
//                if (view.getId() == R.id.tv0) {
//                    return;
//                }
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun---", bean.getLiveCouponId());
                //订单详情
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderDetailActivity");
//                intent.putExtra("orderId", bean.getId());
//                startActivity(intent);
            }
        });
        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, final int position) {
                final HCouponsListBean1 bean = (HCouponsListBean1) adapter.getData().get(position);
                int i = view.getId();
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun1---", bean.getLiveCouponId());

            }
        });
        mAdapter1.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
//                is_zhu = false;
                loadMore();
                MyLogUtil.e("--geekyun--", "loadMore");
            }
        }, recyclerView1);
    }

    @Override
    protected void findview1() {

        //
        emptyview1.notices("你还没有优惠券哦", "没有网络了,检查一下吧", "正在加载....", "");
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        recyclerView1.addItemDecoration(new SpaceItemDecoration(getContext()));
        mList1 = new ArrayList<>();
        mAdapter1 = new CouponsCommon1Adapter(R.layout.activity_hxkt_coupons11_item);
        recyclerView1.setAdapter(mAdapter1);
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        if (presenter1 != null) {
            presenter1.onDestory();
        }
        MyLogUtil.e("sssssss", "onDestroy");
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen
        MyLogUtil.e("sssssss", "onResume");
        super.onResume();
    }

    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OrderAct".equals(intent.getAction())) {
                    OrderCommonItem2Bean bean = intent.getParcelableExtra("OrderAct1");
//                    MyLogUtil.e("--geekyun-updateUI-tttttt", bean.getId());
                    if (bean != null) {
                        tablayoutId = bean.getId();
                        if (TextUtils.equals(tablayoutId, "0")) {
                            // 刷新
                            donetwork1_init(mAdapter1);
                            mAdapter1.setNewData(null);
                            retryData1();
                        }
                        if (TextUtils.equals(tablayoutId, "1")) {
                            // 滚动到顶部
                            getCate(bean);
                        }
                    } else {
                        // 其他页面通知更新订单状态
                        String id = intent.getStringExtra("id");
                        if (!TextUtils.isEmpty(id)) {
//                        emptyview1.loading();
                            donetwork1_init(mAdapter1);
                            mAdapter1.setNewData(null);
                            retryData1();
                        }
                    }
//                    int pos = 0;
//                    if (!TextUtils.isEmpty(id)) {
//                        // 更新订单状态
//                        List<HMyorderBean1> mlist = mAdapter1.getData();
//                        for (int i = 0; i < mlist.size(); i++) {
//                            if (TextUtils.equals(mlist.get(i).getId(), id)) {
//                                pos = i;
//                                mlist.get(pos).setButtonCancel("");
//                                mlist.get(pos).setButtonPay("");
//                            }
//                        }
//                        mAdapter1.setNewData(mlist);
//                        mAdapter1.notifyItemChanged(pos);
                }
            } catch (
                    Exception e) {
            }
        }
    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    private ViewSkeletonScreen skeletonScreen;

    @Override
    public void OnCouponsListSuccess(HCouponsListBean hCouponsListBean) {
        OnOrderSuccess();
        mList1 = new ArrayList<>();
        mList1 = hCouponsListBean.getList();
        if (mNextRequestPage == 1) {
            setData(mAdapter1, true, mList1);
            if (mList1.size() == 0) {
                emptyview1.nodata();
            }
        } else {
            setData(mAdapter1, false, mList1);
        }
//        //
//        skeletonScreen = Skeleton.bind(refreshLayout1)
//                .load(R.layout.skeleton_view_layout)
//                .shimmer(true)
//                .angle(20)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .show();
//        //
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                skeletonScreen.hide();
//            }
//        }, 1500);
    }

    @Override
    public void OnCouponsListNodata(String s) {
        OnOrderNodata();
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            emptyview1.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

    @Override
    public void OnCouponsListFail(String s) {
        OnOrderFail();
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            // 根据需求修改bufen
            mAdapter1.setNewData(null);
            emptyview1.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }
}
