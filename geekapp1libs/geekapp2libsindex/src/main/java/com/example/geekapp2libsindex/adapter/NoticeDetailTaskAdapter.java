package com.example.geekapp2libsindex.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.Task;

public class NoticeDetailTaskAdapter extends BaseQuickAdapter<Task, BaseViewHolder> {

    public NoticeDetailTaskAdapter() {
        super(R.layout.adapter_notice_task);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final Task item) {
        TextView numTv = helper.itemView.findViewById(R.id.numTv);
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView contentTv = helper.itemView.findViewById(R.id.contentTv);
        TextView btnTv = helper.itemView.findViewById(R.id.btnTv);
        numTv.setText(item.getNum() + "");
        titleTv.setText(item.getTitle());
        contentTv.setText(item.getContent());
        if (helper.getAdapterPosition() == 0) {
            btnTv.setText(item.isEnable() ? "去完成" : "已完成");
        } else {
            btnTv.setText(item.isEnable() ? "去查看" : "已完成");
        }
        btnTv.setEnabled(item.isEnable());
        helper.addOnClickListener(R.id.btnTv);
    }
}
