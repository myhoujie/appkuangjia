package com.example.geekapp2libsindex.common;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import org.greenrobot.eventbus.EventBus;

public abstract class SlbBaseOrderFragment extends SlbBaseIndexFragment {

    //
    protected SmartRefreshLayout refreshLayout1;
    protected ClassicsHeader smartHeader1;
    //
    protected EmptyViewNew1 emptyview1;
    protected LinearLayout ll_refresh1;
    protected NestedScrollView scrollView1;
    protected XRecyclerView recyclerView1;
    //    private CommonClassAdapter mAdapter1;
//    private List<CommonClassAdapterType> mList1;
    //
    protected String current_id;
    protected int mOffset = 0;
    protected int mScrollY = 0;
    protected int scrolly = 0;

    public int getScrolly() {
        return scrolly;
    }

    public void setScrolly(int scrolly) {
        this.scrolly = scrolly;
    }


    @Override
    public void net_con_none() {
        emptyview1.errorNet();
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        findview(rootView);
        onclick();
//        donetwork();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }


    @Override
    public void loadData() {
        super.loadData();
        donetwork();
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
//        tvCenterContent.setText("我的订单");
        emptyview1.loading();
//        is_zhu = true;
        donetwork1();
        retryData();
//        current_id = id1;
    }

    protected abstract void donetwork1();

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
        // ceshi
//        ((ShouyeActivity)getActivity()).callFragment("传值3",OneFragment3.class.getName());
        retryData1();
    }

    protected abstract void retryData1();

    private void onclick() {
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                retryData();
            }
        });
        emptyview1.bind(scrollView1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                retryData();
            }
        });
        //
        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            private int lastScrollY = 0;
            private int h = DensityUtil.dp2px(170);

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                scrolly = scrollY;
                // 传值给act
                EventBus.getDefault().post(new Integer(scrolly));
                if (lastScrollY < h) {
                    scrollY = Math.min(h, scrollY);
                    mScrollY = scrollY > h ? h : scrollY;
                }
                lastScrollY = scrollY;
            }
        });
        onclick1();

    }

    /**
     * 底部点击
     */
    protected void getCate(OrderCommonItem2Bean tablayoutId) {

        if (!tablayoutId.isText2()) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        if (scrolly == 0) {
            if (refreshLayout1 != null) {
                refreshLayout1.autoRefresh();
            }
        } else {
//            // 通知act可以主页面更新操作了
//            OrderCommonItem2Bean bean = new OrderCommonItem2Bean("0", current_id, false);
//            EventBus.getDefault().post(bean);
            scrollView1.smoothScrollTo(0, 0);
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    protected void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    protected abstract void onclick1();

    private void findview(View rootView) {
        ll_refresh1 = rootView.findViewById(R.id.ll_refresh1_order);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);
        smartHeader1 = rootView.findViewById(R.id.smart_header1_order);
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
        scrollView1 = rootView.findViewById(R.id.scroll_view1_order);
        recyclerView1 = rootView.findViewById(R.id.recycler_view1_order);
        //
        //
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView1.setFocusable(false);
        findview1();

//        emptyview1.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");
//        recyclerView1.setLayoutManager(new GridLayoutManager(SlbBaseOrderAct.this, 1, RecyclerView.VERTICAL, false));
//        mList1 = new ArrayList<>();
//        mAdapter1 = new CommonClassAdapter(mList1);
//        recyclerView1.setAdapter(mAdapter1);
    }

    protected abstract void findview1();

    protected void OnOrderSuccess() {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    protected void OnOrderNodata() {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    protected void OnOrderFail() {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

}
