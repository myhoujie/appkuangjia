package com.example.geekapp2libsindex.classtest;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HExerciseResponseBean;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.bean.HZuoyeinfoBean;
import com.example.biz3hxktindex.bean.PicBean;
import com.example.biz3hxktindex.presenter.HExerciseInfoPresenter;
import com.example.biz3hxktindex.presenter.HTouxiangsPresenter;
import com.example.biz3hxktindex.presenter.HUploadurlsPresenter;
import com.example.biz3hxktindex.view.HExerciseView;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.example.biz3hxktindex.view.HUploadurlsView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.ExerciseAdapter;
import com.example.geekapp2libsindex.domain.HZuoyeinfoWrapperBean;
import com.example.geekapp2libsindex.domain.UnionPicBean;
import com.example.geekapp2libsindex.pop.TakePhotoPopup;
import com.example.geekapp2libsindex.pop.TwoBtnPop;
import com.example.geekapp2libsindex.widget.PicHelper;
import com.example.geekapp2libsindex.widget.RVMoveToPosition;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.permissions.RxPermissions;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 1.4.1版本：已批改/重新提交
 */
public class ClassTest3Act extends SlbBaseActivity implements View.OnClickListener, HExerciseView, HTouxiangsView, HUploadurlsView {
    private ImageView ivBack;
    private TextView topRightTv;
    private TextView topTipTv;
    private TextView tvCenterContent;
    private RecyclerView exerciseRv;
    private ExerciseAdapter mAdapter;
    private HExerciseInfoPresenter presenter;
    private String topRightFlag = "0";//0：不显示  1：重新上传  2：提交
    private String itemId;
    private int errorCount;//错题数

    private BasePopupView loadingPopup;
    private HTouxiangsPresenter uploadToAliPresenter;
    private HUploadurlsPresenter uploadToServerPresenter;
    private EmptyViewNew1 emptyView;
    private BasePopupView takePhotoPopup;

    @Override
    protected void onDestroy() {
        presenter.onDestory();
        uploadToAliPresenter.onDestory();
        uploadToServerPresenter.onDestory();
        PicHelper.getInstance().release();
        releasePop(loadingPopup);
        releasePop(submitConfirmPopView);
        releasePop(takePhotoPopup);
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_classtest3;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void findview() {
        itemId = getIntent().getStringExtra("id");
        topRightTv = findViewById(R.id.tv_right);
//        topRightTv.setTextColor(Color.parseColor("#519AF4"));
//        topRightTv.setTextSize(DisplayUtil.sp2px(this, 14));
        topTipTv = findViewById(R.id.topTipTv);
        ivBack = findViewById(R.id.iv_back1_order);
        tvCenterContent = findViewById(R.id.tv_center_content);
        tvCenterContent.setText("课后练习");

        View sv = findViewById(R.id.sv);
        emptyView = new EmptyViewNew1(this);
        emptyView.attach(sv);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");

        exerciseRv = findViewById(R.id.exerciseRv);
        exerciseRv.setHasFixedSize(true);
        exerciseRv.setNestedScrollingEnabled(false);
        exerciseRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        mAdapter = new ExerciseAdapter(R.layout.adapter_classtest3, this);
        exerciseRv.setAdapter(mAdapter);

        PicHelper.getInstance().addOnPicOperateListener(mAdapter);

        presenter = new HExerciseInfoPresenter();
        presenter.onCreate(this);

        uploadToAliPresenter = new HTouxiangsPresenter();
        uploadToAliPresenter.onCreate(this);

        uploadToServerPresenter = new HUploadurlsPresenter();
        uploadToServerPresenter.onCreate(this);
    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        topRightTv.setOnClickListener(this);
        mAdapter.setOnListener(new ExerciseAdapter.OnListener() {
            @Override
            public void onInsertToFirst() {
                exerciseRv.post(new Runnable() {
                    @Override
                    public void run() {
                        RVMoveToPosition.createRVMoveToPosition(exerciseRv, 0).smoothMoveToPosition();
                    }
                });
            }
        });
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                donetwork();
            }
        });
    }

    private void donetwork() {
        emptyView.loading();
        presenter.get_exerciseInfo(itemId);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.tv_right) {
            // 1：重新上传  2：提交
            switch (topRightFlag) {
                case "1":
                    showTakePhotoPopView();
//                    PicHelper.getInstance().toSelectPic(ClassTest3Act.this);
                    break;
                case "2":
                    showSubmitConfirmDialog();
                    break;
            }
        }
    }

    private void setTopRightTvStatus() {
        if (TextUtils.equals(topRightFlag, "0")) {
            topRightTv.setVisibility(View.GONE);
        } else if (TextUtils.equals(topRightFlag, "1")) {
            topRightTv.setText("重新上传");
            topRightTv.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals(topRightFlag, "2")) {
            topRightTv.setText("提交");
            topRightTv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnExerciseInfoSuccess(HExerciseResponseBean hExerciseResponseBean) {
        emptyView.success();
        topRightFlag = hExerciseResponseBean.getSecondHandFlag();
        setTopRightTvStatus();
        //练习状态(0-未布置,1-未提交,2-待批改,3-已批改 )
        switch (hExerciseResponseBean.getExerciseStatus()) {
            case "2":
                topTipTv.setText(getResources().getString(R.string.slb_classtest_tip2));
                topTipTv.setVisibility(View.VISIBLE);
                break;
            case "3":
                if (TextUtils.equals(topRightFlag, "0")) {
                    topTipTv.setVisibility(View.GONE);
                } else {
                    topTipTv.setText(getResources().getString(R.string.slb_classtest_tip3));
                    topTipTv.setVisibility(View.VISIBLE);
                }
                break;
        }
        if (!ListUtil.isEmpty(hExerciseResponseBean.getExercises())) {
            List<HZuoyeinfoWrapperBean> hZuoyeinfoWrapperBeanList = new ArrayList<>();
            for (HZuoyeinfoBean bean : hExerciseResponseBean.getExercises()) {
                HZuoyeinfoWrapperBean wrapperBean = createWrapperBean(bean);
                System.out.println("wrapperBean:" + wrapperBean);
                List<UnionPicBean> unionPicBeanList = new ArrayList<>();
                for (PicBean picbean : bean.getPicModels()) {
                    UnionPicBean unionPicBean = new UnionPicBean();
                    unionPicBean.setPicUrl(picbean.getPicUrl());
                    unionPicBean.setId(picbean.getId());
                    unionPicBean.setCorrectFlag(picbean.getCorrectFlag());
                    unionPicBeanList.add(unionPicBean);
                }
                wrapperBean.setUnionPicBeanList(unionPicBeanList);
                hZuoyeinfoWrapperBeanList.add(wrapperBean);
            }
            if (hZuoyeinfoWrapperBeanList.size() > 1) {
                Collections.reverse(hZuoyeinfoWrapperBeanList);
            }
            //统计错题数
            errorCount = 0;
            for (PicBean picBean : hZuoyeinfoWrapperBeanList.get(0).getPicModels()) {
                if (!TextUtils.equals(picBean.getCorrectFlag(), "1")) {
                    ++errorCount;
                }
            }
            mAdapter.setNewData(hZuoyeinfoWrapperBeanList);
        } else {
            mAdapter.setNewData(null);
        }

        if (exerciseRv.getScrollY() != 0) {
            exerciseRv.scrollTo(0, 0);
        }
    }

    private HZuoyeinfoWrapperBean createWrapperBean(HZuoyeinfoBean bean) {
        HZuoyeinfoWrapperBean ret = new HZuoyeinfoWrapperBean();
        ret.setExerciseStatus(bean.getExerciseStatus());
        ret.setExerciseSuggest(bean.getExerciseSuggest());
        ret.setId(bean.getId());
        ret.setLiveCourseId(bean.getLiveCourseId());
        ret.setLiveCourseItemId(bean.getLiveCourseItemId());
        ret.setSwUserId(bean.getSwUserId());
        ret.setPics(bean.getPics());
        ret.setTeachers(bean.getTeachers());
        ret.setPicModels(bean.getPicModels());
        return ret;
    }

    @Override
    public void OnExerciseInfoNodata(String s) {
//        Toasty.normal(this, s).show();
        emptyView.nodata();
    }

    @Override
    public void OnExerciseInfoFail(String s) {
        emptyView.errorNet();
//        Toasty.normal(this, s).show();
    }

    // 提交
    private void submit() {
        List<LocalMedia> selectList = PicHelper.getInstance().getSelectedList();
        List<File> list = new ArrayList<>();
        for (int i = 0; i < selectList.size(); i++) {
            if (selectList.get(i).isCompressed()) {
                File file = new File(selectList.get(i).getCompressPath());
                list.add(file);
            } else {
                File file = new File(selectList.get(i).getPath());
                list.add(file);
            }
        }
        loadingPopup = new XPopup.Builder(this)
                .dismissOnBackPressed(true)
                .dismissOnTouchOutside(false)
                .asLoading("上传中...")
                .show();
        uploadToAliPresenter.get_touxiang3(list);
    }

    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    PicHelper.getInstance().getBackSelectedList(data);
                    if (!ListUtil.isEmpty(PictureSelector.obtainMultipleResult(data))) {
                        topRightFlag = "2";
                        setTopRightTvStatus();
                    }
                    break;
            }
        }
    }

    private BasePopupView submitConfirmPopView;

    private void showSubmitConfirmDialog() {
        List<LocalMedia> selectList = PicHelper.getInstance().getSelectedList();
        if (ListUtil.isEmpty(selectList)) {
            ToastUtils.showLong("请先上传至少一张图片");
            return;
        }
        submitConfirmPopView = new XPopup.Builder(this)
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .asCustom(new TwoBtnPop(this, "提示", errorCount > selectList.size() ? "还有错题未修改，是否确定提交作业？" : "确定提交作业吗？", "取消", "确定", new TwoBtnPop.OnBtnListener() {
                    @Override
                    public void onLeft() {
                        submitConfirmPopView.dismiss();
                    }

                    @Override
                    public void onRight() {
                        submitConfirmPopView.dismiss();
                        submit();
                    }
                })).show();
    }

    //上传云端成功
    @Override
    public void OnTouxiangsSuccess(HTouxiangBean1 hTouxiangBean1) {
        String imgStr = hTouxiangBean1.getUrls();
        List<String> list = new ArrayList<>();
        if (!TextUtils.isEmpty(imgStr)) {
            String[] str = imgStr.split(",");
            list = Arrays.asList(str);
        }
        //上传至服务器
        uploadToServerPresenter.get_upload_img_urls(itemId, list);
    }

    @Override
    public void OnTouxiangsNodata(String s) {
        loadingPopup.dismiss();
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnTouxiangsFail(String s) {
        loadingPopup.dismiss();
        Toasty.normal(this, s).show();
    }

    //提交到服务器成功
    @Override
    public void OnUploadurlsSuccess(String s) {
        clearLocalPics();
        donetwork();//刷新
        loadingPopup.dismiss();
    }

    @Override
    public void OnUploadurlsNodata(String s) {
        loadingPopup.dismiss();
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnUploadurlsFail(String s) {
        loadingPopup.dismiss();
        Toasty.normal(this, s).show();
    }

    private void releasePop(BasePopupView popupView) {
        if (popupView != null && popupView.isShow()) {
            popupView.dismiss();
        }
        popupView = null;
    }

    private void clearLocalPics() {
        // 清空图片缓存，包括裁剪、压缩后的图片 注意:必须要在上传完成后调用 必须要获取权限
        RxPermissions permissions = new RxPermissions(this);
        permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PictureFileUtils.deleteCacheDirFile(ClassTest3Act.this);
                } else {
                    Toast.makeText(ClassTest3Act.this,
                            getString(R.string.picture_jurisdiction), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void showTakePhotoPopView() {
        takePhotoPopup = new XPopup.Builder(this)
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .asCustom(new TakePhotoPopup(this)/*.enableDrag(false)*/)
                .show();
    }
}
