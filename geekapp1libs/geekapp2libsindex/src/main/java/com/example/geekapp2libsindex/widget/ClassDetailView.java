package com.example.geekapp2libsindex.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.OrderBean;

public class ClassDetailView extends FrameLayout {
    private Context context;
    private TextView titleTv;
    private TextView dateTv;
    private TextView teacherTv;
    private TextView priceTv;
    private TextView nPriceTv;
    private TextView oPriceTv;
    private boolean isShowOPrice = true;
    private TextView tipTv;

    public ClassDetailView(@NonNull Context context) {
        this(context, null);
    }

    public ClassDetailView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClassDetailView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.layout_classdetail, this, true);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ClassDetailView);
        if (attrs != null && typedArray != null) {
            isShowOPrice = typedArray.getBoolean(R.styleable.ClassDetailView_showOPrice, true);
            typedArray.recycle();
        }
        tipTv = findViewById(R.id.tipTv);
        titleTv = findViewById(R.id.titleTv);
        dateTv = findViewById(R.id.dateTv);
        teacherTv = findViewById(R.id.teacherTv);
        priceTv = findViewById(R.id.priceTv);
        nPriceTv = findViewById(R.id.nPriceTv);
        oPriceTv = findViewById(R.id.oPriceTv);

        oPriceTv.getPaint().setAntiAlias(true);//抗锯齿
        oPriceTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);  // 设置中划线并加清晰

        setIsShowOPrice();
    }

    public void setTipColor999() {
        tipTv.setTextColor(Color.parseColor("#999999"));
    }

    public void setData(CourseBean courseBean) {
        //标题
        String title = "";
        boolean hasSubject = courseBean.getSubjectNames() != null && courseBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : courseBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + courseBean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < courseBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(context, TextUtils.equals(courseBean.getClassAttribute(), "experience") ? Color.parseColor("#666666") : Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        if (TextUtils.equals("0", courseBean.getChargeFlag())) {//免费
            nPriceTv.setText("免费");
//            priceTv.setText("应付：¥0");
        } else {
            SpannableString nPriceSpannableString = new SpannableString("¥" + courseBean.getPricePayable());
            nPriceSpannableString.setSpan(new AbsoluteSizeSpan(14, true), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            nPriceTv.setText(nPriceSpannableString);
        }
//        String price = TextUtils.equals("0", courseBean.getChargeFlag()) ? "0" : (TextUtils.equals(courseBean.getOrderStatus(), "unpaid") ? courseBean.getLiveOrderAmount() : courseBean.getPricePayable());
//        price = "应付：¥" + price;
        String price = "应付：¥" + (TextUtils.equals("0", courseBean.getChargeFlag()) ? "0" : courseBean.getPricePayable());
        SpannableString priceSpannableString = new SpannableString(price);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(14, true), 3, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(18, true), 4, price.length() - 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceSpannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_333333)), 3, price.length() - 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceSpannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 3, price.length() - 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceTv.setText(priceSpannableString);

        if (Double.valueOf(courseBean.getPriceMarked()) == 0) {
            oPriceTv.setVisibility(View.GONE);
        } else {
            oPriceTv.setText("¥" + courseBean.getPriceMarked());
            oPriceTv.setVisibility(View.VISIBLE);
        }

        dateTv.setText(courseBean.getCourseTimeRange());
        teacherTv.setText("授课老师：" + courseBean.getLecturer().getNikeName());
//        oPriceTv.setText("¥" + courseBean.getPriceMarked());
    }

    public void setData(OrderBean orderBean) {
        setShowOPrice(false);
        //标题
        String title = "";
        boolean hasSubject = orderBean.getSubjectNames() != null && orderBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : orderBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + orderBean.getOrderName());
        if (hasSubject) {
            for (int i = 0; i < orderBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(context, Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        String price = "应付：¥" + orderBean.getAmountPayable();
        SpannableString priceSpannableString = new SpannableString(price);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(14, true), 3, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(18, true), 4, price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceSpannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_333333)), 3, price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceSpannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 3, price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        priceTv.setText(priceSpannableString);

        dateTv.setText(orderBean.getCourseTimeRange());
        teacherTv.setText(orderBean.getLecturerName());
    }

    private void setIsShowOPrice() {
        if (isShowOPrice) {
            priceTv.setVisibility(GONE);
            nPriceTv.setVisibility(VISIBLE);
            oPriceTv.setVisibility(VISIBLE);
        } else {
            priceTv.setVisibility(VISIBLE);
            nPriceTv.setVisibility(GONE);
            oPriceTv.setVisibility(GONE);
        }
    }

    public void setShowOPrice(boolean showOPrice) {
        isShowOPrice = showOPrice;
        setIsShowOPrice();
    }
}
