package com.example.geekapp2libsindex.domain;

import com.example.biz3hxktindex.bean.HZuoyeinfoBean;

import java.util.List;

public class HZuoyeinfoWrapperBean extends HZuoyeinfoBean {
    private List<UnionPicBean> unionPicBeanList;

    public List<UnionPicBean> getUnionPicBeanList() {
        return unionPicBeanList;
    }

    public void setUnionPicBeanList(List<UnionPicBean> unionPicBeanList) {
        this.unionPicBeanList = unionPicBeanList;
    }

    @Override
    public String toString() {
        return this.getId();
    }
}
