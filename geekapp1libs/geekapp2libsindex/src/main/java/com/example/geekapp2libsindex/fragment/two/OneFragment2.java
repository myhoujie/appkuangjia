package com.example.geekapp2libsindex.fragment.two;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCategoryBean1;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.bean.HStudycategoryBean;
import com.example.biz3hxktindex.bean.HStudycategoryBean1;
import com.example.biz3hxktindex.presenter.HCategoryPresenter;
import com.example.biz3hxktindex.presenter.HStudycategoryPresenter;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.biz3hxktindex.view.HStudycategoryView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.Studydapter;
import com.example.geekapp2libsindex.adapter.Tablayoutdapter;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;

import java.util.ArrayList;
import java.util.List;


public class OneFragment2 extends SlbBaseIndexFragment implements HCategoryView, HStudycategoryView {

    private boolean is_zhu;// ceshi
    private String id1 = "1";//tablayout
    private String id2 = "2";
    private String id3 = "3";
    private String id4 = "4";
    private String current_id;
    private String ids;
    //
    private TabLayout tablayoutMy;
    private XRecyclerView recycler_view1_order11;
    private Tablayoutdapter mAdapter11;
    private SmartRefreshLayout refreshLayout1;
    private ClassicsHeader smartHeader1;
    private EmptyViewNew1 emptyview;
    private TextView tv_mycourse1;

    // 分页
    private static final int PAGE_SIZE = 10;
    private int mNextRequestPage = 1;
    private int mPage;
    private static boolean mFirstPageNoMore;
    private static boolean mFirstError = true;
    private EmptyViewNew1 emptyview1;
    private LinearLayout ll_refresh1;
    //    private NestedScrollView scrollView1;
    private XRecyclerView recyclerView1;
    private Studydapter mAdapter1;
    private List<HStudycategoryBean1> mList1;

    private LinearLayout ll1;
    private LinearLayout ll2;

    //
    private List<HLunbotuBean1> mListbanner1;
    private HCategoryPresenter presenter1;
    private HStudycategoryPresenter presenter2;

    private int mOffset = 0;
    private int mScrollY = 0;
    private int scrolly = 0;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        set_re_data();
    }

    private MessageReceiverIndex mMessageReceiver;


    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OneFragment2".equals(intent.getAction())) {
                    recyclerView1.scrollToPosition(0);
                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        presenter1.onDestory();
        presenter2.onDestory();
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one2;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        tv_mycourse1 = rootView.findViewById(R.id.tv_mycourse1);
        tv_mycourse1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent("com.BaseRecActDemo99.act"));
            }
        });
        emptyview = rootView.findViewById(R.id.emptyview1_order);
        ll_refresh1 = rootView.findViewById(R.id.ll_refresh1_order);
//        tablayoutMy = rootView.findViewById(R.id.tab1_order);
        recycler_view1_order11 = rootView.findViewById(R.id.recycler_view1_order11);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);
        smartHeader1 = rootView.findViewById(R.id.smart_header1_order);
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
//        scrollView1 = rootView.findViewById(R.id.scroll_view1_order);
        recyclerView1 = rootView.findViewById(R.id.recycler_view1_order);
        onclick();
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OneFragment2");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
        //
        mListbanner1 = new ArrayList<>();
        presenter1 = new HCategoryPresenter();
        presenter1.onCreate(this);
        presenter2 = new HStudycategoryPresenter();
        presenter2.onCreate(this);
        donetwork();
    }

    private void onclick() {
        //
        recycler_view1_order11.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        mList1 = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recycler_view1_order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                MyLogUtil.e("---geekyun----", current_id);
                is_zhu = false;
                emptyview1.loading();
                mNextRequestPage = 1;
                mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
                set_re_data();
            }
        });
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                mAdapter1.setNewData(null);
                donetwork_ci();
            }
        });
        //
//        emptyview1.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");
        emptyview.bind(ll_refresh1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 主布局
                donetwork();
            }
        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                donetwork_ci();
            }
        });
        //
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView1.setFocusable(false);
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        mList1 = new ArrayList<>();
        mAdapter1 = new Studydapter();
        recyclerView1.setAdapter(mAdapter1);
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HCategoryBean1 bean1 = (HCategoryBean1) adapter.getData().get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                intent.putExtra("courseId", bean1.getCode());
                startActivity(intent);
            }
        });
        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                HCategoryBean1 bean1 = (HCategoryBean1) adapter.getData().get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                intent.putExtra("courseId", bean1.getCode());
                startActivity(intent);
            }
        });
        mAdapter1.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                is_zhu = false;
                loadMore();
                MyLogUtil.e("--geekyun--", "loadMore");
            }
        }, recyclerView1);
        //
//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
        //
        recyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                MyLogUtil.e("ssssssssssss", "" + newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                dx > 0 时为手指向左滚动,列表滚动显示右面的内容
//                dx < 0 时为手指向右滚动,列表滚动显示左面的内容
//                dy > 0 时为手指向上滚动,列表滚动显示下面的内容
//                dy < 0 时为手指向下滚动,列表滚动显示上面的内容
                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                mLastItemVisible = layoutManager.findLastVisibleItemPosition();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            }
        });
    }

    private int mLastItemVisible;
    private int firstVisibleItemPosition;

    private int getScollYDistance() {
        GridLayoutManager layoutManager = (GridLayoutManager) recyclerView1.getLayoutManager();
        int position = layoutManager.findFirstVisibleItemPosition();
        View firstVisiableChildView = layoutManager.findViewByPosition(position);
        int itemHeight = firstVisiableChildView.getHeight();
        return (position) * itemHeight - firstVisiableChildView.getTop();
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        is_zhu = true;
        setDatas_init();

    }

    private void donetwork_ci() {
        is_zhu = false;
        setDatas();
    }

    private void setData(boolean isRefresh, List data) {
        mNextRequestPage++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            mAdapter1.setNewData(data);
            recyclerView1.scrollToPosition(0);
        } else {
            if (size > 0) {
                mAdapter1.addData(data);
            }
        }
//        if (mAdapter1.getData().size() > 12) {
//            mAdapter1.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了12");
//            return;
//        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            mAdapter1.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了");
        } else {
            mAdapter1.loadMoreComplete();
        }

    }

    /**
     * 加载更多bufen
     */
    private void loadMore() {
        set_re_data();
    }

    /**
     * 加载第一页数据 主布局
     */
    private void setDatas_init() {
        emptyview.loading();
        mNextRequestPage = 1;
        mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        set_re_data_init();

    }

    /**
     * 加载第一页数据 分布局
     */
    private void setDatas() {
        emptyview1.loading();
        mNextRequestPage = 1;
        mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        set_re_data();

    }

    private void set_re_data_init() {
        presenter1.get_category("courseSchedule", "1");

    }

    private void set_re_data() {
//        current_id = (String) tablayoutMy.getTabAt(0).getTag();
        String limit = PAGE_SIZE + "";
        String page = mNextRequestPage + "";
        String sheduleStatus = "courseSchedule";
        presenter2.get_studycategorylist(current_id, limit, page, current_id);

    }

    private boolean is_first;

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        MyLogUtil.e("ssssssssssss", "firstVisibleItemPosition->" + firstVisibleItemPosition);
        if (firstVisibleItemPosition == 0) {
//            if (refreshLayout1 != null) {
//                refreshLayout1.autoRefresh();
//            }
            donetwork();
        } else {
            recyclerView1.scrollToPosition(0);
            MyLogUtil.e("ssssssssssss", "smoothScrollBy");
        }
        // fragment传值
//        OrderCommonItem2Bean bean = new OrderCommonItem2Bean(2, current_id, true);
//        EventBus.getDefault().post(bean);
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 业务逻辑部分
     */

    private boolean once;

    private void init_tablayout(List<OneBean1> mDataTablayout) {
//        mDataTablayout = new ArrayList<>();
//        mDataTablayout.add(new OneBean1(id1, "全部"));
//        mDataTablayout.add(new OneBean1(id2, "未开始"));
//        mDataTablayout.add(new OneBean1(id3, "学习中"));
//        mDataTablayout.add(new OneBean1(id4, "已结束"));
//        tablayoutMy.setupWithViewPager(mViewPager);
        tablayoutMy.removeAllTabs();
        for (OneBean1 item : mDataTablayout) {
            tablayoutMy.addTab(tablayoutMy.newTab()
                    .setTag(item.getTab_id()).setText(item.getTab_name()));
        }
        TabUtils.setIndicatorWidth(tablayoutMy, 50);
        tablayoutMy.clearAnimation();
        tablayoutMy.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                TabUtils.tabSelect(tablayoutMy, tab);
                String tag = (String) tab.getTag();
                current_id = tag;
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == tag) {
                    return;
                }
                MyLogUtil.e("---geekyun----", current_id);
                is_zhu = false;
                mNextRequestPage = 1;
                mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
                set_re_data();

            }
        });

    }

    private List<OneBean1> mDataTablayout;

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void OnCategorySuccess(HCategoryBean hCategoryBean) {
        emptyview.success();
        // old
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
//        init_tablayout(mDataTablayout);
        current_id = mDataTablayout.get(0).getTab_id();
        // new
        mAdapter11.setNewData(mDataTablayout);
        donetwork_ci();
        MyLogUtil.e("---geekyun----", current_id);
    }

    @Override
    public void OnCategoryNodata(String s) {
        emptyview.nodata();
    }

    @Override
    public void OnCategoryFail(String s) {
        emptyview.errorNet();
    }

    @Override
    public void OnStudycategorySuccess(HStudycategoryBean hStudycategoryBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        // 测试
        mList1 = new ArrayList<>();
        mList1 = hStudycategoryBean.getList();
        if (mNextRequestPage == 1) {
            setData(true, mList1);
            if (mList1.size() == 0) {
//            mAdapter.setEmptyView(getView());
                emptyview1.nodata();
                ;
            }
        } else {
            setData(false, mList1);
        }
    }

    @Override
    public void OnStudycategoryNodata(String s) {
        refreshLayout1.finishRefresh(false);
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            emptyview1.nodata();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

    @Override
    public void OnStudycategoryFail(String s) {
        refreshLayout1.finishRefresh(false);
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            // 根据需求修改bufen
            mAdapter1.setNewData(null);
            emptyview1.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

}
