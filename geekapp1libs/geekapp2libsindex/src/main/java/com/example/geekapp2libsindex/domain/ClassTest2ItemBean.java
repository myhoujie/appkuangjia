package com.example.geekapp2libsindex.domain;


import java.io.Serializable;

/**
 * Created by geek on 2016/2/25.
 */
public class ClassTest2ItemBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String text_id;
    private String text_content;

    public ClassTest2ItemBean() {
    }

    public ClassTest2ItemBean(String text_id, String text_content) {
        this.text_id = text_id;
        this.text_content = text_content;
    }

    public String getText_id() {
        return text_id;
    }

    public void setText_id(String text_id) {
        this.text_id = text_id;
    }

    public String getText_content() {
        return text_content;
    }

    public void setText_content(String text_content) {
        this.text_content = text_content;
    }
}
