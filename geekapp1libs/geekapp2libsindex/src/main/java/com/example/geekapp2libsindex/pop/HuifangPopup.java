package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.Huifangdapter;
import com.geek.libglide47.base.util.DisplayUtil;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;

import java.util.List;

public class HuifangPopup extends BottomPopupView {
    private RecyclerView rv;
    private Huifangdapter adapter;

    public HuifangPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_huifang;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rv = findViewById(R.id.rv);
        rv.setAdapter(adapter);
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        adapter = new Huifangdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv.setNestedScrollingEnabled(false);
        rv.setLayoutManager(linearLayoutManager);
        rv.addItemDecoration(new SpaceItemDecoration(getContext(), 1));
        rv.setAdapter(adapter);
    }


    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int mSpace;

        public SpaceItemDecoration(Context context, int dpValue) {
            mSpace = DisplayUtil.dip2px(context, dpValue);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if (position != 0) {
                outRect.bottom = mSpace;
            }
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {

    }

    @Override
    protected int getMaxHeight() {
        return (int) (XPopupUtils.getWindowHeight(getContext()) * .6f);
    }

    public void setDataList(List list) {
        adapter.setNewData(list);
    }
}
