package com.example.geekapp2libsindex.fragment.one;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.haier.cellarette.baselibrary.toasts2.Toasty;


public class OneFragment14 extends SlbBaseIndexFragment {

    private String ids;
    private TextView tv1;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one14;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        tv1 = rootView.findViewById(R.id.tv1);

    }

}
