package com.example.geekapp2libsindex.fragment.demo.f4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HClasscategoryBean3;
import com.example.biz3hxktindex.bean.HStudycategoryBean;
import com.example.biz3hxktindex.bean.HStudycategoryBean1;
import com.example.biz3hxktindex.presenter.HStudycategoryPresenter;
import com.example.biz3hxktindex.view.HStudycategoryView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


public class F4NewItem2 extends SlbBaseIndexFragment implements HStudycategoryView {

    private EditText edit_query1;
    private boolean is_zhu;// ceshi
    private String current_id;
    private String ids;
    // 分页
    private static final int PAGE_SIZE = 10;
    private int mNextRequestPage = 1;
    private int mPage;
    private static boolean mFirstPageNoMore;
    private static boolean mFirstError = true;
    //
    private EmptyViewNew1 emptyview2Order;
    private SmartRefreshLayout refreshLayout1Order;
    private ClassicsHeader smartHeader1Order;
    private XRecyclerView recyclerView1Order;
    //
    private F4NewAdapter1 mAdapter1;
    private List<HStudycategoryBean1> mList1;

    //
    private HStudycategoryPresenter presenter2;

    private int mOffset = 0;
    private int mScrollY = 0;
    private int scrolly = 0;

    private int mLastItemVisible;
    private int firstVisibleItemPosition;
    private boolean bbbbb;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        set_re_data();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            current_id = getArguments().getString("id");
        }
    }

    private MessageReceiverIndex mMessageReceiver;


    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OneFragment22Item1".equals(intent.getAction())) {
                    boolean oneFragment22Item11 = intent.getBooleanExtra("OneFragment22Item11", true);
                    int oneFragment22Item12 = intent.getIntExtra("OneFragment22Item12", -1);
                    if (!oneFragment22Item11) {
//            if (refreshLayout1 != null) {
//                refreshLayout1.autoRefresh();
//            }
                        donetwork();
                    } else {
                        recyclerView1Order.scrollToPosition(0);
                        if (oneFragment22Item12 == 0) {
                            donetwork2();
                        }
                        MyLogUtil.e("ssssssssssss", "smoothScrollBy");
                    }
                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroyView() {
        presenter2.onDestory();
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f4_new_item2;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        emptyview2Order = rootView.findViewById(R.id.emptyview2_order);
        refreshLayout1Order = rootView.findViewById(R.id.refreshLayout1_order);
        smartHeader1Order = rootView.findViewById(R.id.smart_header1_order);
        recyclerView1Order = rootView.findViewById(R.id.recycler_view1_order);
        edit_query1 = rootView.findViewById(R.id.edit_query1);
        edit_query1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
//                if (keyCode == EditorInfo.IME_ACTION_SEND || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    hideInput();
                    String searchContext = edit_query1.getText().toString().trim();
                    if (!TextUtils.isEmpty(searchContext)) {
                        // 请求接口
                        donetwork();
                        edit_query1.getText().clear();
                    }
                }
                return false;
            }
        });
        onclick();
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OneFragment22Item1");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
        //
        presenter2 = new HStudycategoryPresenter();
        presenter2.onCreate(this);
        donetwork();
    }


    private void onclick() {
        smartHeader1Order.setEnableLastTime(false);
        refreshLayout1Order.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
        refreshLayout1Order.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                donetwork();
            }
        });
        //
        emptyview2Order.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview2Order.bind(refreshLayout1Order).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                donetwork();
            }
        });
        //
        recyclerView1Order.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        mList1 = new ArrayList<>();
        mAdapter1 = new F4NewAdapter1();
        recyclerView1Order.setAdapter(mAdapter1);
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HStudycategoryBean1 bean1 = (HStudycategoryBean1) adapter.getData().get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                intent.putExtra("courseId", bean1.getId() + "");
                startActivity(intent);
            }
        });
//        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                HStudycategoryBean1 bean1 = (HStudycategoryBean1) adapter.getData().get(position);
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
//                intent.putExtra("courseId", bean1.getId() + "");
//                startActivity(intent);
//            }
//        });
        mAdapter1.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                is_zhu = false;
                loadMore();
                MyLogUtil.e("--geekyun--", "loadMore");
            }
        }, recyclerView1Order);
        //
//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
        //
        recyclerView1Order.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                MyLogUtil.e("ssssssssssss", "" + newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                dx > 0 时为手指向左滚动,列表滚动显示右面的内容
//                dx < 0 时为手指向右滚动,列表滚动显示左面的内容
//                dy > 0 时为手指向上滚动,列表滚动显示下面的内容
//                dy < 0 时为手指向下滚动,列表滚动显示上面的内容
                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                mLastItemVisible = layoutManager.findLastVisibleItemPosition();
                firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                bbbbb = recyclerView.canScrollVertically(-1);
                EventBus.getDefault().post(new Boolean(bbbbb));
            }
        });
    }

    private void donetwork() {
        //
        is_zhu = false;
        mAdapter1.setNewData(null);
        setDatas();
    }

    private void donetwork2() {
        is_zhu = false;
//        mAdapter1.setNewData(null);
        setDatas2();
    }

    private void setData(boolean isRefresh, List data) {
        mNextRequestPage++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            mAdapter1.setNewData(data);
//            recyclerView1Order.scrollToPosition(0);
        } else {
            if (size > 0) {
                mAdapter1.addData(data);
            }
        }
//        if (mAdapter1.getData().size() > 12) {
//            mAdapter1.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了12");
//            return;
//        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            mAdapter1.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了");
        } else {
            mAdapter1.loadMoreComplete();
        }

    }

    /**
     * 加载更多bufen
     */
    private void loadMore() {
        set_re_data();
    }


    /**
     * 加载第一页数据 分布局
     */
    private void setDatas() {
        emptyview2Order.loading();
        setDatas2();
    }

    /**
     * 加载第一页数据 分布局
     */
    private void setDatas2() {
        mNextRequestPage = 1;
        mAdapter1.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        set_re_data();
    }

    private void set_re_data() {
//        current_id = (String) tablayoutMy.getTabAt(0).getTag();
        String limit = PAGE_SIZE + "";
        String page = mNextRequestPage + "";
        String sheduleStatus = "courseSchedule";
//        presenter2.get_studycategorylist(current_id, limit, page, current_id);
        //测试
        emptyview2Order.success();
        refreshLayout1Order.finishRefresh(0);
        HStudycategoryBean hStudycategoryBean = new HStudycategoryBean();
        mList1 = new ArrayList<>();
        List<String> aa = new ArrayList<>();
        aa.add("1");
        aa.add("1");
        List<HClasscategoryBean3> bb = new ArrayList<>();
        bb.add(new HClasscategoryBean3("1", "1", "1", "1"));
        mList1.add(new HStudycategoryBean1("1", "1", "1", aa, "1", bb, 1, "1", "1", "1", 1));
        hStudycategoryBean.setList(mList1);
        mList1 = hStudycategoryBean.getList();
        if (mNextRequestPage == 1) {
            setData(true, mList1);
            if (mList1.size() == 0) {
//            mAdapter.setEmptyView(getView());
                emptyview2Order.nodata();
                ;
            }
        } else {
            setData(false, mList1);
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        MyLogUtil.e("ssssssssssss", "firstVisibleItemPosition->" + firstVisibleItemPosition);
        if (!bbbbb) {
//            if (refreshLayout1 != null) {
//                refreshLayout1.autoRefresh();
//            }
            donetwork();
        } else {
            recyclerView1Order.scrollToPosition(0);
            MyLogUtil.e("ssssssssssss", "smoothScrollBy");
        }
        // fragment传值
//        OrderCommonItem2Bean bean = new OrderCommonItem2Bean(2, current_id, true);
//        EventBus.getDefault().post(bean);
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 业务逻辑部分
     */


    @Override
    public void OnStudycategorySuccess(HStudycategoryBean hStudycategoryBean) {
        emptyview2Order.success();
        refreshLayout1Order.finishRefresh(0);
        // 测试
        mList1 = new ArrayList<>();
        mList1 = hStudycategoryBean.getList();
        if (mNextRequestPage == 1) {
            setData(true, mList1);
            if (mList1.size() == 0) {
//            mAdapter.setEmptyView(getView());
                emptyview2Order.nodata();
                ;
            }
        } else {
            setData(false, mList1);
        }
    }

    @Override
    public void OnStudycategoryNodata(String s) {
        refreshLayout1Order.finishRefresh(false);
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            emptyview2Order.nodata();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

    @Override
    public void OnStudycategoryFail(String s) {
        refreshLayout1Order.finishRefresh(false);
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            // 根据需求修改bufen
            mAdapter1.setNewData(null);
            emptyview2Order.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

}
