package com.example.geekapp2libsindex.fragment.demo.f3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCategoryBean1;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.presenter.HCategoryPresenter;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.Tablayoutdapter;
import com.example.geekapp2libsindex.common.OrderFragmentPagerAdapter;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerSlide;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class F3New extends SlbBaseIndexFragment implements HCategoryView {

    private String current_id;
    private String ids;
    //
    private ImageView iv_back1_order;
    private TextView tv_center_content;
    private EmptyViewNew1 emptyview1Order;
    private LinearLayout llRefresh1Order;
    private XRecyclerView recyclerView1Order11;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private ViewPagerSlide viewpagerMy1Order;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    //
    private List<HLunbotuBean1> mListbanner1;
    private HCategoryPresenter presenter1;
    protected boolean enscrolly;

    public boolean isEnscrolly() {
        return enscrolly;
    }

    public void setEnscrolly(boolean enscrolly) {
        this.enscrolly = enscrolly;
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void updateUI(final Boolean scrolly) {
        setEnscrolly(scrolly);
        MyLogUtil.e("----geekyun11---", scrolly + "");
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        set_re_data();
    }

    private MessageReceiverIndex mMessageReceiver;


    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OneFragment22".equals(intent.getAction())) {

                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        presenter1.onDestory();
        EventBus.getDefault().unregister(this);
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f3_new;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        iv_back1_order.setVisibility(View.GONE);
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("报警管理");
        emptyview1Order = rootView.findViewById(R.id.emptyview1_order);
        llRefresh1Order = rootView.findViewById(R.id.ll_refresh1_order);
        recyclerView1Order11 = rootView.findViewById(R.id.recycler_view1_order11);
        viewpagerMy1Order = rootView.findViewById(R.id.viewpager_my1_orderf3);
        onclick();
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OneFragment22");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
        //
        mListbanner1 = new ArrayList<>();
        presenter1 = new HCategoryPresenter();
        presenter1.onCreate(this);
        donetwork();
    }

    private void donetwork() {
        emptyview1Order.loading();
        //
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "实时报警"));
        mDataTablayout1.add(new HCategoryBean1("2", "历史报警"));
        hCategoryBean.setList(mDataTablayout1);
        //
        emptyview1Order.success();
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        MyLogUtil.e("---geekyun----", current_id);
//        presenter1.get_category("courseSchedule", "1");
    }

    private void onclick() {
        //
        recyclerView1Order11.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
//        JackSnapHelper mLinearSnapHelper = new JackSnapHelper(JackSnapHelper.TYPE_SNAP_START);
//        mLinearSnapHelper.attachToRecyclerView(recyclerView1Order11);
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerView1Order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                MyLogUtil.e("---geekyun----", current_id);
                MyLogUtil.e("---geekyun-position---", position + "");
                viewpagerMy1Order.setCurrentItem(position, true);
            }
        });
        //
        emptyview1Order.notices("暂无课程，去选课中心看看吧…", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview1Order.bind(llRefresh1Order).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 主布局
                donetwork();
            }
        });
    }


    private void init_viewp(List<OneBean1> mlist) {
        //
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if(i == 0){
                F3NewItem1 fragment1 = FragmentHelper.newFragment(F3NewItem1.class, bundle);
                mFragmentList.add(fragment1);
            }else {
                F3NewItem2 fragment1 = FragmentHelper.newFragment(F3NewItem2.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), mFragmentList);
        viewpagerMy1Order.setAdapter(orderFragmentPagerAdapter);
        viewpagerMy1Order.setOffscreenPageLimit(4);
        viewpagerMy1Order.setScroll(true);
        viewpagerMy1Order.setCurrentItem(0, false);
        viewpagerMy1Order.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                OneBean1 bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            Intent msgIntent = new Intent();
            msgIntent.setAction("OneFragment22Item1");
            msgIntent.putExtra("OneFragment22Item11", true);
            msgIntent.putExtra("OneFragment22Item12", 0);
            LocalBroadcastManagers.getInstance(getActivity()).sendBroadcast(msgIntent);
            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        if (!isEnscrolly()) {
            donetwork();
        } else {
            Intent msgIntent = new Intent();
            msgIntent.setAction("OneFragment22Item1");
            msgIntent.putExtra("OneFragment22Item11", isEnscrolly());
            LocalBroadcastManagers.getInstance(getActivity()).sendBroadcast(msgIntent);
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void OnCategorySuccess(HCategoryBean hCategoryBean) {
        emptyview1Order.success();
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        MyLogUtil.e("---geekyun----", current_id);
    }

    @Override
    public void OnCategoryNodata(String s) {
        emptyview1Order.nodata();
    }

    @Override
    public void OnCategoryFail(String s) {
        emptyview1Order.errorNet();
    }

}
