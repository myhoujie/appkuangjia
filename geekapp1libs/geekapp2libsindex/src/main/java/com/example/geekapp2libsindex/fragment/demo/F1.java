package com.example.geekapp2libsindex.fragment.demo;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.videoplay.gsy.EmptyControlVideo;
import com.github.commonlibs.libupdateapputils.DemoUpdateAppMainActivity;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.model.VideoOptionModel;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;

import java.util.ArrayList;
import java.util.List;

import tv.danmaku.ijk.media.player.IjkMediaPlayer;


public class F1 extends SlbBaseIndexFragment {

    private String ids;
    private String tablayoutId;
    private ImageView iv_back1_order;
    private ImageView shareIv;
    private RelativeLayout f1_ll0;
    private TextView f1_tv0;
    private TextView tv_center_content;
    private TextView f1_tv1;
    private OrientationUtils orientationUtils;
    private LinearLayout f1_ll11;
    private LinearLayout f1_ll22;
    private LinearLayout f1_ll33;
    private LinearLayout f1_ll4;
    private EmptyControlVideo video_player11;
    private EmptyControlVideo video_player221;
    private EmptyControlVideo video_player222;
    private EmptyControlVideo video_player223;
    private EmptyControlVideo video_player224;
    private EmptyControlVideo video_player331;
    private EmptyControlVideo video_player332;
    private EmptyControlVideo video_player333;
    private EmptyControlVideo video_player334;
    private EmptyControlVideo video_player335;
    private EmptyControlVideo video_player336;
    private EmptyControlVideo video_player337;
    private EmptyControlVideo video_player338;
    private EmptyControlVideo video_player339;
    //    private final String url = "rtmp://58.200.131.2:1935/livetv/hunantv";
    private final String url = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";// rtsp://test:123456@101.200.48.11/tesm.mp4
//    private final String url = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4";

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        video_player11.release();
        video_player221.release();
        video_player222.release();
        video_player223.release();
        video_player224.release();
        video_player331.release();
        video_player332.release();
        video_player333.release();
        video_player334.release();
        video_player335.release();
        video_player336.release();
        video_player337.release();
        video_player338.release();
        video_player339.release();
        if (orientationUtils != null) {
            orientationUtils.releaseListener();
        }
        super.onDestroyView();
    }

//    @Override
//    public void onBackPressed() {
//        //释放所有
//        videoPlayer.setVideoAllCallBack(null);
//        GSYVideoManager.releaseAllVideos();
//        super.onBackPressed();
//
//    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f1;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        startActivity(new Intent(getActivity(), DemoUpdateAppMainActivity.class));
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        f1_ll0 = rootView.findViewById(R.id.f1_ll0);
        f1_tv0 = rootView.findViewById(R.id.f1_tv0);
        f1_tv0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 打开监控
                set0X0();
                f1_ll11.setVisibility(View.VISIBLE);
                f1_ll4.setVisibility(View.VISIBLE);
                f1_ll0.setVisibility(View.GONE);
                set1X1(url);
            }
        });
        f1_ll4 = rootView.findViewById(R.id.f1_ll4);
        f1_tv1 = rootView.findViewById(R.id.f1_tv1);
        f1_tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close
                set0X0();
                f1_ll0.setVisibility(View.VISIBLE);
                f1_ll4.setVisibility(View.GONE);
            }
        });
        f1_ll11 = rootView.findViewById(R.id.f1_ll11);
        f1_ll22 = rootView.findViewById(R.id.f1_ll22);
        f1_ll33 = rootView.findViewById(R.id.f1_ll33);
        video_player11 = rootView.findViewById(R.id.video_player11);
        video_player221 = rootView.findViewById(R.id.video_player221);
        video_player222 = rootView.findViewById(R.id.video_player222);
        video_player223 = rootView.findViewById(R.id.video_player223);
        video_player224 = rootView.findViewById(R.id.video_player224);
        video_player331 = rootView.findViewById(R.id.video_player331);
        video_player332 = rootView.findViewById(R.id.video_player332);
        video_player333 = rootView.findViewById(R.id.video_player333);
        video_player334 = rootView.findViewById(R.id.video_player334);
        video_player335 = rootView.findViewById(R.id.video_player335);
        video_player336 = rootView.findViewById(R.id.video_player336);
        video_player337 = rootView.findViewById(R.id.video_player337);
        video_player338 = rootView.findViewById(R.id.video_player338);
        video_player339 = rootView.findViewById(R.id.video_player339);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        shareIv = rootView.findViewById(R.id.shareIv);
        iv_back1_order.setVisibility(View.GONE);
        shareIv.setVisibility(View.VISIBLE);
        shareIv.setImageResource(R.drawable.icon_dh1);
        shareIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //切换
                new XPopup.Builder(getContext())
                        .hasShadowBg(false)
//                        .popupAnimation(PopupAnimation.NoAnimation) //NoAnimation表示禁用动画
//                        .isCenterHorizontal(true) //是否与目标水平居中对齐
//                        .offsetY(-10)
//                        .popupPosition(PopupPosition.Bottom) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList(new String[]{"1X1", "2X2", "3X3"},
                                new int[]{R.drawable.f1_icon_ssjk1, R.drawable.f1_icon_ssjk2, R.drawable.f1_icon_ssjk3},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        set0X0();
                                        if (TextUtils.equals("1X1", text)) {
                                            f1_ll11.setVisibility(View.VISIBLE);
                                            f1_ll4.setVisibility(View.VISIBLE);
                                            f1_ll0.setVisibility(View.GONE);
                                            set1X1(url);
                                        }
                                        if (TextUtils.equals("2X2", text)) {
                                            f1_ll22.setVisibility(View.VISIBLE);
                                            f1_ll4.setVisibility(View.VISIBLE);
                                            f1_ll0.setVisibility(View.GONE);
                                            set2X2(url);
                                        }
                                        if (TextUtils.equals("3X3", text)) {
                                            f1_ll33.setVisibility(View.VISIBLE);
                                            f1_ll4.setVisibility(View.VISIBLE);
                                            f1_ll0.setVisibility(View.GONE);
                                            set3X3(url);
                                        }
                                    }
                                })
                        .show();
            }
        });
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("实时监控");
        tv_center_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent("hs.act.slbapp.VideoPlayActivity"));
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SaomaActDemo"));
            }
        });
        /**此中内容：优化加载速度，降低延迟*/
        VideoOptionModel videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "rtsp_transport", "tcp");
        List<VideoOptionModel> list = new ArrayList<>();
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "rtsp_flags", "prefer_tcp");
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "allowed_media_types", "video"); //根据媒体类型来配置
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "timeout", 20000);
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "buffer_size", 1316);
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "infbuf", 1);  // 无限读
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "dns_cache_clear", 1);  // 无限读
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "dns_cache_timeout", -1);  // 无限读
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "analyzemaxduration", 100);
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "probesize", 10240);
        list.add(videoOptionModel);
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "flush_packets", 1);
        list.add(videoOptionModel);
        //  关闭播放器缓冲，这个必须关闭，否则会出现播放一段时间后，一直卡主，控制台打印 FFP_MSG_BUFFERING_START
        videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "packet-buffering", 0);
        list.add(videoOptionModel);
        GSYVideoManager.instance().setOptionModelList(list);
        //EXOPlayer内核，支持格式更多
//        PlayerFactory.setPlayManager(Exo2PlayerManager.class);
        //exo缓存模式，支持m3u8，只支持exo
//        CacheFactory.setCacheManager(ExoPlayerCacheManager.class);
        donetwork();
    }

    private void set0X0() {
        //释放所有
        video_player11.setVideoAllCallBack(null);
        video_player221.setVideoAllCallBack(null);
        video_player222.setVideoAllCallBack(null);
        video_player223.setVideoAllCallBack(null);
        video_player224.setVideoAllCallBack(null);
        video_player331.setVideoAllCallBack(null);
        video_player332.setVideoAllCallBack(null);
        video_player333.setVideoAllCallBack(null);
        video_player334.setVideoAllCallBack(null);
        video_player335.setVideoAllCallBack(null);
        video_player336.setVideoAllCallBack(null);
        video_player337.setVideoAllCallBack(null);
        video_player338.setVideoAllCallBack(null);
        video_player339.setVideoAllCallBack(null);
        GSYVideoManager.releaseAllVideos();
        //
        f1_ll0.setVisibility(View.GONE);
        f1_ll11.setVisibility(View.GONE);
        f1_ll22.setVisibility(View.GONE);
        f1_ll33.setVisibility(View.GONE);
        f1_ll4.setVisibility(View.GONE);
    }

    private void set1X1(String url) {
        video_player11.setTag("video_player");
        video_player11.setPlayPosition(0);
        video_player11.setUpLazy(url, true, null, null, "");
        video_player11.startPlayLogic();

    }

    private void set2X2(String url) {
        video_player221.setTag("video_player");
        video_player221.setPlayPosition(1);
        video_player221.setUpLazy(url, true, null, null, "");
        video_player221.startPlayLogic();
        video_player222.setTag("video_player");
        video_player222.setPlayPosition(2);
        video_player222.setUpLazy(url, true, null, null, "");
        video_player222.startPlayLogic();
        video_player223.setTag("video_player");
        video_player223.setPlayPosition(3);
        video_player223.setUpLazy(url, true, null, null, "");
        video_player223.startPlayLogic();
        video_player224.setTag("video_player");
        video_player224.setPlayPosition(4);
        video_player224.setUpLazy(url, true, null, null, "");
        video_player224.startPlayLogic();
    }

    private void set3X3(String url) {
        video_player331.setTag("video_player");
        video_player331.setPlayPosition(5);
        video_player331.setUp(url, true, "");
        video_player331.startPlayLogic();
        video_player332.setTag("video_player");
        video_player332.setPlayPosition(6);
        video_player332.setUp(url, true, "");
        video_player332.startPlayLogic();
        video_player333.setTag("video_player");
        video_player333.setPlayPosition(7);
        video_player333.setUp(url, true, "");
        video_player333.startPlayLogic();
        video_player334.setTag("video_player");
        video_player334.setPlayPosition(8);
        video_player334.setUp(url, true, "");
        video_player334.startPlayLogic();
        video_player335.setTag("video_player");
        video_player335.setPlayPosition(9);
        video_player335.setUp(url, true, "");
        video_player335.startPlayLogic();
        video_player336.setTag("video_player");
        video_player336.setPlayPosition(10);
        video_player336.setUp(url, true, "");
        video_player336.startPlayLogic();
        video_player337.setTag("video_player");
        video_player337.setPlayPosition(11);
        video_player337.setUp(url, true, "");
        video_player337.startPlayLogic();
        video_player338.setTag("video_player");
        video_player338.setPlayPosition(12);
        video_player338.setUp(url, true, "");
        video_player338.startPlayLogic();
        video_player339.setTag("video_player");
        video_player339.setPlayPosition(13);
        video_player339.setUp(url, true, "");
        video_player339.startPlayLogic();
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */


}
