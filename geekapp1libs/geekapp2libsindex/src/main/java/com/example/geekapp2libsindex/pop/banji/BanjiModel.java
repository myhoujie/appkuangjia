package com.example.geekapp2libsindex.pop.banji;

import java.io.Serializable;
import java.util.List;

public class BanjiModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<BanjiModel2> list;

    public BanjiModel() {
    }

    public BanjiModel(List<BanjiModel2> list) {
        this.list = list;
    }

    public List<BanjiModel2> getList() {
        return list;
    }

    public void setList(List<BanjiModel2> list) {
        this.list = list;
    }
}
