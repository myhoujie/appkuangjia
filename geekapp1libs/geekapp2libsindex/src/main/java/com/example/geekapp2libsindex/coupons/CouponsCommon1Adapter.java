package com.example.geekapp2libsindex.coupons;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HCouponsListBean1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.HalfCircleDecorView;

import java.util.HashSet;
import java.util.Set;

public class CouponsCommon1Adapter extends BaseQuickAdapter<HCouponsListBean1, BaseViewHolder> {
    private int lastSelectIndex = -1;
    private Set<Integer> unfoldSet;

    public CouponsCommon1Adapter(int layoutResId) {
        super(layoutResId);
        unfoldSet = new HashSet<>();
    }

    @Override
    protected void convert(final BaseViewHolder helper, HCouponsListBean1 item) {
        final int position = helper.getAdapterPosition();
        HalfCircleDecorView halfCdv = helper.itemView.findViewById(R.id.halfCdv);
        TextView tv0 = helper.itemView.findViewById(R.id.tv0);
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        View ll_right = helper.itemView.findViewById(R.id.ll_right);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        TextView simpleTipTv = helper.itemView.findViewById(R.id.simpleTipTv);
        final View ll_detail = helper.itemView.findViewById(R.id.ll_detail);
        final TextView detailTv = helper.itemView.findViewById(R.id.detailTv);
        final View arrowIv = helper.itemView.findViewById(R.id.arrowIv);
        if (TextUtils.equals(item.getCouponInfoState(), "0")) {
            // 未使用
            helper.itemView.setEnabled(true);
            halfCdv.setEnabled(true);
            ll_right.setEnabled(true);
            iv1.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color_333333, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color_333333));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        } else if (TextUtils.equals(item.getCouponInfoState(), "1")) {
            // 已使用
            helper.itemView.setEnabled(false);
            halfCdv.setEnabled(false);
            ll_right.setEnabled(false);
            iv1.setVisibility(View.VISIBLE);
            iv1.setBackgroundResource(R.drawable.quan_used2);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        } else if (TextUtils.equals(item.getCouponInfoState(), "2")) {
            // 已过期
            helper.itemView.setEnabled(false);
            halfCdv.setEnabled(false);
            ll_right.setEnabled(false);
            iv1.setVisibility(View.VISIBLE);
            iv1.setBackgroundResource(R.drawable.quan_guoqi1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                simpleTipTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
                detailTv.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        }
        tv1.setText(item.getAmount());
        tv2.setText(item.getCouponTypeDesc());
        tv3.setText(item.getCouponName());
        tv4.setText(item.getUseTimeRange());
        simpleTipTv.setText(item.getCouponRangeDesc());
        final String detail = "仅限指定课程使用\n" + item.getCouponRangeDesc().replaceAll(",", "    ");
        detailTv.setText(detail);
        if (TextUtils.equals(item.getStrShrink(), "1")) {
            simpleTipTv.setVisibility(View.GONE);
            ll_detail.setVisibility(View.VISIBLE);

            if (unfoldSet.contains(position)) {
                detailTv.setVisibility(View.VISIBLE);
                arrowIv.setRotation(180);
                unfoldSet.add(position);
            } else {
                detailTv.setVisibility(View.GONE);
                arrowIv.setRotation(0);
                unfoldSet.remove(position);
            }
        } else {
            simpleTipTv.setVisibility(View.VISIBLE);
            ll_detail.setVisibility(View.GONE);
        }
        ll_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unfoldSet.contains(position)) {//已展开
                    detailTv.setVisibility(View.GONE);
                    arrowIv.setRotation(0);
                    unfoldSet.remove(position);
                } else {//未展开
                    detailTv.setVisibility(View.VISIBLE);
                    arrowIv.setRotation(180);
                    unfoldSet.add(position);
                }
            }
        });
    }

}
