package com.example.geekapp2libsindex.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.geek.libglide47.base.util.DisplayUtil;

public class TeacherSessionAdatper extends BaseQuickAdapter<CourseBean, BaseViewHolder> {

    public TeacherSessionAdatper() {
        super(R.layout.adapter_teacher_session);
    }

    @Override
    protected void convert(BaseViewHolder helper, CourseBean courseBean) {
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView dateTv = helper.itemView.findViewById(R.id.dateTv);
        RecyclerView teacherRv = helper.itemView.findViewById(R.id.teacherRv);
        teacherRv.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        if (teacherRv.getItemDecorationCount() > 0) {
            RecyclerView.ItemDecoration itemDecoration = teacherRv.getItemDecorationAt(0);
            if (itemDecoration == null) {
                teacherRv.addItemDecoration(new SpaceItemDecoration(mContext, 40));
            }
        } else {
            teacherRv.addItemDecoration(new SpaceItemDecoration(mContext, 40));
        }
        ClassDetailTeacherAdapter teacherAdapter = new ClassDetailTeacherAdapter(mContext, false);
        teacherRv.setAdapter(teacherAdapter);

        TextView classHourTv = helper.itemView.findViewById(R.id.classHourTv);
        TextView nPriceTv = helper.itemView.findViewById(R.id.nPriceTv);
        TextView oPriceTv = helper.itemView.findViewById(R.id.oPriceTv);

        //标题
        String title = "";
        boolean hasSubject = courseBean.getSubjectNames() != null && courseBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : courseBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + courseBean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < courseBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(mContext, TextUtils.equals(courseBean.getClassAttribute(), "experience") ? Color.parseColor("#666666") : Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        dateTv.setText(courseBean.getCourseTimeRange());
        classHourTv.setText(courseBean.getItemCount() + "课节");
        if (TextUtils.equals("0", courseBean.getChargeFlag())) {//免费
            nPriceTv.setText("免费");
        } else {
            SpannableString nPriceSpannableString = new SpannableString("¥" + courseBean.getPricePayable());
            AbsoluteSizeSpan relativeSizeSpan = new AbsoluteSizeSpan(14, true);
            nPriceSpannableString.setSpan(relativeSizeSpan, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            nPriceTv.setText(nPriceSpannableString);
        }

        if (Double.valueOf(courseBean.getPriceMarked()) == 0) {
            oPriceTv.setVisibility(View.GONE);
        } else {
            oPriceTv.setText("¥" + courseBean.getPriceMarked());
            oPriceTv.setVisibility(View.VISIBLE);
        }
        oPriceTv.getPaint().setAntiAlias(true);//抗锯齿
        oPriceTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);  // 设置中划线并加清晰
        teacherAdapter.setNewData(courseBean.getTeacherList());
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int mSpace;

        public SpaceItemDecoration(Context context, int dpValue) {
            mSpace = DisplayUtil.dip2px(context, dpValue);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = mSpace;
        }
    }

}
