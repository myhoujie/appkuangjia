package com.example.geekapp2libsindex.fragment.three.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.example.biz3hxktindex.bean.HGengxinBean;
import com.example.biz3hxktindex.presenter.HGengxinPresenter;
import com.example.biz3hxktindex.view.HGengxinView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.github.commonlibs.libupdateapputils.util.UpdateAppReceiver;
import com.github.commonlibs.libupdateapputils.util.UpdateAppUtils;
import com.haier.cellarette.baselibrary.cacheutil.CacheUtil;
import com.haier.cellarette.baselibrary.common.BaseApp;
import com.haier.cellarette.baselibrary.widget.AlertView;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.SlbLoginUtil2;

public class SettingAct extends SlbBaseActivity implements View.OnClickListener, HGengxinView {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private RelativeLayout ll1;
    private RelativeLayout ll2;
    private RelativeLayout ll3;
    private TextView tv1;
    private TextView tv2;
    private TextView tv_qchc;
    //
    private HGengxinPresenter presenter;
    private String apkPath = "";
    private int serverVersionCode = 0;
    private String serverVersionName = "";
    private String updateInfoTitle = "";
    private String updateInfo = "";
    private String md5 = "";
    private String appPackageName = "";
    private UpdateAppReceiver updateAppReceiver;
    private UpdateAppUtils updateAppUtils;

    @Override
    protected void onDestroy() {
        presenter.onDestory();
        if (updateAppReceiver != null) {
            updateAppReceiver.desBr(SettingAct.this);
        }
        if (updateAppUtils != null) {
            updateAppUtils.mProgressDialog = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if (SlbLoginUtil2.get().isUserLogin()) {
            tv1.setText(getResources().getString(R.string.slb_my_tip_login));
        } else {
            tv1.setText(getResources().getString(R.string.slb_my_tip_outlogin));
        }
        if (updateAppReceiver == null) {
            updateAppReceiver = new UpdateAppReceiver();
        }
        updateAppReceiver.setBr(this);
        if (updateAppUtils == null) {
            updateAppUtils = new UpdateAppUtils(this);
        }
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_setting;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("设置");
        try {
            tv_qchc.setText(CacheUtil.getTotalCacheSize(BaseApp.get()));
        } catch (Exception e) {
            tv_qchc.setText("0.00B");
        }
        apkPath = "";
        serverVersionCode = AppUtils.getAppVersionCode();
        serverVersionName = AppUtils.getAppVersionName();
        appPackageName = AppUtils.getAppPackageName();
        updateInfoTitle = "";
        updateInfo = "";
        md5 = AppUtils.getAppSignatureMD5(appPackageName);
        presenter = new HGengxinPresenter();
        presenter.onCreate(this);
        presenter.get_gengxin("android", serverVersionCode + "", appPackageName, serverVersionName);
    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        ll1.setOnClickListener(this);
        ll2.setOnClickListener(this);
        ll3.setOnClickListener(this);
    }

    private void findview() {
        ivBack = (ImageView) findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) findViewById(R.id.tv_center_content);
        ll1 = (RelativeLayout) findViewById(R.id.ll1);
        ll2 = (RelativeLayout) findViewById(R.id.ll2);
        ll3 = (RelativeLayout) findViewById(R.id.ll3);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv_qchc = (TextView) findViewById(R.id.tv_qchc);
        tv2 = (TextView) findViewById(R.id.tv_versoncode);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.ll1) {
            // 清除缓存
            final AlertView dialog = new AlertView(this, "提示", "缓存大小为" + CacheUtil.getTotalCacheSize(BaseApp.get()) + ",确定要清理吗?",
                    "取消", "确定");
            dialog.show();
            dialog.setClicklistener(new AlertView.ClickListenerInterface() {
                                        @Override
                                        public void doLeft() {
                                            dialog.dismiss();
                                        }

                                        @Override
                                        public void doRight() {
                                            //TODO 清除应用缓存
                                            CacheUtil.clearAllCache(BaseApp.get());
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        tv_qchc.setText(CacheUtil.getTotalCacheSize(BaseApp.get()));
                                                    } catch (Exception e) {
                                                        tv_qchc.setText("0.00B");
                                                    }
                                                }
                                            }, 500);
                                            dialog.dismiss();

                                        }
                                    }
            );
        } else if (view.getId() == R.id.ll2) {
            // 当前版本
            updateAppUtils.from(SettingAct.this)
                    .serverVersionCode(serverVersionCode)
                    .serverVersionName(serverVersionName)
                    .downloadPath("apk/" + getPackageName() + ".apk")
                    .showProgress(true)
                    .apkPath(apkPath)
                    .downloadBy(UpdateAppUtils.DOWNLOAD_BY_APP)    //default
                    .checkBy(UpdateAppUtils.CHECK_BY_VERSION_CODE) //default
                    .updateInfoTitle(updateInfoTitle)
                    .updateInfo(updateInfo.replace("|", "\n"))
                    .showNotification(true)
//                    .needFitAndroidN(true)
//                    .updateInfoTitle("新版本已准备好")
//                    .updateInfo("版本：1.01" + "    " + "大小：10.41M\n" + "1.修改已知问题\n2.加入动态绘本\n3.加入小游戏等你来学习升级")
                    .update();
        } else if (view.getId() == R.id.ll3) {
            // 登录
            String aaa = tv1.getText().toString().trim();
            if (TextUtils.equals(aaa, getResources().getString(R.string.slb_my_tip_login))) {
                // 退出登录bufen
                SlbLoginUtil2.get().loginOutTowhere(SettingAct.this, new Runnable() {
                    @Override
                    public void run() {
                        // 刷新个人中心bufen
                        tv1.setText(getResources().getString(R.string.slb_my_tip_outlogin));
//                        onBackPressed();
                        try {
                            Class<? extends Activity> klass = (Class<? extends Activity>) Class.forName("com.example.geekapp2libsindex.ShouyeActivity");
                            ActivityUtils.finishToActivity(klass, false);
                            Intent msgIntent = new Intent();
                            msgIntent.setAction("ShouyeActivity");
                            msgIntent.putExtra("id", 0);
                            LocalBroadcastManagers.getInstance(SettingAct.this).sendBroadcast(msgIntent);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
            if (TextUtils.equals(aaa, getResources().getString(R.string.slb_my_tip_outlogin))) {
                // 登录bufen
                SlbLoginUtil2.get().loginTowhere(SettingAct.this, new Runnable() {
                    @Override
                    public void run() {
                        // 刷新个人中心bufen
                        tv1.setText(getResources().getString(R.string.slb_my_tip_login));
                        onBackPressed();

                    }
                });
            }
        } else {

        }
    }

    @Override
    public void OnGengxinSuccess(HGengxinBean hGengxinBean) {
        apkPath = hGengxinBean.getApkUrl();
        serverVersionCode = Integer.valueOf(hGengxinBean.getVersionNo());
        if (TextUtils.isEmpty(hGengxinBean.getVersionName())) {
            tv2.setText(hGengxinBean.getVersionName());
        } else {
            tv2.setText(serverVersionName);
        }
        updateInfoTitle = hGengxinBean.getTitle();
        updateInfo = hGengxinBean.getUpgradeInfo();
    }

    @Override
    public void OnGengxinNodata(String s) {
        tv2.setText(serverVersionName);
    }

    @Override
    public void OnGengxinFail(String s) {
        tv2.setText(serverVersionName);
    }
}
