package com.example.geekapp2libsindex.classtest;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.presenter.HTouxiangsPresenter;
import com.example.biz3hxktindex.presenter.HUploadurlsPresenter;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.example.biz3hxktindex.view.HUploadurlsView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.ClassSectionStatusBean;
import com.example.slbappcomm.uploadimg3.FullyGridLayoutManager;
import com.example.slbappcomm.uploadimg3.GridImageAdapter;
import com.haier.cellarette.baselibrary.statusbar.StatusBarUtil;
import com.haier.cellarette.baselibrary.toasts3.MProgressBarDialog;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.permissions.RxPermissions;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.lzy.imagepicker.util.Utils;
import com.lzy.imagepicker.view.GridSpacingItemDecoration;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ClassTest1Act extends AppCompatActivity implements View.OnClickListener, HTouxiangsView, HUploadurlsView {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private TextView tv1;

    private LinearLayout ll1;
    private RecyclerView recyclerView;
    private GridImageAdapter adapter;

    //
    private HTouxiangsPresenter presenter;
    private BasePopupView loadingPopup;
    private HUploadurlsPresenter presenter2;
    private String id;
    private int position = -1;
    //
    private List<LocalMedia> selectList;
    private int themeId;
    private int chooseMode = PictureMimeType.ofImage();
    private int maxSelectNum = 9;
    private int mCurrentPosition;

    @Override
    protected void onDestroy() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        presenter.onDestory();
        presenter2.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public void onBackPressed() {
//        if (adapter != null && adapter.isAllchoose()) {
//            adapter.setAllchoose(false);
//            adapter.notifyDataSetChanged();
//        }
        super.onBackPressed();
    }

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_hxkt_classtest1;
//    }
//
//    @Override
//    protected void setup(@Nullable Bundle savedInstanceState) {
//        super.setup(savedInstanceState);
//        findview();
//        onclick();
//        donetwork();
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hxkt_classtest1);
        //
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        StatusBarUtil.setLightMode(this);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("课后练习");
        id = getIntent().getStringExtra("id");
        position = getIntent().getIntExtra("position", -1);
        //
        MobclickAgent.onEvent(this, "lessons_practice_nocorrections");
        //
        set_vis_layout();
        //
        presenter = new HTouxiangsPresenter();
        presenter.onCreate(this);
        presenter2 = new HUploadurlsPresenter();
        presenter2.onCreate(this);

    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        tv1.setOnClickListener(this);
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_pop();
            }
        });
        adapter.setOnItemClickListener(new GridImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (selectList.size() > 0) {
                    LocalMedia media = selectList.get(position);
                    String pictureType = media.getPictureType();
                    int mediaType = PictureMimeType.pictureToVideo(pictureType);
                    switch (mediaType) {
                        case 1:
                            // 预览图片 可自定长按保存路径
                            //PictureSelector.create(MainActivity.this).themeStyle(themeId).externalPicturePreview(position, "/custom_file", selectList);
                            PictureSelector.create(ClassTest1Act.this).themeStyle(themeId).openExternalPreview(position, selectList);
                            break;
                        case 2:
                            // 预览视频
                            PictureSelector.create(ClassTest1Act.this).externalPictureVideo(media.getPath());
                            break;
                        case 3:
                            // 预览音频
                            PictureSelector.create(ClassTest1Act.this).externalPictureAudio(media.getPath());
                            break;
                    }
                }
            }
        });
        adapter.setOnItemClickListener2(new GridImageAdapter.OnItemClickListener2() {
            @Override
            public void onItemClick2(int position, View v) {
                mCurrentPosition = position;
                showDeleteDialog();
            }
        });
    }

    private void findview() {
        ivBack = (ImageView) findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) findViewById(R.id.tv_center_content);
        tv1 = (TextView) findViewById(R.id.tv1);
        recyclerView = findViewById(R.id.recyclerView);
        ll1 = findViewById(R.id.ll1);

        //
        selectList = new ArrayList<>();
        themeId = R.style.picture_white_style;

        FullyGridLayoutManager manager = new FullyGridLayoutManager(ClassTest1Act.this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, Utils.dp2px(this, 20), false));
        adapter = new GridImageAdapter(ClassTest1Act.this, onAddPicClickListener);
        adapter.setList(selectList);
        adapter.setSelectMax(maxSelectNum);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.tv1) {
            // 提交
            if (selectList == null || selectList.size() == 0) {
                ToastUtils.showLong("请先上传至少一张图片");
                return;
            }
            List<File> list = new ArrayList<>();
            for (int i = 0; i < selectList.size(); i++) {
                if (selectList.get(i).isCompressed()) {
                    File file = new File(selectList.get(i).getCompressPath());
                    list.add(file);
                } else {
                    File file = new File(selectList.get(i).getPath());
                    list.add(file);
                }
            }
            // 1
            loadingPopup = new XPopup.Builder(ClassTest1Act.this)
                    .dismissOnBackPressed(true)
                    .dismissOnTouchOutside(false)
                    .asLoading("上传中...")
                    .show();
            // 2
//            set_update();
            presenter.get_touxiang3(list);
        } else {

        }
    }

    private void set_pop() {
        //
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(ClassTest1Act.this)
                .openGallery(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(maxSelectNum)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(false)// 是否可预览视频
                .enablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .compressSavePath(getPath())//压缩图片保存地址
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(true)// 是否显示gif图片
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                //.isDragFrame(false)// 是否可拖动裁剪框(固定)
//                        .videoMaxSecond(15)
//                        .videoMinSecond(10)
                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                //.rotateEnabled(true) // 裁剪是否可旋转图片
                //.scaleEnabled(true)// 裁剪是否可放大缩小图片
                //.videoQuality()// 视频录制质量 0 or 1
                //.videoSecond()//显示多少秒以内的视频or音频也可适用
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
        //
//        List<String> names = new ArrayList<>();
//        names.add("拍照");
//        names.add("相册");
//        showDialog(new ClassTest1SelectDialog.SelectDialogListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                switch (position) {
//                    case 0: // 直接调起相机
//                        // 单独拍照
//                        PictureSelector.create(ClassTest1Act.this)
//                                .openCamera(chooseMode)// 单独拍照，也可录像或也可音频 看你传入的类型是图片or视频
//                                .theme(themeId)// 主题样式设置 具体参考 values/styles
//                                .maxSelectNum(maxSelectNum)// 最大图片选择数量
//                                .minSelectNum(1)// 最小选择数量
//                                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
//                                .previewImage(true)// 是否可预览图片
//                                .previewVideo(true)// 是否可预览视频
//                                .enablePreviewAudio(true) // 是否可播放音频
//                                .isCamera(true)// 是否显示拍照按钮
//                                .enableCrop(true)// 是否裁剪
//                                .compress(true)// 是否压缩
//                                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
//                                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
//                                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
//                                .isGif(true)// 是否显示gif图片
//                                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
//                                .circleDimmedLayer(false)// 是否圆形裁剪
//                                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
//                                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
//                                .openClickSound(true)// 是否开启点击声音
//                                .selectionMedia(selectList)// 是否传入已选图片
//                                .previewEggs(false)//预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
//                                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
//                                //.cropCompressQuality(90)// 裁剪压缩质量 默认为100
//                                .minimumCompressSize(100)// 小于100kb的图片不压缩
//                                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
//                                //.rotateEnabled() // 裁剪是否可旋转图片
//                                //.scaleEnabled()// 裁剪是否可放大缩小图片
//                                //.videoQuality()// 视频录制质量 0 or 1
//                                //.videoSecond()////显示多少秒以内的视频or音频也可适用
//                                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
//                        break;
//                    case 1:
//                        // 进入相册 以下是例子：不需要的api可以不写
//                        PictureSelector.create(ClassTest1Act.this)
//                                .openGallery(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
//                                .maxSelectNum(maxSelectNum)// 最大图片选择数量
//                                .minSelectNum(1)// 最小选择数量
//                                .imageSpanCount(4)// 每行显示个数
//                                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
//                                .previewImage(true)// 是否可预览图片
//                                .previewVideo(true)// 是否可预览视频
//                                .enablePreviewAudio(true) // 是否可播放音频
//                                .isCamera(true)// 是否显示拍照按钮
//                                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
//                                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
//                                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
//                                .enableCrop(false)// 是否裁剪
//                                .compress(true)// 是否压缩
//                                .synOrAsy(true)//同步true或异步false 压缩 默认同步
//                                .compressSavePath(getPath())//压缩图片保存地址
//                                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
//                                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
//                                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
//                                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
//                                .isGif(true)// 是否显示gif图片
//                                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
//                                .circleDimmedLayer(false)// 是否圆形裁剪
//                                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
//                                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
//                                .openClickSound(true)// 是否开启点击声音
//                                .selectionMedia(selectList)// 是否传入已选图片
//                                //.isDragFrame(false)// 是否可拖动裁剪框(固定)
////                        .videoMaxSecond(15)
////                        .videoMinSecond(10)
//                                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
//                                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
//                                .minimumCompressSize(100)// 小于100kb的图片不压缩
//                                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
//                                //.rotateEnabled(true) // 裁剪是否可旋转图片
//                                //.scaleEnabled(true)// 裁剪是否可放大缩小图片
//                                //.videoQuality()// 视频录制质量 0 or 1
//                                //.videoSecond()//显示多少秒以内的视频or音频也可适用
//                                //.recordVideoSecond()//录制视频秒数 默认60s
//                                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
//                        break;
//                    default:
//                        break;
//                }
//
//            }
//        }, names);
    }


    private ClassTest1SelectDialog showDialog(ClassTest1SelectDialog.SelectDialogListener listener, List<String> names) {
        ClassTest1SelectDialog dialog = new ClassTest1SelectDialog(this, R.style.transparentFrameWindowStyle,
                listener, names);
        if (!this.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    /**
     * 是否删除此张图片
     */
    private void showDeleteDialog() {
        new XPopup.Builder(this)
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onCreated() {
                        Log.e("tag", "弹窗创建了");
                    }

                    @Override
                    public void onShow() {
                        Log.e("tag", "onShow");
                    }

                    @Override
                    public void onDismiss() {
                        Log.e("tag", "onDismiss");
                    }

                    //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                    @Override
                    public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                        return true;
                    }
                }).asConfirm("提示", "要删除这张照片吗？",
                "取消", "确定",
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        //移除当前图片刷新界面
                        selectList.remove(mCurrentPosition);
                        adapter.notifyItemRemoved(mCurrentPosition);
                        adapter.notifyItemRangeChanged(mCurrentPosition, selectList.size());
                        set_vis_layout();

                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                }, false)
                .show();
    }

    private void set_vis_layout() {
        if (selectList == null || selectList.size() == 0) {
            ll1.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            tv1.setVisibility(View.GONE);
        } else {
            ll1.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            tv1.setVisibility(View.VISIBLE);
        }
    }


    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            set_pop();
        }
    };

    // onActResult onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    selectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
                    for (LocalMedia media : selectList) {
                        Log.i("ssss", "压缩---->" + media.getCompressPath());
                        Log.i("ssss", "原图---->" + media.getPath());
                        Log.i("ssss", "裁剪---->" + media.getCutPath());
                    }
                    adapter.setList(selectList);
                    adapter.notifyDataSetChanged();
                    set_vis_layout();
                    break;
            }
        }
    }

    /**
     * 业务逻辑
     *
     * @param hTouxiangBean1
     */

    @Override
    public void OnTouxiangsSuccess(HTouxiangBean1 hTouxiangBean1) {
        String imgStr = hTouxiangBean1.getUrls();
        List<String> list1 = new ArrayList<>();
        if (!TextUtils.isEmpty(imgStr)) {
            String[] str = imgStr.split(",");
            list1 = Arrays.asList(str);
        }
        presenter2.get_upload_img_urls(id, list1);
    }

    @Override
    public void OnTouxiangsNodata(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnTouxiangsFail(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public String getIdentifier() {
        return SystemClock.currentThreadTimeMillis() + "";
    }

    @Override
    public void OnUploadurlsSuccess(String s) {
        if (position != -1) {
            ClassSectionStatusBean classSectionStatusBean = new ClassSectionStatusBean();
            classSectionStatusBean.setExerciseStatus("2");
            classSectionStatusBean.setPosition(position);
            EventBus.getDefault().post(classSectionStatusBean);
        }
        //
        set_clear_img_lujing();
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassTest3Act");
                        intent.putExtra("id", id);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }, 500);
    }

    @Override
    public void OnUploadurlsNodata(final String s) {
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(s);
//                        set_clear_img_lujing();
                    }
                });
            }
        }, 500);
    }

    @Override
    public void OnUploadurlsFail(final String s) {
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(s);
//                        set_clear_img_lujing();
                    }
                });
            }
        }, 500);
    }

    private void set_update() {
        configProgressbarHorizontalDialog();
        startProgress(true);
    }

    private void configProgressbarHorizontalDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Horizontal)
                .build();
    }

    //当前进度
    private int currentProgress;
    //是否开启动画：平滑过度，默认开启
    private boolean animal = true;
    private Handler mHandler = new Handler();
    private MProgressBarDialog mProgressBarDialog;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentProgress < 100) {
                mProgressBarDialog.showProgress(currentProgress, "当前进度为：" + currentProgress + "%", animal);
                currentProgress += 5;
                mHandler.postDelayed(runnable, 200);
            } else {
                mHandler.removeCallbacks(runnable);
                currentProgress = 0;
                mProgressBarDialog.showProgress(100, "完成", animal);
                //关闭
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarDialog.dismiss();
                    }
                }, 1000);
            }
        }
    };

    private void startProgress(boolean animal) {
        this.animal = animal;
        mHandler.post(runnable);
    }


    private void set_clear_img_lujing() {
        // 清空图片缓存，包括裁剪、压缩后的图片 注意:必须要在上传完成后调用 必须要获取权限
        RxPermissions permissions = new RxPermissions(this);
        permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PictureFileUtils.deleteCacheDirFile(ClassTest1Act.this);
                } else {
                    Toast.makeText(ClassTest1Act.this,
                            getString(R.string.picture_jurisdiction), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }


    /**
     * 自定义压缩存储地址
     *
     * @return
     */
    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        if (file.mkdirs()) {
            return path;
        }
        return path;
    }

}
