package com.example.geekapp2libsindex.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CustomWebView extends WebView {

    WebSettings settings;

    public CustomWebView(Context context) {
        this(context, null);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(getFixedContext(context), attrs, defStyleAttr, false);
        settings = getSettings();
        // 设置WebView支持JavaScript
        settings.setJavaScriptEnabled(true);
    }

    public static Context getFixedContext(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return context.createConfigurationContext(new Configuration());
        } else {
            return context;
        }
    }

    public void loadRichText(String str) {
        String js = "<script type=\"text/javascript\">" +
                "var imgs = document.getElementsByTagName('img');" +
                "for(var i = 0; i<imgs.length; i++){" +
                "imgs[i].style.width = '100%';" +
                "imgs[i].style.height = 'auto';" +
                "}" +
                "</script>";
        loadData(str + js, "text/html; charset=UTF-8", null);
        // 富文本
//        loadDataWithBaseURL(null, getNewContent(str), "text/html", "UTF-8", null);
    }

    private String getNewContent(String htmltext) {
        Document doc = Jsoup.parse(htmltext);
        Elements elements = doc.getElementsByTag("img");
        for (Element element : elements) {
            if (element.className() != null && element.className().length() > 0)
                element.attr("width", "100%").attr("height", "auto");
        }
        return doc.toString();
    }

    public void onDestroy() {
//        FixInputMethodBug.fixFocusedViewLeak(getApplication());
        loadUrl("javascript:onDestroy()");

        ViewParent parent = this.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this);
        }

        clearCache(true);
        stopLoading();
        getSettings().setJavaScriptEnabled(false);
        clearHistory();
        clearView();
        removeAllViews();

        try {
            destroy();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }
}
