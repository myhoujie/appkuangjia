package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.CouponSelectAdapter;
import com.fanli.biz3hxktdetail.bean.Coupon;
import com.geek.libglide47.base.util.DisplayUtil;
import com.lxj.xpopup.core.BottomPopupView;

import java.util.ArrayList;
import java.util.List;

public class CouponSelectPopup extends BottomPopupView {
    private RecyclerView rv;
    private CouponSelectAdapter adapter;
    private OnCouponListener onCouponListener;
    private List<Coupon> dataList;

    public CouponSelectPopup(@NonNull Context context, OnCouponListener onCouponListener, List<Coupon> dataList) {
        super(context);
        this.onCouponListener = onCouponListener;
        this.dataList = dataList;
    }

    public CouponSelectPopup(@NonNull Context context, OnCouponListener onCouponListener) {
        this(context, onCouponListener, null);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_coupon_select;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rv = findViewById(R.id.rv);
        rv.setAdapter(adapter);
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        adapter = new CouponSelectAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv.setNestedScrollingEnabled(false);
        rv.setLayoutManager(linearLayoutManager);
        rv.addItemDecoration(new SpaceItemDecoration(getContext()));
        rv.setAdapter(adapter);
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (true) {
                    if (onCouponListener != null) {
                        onCouponListener.onSelectUse(position, (dataList.get(position)).getLiveCouponInfoId());
                    }
                }

            }
        });
        dataList = new ArrayList<>();
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());
        dataList.add(new Coupon());


        adapter.setNewData(dataList);

        findViewById(R.id.notUseCouponTv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCouponListener != null) {
                    onCouponListener.onNoUse();
                }
            }
        });
    }

    public void setNewData(List<Coupon> dataList) {
        this.dataList = dataList;
        adapter.setNewData(dataList);
    }

    public CouponSelectAdapter getAdapter() {
        return adapter;
    }

    public interface OnCouponListener {
        void onNoUse();

        void onSelectUse(int position, String checkedLiveCouponInfoId);
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 16);
            tbSpace = DisplayUtil.dip2px(context, 14);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = tbSpace;
            }
            outRect.bottom = tbSpace;
            outRect.left = outRect.right = lrSpace;
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {

    }

    public void setDataList(List list) {
        adapter.setNewData(list);
    }
}
