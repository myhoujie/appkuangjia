package com.example.geekapp2libsindex.fragment.demo;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

import com.aigestudio.wheelpicker.WheelPicker;
import com.blankj.utilcode.util.ToastUtils;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.listener.InitSuccessListener2;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.haier.cellarette.baselibrary.lobsterpicker.OnColorListener;
import com.haier.cellarette.baselibrary.lobsterpicker.adapters.BitmapColorAdapter;
import com.haier.cellarette.baselibrary.lobsterpicker.sliders.LobsterShadeSlider;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.widget.LxLinearLayout;
import com.haier.cellarette.baselibrary.widget.SmoothCheckBox;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import java.util.ArrayList;
import java.util.List;

import me.shaohui.bottomdialog.BottomDialog;


public class F5 extends SlbBaseIndexFragment implements View.OnClickListener, SmoothCheckBox.OnCheckedChangeListener {

    private String ids;
    private String tablayoutId;
    private ImageView iv_back1_order;
    private TextView tv_center_content;
    private TextView f5_tv11;
    private TextView f5_tv22;
    private TextView f5_tv33;
    private TextView f5_tv44;
    private TextView f5_tv55;
    private String f5_tv11_content = "#f21fb4a5";
    private String f5_tv22_content = "#ff0086ff";
    private String f5_tv33_content = "#ffffff00";
    private String f5_tv44_content = "#ffff7500";
    private String f5_tv55_content = "#f2ff0000";
    private LxLinearLayout f5_ll1;//
    private LxLinearLayout f5_ll2;//
    private SmoothCheckBox f5_scb1;//
    private SmoothCheckBox f5_scb2;//
    private TextView f1_tv1;//
    private TextView f1_tv2;//
    private TextView f1_tv3;//
    private TextView f1_tv4;//
    private TextView f1_tv5;//
    private TextView f1_tv6;//
    private TextView f1_tv7;//
    private TextView submitTv;//
    private LobsterShadeSlider shadeslider;//

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f5;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        f5_tv11 = rootView.findViewById(R.id.f5_tv11);
        f5_tv22 = rootView.findViewById(R.id.f5_tv22);
        f5_tv33 = rootView.findViewById(R.id.f5_tv33);
        f5_tv44 = rootView.findViewById(R.id.f5_tv44);
        f5_tv55 = rootView.findViewById(R.id.f5_tv55);
        f1_tv1 = rootView.findViewById(R.id.f1_tv1);
        f1_tv2 = rootView.findViewById(R.id.f1_tv2);
        f1_tv3 = rootView.findViewById(R.id.f1_tv3);
        f1_tv4 = rootView.findViewById(R.id.f1_tv4);
        f1_tv5 = rootView.findViewById(R.id.f1_tv5);
        f1_tv6 = rootView.findViewById(R.id.f1_tv6);
        f1_tv7 = rootView.findViewById(R.id.f1_tv7);
        submitTv = rootView.findViewById(R.id.submitTv);
        f1_tv1.setOnClickListener(this);
        f1_tv2.setOnClickListener(this);
        f1_tv3.setOnClickListener(this);
        f1_tv4.setOnClickListener(this);
        f1_tv5.setOnClickListener(this);
        f1_tv6.setOnClickListener(this);
        f1_tv7.setOnClickListener(this);
        submitTv.setOnClickListener(this);
        f5_ll1 = rootView.findViewById(R.id.f5_ll1);
        f5_ll1.setTouch(true);
        f5_ll1.setOnClickListener(this);
        f5_ll2 = rootView.findViewById(R.id.f5_ll2);
        f5_ll2.setTouch(true);
        f5_ll2.setOnClickListener(this);
        f5_scb1 = rootView.findViewById(R.id.f5_scb1);
        f5_scb1.setOnCheckedChangeListener(this);
        f5_scb2 = rootView.findViewById(R.id.f5_scb2);
        f5_scb2.setOnCheckedChangeListener(this);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        iv_back1_order.setVisibility(View.GONE);
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("火险预测");
        shadeslider = rootView.findViewById(R.id.shadeslider);
        shadeslider.setColorAdapter(new BitmapColorAdapter(getActivity(), R.drawable.default_shader_pallete4));
        shadeslider.setShadePosition(5);
//        shadeslider.setColor(R.drawable.default_shader_pallete11);
        shadeslider.addOnColorListener(new OnColorListener() {
            @Override
            public void onColorChanged(@ColorInt int color) {
                MyLogUtil.e("geek111111", "" + color);
                MyLogUtil.e("geek111111", "" + shadeslider.adjustAlpha(color, 1.0f));
//                if (TextUtils.equals(f5_tv11_content, adjustAlpha(color, 1.0f) + "")) {
//                    Toasty.normal(getActivity(), f5_tv11.getText().toString().trim()).show();
//                } else if (TextUtils.equals(f5_tv22_content, adjustAlpha(color, 1.0f) + "")) {
//                    Toasty.normal(getActivity(), f5_tv22.getText().toString().trim()).show();
//                } else if (TextUtils.equals(f5_tv33_content, adjustAlpha(color, 1.0f) + "")) {
//                    Toasty.normal(getActivity(), f5_tv33.getText().toString().trim()).show();
//                } else if (TextUtils.equals(f5_tv44_content, adjustAlpha(color, 1.0f) + "")) {
//                    Toasty.normal(getActivity(), f5_tv44.getText().toString().trim()).show();
//                } else if (TextUtils.equals(f5_tv55_content, adjustAlpha(color, 1.0f) + "")) {
//                    Toasty.normal(getActivity(), f5_tv55.getText().toString().trim()).show();
//                }
            }

            @Override
            public void onColorSelected(@ColorInt int color) {
                MyLogUtil.e("geek111111", "" + color);
                MyLogUtil.e("geek111111", "" + shadeslider.adjustAlpha(color, 1.0f));
                if (TextUtils.equals(f5_tv11_content, shadeslider.adjustAlpha(color, 1.0f) + "")) {
                    ToastUtils.showLong(f5_tv11.getText().toString().trim());
                } else if (TextUtils.equals(f5_tv22_content, shadeslider.adjustAlpha(color, 1.0f) + "")) {
                    ToastUtils.showLong(f5_tv22.getText().toString().trim());
                } else if (TextUtils.equals(f5_tv33_content, shadeslider.adjustAlpha(color, 1.0f) + "")) {
                    ToastUtils.showLong(f5_tv33.getText().toString().trim());
                } else if (TextUtils.equals(f5_tv44_content, shadeslider.adjustAlpha(color, 1.0f) + "")) {
                    ToastUtils.showLong(f5_tv44.getText().toString().trim());
                } else if (TextUtils.equals(f5_tv55_content, shadeslider.adjustAlpha(color, 1.0f) + "")) {
                    ToastUtils.showLong(f5_tv55.getText().toString().trim());
                }
            }
        });
        donetwork();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.f1_tv1) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv1");
            data.add("geek2f1_tv1");
            data.add("geek3f1_tv1");
            data.add("geek4f1_tv1");
            data.add("geek5f1_tv1");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv1", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv1.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv2) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv2");
            data.add("geek2f1_tv2");
            data.add("geek3f1_tv2");
            data.add("geek4f1_tv2");
            data.add("geek5f1_tv2");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv2", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv2.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv3) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv3");
            data.add("geek2f1_tv3");
            data.add("geek3f1_tv3");
            data.add("geek4f1_tv3");
            data.add("geek5f1_tv3");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv3", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv3.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv4) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv4");
            data.add("geek2f1_tv4");
            data.add("geek3f1_tv4");
            data.add("geek4f1_tv4");
            data.add("geek5f1_tv4");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv4", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv4.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv5) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv5");
            data.add("geek2f1_tv5");
            data.add("geek3f1_tv5");
            data.add("geek4f1_tv5");
            data.add("geek5f1_tv5");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv5", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv5.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv6) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv6");
            data.add("geek2f1_tv6");
            data.add("geek3f1_tv6");
            data.add("geek4f1_tv6");
            data.add("geek5f1_tv6");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv6", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv6.setText(aaaa);
                }
            });
        } else if (id == R.id.f1_tv7) {
            List<String> data = new ArrayList<>();
            data.add("geek1f1_tv7");
            data.add("geek2f1_tv7");
            data.add("geek3f1_tv7");
            data.add("geek4f1_tv7");
            data.add("geek5f1_tv7");
            setpop(data, R.layout.pop_hxkt_banji, "f1_tv7", new InitSuccessListener2() {
                @Override
                public void onSuccess(String aaaa) {
                    f1_tv7.setText(aaaa);
                }
            });
        } else if (id == R.id.submitTv) {
            if (TextUtils.isEmpty(f1_tv1.getText().toString().trim())) {
                ToastUtils.showLong("请选择最高气温");
                return;
            }
            if (TextUtils.isEmpty(f1_tv2.getText().toString().trim())) {
                ToastUtils.showLong("请选择最下相对湿度");
                return;
            }
            if (TextUtils.isEmpty(f1_tv3.getText().toString().trim())) {
                ToastUtils.showLong("请选择最近一次降水时间");
                return;
            }
            if (TextUtils.isEmpty(f1_tv4.getText().toString().trim())) {
                ToastUtils.showLong("请选择降雨量");
                return;
            }
            if (TextUtils.isEmpty(f1_tv5.getText().toString().trim())) {
                ToastUtils.showLong("请选择最大风力等级");
                return;
            }
            if (TextUtils.isEmpty(f1_tv6.getText().toString().trim()) || TextUtils.isEmpty(f1_tv7.getText().toString().trim())) {
                ToastUtils.showLong("请选择覆盖率");
                return;
            }
            shadeslider.setShadePosition(1);
            if (TextUtils.equals(f5_tv11_content, shadeslider.adjustAlpha(shadeslider.getColor(), 1.0f) + "")) {
                ToastUtils.showLong(f5_tv11.getText().toString().trim());
            } else if (TextUtils.equals(f5_tv22_content, shadeslider.adjustAlpha(shadeslider.getColor(), 1.0f) + "")) {
                ToastUtils.showLong(f5_tv22.getText().toString().trim());
            } else if (TextUtils.equals(f5_tv33_content, shadeslider.adjustAlpha(shadeslider.getColor(), 1.0f) + "")) {
                ToastUtils.showLong(f5_tv33.getText().toString().trim());
            } else if (TextUtils.equals(f5_tv44_content, shadeslider.adjustAlpha(shadeslider.getColor(), 1.0f) + "")) {
                ToastUtils.showLong(f5_tv44.getText().toString().trim());
            } else if (TextUtils.equals(f5_tv55_content, shadeslider.adjustAlpha(shadeslider.getColor(), 1.0f) + "")) {
                ToastUtils.showLong(f5_tv55.getText().toString().trim());
            }
        } else if (id == R.id.f5_ll1) {
            if (f5_scb1.isChecked()) {
                //设置取消
                if (f5_scb2.isChecked()) {
                    Toasty.normal(getActivity(), "请先取消!").show();
                    return;
                }
                f5_scb1.setChecked(false, true);
                f5_scb2.setChecked(false, false);
            } else {
                //设置选中
                f5_scb1.setChecked(true, true);
                f5_scb2.setChecked(false, false);
            }
        } else if (id == R.id.f5_ll2) {
            if (f5_scb2.isChecked()) {
                //设置取消
                if (f5_scb1.isChecked()) {
                    Toasty.normal(getActivity(), "请先取消!").show();
                    return;
                }
                f5_scb2.setChecked(false, true);
                f5_scb1.setChecked(false, false);
            } else {
                //设置选中
                f5_scb2.setChecked(true, true);
                f5_scb1.setChecked(false, false);
            }
        }
    }


    private void setpop(final List<String> data, int layout1, String tag1, final InitSuccessListener2 mListener) {
        final BottomDialog bottomDialog = BottomDialog.create(getActivity().getSupportFragmentManager());
        bottomDialog.setViewListener(new BottomDialog.ViewListener() {
            String content = "";

            @Override
            public void bindView(View v) {
                // // You can do any of the necessary the operation with the view
                TextView tv111 = v.findViewById(R.id.tv1);
                TextView tv222 = v.findViewById(R.id.tv2);
                final WheelPicker wheelPicker = v.findViewById(R.id.wheelview11);
                wheelPicker.setData(data);
                //
                tv111.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomDialog.setViewListener(null);
                        bottomDialog.dismiss();
                    }
                });
                //
                tv222.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomDialog.setViewListener(null);
                        bottomDialog.dismiss();
                        if (TextUtils.isEmpty(content)) {
                            content = wheelPicker.getData().get(0).toString();
//                            f1_tv7.setText(content);
                        } else {
//                            f1_tv7.setText(content);
                        }
                        if (mListener != null) {
                            mListener.onSuccess(content);
                        }

                        //更新资料
//                            for (int i = 0; i < hCategoryBean.getList().size(); i++) {
//                                if (TextUtils.equals(content, hCategoryBean.getList().get(i).getName())) {
//                                    nianji = hCategoryBean.getList().get(i).getCode();
//                                }
//                            }
//                            hUserInfoupdatePresenter.get_myaddressupdate(sex, nianji, name, swChildId, swUserId, address);
                    }
                });
                //
                wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(WheelPicker picker, Object data, int position) {
                        content = data.toString();
                    }
                });
            }
        }).setLayoutRes(layout1)
                .setDimAmount(0.2f)            // Dialog window dim amount(can change window background color）, range：0 to 1，default is : 0.2f
                .setCancelOutside(true)     // click the external area whether is closed, default is : true
                .setTag(tag1)     // setting the DialogFragment tag
                .show();
    }


    @Override
    public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
        int id = checkBox.getId();
        if (id == R.id.f5_scb1) {
            if (isChecked) {
//                jzmm_xuanzhong();
                Toasty.normal(getActivity(), "f1_tv6").show();
            } else {
//                jzmm_quxiao();
                f1_tv6.setText("");
            }
        } else if (id == R.id.f5_scb2) {
            if (isChecked) {
//                zddl_xuanzhong();
                Toasty.normal(getActivity(), "f1_tv7").show();
            } else {
//                zddl_quxiao();
                f1_tv7.setText("");
            }
        }
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */


}
