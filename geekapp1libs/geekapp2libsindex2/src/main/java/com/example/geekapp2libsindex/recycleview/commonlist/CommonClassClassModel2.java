package com.example.geekapp2libsindex.recycleview.commonlist;

import java.io.Serializable;
import java.util.List;

public class CommonClassClassModel2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String text1;// title
    private List<CommonClassClassCommonbean> list;// text

    public CommonClassClassModel2() {
    }

    public CommonClassClassModel2(String text1, List<CommonClassClassCommonbean> list) {
        this.text1 = text1;
        this.list = list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public List<CommonClassClassCommonbean> getList() {
        return list;
    }

    public void setList(List<CommonClassClassCommonbean> list) {
        this.list = list;
    }
}
