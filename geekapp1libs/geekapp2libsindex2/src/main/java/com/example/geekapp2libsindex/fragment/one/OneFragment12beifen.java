//package com.example.geekapp2libsindex.fragment.one;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.View;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.core.widget.NestedScrollView;
//import androidx.recyclerview.widget.GridLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.blankj.utilcode.util.AppUtils;
//import com.blankj.utilcode.util.ToastUtils;
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.example.biz3hxktindex.bean.HClasscategoryBean;
//import com.example.biz3hxktindex.bean.HClasscategoryBean1;
//import com.example.biz3hxktindex.bean.HClasscategoryBean2;
//import com.example.biz3hxktindex.bean.HLunbotuBean;
//import com.example.biz3hxktindex.bean.HLunbotuBean1;
//import com.example.biz3hxktindex.presenter.HClasscategoryPresenter;
//import com.example.biz3hxktindex.presenter.HLunbotuPresenter;
//import com.example.biz3hxktindex.view.HClasscategoryView;
//import com.example.biz3hxktindex.view.HLunbotuView;
//import com.example.geekapp2libsindex.R;
//import com.example.geekapp2libsindex.pop.BanjiPopup;
//import com.example.geekapp2libsindex.pop.banji.BanjiCommonbean;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassAdapter;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassAdapterType;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassClassCommonbean;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassClassCommonbean2;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassClassCommonbean3;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassClassModel;
//import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassClassModel2;
//import com.example.slbappcomm.base.SlbBaseIndexFragment;
//import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
//import com.haier.cellarette.baselibrary.emptyview.EmptyView;
//import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
//import com.haier.cellarette.baselibrary.toasts2.Toasty;
//import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
//import com.haier.cellarette.libutils.CommonUtils;
//import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
//import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
//import com.scwang.smartrefresh.layout.SmartRefreshLayout;
//import com.scwang.smartrefresh.layout.api.RefreshHeader;
//import com.scwang.smartrefresh.layout.api.RefreshLayout;
//import com.scwang.smartrefresh.layout.header.ClassicsHeader;
//import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
//import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
//import com.scwang.smartrefresh.layout.util.DensityUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
//
//public class OneFragment12beifen extends SlbBaseIndexFragment implements HLunbotuView, HClasscategoryView {
//
//    private String ids;
//    private boolean is_zhu;
//    private TextView tv1;
//    private SmartRefreshLayout refreshLayout1;
//    private ClassicsHeader smartHeader1;
//    private EmptyViewNew1 emptyview1;
//    private NestedScrollView scrollView1;
//    private XRecyclerView recyclerView1;
//    private CommonClassAdapter mAdapter1;
//    private List<CommonClassAdapterType> mList1;
//    //
//    private int mOffset = 0;
//    private int mScrollY = 0;
//    private int scrolly = 0;
//
//    //
//    private List<HLunbotuBean1> mListbanner1;
//    private HLunbotuPresenter presenter1;
//    private HClasscategoryPresenter presenter2;
//
//
//    @Override
//    public void call(Object value) {
//        ids = (String) value;
//        Toasty.normal(getActivity(), ids).show();
//        if (tv1 != null) {
//            tv1.setText(ids);
//        }
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Bundle bundle = this.getArguments();
//        if (bundle != null) {
//            ids = bundle.getString("id");
//        }
//    }
//
//    @Override
//    public void loadData() {
//        super.loadData();
//        donetwork();
//    }
//
//
//    private MessageReceiverIndex mMessageReceiver;
//    private boolean is_first;
//
//    public class MessageReceiverIndex extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            try {
//                if ("OneFragment12".equals(intent.getAction())) {
//                    scrollView1.smoothScrollTo(0, 0);
//                }
//                if ("OneFragment12_1".equals(intent.getAction())) {
//                    //
//                    ids = intent.getStringExtra("OneFragment12_1");
//                    is_zhu = intent.getBooleanExtra("OneFragment12_1", true);
//                    if (!is_zhu) {
//                        // 下啦刷新
//                        donetwork();
//                    } else {
////                        if (!is_first){
////                            is_first = true;
////                            donetwork();
////                            return;
////                        }
////                        donetwork();
//                    }
//                    MyLogUtil.e("---geekyun--", ids);
//                    ToastUtils.showLong(ids);
//                }
//
//            } catch (Exception e) {
//            }
//        }
//    }
//
//    @Override
//    public void onDestroyView() {
//        MyLogUtil.e("--geekyun---", "销毁了～");
//        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
//        presenter1.onDestory();
//        presenter2.onDestory();
//        super.onDestroyView();
//    }
//
//    @Override
//    protected int getLayoutId() {
//        return R.layout.fragment_hxkt_one12;
//    }
//
//    @Override
//    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
//        super.setup(rootView, savedInstanceState);
//        tv1 = rootView.findViewById(R.id.tv1);
//        //
//        MyLogUtil.e("---geekyun--", "进来了～OneFragment12～");
//        mMessageReceiver = new MessageReceiverIndex();
//        IntentFilter filter = new IntentFilter();
//        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
//        filter.addAction("OneFragment12");
//        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
//        //
//        emptyview1 = rootView.findViewById(R.id.emptyview1);
//        scrollView1 = rootView.findViewById(R.id.scroll_view1);
//        recyclerView1 = rootView.findViewById(R.id.recycler_view1);
//        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1);
//        smartHeader1 = rootView.findViewById(R.id.smart_header1);
//        //
//        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(final RefreshLayout refreshLayout) {
//                // 分布局
//                is_zhu = false;
//                retryData();
//            }
//        });
//        smartHeader1.setEnableLastTime(false);
//        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
//            @Override
//            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
//                mOffset = offset / 2;
//            }
//
//            @Override
//            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
//                mOffset = offset / 2;
//            }
//        });
//        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
//            @Override
//            public void retry() {
//                //
//                retryData();
//            }
//        });
//        //
//        recyclerView1.setHasFixedSize(true);
//        recyclerView1.setNestedScrollingEnabled(false);
//        recyclerView1.setFocusable(false);
//        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
//        mList1 = new ArrayList<>();
//        mAdapter1 = new CommonClassAdapter(mList1);
//        mAdapter1.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
////                int type = mList1.get(position).type;
//                int type = Objects.requireNonNull(mAdapter1.getItem(position)).type;
//                if (type == CommonClassAdapterType.style1) {
//                    return 1;
//                } else if (type == CommonClassAdapterType.style2) {
//                    return 1;
//                } else {
//                    return 1;
//                }
//            }
//        });
//        recyclerView1.setAdapter(mAdapter1);
//        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                CommonClassAdapterType bean = (CommonClassAdapterType) adapter.getData().get(position);
////                if (view.getId() == R.id.tv0) {
////                    return;
////                }
////                Toasty.normal(getActivity(), bean.getmBean().getText1()).show();
////                MyLogUtil.e("--geekyun---", bean.getmBean().getText1());
//                //首页选课进来的课程详情
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
//                intent.putExtra("courseId", bean.getmBean().getText1());
//                startActivity(intent);
//            }
//        });
//        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                CommonClassAdapterType bean = (CommonClassAdapterType) adapter.getData().get(position);
//                int i = view.getId();
//                //首页选课进来的课程详情
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
//                intent.putExtra("courseId", bean.getmBean().getText1());
//                startActivity(intent);
////                Toasty.normal(getActivity(), bean.getmBean().getText1()).show();
////                MyLogUtil.e("--geekyun1---", bean.getmBean().getText1());
//
//            }
//        });
//        //
//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
//        //
//        mListbanner1 = new ArrayList<>();
//        presenter1 = new HLunbotuPresenter();
//        presenter1.onCreate(this);
//        presenter2 = new HClasscategoryPresenter();
//        presenter2.onCreate(this);
//    }
//
//    /**
//     * 第一次进来加载bufen
//     */
//    private void donetwork() {
//        emptyview1.loading();
//        retryData();
//    }
//
//    private String ids2;
//
//    private void retryData() {
//        emptyview1.loading();
//        presenter1.get_lunbotu();
//        BanjiCommonbean banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, BanjiCommonbean.class);
//        if (banjiCommonbean == null || TextUtils.equals(banjiCommonbean.getText2(), BanjiPopup.TAG_CONTENT)) {
//            ids2 = "";
//        } else {
//            ids2 = banjiCommonbean.getText1();
//        }
//        presenter2.get_classcategorylist(ids2, ids);
//    }
//
//
//    @Override
//    public void OnLunbotuSuccess(HLunbotuBean hLunbotuBean) {
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
//
//    }
//
//    @Override
//    public void OnLunbotuNodata(String s) {
//        refreshLayout1.finishRefresh(false);
//        emptyview1.nodata();
//    }
//
//    @Override
//    public void OnLunbotuFail(String s) {
//        refreshLayout1.finishRefresh(false);
//        emptyview1.errorNet();
//    }
//
//
//    @Override
//    public void OnClasscategorySuccess(HClasscategoryBean hClasscategoryBean) {
//        mList1 = new ArrayList<>();
////        String gson_url = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity().getApplicationContext()).getJson(getActivity().getApplicationContext(), "jsonbean/classbean.json");
////        CommonClassClassModel bean_gson = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity().getApplicationContext()).JsonToObject(gson_url, CommonClassClassModel.class);
//        //
//        CommonClassClassModel bean_gson = new CommonClassClassModel();
//        List<CommonClassClassModel2> bean_list = new ArrayList<>();
//        List<HClasscategoryBean1> list = hClasscategoryBean.getList();
//        for (int j1 = 0; j1 < list.size(); j1++) {
//            HClasscategoryBean1 hClasscategoryBean1 = list.get(j1);
//            List<HClasscategoryBean2> list1 = hClasscategoryBean1.getCourseList();
//            //
//            CommonClassClassModel2 bean1 = new CommonClassClassModel2();
//            bean1.setText1(hClasscategoryBean1.getCourseTypeName());
//            List<CommonClassClassCommonbean> list10 = new ArrayList<>();
//            for (int j2 = 0; j2 < list1.size(); j2++) {
//                CommonClassClassCommonbean commonClassClassCommonbean1 = new CommonClassClassCommonbean();
//                //
//                List<CommonClassClassCommonbean3> bean_list10 = new ArrayList<>();
//                for (String bean4 : list1.get(j2).getSubjectNames()) {
//                    CommonClassClassCommonbean3 commonClassClassCommonbean3 = new CommonClassClassCommonbean3();
//                    if (TextUtils.equals("system", list1.get(j2).getCourseType())) {
//                        commonClassClassCommonbean3.setText1("#F78451");
//                    } else if (TextUtils.equals("experience", list1.get(j2).getCourseType())) {
//                        commonClassClassCommonbean3.setText1("#666666");
//                    }
//                    commonClassClassCommonbean3.setText2(bean4);
//                    bean_list10.add(commonClassClassCommonbean3);
//                }
//                commonClassClassCommonbean1.setText1(list1.get(j2).getId());
//                commonClassClassCommonbean1.setText1_list(bean_list10);
//                commonClassClassCommonbean1.setText2(list1.get(j2).getCourseName());
//                commonClassClassCommonbean1.setText3(list1.get(j2).getCourseTimeRange());
//                commonClassClassCommonbean1.setText4(list1.get(j2).getItemCount() + "课节");
//                commonClassClassCommonbean1.setText5(list1.get(j2).getSignUpCount() + "人已报名");
//                commonClassClassCommonbean1.setText6(list1.get(j2).getPriceMarked() + "");
//                commonClassClassCommonbean1.setText8(list1.get(j2).getPricePayable() + "");
//                //
//                List<CommonClassClassCommonbean2> bean_list1 = new ArrayList<>();
//                if (list1.get(j2).getTeacherList() != null && list1.get(j2).getTeacherList().size() > 0) {
//                    for (int j3 = 0; j3 < list1.get(j2).getTeacherList().size(); j3++) {
//                        CommonClassClassCommonbean2 commonClassClassCommonbean2 = new CommonClassClassCommonbean2();
//                        commonClassClassCommonbean2.setText1(list1.get(j2).getTeacherList().get(j3).getAvatar());
//                        commonClassClassCommonbean2.setText2(list1.get(j2).getTeacherList().get(j3).getNikeName());
//                        commonClassClassCommonbean2.setText3(list1.get(j2).getTeacherList().get(j3).getTeacherType());
//                        bean_list1.add(commonClassClassCommonbean2);
//                    }
//                }
//                commonClassClassCommonbean1.setList(bean_list1);
//                list10.add(commonClassClassCommonbean1);
//            }
//            bean1.setList(list10);
//            bean_list.add(bean1);
//        }
//        bean_gson.setList(bean_list);
//        for (int i = 0; i < bean_gson.getList().size(); i++) {
//            CommonClassClassModel2 commonClassClassModel2 = bean_gson.getList().get(i);
//            for (int j = 0; j < commonClassClassModel2.getList().size(); j++) {
//                if (j == 0) {
//                    CommonClassClassCommonbean classCommonbean = new CommonClassClassCommonbean();
//                    classCommonbean.setText1(commonClassClassModel2.getText1());
//                    mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style1, classCommonbean));
//                }
//                mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style2, commonClassClassModel2.getList().get(j)));
//            }
//        }
//        mAdapter1.setNewData(mList1);
//    }
//
//    @Override
//    public void OnClasscategoryNodata(String s) {
//
//    }
//
//    @Override
//    public void OnClasscategoryFail(String s) {
//
//    }
//}
