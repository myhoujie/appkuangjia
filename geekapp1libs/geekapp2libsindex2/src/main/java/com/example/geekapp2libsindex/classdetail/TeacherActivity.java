package com.example.geekapp2libsindex.classdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.listener.InitSuccessListener;
import com.example.geekapp2libsindex.pop.CopyShareSuccessPopup;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.pop.share.PopForShare;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;
import com.fanli.biz3hxktdetail.presenter.TeacherPresenter;
import com.fanli.biz3hxktdetail.view.TeacherView;
import com.geek.libglide47.base.GlideImageView;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupAnimation;

import java.util.ArrayList;
import java.util.List;

public class TeacherActivity extends SlbBaseActivity implements TeacherView, InitSuccessListener {

    private ImageView shareIv;
    private GlideImageView topIv;
    private TextView nameTv;
    private TextView infoTv;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;
    private TeacherPresenter presenter;
    private TeacherInfoFragment teacherInfoFragment;
    private ShareInfoBean shareInfoBean;
    private String teacherId;
    private EmptyViewNew1 emptyView;

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_teacher;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.setup(savedInstanceState);
        teacherId = getIntent().getStringExtra("teacherId");
        findview();
        onclick();
        presenter = new TeacherPresenter();
        presenter.onCreate(this);
        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(), getTitleList(), getFragmentList());
        viewPager.setAdapter(viewPageAdapter);

        tabLayout.setTabIndicatorFullWidth(false);
        TabUtils.setIndicatorWidth(tabLayout, 50);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);
    }


    private void doNetWork() {
        emptyView.loading();
        teacherId = getIntent().getStringExtra("teacherId");
        presenter.getTeacherInfo(teacherId);
    }

    private List<String> getTitleList() {
        List<String> titleList = new ArrayList();
        titleList.add("介绍");
        titleList.add("课程");
        return titleList;
    }

    private List<Fragment> getFragmentList() {
        List<Fragment> fragmentList = new ArrayList();
        teacherInfoFragment = TeacherInfoFragment.newInstance();
        teacherInfoFragment.setInitSuccessListener(this);
        fragmentList.add(teacherInfoFragment);
        fragmentList.add(TeacherSessionListFragment.newInstance(teacherId));
        return fragmentList;
    }


    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("老师介绍");
        View ll_wrap = findViewById(R.id.ll_wrap);
//        emptyView = new EmptyViewNew1(this);
//        emptyView.attach(ll_wrap);
        emptyView = findViewById(R.id.emptyview1);
        emptyView.bind(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        shareIv = findViewById(R.id.shareIv);
        shareIv.setVisibility(View.VISIBLE);
        topIv = findViewById(R.id.topIv);
        nameTv = findViewById(R.id.nameTv);
        infoTv = findViewById(R.id.infoTv);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewpager);
//        emptyView.loading();
    }

    private void onclick() {
        shareIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });
    }

    // 试读完成弹窗bufen
    private PopForShare popSharePay;
    private String share_title = "";
    private String share_name = "";
    private String share_img = "";
    private String share_imgUrl = "";
    private String share_imgUrl2 = "";
    private String share_url = "";

    private void share() {
        popSharePay = new PopForShare(this, share_title, share_name, share_img, share_imgUrl, share_imgUrl2, share_url, "", "", "", "", new PopForShare.OnFinishResultClickListener() {
            @Override
            public void onWeChat1() {
                // 微信小程序

            }

            @Override
            public void onWeChat2() {
                // 朋友圈
                new XPopup.Builder(TeacherActivity.this)
                        .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                        .autoOpenSoftInput(true)
                        .asCustom(new CopyShareSuccessPopup(TeacherActivity.this, "分享成功")).show();
            }
        });

    }


    @Override
    public void OnTeacherInfoSuccess(TeacherBean teacherBean) {
        emptyView.success();
        share_title = teacherBean.getShareInfo().getTitle();
        share_name = teacherBean.getShareInfo().getDesc();
        share_img = teacherBean.getShareInfo().getImgUrl();
        share_imgUrl = teacherBean.getShareInfo().getUrlForFriend();
        share_imgUrl2 = teacherBean.getShareInfo().getUrlForMoments();
        share_url = teacherBean.getShareInfo().getShareUrl();
        topIv.loadImage(teacherBean.getOfficePic(), R.drawable.default_teather);
        nameTv.setText(teacherBean.getName());
        infoTv.setText(teacherBean.getDescription());
        teacherInfoFragment.onTeacherReceive(teacherBean);
    }

    @Override
    public void OnTeacherInfoNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OnTeacherInfoFail(String s) {
        emptyView.errorNet();
    }

    @Override
    public void OnTeacherSessionListSuccess(List<CourseBean> list) {

    }

    @Override
    public void OnTeacherSessionListNodata(String s) {

    }

    @Override
    public void OnTeacherSessionListFail(String s) {

    }

    @Override
    public void onSuccess() {
        doNetWork();
    }

}
