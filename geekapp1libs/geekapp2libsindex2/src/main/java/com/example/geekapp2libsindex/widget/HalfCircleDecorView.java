package com.example.geekapp2libsindex.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.geek.libglide47.base.util.DisplayUtil;

public class HalfCircleDecorView extends LinearLayout {
    private Paint mPaint;
    private float d;
    private float radius;
    private int num;
    private int w;
    private float tbGap;//上下圆角半径
    private float gap = 12;//半圆间距

    public HalfCircleDecorView(@NonNull Context context) {
        this(context, null);
    }

    public HalfCircleDecorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HalfCircleDecorView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        d = 14;
        radius = d / 2f;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setDither(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.w = w;
        tbGap = DisplayUtil.dip2px(getContext(), 6);
        num = (int) ((h - 2 * tbGap + gap) / (gap + d));
        tbGap = (h - (num * d + (num - 1) * gap)) / 2f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < num; i++) {
            canvas.drawCircle(w, tbGap + radius + (d + gap) * i, radius, mPaint);
        }
    }
}
