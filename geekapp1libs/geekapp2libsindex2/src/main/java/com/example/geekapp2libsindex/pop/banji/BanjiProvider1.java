package com.example.geekapp2libsindex.pop.banji;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.geekapp2libsindex.R;

public class BanjiProvider1 extends BaseItemProvider<BanjiAdapterType, BaseViewHolder> {

    @Override
    public int viewType() {
        return BanjiAdapter.STYLE_ONE;
    }

    @Override
    public int layout() {
        return R.layout.pop_hxkt_banjiprovider_item1;
    }

    @Override
    public void convert(BaseViewHolder helper, BanjiAdapterType data, int position) {
        helper.setText(R.id.tv1, data.getmBean().getName());
    }
}
