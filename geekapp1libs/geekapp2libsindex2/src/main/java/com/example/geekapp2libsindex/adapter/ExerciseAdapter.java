package com.example.geekapp2libsindex.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.HZuoyeinfoWrapperBean;
import com.example.geekapp2libsindex.domain.UnionPicBean;
import com.example.geekapp2libsindex.widget.PicHelper;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.geek.libglide47.base.util.DisplayUtil;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

public class ExerciseAdapter extends BaseQuickAdapter<HZuoyeinfoWrapperBean, BaseViewHolder> implements PicHelper.OnPicOperateListener {
    private OnListener onListener;
    private Activity activity;

    public ExerciseAdapter(int layoutResId, Activity activity) {
        super(layoutResId);
        this.activity = activity;
    }

    @Override
    protected void convert(BaseViewHolder helper, HZuoyeinfoWrapperBean item) {
        int position = helper.getAdapterPosition();
        View fl_result = helper.itemView.findViewById(R.id.fl_result);
        TextView countTitleTv = helper.itemView.findViewById(R.id.countTitleTv);
        RecyclerView pictureRv = helper.itemView.findViewById(R.id.pictureRv);
        TextView waitingTv = helper.itemView.findViewById(R.id.waitingTv);
        View ll_result = helper.itemView.findViewById(R.id.ll_result);
        TextView resultTv = helper.itemView.findViewById(R.id.resultTv);
        TextView teacherInfoTv = helper.itemView.findViewById(R.id.teacherInfoTv);
        View lineV = helper.itemView.findViewById(R.id.lineV);
        if (getData().size() <= 1) {//少于一次提交记录，不显示提交次数
            countTitleTv.setVisibility(View.GONE);
        } else {
            if (TextUtils.isEmpty(item.getId())) {
                countTitleTv.setVisibility(View.GONE);
            } else {
                int count = TextUtils.isEmpty(getData().get(0).getId()) ? (getData().size() - position - 1) : (getData().size() - position);
                countTitleTv.setText("第" + count + "次提交");
                countTitleTv.setVisibility(View.VISIBLE);
            }
        }

        if (position == getData().size() - 1) {
            lineV.setVisibility(View.INVISIBLE);
        } else {
            lineV.setVisibility(View.VISIBLE);
        }

        //是否提交过
        if (TextUtils.isEmpty(item.getId())) {//还没提交过
            fl_result.setVisibility(View.GONE);
        } else {
            fl_result.setVisibility(View.VISIBLE);
            //练习状态(0-未布置,1-未提交,2-待批改,3-已批改 )
            if (TextUtils.equals(item.getExerciseStatus(), "2")) {
                ll_result.setVisibility(View.GONE);
                waitingTv.setVisibility(View.VISIBLE);
            } else {
                waitingTv.setVisibility(View.GONE);
                ll_result.setVisibility(View.VISIBLE);

                resultTv.setText(item.getExerciseSuggest().replace("|", "\n"));
                StringBuilder teacherInfo = new StringBuilder();
                for (int i = 0; i < item.getTeachers().size(); i++) {
                    teacherInfo.append(item.getTeachers().get(i)).append("\n");
                }
                teacherInfoTv.setText(teacherInfo.toString());
            }
        }

        UploadExercisePicAdapter uploadExercisePicAdapter = new UploadExercisePicAdapter(R.layout.adapter_classtest3_pic, activity, item.getExerciseStatus());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3, LinearLayoutManager.VERTICAL, false) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        if (pictureRv.getItemDecorationCount() == 0) {
            pictureRv.addItemDecoration(new SpaceItemDecoration(mContext));
        }
        pictureRv.setNestedScrollingEnabled(false);
        pictureRv.setLayoutManager(gridLayoutManager);
        pictureRv.setAdapter(uploadExercisePicAdapter);
        uploadExercisePicAdapter.setNewData(item.getUnionPicBeanList());
    }

    @Override
    public void onPicSelect(List<LocalMedia> selectList) {//获取选中的图片
        if (!ListUtil.isEmpty(selectList)) {
            List<UnionPicBean> newSelectUnionPicBeanList = new ArrayList<>();
            for (LocalMedia localMedia : selectList) {
                newSelectUnionPicBeanList.add(createUnionPicBean(localMedia));
            }

            if (newSelectUnionPicBeanList.size() < 9) {
                newSelectUnionPicBeanList.add(new UnionPicBean());//添加照相机图片
            }

            //判断第一个是否已经存在本地选择的图片
            if (!TextUtils.isEmpty(getData().get(0).getId())) {//本地还没有新选图片
                HZuoyeinfoWrapperBean zuoyeinfoBean = new HZuoyeinfoWrapperBean();
                zuoyeinfoBean.setUnionPicBeanList(newSelectUnionPicBeanList);
                getData().add(0, zuoyeinfoBean);
                notifyItemInserted(0);
            } else {//本地已经选过，不需要重新插入头部，更新图片即可
                getData().get(0).setUnionPicBeanList(newSelectUnionPicBeanList);
                notifyItemChanged(0);
            }
            if (onListener != null) {
                onListener.onInsertToFirst();
            }
        }
    }

    private UnionPicBean createUnionPicBean(LocalMedia localMedia) {
        UnionPicBean unionPicBean = new UnionPicBean();
        unionPicBean.setPath(localMedia.getPath());
        unionPicBean.setCompressPath(localMedia.getCompressPath());
        unionPicBean.setCutPath(localMedia.getCutPath());
        unionPicBean.setDuration(localMedia.getDuration());
        unionPicBean.setChecked(localMedia.isChecked());
        unionPicBean.setCut(localMedia.isCut());
        unionPicBean.setPosition(localMedia.getPosition());
        unionPicBean.setNum(localMedia.getNum());
        unionPicBean.setMimeType(localMedia.getMimeType());
        unionPicBean.setPictureType(localMedia.getPictureType());
        unionPicBean.setCompressed(localMedia.isCompressed());
        unionPicBean.setWidth(localMedia.getWidth());
        unionPicBean.setHeight(localMedia.getHeight());
        return unionPicBean;
    }

    public static class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        int lrSpace, tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 8);
            tbSpace = DisplayUtil.dip2px(context, 12);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = tbSpace;
            int position = parent.getChildAdapterPosition(view);
            if (position < 3) {
                outRect.top = tbSpace;
            }
            outRect.left = outRect.right = lrSpace;
        }
    }

    public interface OnListener {
        void onInsertToFirst();
    }

    public void setOnListener(OnListener onListener) {
        this.onListener = onListener;
    }
}
