package com.example.geekapp2libsindex.domain;

import com.luck.picture.lib.entity.LocalMedia;

public class UnionPicBean extends LocalMedia {
    private String correctFlag;
    private String id;
    private String picUrl;

    public String getCorrectFlag() {
        return correctFlag;
    }

    public void setCorrectFlag(String correctFlag) {
        this.correctFlag = correctFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
