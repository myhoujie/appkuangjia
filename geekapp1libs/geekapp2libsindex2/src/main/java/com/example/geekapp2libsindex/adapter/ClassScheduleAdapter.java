package com.example.geekapp2libsindex.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.ScheduleBean;

public class ClassScheduleAdapter extends BaseQuickAdapter<ScheduleBean, BaseViewHolder> {

    public ClassScheduleAdapter() {
        super(R.layout.adapter_classdetail_schdeule);
    }

    @Override
    protected void convert(BaseViewHolder helper, ScheduleBean item) {
        TextView noTv = helper.itemView.findViewById(R.id.noTv);
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView dateTv = helper.itemView.findViewById(R.id.dateTv);
        noTv.setText((helper.getAdapterPosition() + 1) + "");
        titleTv.setText(item.getItemTitle());
        dateTv.setText(item.getItemTimeRange());
    }
}
