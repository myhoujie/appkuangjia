package com.example.geekapp2libsindex.pop.banji;

import java.io.Serializable;

public class BanjiCommonbean implements Serializable {
    private String text1;// id
    private String text2;// text
    private boolean text3;// is_xuanzhong
    private int text4;// is_enable

    public BanjiCommonbean() {
    }

    public BanjiCommonbean(String text1, String text2, boolean text3, int text4) {
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
        this.text4 = text4;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public boolean isText3() {
        return text3;
    }

    public void setText3(boolean text3) {
        this.text3 = text3;
    }

    public int getText4() {
        return text4;
    }

    public void setText4(int text4) {
        this.text4 = text4;
    }
}
