package com.example.geekapp2libsindex.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.ClassSectionBean;

public class ClassSectionAdapter extends BaseQuickAdapter<ClassSectionBean, BaseViewHolder> {
    private int toSectionPosition = -1;
    private int pos = -1;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public ClassSectionAdapter() {
        super(R.layout.adapter_classdetail_section);
//        setHasStableIds(true);
    }

    public ClassSectionAdapter(int toSectionPosition) {
        this();
        this.toSectionPosition = toSectionPosition;
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassSectionBean item) {
        final ConstraintLayout c11 = helper.itemView.findViewById(R.id.c1);
        TextView noTv = helper.itemView.findViewById(R.id.noTv);
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView dateTv = helper.itemView.findViewById(R.id.dateTv);
        TextView huifangTv = helper.itemView.findViewById(R.id.huifangTv);
        TextView lianxiTv = helper.itemView.findViewById(R.id.lianxiTv);
        TextView baogaoTv = helper.itemView.findViewById(R.id.baogaoTv);

        View huifangV = helper.itemView.findViewById(R.id.huifangV);
        View lianxiV = helper.itemView.findViewById(R.id.lianxiV);
        View baogaoV = helper.itemView.findViewById(R.id.baogaoV);

        helper.addOnClickListener(R.id.ll_huifang);
        helper.addOnClickListener(R.id.ll_lianxi);
        helper.addOnClickListener(R.id.ll_baogao);

        noTv.setText((helper.getAdapterPosition() + 1) + "");
        titleTv.setText(item.getItemTitle());
        dateTv.setText(item.getItemTimeRange());
        if (helper.getAdapterPosition() + 1 == toSectionPosition) {
            if (on_huxi != null) {
                on_huxi.OnFinish(c11);
            }
            c11.setBackgroundColor(Color.parseColor("#F7F7F7"));
        } else {
            c11.setBackgroundColor(Color.WHITE);
        }
        //播放状态(0, "未开始"), (1, "直播中"), (2, "看回放"), (3, "已结束");
        switch (item.getItemStatus()) {
            case "0":
                huifangTv.setText("未开始");
                huifangTv.setEnabled(false);
                huifangV.setEnabled(false);
                break;
            case "1":
                huifangTv.setText("直播中");
                huifangTv.setEnabled(true);
                huifangV.setEnabled(true);
                break;
            case "2":
                huifangTv.setText("看回放");
                huifangTv.setEnabled(true);
                huifangV.setEnabled(true);
                break;
            case "3":
                huifangTv.setText("已结束");
                huifangTv.setEnabled(false);
                huifangV.setEnabled(false);
                break;
        }
        //练习状态(0, "未布置"), (1, "待提交"), (2, "批改0中"), (3, "已批改")
        switch (item.getExerciseStatus()) {
            case "0":
                lianxiTv.setText("未布置");
                lianxiTv.setEnabled(false);
                lianxiV.setEnabled(false);
                break;
            case "1":
                lianxiTv.setText("待提交");
                lianxiTv.setEnabled(true);
                lianxiV.setEnabled(true);
                break;
            case "2":
                lianxiTv.setText("批改中");
                lianxiTv.setEnabled(true);
                lianxiV.setEnabled(true);
                break;
            case "3":
                lianxiTv.setText("已批改");
                lianxiTv.setEnabled(true);
                lianxiV.setEnabled(true);
                break;
        }

        //是否有报告(0:没有,1:有)
        switch (item.getHasReport()) {
            case "0":
                baogaoTv.setText("未生成");
                baogaoTv.setEnabled(false);
                baogaoV.setEnabled(false);
                break;
            case "1":
                baogaoTv.setText("可查看");
                baogaoTv.setEnabled(true);
                baogaoV.setEnabled(true);
                break;
        }
//        if (helper.getAdapterPosition() == toSectionPosition) {
//
//        } else {
//            helper.itemView.setBackgroundColor(Color.WHITE);
//        }
//        huifangTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (true) {//看回放
////                    HuifangPopup popup = (HuifangPopup) new XPopup.Builder(context)
////                            .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
////                            .asCustom(new ClassDetailServerPopup(context)/*.enableDrag(false)*/)
////                            .show();
////                    List<String> urlList = new ArrayList<>();
////                    urlList.add("http://sairobo.gensee.com/training/site/r/92195737");
////                    urlList.add("http://sairobo.gensee.com/training/site/r/92195737");
////                    urlList.add("http://sairobo.gensee.com/training/site/r/92195737");
////                    urlList.add("http://sairobo.gensee.com/training/site/r/92195737");
////                    popup.setDataList(urlList);
//
//                    Intent intent = new Intent("hs.act.slbapp.JiaoZiVideoActivity");
//                    intent.putExtra("videourl", "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4");
//                    intent.putExtra("goodsname", "测试");
//                    mContext.startActivity(intent);
//                } else {//看直播
//                    GenseeLiveHelper.startLive(mContext, "", "", "", "");
//                }
//            }
//        });
    }

    public interface On_huxi {
        void OnFinish(ConstraintLayout cl);
    }

    public On_huxi on_huxi;

    public On_huxi getOn_huxi() {
        return on_huxi;
    }

    public void setOn_huxi(On_huxi on_huxi) {
        this.on_huxi = on_huxi;
    }
}
