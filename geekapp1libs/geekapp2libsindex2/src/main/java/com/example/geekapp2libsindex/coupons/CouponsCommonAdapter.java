package com.example.geekapp2libsindex.coupons;

import android.os.Build;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HCouponsListBean1;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.widgets.HalfCircleDecorView2;
import com.ms.square.android.expandabletextview.ExpandableTextView;

public class CouponsCommonAdapter extends BaseQuickAdapter<HCouponsListBean1, BaseViewHolder> {

    private final SparseBooleanArray mCollapsedStatus;

    public CouponsCommonAdapter(int layoutResId) {
        super(layoutResId);
        mCollapsedStatus = new SparseBooleanArray();
    }

    @Override
    protected void convert(BaseViewHolder helper, HCouponsListBean1 item) {
        HalfCircleDecorView2 rl1 = helper.itemView.findViewById(R.id.rl1);
        TextView tv0 = helper.itemView.findViewById(R.id.tv0);
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        TextView expandable_text = helper.itemView.findViewById(R.id.expandable_text);
        ExpandableTextView tv6 = helper.itemView.findViewById(R.id.expand_text_view);
        if (TextUtils.equals(item.getCouponInfoState(), "0")) {
            // 未使用
            rl1.setBackgroundResource(R.drawable.hxkt_card_bg22);
            iv1.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color_FF701B, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color_333333, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color_FF701B));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color_333333));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        } else if (TextUtils.equals(item.getCouponInfoState(), "1")) {
            // 已使用
            rl1.setBackgroundResource(R.drawable.hxkt_card_bg_gray22);
            iv1.setVisibility(View.VISIBLE);
            iv1.setBackgroundResource(R.drawable.quan_used2);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        } else if (TextUtils.equals(item.getCouponInfoState(), "2")) {
            // 已过期
            rl1.setBackgroundResource(R.drawable.hxkt_card_bg_gray22);
            iv1.setVisibility(View.VISIBLE);
            iv1.setBackgroundResource(R.drawable.quan_guoqi1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999, mContext.getTheme()));
            } else {
                tv0.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv1.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv2.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv3.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv4.setTextColor(mContext.getResources().getColor(R.color.color999999));
                tv5.setTextColor(mContext.getResources().getColor(R.color.color999999));
                expandable_text.setTextColor(mContext.getResources().getColor(R.color.color999999));
            }
        }
        tv1.setText(item.getAmount());
        tv2.setText(item.getCouponTypeDesc());
        tv3.setText(item.getCouponName());
        tv4.setText(item.getUseTimeRange());
        if (TextUtils.equals(item.getStrShrink(), "0")) {
            tv5.setVisibility(View.VISIBLE);
            tv6.setVisibility(View.GONE);
            tv5.setText(item.getCouponRangeDesc());
        } else if (TextUtils.equals(item.getStrShrink(), "1")) {
            tv5.setVisibility(View.GONE);
            tv6.setVisibility(View.VISIBLE);
            String detail = "仅限指定课程使用\n" + item.getCouponRangeDesc().replaceAll(",", "    ");
            tv6.setText(detail, mCollapsedStatus, helper.getAdapterPosition());
        }
//        helper.addOnClickListener(R.id.tv7);
    }

}