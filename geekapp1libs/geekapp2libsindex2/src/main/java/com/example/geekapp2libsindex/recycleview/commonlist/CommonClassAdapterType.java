package com.example.geekapp2libsindex.recycleview.commonlist;

import com.example.biz3hxktindex.bean.HClasscategoryBean2;

import java.io.Serializable;

public class CommonClassAdapterType implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int style1 = 1;
    public static final int style2 = 2;

    public int type;
    private HClasscategoryBean2 mBean;

    public CommonClassAdapterType(int type) {
        this.type = type;
    }

    public CommonClassAdapterType(int type, HClasscategoryBean2 mBean) {
        this.type = type;
        this.mBean = mBean;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public HClasscategoryBean2 getmBean() {
        return mBean;
    }

    public void setmBean(HClasscategoryBean2 mBean) {
        this.mBean = mBean;
    }


}
