package com.example.geekapp2libsindex.fragment.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.haier.cellarette.baselibrary.toasts2.Toasty;


public class F2 extends SlbBaseIndexFragment {

    private String ids;
    private String tablayoutId;
    private ImageView iv_back1_order;
    private TextView tv_center_content;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f2;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        iv_back1_order.setVisibility(View.GONE);
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("保存到相册");
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */


}
