package com.example.geekapp2libsindex.classdetail;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.gensee.app.ApplicationUtil;
import com.gensee.app.ConfigApp;
import com.gensee.common.ServiceType;
import com.gensee.entity.InitParam;
import com.gensee.entity.JoinParams;
import com.gensee.entity.LoginResEntity;
import com.gensee.glive.DestopStartUpActivity;
import com.gensee.glive.vod.VodActivity;
import com.gensee.holder.join.Join;

public class LiveUtil {

    public static void startLive(Context context, GenseeBean genseeBean) {
        ApplicationUtil.init(context);
        JoinParams joinParams = new JoinParams();
        joinParams.setAppServiceType(ConfigApp.AppServiceType.TRAINING);
        joinParams.setJoinType(JoinParams.JOIN_LIVE_TYPE);
        joinParams.setNeedWatchWord(true);
        joinParams.setPubMode(JoinParams.PUB_MODE_DEF);

//        joinParams.setDomain("sairobo.gensee.com");
//        joinParams.setLiveId("HUHCnFXWRe");
//        joinParams.setName("qwerdf");
//        joinParams.setNumber("90539913");
//        joinParams.setPwd("121212");

        joinParams.setDomain(genseeBean.getDomain());
        joinParams.setLiveId(genseeBean.getSdkId());
        joinParams.setName(genseeBean.getNickname());
        joinParams.setNumber(genseeBean.getNumber());
        joinParams.setPwd(genseeBean.getToken());
        joinParams.setK(genseeBean.getK());
        joinParams.setUserId(Long.parseLong(genseeBean.getUserId()));
        live(context, joinParams);
    }

    public static void startTestLive(Context context) {
        JoinParams joinParams = new JoinParams();
        joinParams.setAppServiceType(ConfigApp.AppServiceType.TRAINING);
        joinParams.setJoinType(JoinParams.JOIN_LIVE_TYPE);
        joinParams.setNeedWatchWord(true);
        joinParams.setPubMode(JoinParams.PUB_MODE_DEF);

        joinParams.setDomain("sairobo.gensee.com");
        joinParams.setLiveId("HUHCnFXWRe");
        joinParams.setName("qwerdf");
        joinParams.setNumber("90539913");
        joinParams.setPwd("121212");
        live(context, joinParams);
    }


    public static void startLiveSelect(final Context context) {
        context.startActivity(new Intent(context, DestopStartUpActivity.class));
    }

    public static void startVod(final Context context, GenseeBean genseeBean) {
        ApplicationUtil.init(context);
        InitParam onlineParam = new InitParam();
//        onlineParam.setDomain("sairobo.gensee.com");
//        onlineParam.setJoinPwd("735008");
//        onlineParam.setNickName("sss");
//        onlineParam.setNumber("30164924");

        onlineParam.setDomain(genseeBean.getDomain());
        onlineParam.setJoinPwd(genseeBean.getToken());
        onlineParam.setNickName(genseeBean.getNickname());
        onlineParam.setNumber(genseeBean.getNumber());

        onlineParam.setDeviceType(256);
        onlineParam.setDownload(false);
        onlineParam.setK(genseeBean.getK());
        onlineParam.setLoginAccount("");
        onlineParam.setLoginPwd("");

        onlineParam.setServiceType(ServiceType.TRAINING);
        onlineParam.setUserData("");
        onlineParam.setUserId(Long.parseLong(genseeBean.getUserId()));

        Intent intent = new Intent();
        intent.putExtra(ConfigApp.VOD_JOIN_PARAM, onlineParam);
        intent.setClass(context, VodActivity.class);
        context.startActivity(intent);
    }


    private static void live(final Context context, JoinParams joinParams) {

        //跳转直播
        final Join join = new Join();
        join.setOnJoinListener(new Join.OnJoinListerner() {
            @Override
            public void needLogin(String subject, String number, int appServiceType, String domain, String webcastId, boolean needWatchWord) {
                Log.d("setOnJoinListener", "needLogin");
            }

            @Override
            public void needWatchWord(String subject, String sNumber, int appServiceType, String domain, String webcastId) {
                Log.d("setOnJoinListener", "needWatchWord");

            }

            @Override
            public void join(String lunachCode, LoginResEntity entity) {
                Log.d("setOnJoinListener", "join");
                join.login(context, lunachCode, entity);

            }

            @Override
            public void onError(int errorCode) {
                Log.d("setOnJoinListener", "onError" + errorCode);

            }

            @Override
            public void needNickName(String subject, String sNumber, int appServiceType, String domain, String webcastId) {
                Log.d("setOnJoinListener", "needLogin");

            }

            @Override
            public boolean getLiveEnterFlag() {
                return false;
            }
        });
        join.initWithGensee(joinParams, context);
    }


}
