package com.example.geekapp2libsindex.fragment.one;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import java.util.Objects;

//import androidx.core.view.PagerAdapter;

/**
 * 首页viewpager adapter
 */
public class OnePagerAdapter extends PagerAdapter {

    private Context context;

    public OnePagerAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return OneFactory1.PAGE_COUNT;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        MyLogUtil.d(OneFactory1.PAGE_LAYOUT_ID + position);
        int layoutId = context.getResources().getIdentifier(OneFactory1.PAGE_LAYOUT_ID + position, "layout", Objects.requireNonNull(context).getPackageName());
        if (layoutId == 0) {
            throw new UnsupportedOperationException("layout not found!");
        }
        View itemLayout = LayoutInflater.from(context).inflate(layoutId, container, false);
        container.addView(itemLayout);
        return itemLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}