package com.example.geekapp2libsindex.domain;


import java.io.Serializable;

/**
 * Created by geek on 2016/2/25.
 */
public class Address1Bean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String text1;// name
    private String text2;// tel
    private String text3;// address

    public Address1Bean() {
    }

    public Address1Bean(String text1, String text2, String text3) {
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }
}
