package com.example.geekapp2libsindex.pop;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.geek.libglide47.base.GlideImageView;
import com.geek.libglide47.glide48.GlideAppUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.util.XPopupUtils;
import com.umeng.analytics.MobclickAgent;

public class AboutWxQrcodePopup extends BottomPopupView {
    private View iconIv;
    private TextView titleTv;
    private TextView tipTv;
    private GlideImageView qrIv;
    private ImageView saveQrIv;
    private ImageView copyQrIv;
    private Context context;
    private String title;
    private String tip;
    private String qrUrl;
    private String qrId;
    private boolean showIcon;
    private String mobclickSave;
    private String mobclickCopy;
    private boolean copyGzh;

    public AboutWxQrcodePopup(@NonNull Context context, String title, String tip, boolean showIcon, String qrUrl, String qrId) {
        super(context);
        this.context = context;
        this.title = title;
        this.tip = tip;
        this.showIcon = showIcon;
        this.qrUrl = qrUrl;
        this.qrId = qrId;
    }

    public AboutWxQrcodePopup(@NonNull Context context, String title, String tip, boolean showIcon, String qrUrl, String qrId, String mobclickSave, String mobclickCopy) {
        this(context, title, tip, showIcon, qrUrl, qrId);
        this.mobclickSave = mobclickSave;
        this.mobclickCopy = mobclickCopy;
    }

    public AboutWxQrcodePopup(@NonNull Context context, String title, String tip, boolean showIcon, String qrUrl, String qrId, String mobclickSave, String mobclickCopy, boolean copyGzh) {
        this(context, title, tip, showIcon, qrUrl, qrId);
        this.mobclickSave = mobclickSave;
        this.mobclickCopy = mobclickCopy;
        this.copyGzh = copyGzh;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_weixin;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        titleTv = findViewById(R.id.titleTv);
        tipTv = findViewById(R.id.tipTv);
        qrIv = findViewById(R.id.qrIv);
        saveQrIv = findViewById(R.id.saveQrIv);
        copyQrIv = findViewById(R.id.copyQrIv);
        if (copyGzh) {
            copyQrIv.setImageResource(R.drawable.class_gzhcopy);
        }
        qrIv.loadImage(qrUrl, R.drawable.default_teather);
        iconIv = findViewById(R.id.iconIv);
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        copyQrIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mobclickCopy)) {
                    MobclickAgent.onEvent(context, mobclickCopy);
                }
                if (copy(qrId)) {
//                    Toasty.normal(getContext().getApplicationContext(), "复制成功").show();
                    final BasePopupView loadview =  new XPopup.Builder(context)
                            .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                            .autoOpenSoftInput(true)
                            .asCustom(new CopyShareSuccessPopup(context, "复制成功")).show();
                    loadview.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                        if(loadingPopup.isShow())
                            loadview.dismissWith(new Runnable() {
                                @Override
                                public void run() {
//                                    Toasty.normal(getActivity(), "传值去~").show();
                                }
                            });
                        }
                    }, 4000);
                }
            }
        });
        saveQrIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mobclickSave)) {
                    MobclickAgent.onEvent(context, mobclickSave);
                }
                GlideAppUtils glideAppUtils = new GlideAppUtils();
                glideAppUtils.setglide11(context, qrUrl);
            }
        });
        titleTv.setText(title);
        tipTv.setText(tip);
        iconIv.setVisibility(showIcon ? VISIBLE : GONE);
    }

    /**
     * 复制内容到剪切板
     *
     * @param copyStr
     * @return
     */
    private boolean copy(String copyStr) {
        try {
            //获取剪贴板管理器
            ClipboardManager cm = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            // 创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("Label", copyStr);
            // 将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //完全可见执行
    @Override
    protected void onShow() {
        super.onShow();
    }

    //完全消失执行
    @Override
    protected void onDismiss() {

    }

    @Override
    protected int getMaxHeight() {
        return (int) (XPopupUtils.getWindowHeight(getContext()) * .6f);
    }

}
