package com.example.geekapp2libsindex.fragment.one;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.example.biz3hxktindex.bean.HClasscategoryBean;
import com.example.biz3hxktindex.bean.HClasscategoryBean1;
import com.example.biz3hxktindex.bean.HClasscategoryBean2;
import com.example.biz3hxktindex.bean.HGradecategoryBean2;
import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.bean.TodayCourseBean;
import com.example.biz3hxktindex.presenter.HClasscategoryPresenter;
import com.example.biz3hxktindex.presenter.HLunbotuPresenter;
import com.example.biz3hxktindex.view.HClasscategoryView;
import com.example.biz3hxktindex.view.HLunbotuView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.BanjiPopup;
import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassAdapter;
import com.example.geekapp2libsindex.recycleview.commonlist.CommonClassAdapterType;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.geek.libglide47.base.GlideImageView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.baselibrary.bannerviewquan.LXBannerView;
import com.haier.cellarette.baselibrary.bannerviewquan.holder.LXHolderCreator;
import com.haier.cellarette.baselibrary.bannerviewquan.holder.LXViewHolder;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.internal.CustomAdapt;


public class OneFragment11 extends SlbBaseIndexFragment implements HLunbotuView, HClasscategoryView, CustomAdapt {

    private String ids;
    private String ids2;
    //
    private SmartRefreshLayout refreshLayout1;
    private ClassicsHeader smartHeader1;
    //    private EmptyViewNew1 emptyview1;
    private List<HLunbotuBean1> mListbanner1;
    private HLunbotuPresenter presenter1;
    private HClasscategoryPresenter presenter2;
    private LXBannerView banner1;
    private View ll_todayClassLayout;
    private TextView today_btnTv;
    private TextView today_titleTv;
    private TextView today_timeTv;
    private EmptyViewNew1 emptyview1;
    private RelativeLayout rl1;
    //    private NestedScrollView scrollView1;
    private XRecyclerView recyclerView1;
    private CommonClassAdapter mAdapter1;
    private List<CommonClassAdapterType> mList1;

    private LinearLayout ll1;
    private LinearLayout ll2;

    private int mOffset = 0;
    private int mScrollY = 0;
    private int scrolly = 0;
    //
    private ViewSkeletonScreen skeletonScreen;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            ids = bundle.getString("id");
        }
    }

    @Override
    public void loadData() {
        super.loadData();
        HLunbotuBean bean = MmkvUtils.getInstance().get_common_json("index11", HLunbotuBean.class);
        if (bean == null) {
//            donetwork();
        } else {
            //
            refreshLayout1.finishRefresh();
            emptyview1.success();
            set_lunbotu();
            set_list();
        }
        donetwork();
    }


    private MessageReceiverIndex mMessageReceiver;
    private boolean is_first;
    private boolean is_zhu;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OneFragment1ToOneFragment11".equals(intent.getAction())) {
//                    scrollView1.smoothScrollTo(0, 0);
                    recyclerView1.scrollToPosition(0);
                }
                if ("OneFragment1ToOneFragment11_1".equals(intent.getAction())) {
                    ids = intent.getStringExtra("OneFragment1ToOneFragment11_1");
                    HGradecategoryBean2 banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class);
                    if (banjiCommonbean == null || TextUtils.equals(banjiCommonbean.getCode(), BanjiPopup.TAG_CONTENT)) {
                        ids2 = "";
                    } else {
                        ids2 = banjiCommonbean.getCode();
                    }
                    is_zhu = intent.getBooleanExtra("OneFragment1ToOneFragment11_1", true);
                    if (!is_zhu) {
                        // 下啦刷新
                        donetwork();
                    } else {
//                        if (!is_first){
//                            is_first = true;
//                            donetwork();
//                            return;
//                        }
                    }
//                    MyLogUtil.e("---geekyun--",ids);
//                    ToastUtils.showLong(ids);
//                    retryData();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        banner1.pause();
        presenter1.onDestory();
        presenter2.onDestory();
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        if (banner1 != null) {
            banner1.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (banner1 != null) {
            banner1.start();
        }
        // 从缓存中拿出头像bufen

        super.onResume();
    }


    @Override
    public boolean isBaseOnWidth() {
        return true;
    }

    @Override
    public float getSizeInDp() {
        return 375;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one11;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        AutoSize.autoConvertDensity(getActivity(), 375, true);
        super.setup(rootView, savedInstanceState);
//        banner1 = rootView.findViewById(R.id.banner1);
        emptyview1 = rootView.findViewById(R.id.emptyview1);
        rl1 = rootView.findViewById(R.id.rl1);
//        scrollView1 = rootView.findViewById(R.id.scroll_view1);
        recyclerView1 = rootView.findViewById(R.id.recycler_view1);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1);
        smartHeader1 = rootView.findViewById(R.id.smart_header1);
        //
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                // 分布局
                is_zhu = false;
                retryData();
            }
        });
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
//        ll1 = rootView.findViewById(R.id.ll1);
//        ll2 = rootView.findViewById(R.id.ll2);
//        ll1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // 关于合象
//                HiosHelper.resolveAd(getActivity(), getActivity(), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_aboutHxEdu));
//            }
//        });
//        ll2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // 如何上课
//                HiosHelper.resolveAd(getActivity(), getActivity(), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_how2Teach));
//            }
//        });

        //
        emptyview1.notices("课程正在准备中哦…", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                //
                retryData();
            }
        });
        //
//        recyclerView1.setHasFixedSize(true);
//        recyclerView1.setNestedScrollingEnabled(false);
//        recyclerView1.setFocusable(false);
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        mList1 = new ArrayList<>();
        mAdapter1 = new CommonClassAdapter(mList1);
        mAdapter1.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
//                int type = mList1.get(position).type;
                int type = Objects.requireNonNull(mAdapter1.getItem(position)).type;
                if (type == CommonClassAdapterType.style1) {
                    return 1;
                } else if (type == CommonClassAdapterType.style2) {
                    return 1;
                } else {
                    return 1;
                }
            }
        });
        //head footer
        View headerView = getHeaderView(0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mAdapter1.addHeaderView(getHeaderView(1, getRemoveHeaderListener()), 0);
            }
        });
        mAdapter1.addHeaderView(headerView);

//        View footerView = getFooterView(0, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mAdapter1.addFooterView(getFooterView(1, getRemoveFooterListener()), 0);
//            }
//        });
//        mAdapter1.addFooterView(footerView, 0);
        recyclerView1.setAdapter(mAdapter1);
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CommonClassAdapterType bean = (CommonClassAdapterType) adapter.getData().get(position);
                if (TextUtils.isEmpty(bean.getmBean().getId())) {
                    return;
                }
//                Toasty.normal(getActivity(), bean.getmBean().getText1()).show();
//                MyLogUtil.e("--geekyun---", bean.getmBean().getText1());
                //首页选课进来的课程详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
                intent.putExtra("courseId", bean.getmBean().getId());
                startActivity(intent);
            }
        });
//        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                CommonClassAdapterType bean = (CommonClassAdapterType) adapter.getData().get(position);
//                int i = view.getId();
////                Toasty.normal(getActivity(), bean.getmBean().getText1()).show();
////                MyLogUtil.e("--geekyun1---", bean.getmBean().getText1());
//                //首页选课进来的课程详情
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
//                intent.putExtra("courseId", bean.getmBean().getId());
//                startActivity(intent);
//            }
//        });
        //
//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
        //
        MyLogUtil.e("---geekyun--", "进来了～OneFragment11～");
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OneFragment1ToOneFragment11");
        filter.addAction("OneFragment1ToOneFragment11_1");
        filter.addAction("OneFragment1ToOneFragment11_2");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
        //
        presenter1 = new HLunbotuPresenter();
        presenter1.onCreate(this);
        presenter2 = new HClasscategoryPresenter();
        presenter2.onCreate(this);
//        donetwork();
    }

    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        emptyview1.loading();
        retryData();
    }


    private void retryData() {
//        mEmptyView.loading();
        presenter1.get_lunbotu();
        HGradecategoryBean2 banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, HGradecategoryBean2.class);
        if (banjiCommonbean == null || banjiCommonbean.getName() == null || TextUtils.equals(banjiCommonbean.getName(), BanjiPopup.TAG_CONTENT)) {
            ids2 = "";
        } else {
            ids2 = banjiCommonbean.getCode();
        }
        presenter2.get_classcategorylist(ids2, ids);
    }

    /**
     * 业务逻辑部分
     */
    private void setBanner() {
        banner1.setPages(mListbanner1, new LXHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });
    }

    public class BannerViewHolder implements LXViewHolder<HLunbotuBean1> {
        private GlideImageView mImageView;

        @Override
        public View createView(Context context) {
            View view = LayoutInflater.from(context).inflate(R.layout.fragment_hxkt_one1_banner_item, null);
            mImageView = view.findViewById(R.id.remote_item_image);
            return view;
        }

        @Override
        public void onBind(final Context context, final int i, final HLunbotuBean1 mBean) {
//            MyLogUtil.e("geek", "current position:" + i);
            mImageView.loadImage(mBean.getImgUrl(), R.drawable.slb_lunbo1);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("position", (i + 1) + "");
//        map.put("quantity","3");
                    MobclickAgent.onEvent(getActivity(), "listenbookpage_banner", map);
                    if (TextUtils.equals(mBean.getLinkType(), "1")) {
                        HiosHelper.resolveAd(getActivity(), getActivity(), mBean.getLinkUrl());
                    } else {
//                        //首页选课进来的课程详情
//                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
//                        intent.putExtra("courseId", mBean.getLiveCourseId());
//                        startActivity(intent);
                        //首页选课进来的课程详情
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
                        intent.putExtra("courseId", mBean.getLiveCourseId());
                        startActivity(intent);
                    }

                }
            });
        }
    }

    @Override
    public void OnLunbotuSuccess(HLunbotuBean hLunbotuBean) {
        refreshLayout1.finishRefresh(0);
        emptyview1.success();
        MmkvUtils.getInstance().set_common_json2("index11", hLunbotuBean);
        set_lunbotu();

    }

    @Override
    public void OnLunbotuNodata(String s) {
        refreshLayout1.finishRefresh(false);
        emptyview1.nodata();
    }

    @Override
    public void OnLunbotuFail(String s) {
        refreshLayout1.finishRefresh(false);
        set_lunbotu();
    }

    @Override
    public void OnClasscategorySuccess(HClasscategoryBean hClasscategoryBean) {
        MmkvUtils.getInstance().set_common_json2("index12", hClasscategoryBean);
        setTodayClass(hClasscategoryBean.getTodayCourseModel());
        set_list();
//        HClasscategoryBean hClasscategoryBean1 = MmkvUtils.getInstance().get_common_json("index12", HClasscategoryBean.class);
//        mList1 = new ArrayList<>();
//        String gson_url = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity().getApplicationContext()).getJson(getActivity().getApplicationContext(), "jsonbean/classbean.json");
//        CommonClassClassModel bean_gson = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity().getApplicationContext()).JsonToObject(gson_url, CommonClassClassModel.class);
        //
//        CommonClassClassModel bean_gson = new CommonClassClassModel();
//        List<CommonClassClassModel2> bean_list = new ArrayList<>();
//        List<HClasscategoryBean1> list = hClasscategoryBean.getList();
//        for (int j1 = 0; j1 < list.size(); j1++) {
//            HClasscategoryBean1 hClasscategoryBean1 = list.get(j1);
//            List<HClasscategoryBean2> list1 = hClasscategoryBean1.getCourseList();
//            //
//            CommonClassClassModel2 bean1 = new CommonClassClassModel2();
//            bean1.setText1(hClasscategoryBean1.getCourseTypeName());
//            List<CommonClassClassCommonbean> list10 = new ArrayList<>();
//            for (int j2 = 0; j2 < list1.size(); j2++) {
//                CommonClassClassCommonbean commonClassClassCommonbean1 = new CommonClassClassCommonbean();
//                //
//                List<CommonClassClassCommonbean3> bean_list10 = new ArrayList<>();
//                for (String bean4 : list1.get(j2).getSubjectNames()) {
//                    CommonClassClassCommonbean3 commonClassClassCommonbean3 = new CommonClassClassCommonbean3();
//                    if (TextUtils.equals("system", list1.get(j2).getCourseType())) {
//                        commonClassClassCommonbean3.setText1("#F78451");
//                    } else if (TextUtils.equals("experience", list1.get(j2).getCourseType())) {
//                        commonClassClassCommonbean3.setText1("#666666");
//                    }
//                    commonClassClassCommonbean3.setText2(bean4);
//                    bean_list10.add(commonClassClassCommonbean3);
//                }
//                commonClassClassCommonbean1.setText1(list1.get(j2).getId());
//                commonClassClassCommonbean1.setText1_list(bean_list10);
//                commonClassClassCommonbean1.setText2(list1.get(j2).getCourseName());
//                commonClassClassCommonbean1.setText3(list1.get(j2).getCourseTimeRange());
//                commonClassClassCommonbean1.setText4(list1.get(j2).getItemCount() + "课节");
//                commonClassClassCommonbean1.setText5(list1.get(j2).getSignUpCount() + "人已报名");
//                commonClassClassCommonbean1.setText6(list1.get(j2).getPriceMarked() + "");
//                commonClassClassCommonbean1.setText8(list1.get(j2).getPricePayable() + "");
//                //
//                List<CommonClassClassCommonbean2> bean_list1 = new ArrayList<>();
//                if (list1.get(j2).getTeacherList() != null && list1.get(j2).getTeacherList().size() > 0) {
//
//                    for (int j3 = 0; j3 < list1.get(j2).getTeacherList().size(); j3++) {
//                        CommonClassClassCommonbean2 commonClassClassCommonbean2 = new CommonClassClassCommonbean2();
//                        commonClassClassCommonbean2.setText1(list1.get(j2).getTeacherList().get(j3).getAvatar());
//                        commonClassClassCommonbean2.setText2(list1.get(j2).getTeacherList().get(j3).getNikeName());
//                        commonClassClassCommonbean2.setText3(list1.get(j2).getTeacherList().get(j3).getTeacherType());
//                        bean_list1.add(commonClassClassCommonbean2);
//                    }
//                }
//                commonClassClassCommonbean1.setList(bean_list1);
//                list10.add(commonClassClassCommonbean1);
//            }
//            bean1.setList(list10);
//            bean_list.add(bean1);
//        }
//        bean_gson.setList(bean_list);
        //
//        for (int i = 0; i < hClasscategoryBean1.getList().size(); i++) {
//            HClasscategoryBean1 commonClassClassModel2 = hClasscategoryBean1.getList().get(i);
//            for (int j = 0; j < commonClassClassModel2.getCourseList().size(); j++) {
//                if (j == 0) {
//                    HClasscategoryBean2 classCommonbean = new HClasscategoryBean2();
//                    classCommonbean.setCourseName(commonClassClassModel2.getCourseTypeName());
//                    mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style1, classCommonbean));
//                }
//                mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style2, commonClassClassModel2.getCourseList().get(j)));
//            }
//        }
//        mAdapter1.setNewData(mList1);
    }

    @Override
    public void OnClasscategoryNodata(String s) {

    }

    @Override
    public void OnClasscategoryFail(String s) {
        set_list();
    }

    private void set_lunbotu() {
        if (MmkvUtils.getInstance().get_common_json("index11", HLunbotuBean.class) != null) {
            mListbanner1 = MmkvUtils.getInstance().get_common_json("index11", HLunbotuBean.class).getBannerList();
        }
        if (mListbanner1 == null || mListbanner1.size() == 0) {
            return;
        }
        // 测试一条
//        List<HLunbotuBean1> list = new ArrayList<>();
//        list.add(mListbanner1.get(0));
//        mListbanner1 = list;
        if (mListbanner1.size() == 1) {
            setBanner();
            banner1.setIndicatorVisible(true);
            banner1.setmIsCanLoop(false);
            banner1.getViewPager().setScroll(false);
            return;
        }
        setBanner();
        banner1.setIndicatorVisible(true);
        banner1.setmIsCanLoop(true);
        banner1.getViewPager().setScroll(true);
        banner1.start();
    }

    //今日课程
    private void setTodayClass(final TodayCourseBean todayCourseBean) {
        if (todayCourseBean != null) {
            today_titleTv.setText(todayCourseBean.getItemTitle());
            today_timeTv.setText("直播开始 " + todayCourseBean.getCourseItemTimeRange());
            GradientDrawable gd = new GradientDrawable();
            gd.setCornerRadius(DisplayUtil.dip2px(getContext(), 20));
            //播放状态(0, "未开始"), (1, "直播中"), (2, "看回放"), (3, "已结束");
            String itemStatus = todayCourseBean.getItemStatus();
            if (TextUtils.equals(itemStatus, "1")) {
                gd.setColor(Color.parseColor("#519AF4"));
                today_btnTv.setText("直播中");
                today_btnTv.setTextColor(Color.WHITE);
            } else {
                gd.setColor(Color.parseColor("#f5f5f5"));
                switch (itemStatus) {
                    case "0":
                        today_btnTv.setText("未开始");
                        break;
                    case "2":
                        today_btnTv.setText("看回放");
                        break;
                    case "3":
                        today_btnTv.setText("已结束");
                        break;
                }
                today_btnTv.setTextColor(Color.parseColor("#999999"));
            }
            today_btnTv.setBackgroundDrawable(gd);
            ll_todayClassLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                    intent.putExtra("courseId", todayCourseBean.getLiveCourseId());
                    intent.putExtra("toSectionPosition", todayCourseBean.getOrders());
                    startActivity(intent);
                }
            });
            ll_todayClassLayout.setVisibility(View.VISIBLE);
        } else {
            ll_todayClassLayout.setVisibility(View.GONE);
        }
    }

    private void set_list() {
        HClasscategoryBean bean = MmkvUtils.getInstance().get_common_json("index12", HClasscategoryBean.class);
        mList1 = new ArrayList<>();
        if (bean == null || bean.getList() == null || bean.getList().size() == 0) {
        } else {
            for (int i = 0; i < bean.getList().size(); i++) {
                HClasscategoryBean1 commonClassClassModel2 = bean.getList().get(i);
                for (int j = 0; j < commonClassClassModel2.getCourseList().size(); j++) {
                    if (j == 0) {
                        HClasscategoryBean2 classCommonbean = new HClasscategoryBean2();
                        classCommonbean.setCourseName(commonClassClassModel2.getCourseTypeName());
                        mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style1, classCommonbean));
                    }
                    mList1.add(new CommonClassAdapterType(CommonClassAdapterType.style2, commonClassClassModel2.getCourseList().get(j)));
                }
            }
        }
        mAdapter1.setNewData(mList1);

    }


    private View.OnClickListener getRemoveHeaderListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter1.removeHeaderView(v);
            }
        };
    }

    private View.OnClickListener getRemoveFooterListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter1.removeFooterView(v);
            }
        };
    }

    private View getHeaderView(int type, View.OnClickListener listener) {
        View view = getLayoutInflater().inflate(R.layout.fragment_hxkt_one11_head1, (ViewGroup) recyclerView1.getParent(), false);
        if (type == 0) {
//            ImageView imageView = view.findViewById(R.id.iv);
//            imageView.setImageResource(R.drawable.rm_icon);
            banner1 = (LXBannerView) view.findViewById(R.id.banner1);
            banner1.setDelayedTime(6000);
            banner1.setmIsCanLoop(true);
            banner1.setmIsOutSideBottom(true);
            banner1.setIndicatorRes(R.drawable.indicator_normal2, R.drawable.indicator_selected2);
            banner1.setIndicatorVisible(true);
            banner1.setIndicatorAlign(LXBannerView.IndicatorAlign.CENTER);
            banner1.getmIndicatorContainer().setPadding(40, 20, 40, 20);
            banner1.getmIndicatorContainer().setBackgroundResource(R.drawable.indicator_bg_trans2);
            ll1 = (LinearLayout) view.findViewById(R.id.ll1);
            ll2 = (LinearLayout) view.findViewById(R.id.ll2);
            ll1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // 关于合象
                    HiosHelper.resolveAd(getActivity(), getActivity(), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_aboutHxEdu));
                }
            });
            ll2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // 如何上课
                    HiosHelper.resolveAd(getActivity(), getActivity(), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_how2Teach));
                }
            });
            ll_todayClassLayout = view.findViewById(R.id.ll_todayClassLayout);
            today_btnTv = ll_todayClassLayout.findViewById(R.id.today_btnTv);
            today_titleTv = ll_todayClassLayout.findViewById(R.id.today_titleTv);
            today_timeTv = ll_todayClassLayout.findViewById(R.id.today_timeTv);
        }
        view.setOnClickListener(listener);
        return view;
    }

    private View getFooterView(int type, View.OnClickListener listener) {
        View view = getLayoutInflater().inflate(R.layout.activity_recycleviewalluses_demo2_footer_view, (ViewGroup) recyclerView1.getParent(), false);
        if (type == 1) {
            ImageView imageView = view.findViewById(R.id.iv);
            imageView.setImageResource(R.drawable.rm_icon);
        }
        view.setOnClickListener(listener);
        return view;
    }

}
