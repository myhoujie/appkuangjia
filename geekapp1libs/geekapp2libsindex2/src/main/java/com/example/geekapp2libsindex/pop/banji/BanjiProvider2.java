package com.example.geekapp2libsindex.pop.banji;

import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.geekapp2libsindex.R;

public class BanjiProvider2 extends BaseItemProvider<BanjiAdapterType, BaseViewHolder> {

    @Override
    public int viewType() {
        return BanjiAdapter.STYLE_TWO;
    }

    @Override
    public int layout() {
        return R.layout.pop_hxkt_banjiprovider_item2;
    }

    @Override
    public void convert(BaseViewHolder helper, BanjiAdapterType data, int position) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv11);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        if (data.getmBean().getEnable() == 0) {
            tv2.setVisibility(View.VISIBLE);
            tv1.setEnabled(false);
            tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_enpress);
            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color888888));
        } else {
            tv2.setVisibility(View.GONE);
            tv1.setEnabled(true);
            if (data.getmBean().isText3()) {
//            tv1.setPressed(true);
                tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_press);
                tv1.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            } else {
//            tv1.setPressed(false);
                tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_enpress);
                tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color2F3D57));
            }
        }
        helper.setText(R.id.tv11, data.getmBean().getName());
        helper.addOnClickListener(R.id.tv11);
    }
}
