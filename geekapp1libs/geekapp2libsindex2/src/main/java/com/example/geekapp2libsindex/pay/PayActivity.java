package com.example.geekapp2libsindex.pay;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.CouponSelectAdapter;
import com.example.geekapp2libsindex.adapter.DiscountAdapter;
import com.example.geekapp2libsindex.adapter.OrderClassAdapter;
import com.example.geekapp2libsindex.domain.PayFail;
import com.example.geekapp2libsindex.domain.PaySuccess;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.pay.AliPay2;
import com.example.slbappcomm.pop.bottompay.wechatutils.WeChat1Utils;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.example.slbappshare.fenxiang.JPushShareUtils;
import com.fanli.biz3hxktdetail.bean.AvailableCouponInfoBean;
import com.fanli.biz3hxktdetail.bean.Coupon;
import com.fanli.biz3hxktdetail.bean.CouponPopResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.DiscountBean;
import com.fanli.biz3hxktdetail.bean.ExpressForm;
import com.fanli.biz3hxktdetail.bean.LivePOrderBean;
import com.fanli.biz3hxktdetail.bean.LiveSonOrderBean;
import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.fanli.biz3hxktdetail.bean.PayBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.presenter.CouponPresenter;
import com.fanli.biz3hxktdetail.presenter.OrderOrCourseDetailPresenter;
import com.fanli.biz3hxktdetail.presenter.PayPresenter;
import com.fanli.biz3hxktdetail.view.CouponView;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.fanli.biz3hxktdetail.view.OrderOrCourseDetailView;
import com.fanli.biz3hxktdetail.view.PayView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MD5Util2;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class PayActivity extends SlbBaseActivity implements View.OnClickListener, PayView, CourseDetailView, CouponView, OrderOrCourseDetailView {
    private View cl_address;
    private TextView addAddressTv;
    private TextView nameTv;
    private TextView phoneTv;
    private View ll_address;
    private View addressArrowIv;
    private TextView addressTv;
    //    private CourseBean courseBean;
    private LivePOrderBean orderBean;
    private View ll_pay;
    private TextView signUpTv;
    private PayPresenter payPresenter;
    //    private DetailPresenter detailPresenter;
    private CouponPresenter couponPresenter;
    private OrderOrCourseDetailPresenter orderOrCourseDetailPresenter;
    private final String PAY_WX = "wechat_pay_app";
    private final String PAY_ZFB = "ali_pay_app";
    private String payType = PAY_WX;//ali_pay_app:支付宝-app,wechat_pay_app:微信支付-app
    private View ll_wx;
    private View ll_zfb;
    private View wxV;
    private View zfbV;
    private List<String> courseIds = new ArrayList<>();
    private String orderId = "";
    private String orderType;
    private EmptyViewNew1 emptyView;
    private View rl_charge;
    private TextView signUpTv2;
    private TextView priceTv;

    private TextView classTitleTv;
    private RecyclerView orderClassRv;
    private OrderClassAdapter orderClassAdapter;
    private RecyclerView discountRv;
    private DiscountAdapter discountAdapter;
    private View cl_coupon;
    private View rightArrowIv;
    private View ll_coupon;
    private TextView couponTv;
    private TextView availableAmountTv;
    private TextView finalMoneyTv;
    private TextView discountTv;

    private String isFirstReqest = "1";

    private View fl_pop;
    private View popContentV;
    private RecyclerView couponRv;
    private View closeCouponIv;
    private View notUseCouponTv;
    private CouponSelectAdapter couponAdapter;
    private String liveCouponInfoId = "";
    private int selectCouponIndex = -1;

    private boolean isOrder = false;//是否已产生订单
    private boolean isFirstRequest = true;
    private View ll_notFree;
    private View freeTv;
    private View lineV;
    private View line1V;

    @Override
    protected void onDestroy() {
        if (payPresenter != null) {
            payPresenter.onDestory();
        }
//        if (detailPresenter != null) {
//            detailPresenter.onDestory();
//        }
        if (couponPresenter != null) {
            couponPresenter.onDestory();
        }
        if (orderOrCourseDetailPresenter != null) {
            orderOrCourseDetailPresenter.onDestory();
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        String courseId = getIntent().getStringExtra("courseId");
        if (!TextUtils.isEmpty(courseId)) {
            courseIds.add(courseId);
        }
        orderId = getIntent().getStringExtra("orderId");
        orderType = getIntent().getStringExtra("orderType");
        isOrder = !TextUtils.isEmpty(orderId);
        findview();
        onclick();
        payPresenter = new PayPresenter();
        payPresenter.onCreate(this);
//        detailPresenter = new DetailPresenter();
//        detailPresenter.onCreate(this);
        couponPresenter = new CouponPresenter();
        couponPresenter.onCreate(this);
        orderOrCourseDetailPresenter = new OrderOrCourseDetailPresenter();
        orderOrCourseDetailPresenter.onCreate(this);
        doNetWork();
        // 测试
//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SignUpSuccessActivity");
//        intent.putExtra("chargeFlag", "1");
//        intent.putExtra("liveCourseId", "1111");
//        intent.putExtra("liveOrderId", "1111");
//        intent.putExtra("orderType", orderType);
//        startActivity(intent);
//        finish();
    }

    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("支付");
        View ll_wrap = findViewById(R.id.ll_wrap);
        emptyView = new EmptyViewNew1(this);
        emptyView.attach(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        cl_address = findViewById(R.id.cl_address);
        addAddressTv = findViewById(R.id.addAddressTv);
        nameTv = findViewById(R.id.nameTv);
        phoneTv = findViewById(R.id.phoneTv);
        addressTv = findViewById(R.id.addressTv);
        ll_notFree = findViewById(R.id.ll_notFree);
        freeTv = findViewById(R.id.freeTv);
        lineV = findViewById(R.id.lineV);
        line1V = findViewById(R.id.line1V);

        classTitleTv = findViewById(R.id.classTitleTv);
        orderClassRv = findViewById(R.id.orderClassRv);
        orderClassAdapter = new OrderClassAdapter();
        LinearLayoutManager orderClassLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        orderClassRv.setNestedScrollingEnabled(false);
        orderClassRv.setLayoutManager(orderClassLinearLayoutManager);
//        orderClassRv.addItemDecoration(new SpaceItemDecoration2(this));
        orderClassRv.setAdapter(orderClassAdapter);

        discountRv = findViewById(R.id.discountRv);
        discountAdapter = new DiscountAdapter();
        LinearLayoutManager discountLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        discountRv.setNestedScrollingEnabled(false);
        discountRv.setLayoutManager(discountLayoutManager);
        discountRv.addItemDecoration(new DiscountSpaceItemDecoration(this));
        discountRv.setAdapter(discountAdapter);

        ll_wx = findViewById(R.id.ll_wx);
        ll_zfb = findViewById(R.id.ll_zfb);
        wxV = findViewById(R.id.wxV);
        wxV.setSelected(true);
        zfbV = findViewById(R.id.zfbV);
        ll_pay = findViewById(R.id.ll_pay);
        ll_address = findViewById(R.id.ll_address);
        addressArrowIv = findViewById(R.id.addressArrowIv);
        signUpTv = findViewById(R.id.signUpTv);
        rl_charge = findViewById(R.id.rl_charge);
        signUpTv2 = findViewById(R.id.signUpTv2);
        priceTv = findViewById(R.id.priceTv2);
        cl_coupon = findViewById(R.id.cl_coupon);
        rightArrowIv = findViewById(R.id.rightArrowIv);
        ll_coupon = findViewById(R.id.ll_coupon);
        couponTv = findViewById(R.id.couponTv);
        availableAmountTv = findViewById(R.id.availableAmountTv);
        finalMoneyTv = findViewById(R.id.finalMoneyTv);
        discountTv = findViewById(R.id.discountTv);

        fl_pop = findViewById(R.id.fl_pop);
        popContentV = fl_pop.findViewById(R.id.popContentV);
        closeCouponIv = fl_pop.findViewById(R.id.closeCouponIv);
        notUseCouponTv = fl_pop.findViewById(R.id.notUseCouponTv);
        couponRv = fl_pop.findViewById(R.id.couponRv);
        couponAdapter = new CouponSelectAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        couponRv.setNestedScrollingEnabled(false);
        couponRv.setLayoutManager(linearLayoutManager);
        couponRv.addItemDecoration(new SpaceItemDecoration(this));
        couponRv.setAdapter(couponAdapter);
        couponAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                selectCouponIndex = position;
                liveCouponInfoId = ((Coupon) adapter.getItem(position)).getLiveCouponInfoId();
                reSetCouponInfo();
                couponAdapter.setSelectCouponId(selectCouponIndex, liveCouponInfoId);
//                reCalOrderPrice(((Coupon) adapter.getData().get(position)).getLiveCouponInfoId());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        nameTv.setText(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_NAME));
        phoneTv.setText(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_TEL));
        String address = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_ALL);
        addressTv.setText(address);

        MyLogUtil.e("MMKV_ADDRESS", address);

        if (orderBean != null) {
            if (!TextUtils.equals(orderBean.getNeedToPost(), "1")) {//不需要物流
                ll_address.setVisibility(View.GONE);
            } else {
                ll_address.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(address)) {
                    cl_address.setVisibility(View.GONE);
                    addAddressTv.setVisibility(View.VISIBLE);
                } else {
                    cl_address.setVisibility(View.VISIBLE);
                    addAddressTv.setVisibility(View.GONE);
                }
            }
        }
    }

    private void doNetWork() {
        if (isFirstRequest) {
            emptyView.loading();
            if (isOrder) {
                orderOrCourseDetailPresenter.get_orderDetail(orderId, orderType);
            } else {
                orderOrCourseDetailPresenter.get_buyDetail(courseIds);
            }
        }
    }

    private void onclick() {
        ll_wx.setOnClickListener(this);
        ll_zfb.setOnClickListener(this);
        cl_address.setOnClickListener(this);
        addAddressTv.setOnClickListener(this);
        addressTv.setOnClickListener(this);
        signUpTv.setOnClickListener(this);
        signUpTv2.setOnClickListener(this);
        cl_coupon.setOnClickListener(this);
        fl_pop.setOnClickListener(this);
        popContentV.setOnClickListener(this);
        notUseCouponTv.setOnClickListener(this);
        closeCouponIv.setOnClickListener(this);
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });

        orderClassAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //首页选课进来的课程详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
                intent.putExtra("courseId", ((LiveSonOrderBean) adapter.getItem(position)).getLiveCourseId());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cl_address || v.getId() == R.id.addAddressTv) {
            //添加或者修改地址
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouhuodizhiAct");
            intent.putExtra(CommonUtils.INTENT_FROM, 1);
            startActivity(intent);
        } else if (v.getId() == R.id.signUpTv || v.getId() == R.id.signUpTv2) {
            if (orderBean == null)
                return;
            //报名
            if (TextUtils.equals(orderBean.getNeedToPost(), "1") && TextUtils.isEmpty(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_ALL))) {//需要物流
                Toasty.normal(this, "请先添加地址~").show();
                return;
            }

            String pro = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHENG);
            String city = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_SHI);
            String district = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_QU);
            String detail = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_DETAIL);
            ExpressForm expressForm = new ExpressForm();
            expressForm.setProvince(pro);
            expressForm.setCity(city);
            expressForm.setDistrict(district);
            expressForm.setAddressDtl(detail);
            expressForm.setUserName(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_NAME));
            expressForm.setUserPhone(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_TEL));

//            if (calPrice(orderBean.getAmountOrigin()) == 0) {//免费
//                payPresenter.signUp(expressForm, orderBean.getSons().get(0).getLiveCourseId(), "", orderBean.getSons().get(0).getNeedToPost());
//            } else {//收费
//                //联报功能之前的支付 start
////                String price = priceTv.getText().toString().trim();
////                if (!TextUtils.isEmpty(liveCouponInfoId) && calPrice(price) == 0) {//券后0元
////                    payPresenter.zeroPay(expressForm, liveCouponInfoId, courseBean.getId(), TextUtils.isEmpty(liveOrderId) ? courseBean.getLiveOrderId() : liveOrderId, price, courseBean.getExpressFlag(), payType);
////                } else {
////                    payPresenter.pay(expressForm, liveCouponInfoId, courseBean.getId(), TextUtils.isEmpty(liveOrderId) ? courseBean.getLiveOrderId() : liveOrderId, price, courseBean.getExpressFlag(), payType);
////                }
//                //联报功能之前的支付 end
//
//                //联报统一支付接口 start
//                String price = orderBean.getAmountPayable();
//                if (!isOrder) {
//                    price = priceTv.getText().toString().trim();
//                }
//                payPresenter.pay2(expressForm, courseIds, liveCouponInfoId, orderBean.getLiveCoursePkgId(), !TextUtils.isEmpty(liveOrderId) ? liveOrderId : orderBean.getId(), price, orderBean.getNeedToPost(), payType);
//                //联报统一支付接口 end
//            }


            //联报统一支付接口 start
            String price;
            if (calPrice(orderBean.getAmountOrigin()) == 0) {
                price = "0";
            } else {
                price = orderBean.getAmountPayable();
                if (!isOrder) {
                    price = priceTv.getText().toString().trim();
                }
            }
            payPresenter.pay2(expressForm, courseIds, liveCouponInfoId, orderBean.getLiveCoursePkgId(), !TextUtils.isEmpty(liveOrderId) ? liveOrderId : orderBean.getId(), price, orderBean.getNeedToPost(), payType);
            //联报统一支付接口 end

        } else if (v.getId() == R.id.ll_wx) {
            wxV.setSelected(true);
            zfbV.setSelected(false);
            payType = PAY_WX;//ali_pay_app:支付宝-app,wechat_pay_app:微信支付-app
        } else if (v.getId() == R.id.ll_zfb) {
            wxV.setSelected(false);
            zfbV.setSelected(true);
            payType = PAY_ZFB;
        } else if (v.getId() == R.id.cl_coupon) {
            showOrHideFlPop(true);
        } else if (v.getId() == R.id.fl_pop || v.getId() == R.id.closeCouponIv) {
            showOrHideFlPop(false);
        } else if (v.getId() == R.id.notUseCouponTv) {
            selectCouponIndex = -1;
            liveCouponInfoId = "";
            couponAdapter.setSelectCouponId(selectCouponIndex, liveCouponInfoId);
            reSetCouponInfo();
        }
    }

    //重新计算显示优惠券相关的数据展示
    private void reSetCouponInfo() {
        if (couponAdapter != null && !ListUtil.isEmpty(couponAdapter.getData())) {
            if (selectCouponIndex == -1) {//不使用优惠券
                ll_coupon.setVisibility(View.GONE);
                availableAmountTv.setVisibility(View.VISIBLE);
                availableAmountTv.setText(couponAdapter.getData().size() + "张优惠券可用");
                showOrHideFlPop(false);
            } else {//选中了一张优惠券
                ll_coupon.setVisibility(View.VISIBLE);
                availableAmountTv.setVisibility(View.GONE);
                couponTv.setText("￥" + couponAdapter.getData().get(selectCouponIndex).getAmount());
            }
            resetPriceInfo();
            showOrHidePayType();
        }
    }

    //重新计算价格相关的数据展示
    private void resetPriceInfo() {
        double oPrice = calPrice(orderBean.getAmountOrigin());
        //折扣总金额
        double discountPrice = 0;
        double finalPrice;
        if (isOrder) {
            finalPrice = calPrice(orderBean.getAmountPayable());
            discountPrice = oPrice - finalPrice;
        } else {
            List<DiscountBean> discountBeanList = orderBean.getDiscounts();
            if (!ListUtil.isEmpty(discountBeanList)) {//所有优惠信息
                for (DiscountBean discountBean : discountBeanList) {
                    if (!TextUtils.equals("1", discountBean.getDiscountType())) {//去除优惠券优惠，优惠券金额单独计算叠加
                        discountPrice += calPrice(discountBean.getAmountDiscount());
                    }
//                    discountPrice += calPrice(discountBean.getAmountDiscount());
                }
            }
            if (selectCouponIndex != -1 && couponAdapter != null && !ListUtil.isEmpty(couponAdapter.getData()) && couponAdapter.getData().size() > selectCouponIndex) {
                discountPrice += calPrice((couponAdapter.getData().get(selectCouponIndex)).getAmount());
            }
            finalPrice = oPrice - discountPrice;
        }
        if (discountPrice > 0) {
            discountTv.setText("已省 ¥" + String.format("%.2f", discountPrice));
            discountTv.setVisibility(View.VISIBLE);
        } else {
            discountTv.setVisibility(View.GONE);
        }

        if (finalPrice > 0) {
            ll_pay.setVisibility(View.VISIBLE);
        } else {
            ll_pay.setVisibility(View.GONE);
        }

        priceTv.setText(String.format("%.2f", finalPrice));
        finalMoneyTv.setText(String.format("%.2f", finalPrice));
    }

    private String liveCourseId;
    private String liveOrderId;

    @Override
    public void OnPaySuccess(PayBean payBean) {
        liveOrderId = payBean.getOrderId();
        liveCourseId = payBean.getLiveCourseId();
        orderType = payBean.getOrderType();

        addressArrowIv.setVisibility(View.GONE);
        rightArrowIv.setVisibility(View.GONE);
        cl_coupon.setEnabled(false);
        cl_address.setEnabled(false);
        orderBean.setOrderStatus(payBean.getOrderStatus());
        orderBean.setId(liveOrderId);
//        if (calPrice(orderBean.getAmountOrigin()) == 0 || (!TextUtils.isEmpty(liveCouponInfoId) && calPrice(priceTv.getText().toString().trim()) == 0)) {//免费或者券后0元
        if (TextUtils.equals("paid", orderBean.getOrderStatus())) {
            paySuccess("PAY_SUCCESS");
        } else {
            isOrder = true;
            MobclickAgent.onEvent(this, "courseselection_systemclass_ordersubmission");
            // 去支付bufen
            if (TextUtils.equals(payType, PAY_WX)) {
                // 微信
                // 选择微信bufen
                if (!WeChat1Utils.getInstance(getApplicationContext()).isWeChatAppInstalled()) {
                    Toasty.normal(getApplicationContext(), "请先安装微信").show();
                    return;
                }
                IWXAPI api = WXAPIFactory.createWXAPI(this, null);
                api.registerApp(payBean.getWxResp().getAppid());
                PayReq req = new PayReq();
                req.appId = payBean.getWxResp().getAppid();
                req.partnerId = payBean.getWxResp().getPartnerid();
                req.prepayId = payBean.getWxResp().getPrepayid();
                req.nonceStr = payBean.getWxResp().getNoncestr();
                req.timeStamp = payBean.getWxResp().getTimestamp();
//                            req.packageValue = "Sign=WXPay";
                req.packageValue = payBean.getWxResp().getPkg();
                req.signType = "HMAC-SHA256";
                // new md5
                // 把参数的值传进去SortedMap集合里面
                SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
                parameters.put("appid", payBean.getWxResp().getAppid());
                parameters.put("noncestr", payBean.getWxResp().getNoncestr());
                parameters.put("package", payBean.getWxResp().getPkg());
                parameters.put("partnerid", payBean.getWxResp().getPartnerid());
                parameters.put("prepayid", payBean.getWxResp().getPrepayid());
                parameters.put("timestamp", payBean.getWxResp().getTimestamp());
                parameters.put("sign_type", "HMAC-SHA256");
                String characterEncoding = "UTF-8";
                String mySign = createSign(characterEncoding, parameters);
//                req.sign = mySign;
                req.sign = payBean.getWxResp().getSign();
                req.extData = "app data"; // optional
                api.sendReq(req);
            } else {
                AliPay2 aliPay = new AliPay2(this);
                if (aliPay.isEmpty_orderInfo(payBean.getAlipayRetStr())) {
                    aliPay.pay2(payBean.getAlipayRetStr());
                }
            }
        }
    }

    /**
     *   * 微信支付签名算法sign
     *   *
     *   * @param characterEncoding
     *   * @param parameters
     *   * @return
     *   
     */
    public String createSign(String characterEncoding,
                             SortedMap<Object, Object> parameters) {
        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();// 所有参与传参的参数按照accsii排序（升序）
        Iterator it = es.iterator();
        while (it.hasNext()) {
            @SuppressWarnings("rawtypes")
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if (null != v && !"".equals(v) && !"sign".equals(k)
                    && !"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }
        sb.append("key=" + JPushShareUtils.APP_KEY); //KEY是商户秘钥
        String sign = MD5Util2.MD5Encode(sb.toString(), characterEncoding)
                .toUpperCase();
        return sign; // D3A5D13E7838E1D453F4F2EA526C4766
// D3A5D13E7838E1D453F4F2EA526C4766
    }

    @Override
    public void OnPayNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnPayFail(String s) {
        Toasty.normal(this, s).show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void paySuccess(String str) {
        MyLogUtil.e("PAY_SUCCESS:" + str);
        if (TextUtils.equals("PAY_SUCCESS", str)) {
            EventBus.getDefault().post(new PaySuccess(liveOrderId));
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SignUpSuccessActivity");
            intent.putExtra("chargeFlag", calPrice(orderBean.getAmountOrigin()) == 0 ? "0" : "1");
            intent.putExtra("liveCourseId", liveCourseId);
            intent.putExtra("liveOrderId", liveOrderId);
            intent.putExtra("orderType", orderBean.getOrderType());
            startActivity(intent);
            finish();
        } else if (TextUtils.equals("PAY_FAIL", str)) {
            EventBus.getDefault().post(new PayFail());
        }
    }

    @Override
    public void OnCourseSuccess(CourseResponseBean courseResponseBean) {

    }

    @Override
    public void OnCourseNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OnCourseFail(String s) {
        emptyView.errorNet();
    }

    @Override
    public void OnGetShareInfoSuccess(ShareInfoBean shareInfoBean) {

    }

    @Override
    public void OnGetShareInfoNodata(String s) {

    }

    @Override
    public void OnGetShareInfoFail(String s) {

    }

    @Override
    public void OnGetPopCouponSuccess(CouponPopResponseBean couponPopResponseBean) {
        List<Coupon> oList = couponPopResponseBean.getCouponDtlList();
        if (!ListUtil.isEmpty(oList)) {
            List<Coupon> list = new ArrayList<>();//可用优惠券
            for (Coupon coupon : oList) {
                if (TextUtils.equals(coupon.getCanSelected(), "1")) {
                    list.add(coupon);
                }
            }
            if (ListUtil.isEmpty(list)) {//没有可用优惠券
                cl_coupon.setVisibility(View.GONE);
            } else {
                cl_coupon.setVisibility(View.VISIBLE);
                couponAdapter.setNewData(list);
                //首次进来默认第一张优惠券为最大金额优惠券
                selectCouponIndex = 0;
                liveCouponInfoId = list.get(0).getLiveCouponInfoId();
                couponAdapter.setSelectCouponId(selectCouponIndex, liveCouponInfoId);
                reSetCouponInfo();
                lineV.setVisibility(View.VISIBLE);
            }
        } else {
            cl_coupon.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnGePopCouponNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnGetPopCouponFail(String s) {
        Toasty.normal(this, s).show();
    }


//    private void reCalOrderPrice(String checkedLiveCouponInfoId) {
//        couponPresenter.reCalOrderPrice(checkedLiveCouponInfoId, isFirstReqest, courseId, courseBean.getLiveOrderId());
//    }

    @Override
    public void OnReCalOrderPriceSuccess(AvailableCouponInfoBean availableCouponInfoBean) {
//        try {
//            this.liveCouponInfoId = availableCouponInfoBean.getCheckedCoupouInfoId();
//            if (TextUtils.equals(availableCouponInfoBean.getShowCoupouBox(), "1")) {
//                cl_coupon.setVisibility(View.VISIBLE);
//                if (TextUtils.isEmpty(liveCouponInfoId)) {//未选择可用券
//                    ll_coupon.setVisibility(View.GONE);
//                    availableAmountTv.setVisibility(View.VISIBLE);
//                    availableAmountTv.setText(availableCouponInfoBean.getAvailableCount() + "张优惠券可用");
//                } else {//已选择一张券
//                    ll_coupon.setVisibility(View.VISIBLE);
//                    availableAmountTv.setVisibility(View.GONE);
//                    couponTv.setText("￥" + availableCouponInfoBean.getDiscountAmount());
//                }
//
//                if (TextUtils.equals(isFirstReqest, "1") || couponAdapter.getData().size() == 0) {
//                    couponPresenter.getCouponDtlList(orderBean.geti);
//                }
//            } else {
//                cl_coupon.setVisibility(View.GONE);
//            }
//
//            if (TextUtils.equals(isFirstReqest, "1")) {//首次进入
//                isFirstReqest = "0";
//            }
//            couponAdapter.setSelectCouponId(selectCouponIndex, liveCouponInfoId);
//
//            priceTv.setText(availableCouponInfoBean.getFinalAmount());
//            finalMoneyTv.setText(availableCouponInfoBean.getFinalAmount());
//        } catch (Exception e) {
//
//        } finally {
//            showOrHidePayType();
//            if (selectCouponIndex == -1) {
//                showOrHideFlPop(false);
//            }
//        }
    }

    private void showOrHidePayType() {
        if (calPrice(orderBean.getAmountOrigin()) > 0 && calPrice(priceTv.getText().toString().trim()) > 0) {//收费课并且券后价>0元
            ll_pay.setVisibility(View.VISIBLE);
        } else {
            ll_pay.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnReCalOrderPriceNodata(String s) {
//        if (!TextUtils.equals(isFirstReqest, "1")) {
//            Toasty.normal(this, s).show();
//        }
//        showOrHidePayType();
    }

    @Override
    public void OnReCalOrderPriceFail(String s) {
//        if (!TextUtils.equals(isFirstReqest, "1")) {
//            Toasty.normal(this, s).show();
//        }
//        showOrHidePayType();
    }

    @Override
    public void OrderOrCourseDetailSuccess(OrderOrCourseDetailResponseBean orderOrCourseDetailResponseBean) {
        isFirstRequest = false;
        emptyView.success();
        orderBean = orderOrCourseDetailResponseBean.getLivePOrder();
        if (!TextUtils.isEmpty(orderBean.getPkgTitle())) {
            classTitleTv.setText(orderBean.getPkgTitle());
            classTitleTv.setVisibility(View.VISIBLE);
        } else {
            classTitleTv.setVisibility(View.GONE);
        }
//        isOrder = TextUtils.equals(orderBean.getOrderStatus(), "unpaid");
        if (isOrder) {
            addressArrowIv.setVisibility(View.GONE);
            rightArrowIv.setVisibility(View.GONE);
            cl_coupon.setEnabled(false);
            cl_address.setEnabled(false);
        } else {
            addressArrowIv.setVisibility(View.VISIBLE);
            rightArrowIv.setVisibility(View.VISIBLE);
            cl_coupon.setEnabled(true);
            cl_address.setEnabled(true);
        }
        if (!TextUtils.equals(orderBean.getNeedToPost(), "1")) {//不需要物流
            ll_address.setVisibility(View.GONE);
        } else {
            ll_address.setVisibility(View.VISIBLE);
            String address = MmkvUtils.getInstance().get_common(CommonUtils.MMKV_ADDRESS_ALL);
            if (TextUtils.isEmpty(address)) {
                cl_address.setVisibility(View.GONE);
                addAddressTv.setVisibility(View.VISIBLE);
            } else {
                cl_address.setVisibility(View.VISIBLE);
                addAddressTv.setVisibility(View.GONE);
            }
        }

        if (calPrice(orderBean.getAmountOrigin()) == 0) {//免费
            ll_pay.setVisibility(View.GONE);
            signUpTv.setVisibility(View.VISIBLE);
            rl_charge.setVisibility(View.GONE);
            freeTv.setVisibility(View.VISIBLE);
            ll_notFree.setVisibility(View.INVISIBLE);
        } else {
            signUpTv.setVisibility(View.GONE);
            rl_charge.setVisibility(View.VISIBLE);
            freeTv.setVisibility(View.INVISIBLE);
            ll_notFree.setVisibility(View.VISIBLE);
        }
//        orderClassAdapter.setNewData(orderBean.getSons());
        resetPriceInfo();

        //所有除了优惠券的优惠信息
        List<DiscountBean> withoutCouponDiscountList = new ArrayList<>();
        List<DiscountBean> discountBeanList = orderBean.getDiscounts();
        DiscountBean couponDiscountBean = null;
        if (!ListUtil.isEmpty(discountBeanList)) {//所有优惠信息
            for (DiscountBean discountBean : discountBeanList) {
                if (!TextUtils.equals("1", discountBean.getDiscountType())) {//去除优惠券优惠，优惠券金额单独计算叠加
                    withoutCouponDiscountList.add(discountBean);
                } else {
                    couponDiscountBean = discountBean;
                }
            }
            lineV.setVisibility(View.VISIBLE);
        } else {
            lineV.setVisibility(View.GONE);
        }
//        discountAdapter.setNewData(withoutCouponDiscountList);

        if (!ListUtil.isEmpty(orderBean.getSons()) && orderBean.getSons().size() > 1) {
            line1V.setVisibility(View.VISIBLE);
        } else {
            line1V.setVisibility(View.GONE);
        }
        orderClassAdapter.setNewData(orderBean.getSons());

        if (ListUtil.isEmpty(withoutCouponDiscountList)) {
            discountRv.setVisibility(View.GONE);
        } else {
            discountAdapter.setNewData(withoutCouponDiscountList);
            discountRv.setVisibility(View.VISIBLE);
        }


        if (!isOrder) {
            //获取优惠券列表
            couponPresenter.getCouponDtlList(liveCouponInfoId, courseIds);
        } else {
            if (couponDiscountBean == null) {
                cl_coupon.setVisibility(View.GONE);
            } else {
                couponTv.setText("￥" + couponDiscountBean.getAmountDiscount());
                cl_coupon.setVisibility(View.VISIBLE);
            }

            if (!ListUtil.isEmpty(orderBean.getSons())) {
                courseIds.clear();
                for (LiveSonOrderBean sonOrderBean : orderBean.getSons()) {
                    courseIds.add(sonOrderBean.getLiveCourseId());
                }
            }
        }
    }

    private double calPrice(String price) {
        double ret = 0;
        try {
            ret = Double.valueOf(price);
        } catch (Exception e) {

        } finally {
            return ret;
        }
    }

    @Override
    public void OrderOrCourseDetailNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OrderOrCourseDetailFail(String s) {
        emptyView.errorNet();
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 16);
            tbSpace = DisplayUtil.dip2px(context, 14);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = tbSpace;
            }
            outRect.bottom = tbSpace;
            outRect.left = outRect.right = lrSpace;
        }
    }

    public class DiscountSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private int tbSpace;

        public DiscountSpaceItemDecoration(Context context) {
            tbSpace = DisplayUtil.dip2px(context, 14);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = tbSpace;
        }
    }

    public class SpaceItemDecoration2 extends RecyclerView.ItemDecoration {
        int space;

        public SpaceItemDecoration2(Context context) {
            space = DisplayUtil.dip2px(context, 1);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = space;
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = space;
            }
        }
    }

    private void showOrHideFlPop(final boolean show) {
        if ((show && fl_pop.getVisibility() == View.VISIBLE) || (!show && fl_pop.getVisibility() != View.VISIBLE)) {
            return;
        }
        final TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, show ? 1 : 0.0f, Animation.RELATIVE_TO_SELF, show ? 0 : 1f);
        animation.setDuration(300);
        fl_pop.postDelayed(new Runnable() {
            @Override
            public void run() {
                fl_pop.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
                fl_pop.startAnimation(animation);
            }
        }, 50);
    }
}
