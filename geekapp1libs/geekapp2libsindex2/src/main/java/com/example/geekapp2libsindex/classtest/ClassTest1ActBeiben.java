package com.example.geekapp2libsindex.classtest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.presenter.HTouxiangsPresenter;
import com.example.biz3hxktindex.presenter.HUploadurlsPresenter;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.example.biz3hxktindex.view.HUploadurlsView;
import com.example.geekapp2libsindex.R;
import com.haier.cellarette.baselibrary.toasts3.MProgressBarDialog;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassTest1ActBeiben extends AppCompatActivity implements View.OnClickListener, HTouxiangsView, HUploadurlsView {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private TextView tv1;
    public static final int IMAGE_ITEM_ADD = -1;
    public static final int REQUEST_CODE_SELECT = 10110;
    public static final int REQUEST_CODE_PREVIEW = 10111;

    private LinearLayout ll1;
    private RecyclerView recyclerView;
    private ClassTest1ImagePickerAdapter adapter;
    private ArrayList<ImageItem> selImageList; //当前选择的所有图片
    private int maxImgCount = 9;               //允许选择图片最大数

    protected ArrayList<ImageItem> mImageItems;
    private int mCurrentPosition;
    private ArrayList<ImageItem> images = null;
    //
    private HTouxiangsPresenter presenter;
    private BasePopupView loadingPopup;
    private HUploadurlsPresenter presenter2;
    private String id;

    @Override
    protected void onDestroy() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        presenter.onDestory();
        presenter2.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (adapter != null && adapter.isAllchoose()) {
            adapter.setAllchoose(false);
            adapter.notifyDataSetChanged();
        }
        super.onBackPressed();
    }

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_hxkt_classtest1;
//    }
//
//    @Override
//    protected void setup(@Nullable Bundle savedInstanceState) {
//        super.setup(savedInstanceState);
//        findview();
//        onclick();
//        donetwork();
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hxkt_classtest1);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("课后练习");
        id = getIntent().getStringExtra("id");
        //
        MobclickAgent.onEvent(this, "lessons_practice_nocorrections");
        //
        set_vis_layout();
        //
        presenter = new HTouxiangsPresenter();
        presenter.onCreate(this);
        presenter2 = new HUploadurlsPresenter();
        presenter2.onCreate(this);

    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        tv1.setOnClickListener(this);
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_pop();
            }
        });
        adapter.setOnItemClickListener(new ClassTest1ImagePickerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position) {
                    case IMAGE_ITEM_ADD:
                        set_pop();
                        break;
                    default:
                        //打开预览
                        Intent intentPreview = new Intent(ClassTest1ActBeiben.this, ImagePreviewDelActivity.class);
                        intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, (ArrayList<ImageItem>) adapter.getImages());
                        intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
                        intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
                        startActivityForResult(intentPreview, REQUEST_CODE_PREVIEW);
                        break;
                }
            }
        });
        adapter.setOnItemClickListenerX(new ClassTest1ImagePickerAdapter.OnRecyclerViewItemClickListenerX() {
            @Override
            public void onItemClickX(View view, int position) {
                //删除操作
                mCurrentPosition = position;
//                ToastUtil.showToastCenter("delete!");
                showDeleteDialog();
            }
        });
    }

    private void findview() {
        ivBack = (ImageView) findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) findViewById(R.id.tv_center_content);
        tv1 = (TextView) findViewById(R.id.tv1);
        recyclerView = findViewById(R.id.recyclerView);
        ll1 = findViewById(R.id.ll1);

        //
        selImageList = new ArrayList<>();
        adapter = new ClassTest1ImagePickerAdapter(this, selImageList, maxImgCount);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.tv1) {
            // 提交
            if (selImageList == null || selImageList.size() == 0) {
                ToastUtils.showLong("请先上传至少一张图片");
                return;
            }
            List<File> list = new ArrayList<>();
            for (int i = 0; i < selImageList.size(); i++) {
                File file = new File(selImageList.get(i).path);
                list.add(file);
            }
            // 1
            loadingPopup = new XPopup.Builder(ClassTest1ActBeiben.this)
                    .dismissOnBackPressed(true)
                    .dismissOnTouchOutside(false)
                    .asLoading("上传中...")
                    .show();
            // 2
//            set_update();
            presenter.get_touxiang3(list);
        } else {

        }
    }

    private void set_pop() {
        //
        //打开选择,本次允许选择的数量
//                                        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
        Intent intent1 = new Intent(ClassTest1ActBeiben.this, ImageGridActivity.class);
        /* 如果需要进入选择的时候显示已经选中的图片，
         * 详情请查看ImagePickerActivity
         * */
        intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES, images);
        startActivityForResult(intent1, REQUEST_CODE_SELECT);
        //
//        List<String> names = new ArrayList<>();
//        names.add("拍照");
//        names.add("相册");
//        showDialog(new ClassTest1SelectDialog.SelectDialogListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                switch (position) {
//                    case 0: // 直接调起相机
//                        /**
//                         * 0.4.7 目前直接调起相机不支持裁剪，如果开启裁剪后不会返回图片，请注意，后续版本会解决
//                         *
//                         * 但是当前直接依赖的版本已经解决，考虑到版本改动很少，所以这次没有上传到远程仓库
//                         *
//                         * 如果实在有所需要，请直接下载源码引用。
//                         */
//                        //打开选择,本次允许选择的数量
////                                        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
//                        Intent intent = new Intent(ClassTest1ActBeiben.this, ImageGridActivity.class);
//                        intent.putExtra(ImageGridActivity.EXTRAS_TAKE_PICKERS, true); // 是否是直接打开相机
//                        intent.putExtra(ImageGridActivity.EXTRAS_IMAGES, images);
//                        startActivityForResult(intent, REQUEST_CODE_SELECT);
//                        break;
//                    case 1:
//                        //打开选择,本次允许选择的数量
////                                        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
//                        Intent intent1 = new Intent(ClassTest1ActBeiben.this, ImageGridActivity.class);
//                        /* 如果需要进入选择的时候显示已经选中的图片，
//                         * 详情请查看ImagePickerActivity
//                         * */
//                        intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES, images);
//                        startActivityForResult(intent1, REQUEST_CODE_SELECT);
//                        break;
//                    default:
//                        break;
//                }
//
//            }
//        }, names);
    }


    private ClassTest1SelectDialog showDialog(ClassTest1SelectDialog.SelectDialogListener listener, List<String> names) {
        ClassTest1SelectDialog dialog = new ClassTest1SelectDialog(this, R.style.transparentFrameWindowStyle,
                listener, names);
        if (!this.isFinishing()) {
            dialog.show();
        }
        return dialog;
    }

    /**
     * 是否删除此张图片
     */
    private void showDeleteDialog() {
        new XPopup.Builder(this)
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onCreated() {
                        Log.e("tag", "弹窗创建了");
                    }

                    @Override
                    public void onShow() {
                        Log.e("tag", "onShow");
                    }

                    @Override
                    public void onDismiss() {
                        Log.e("tag", "onDismiss");
                    }

                    //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                    @Override
                    public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                        return true;
                    }
                }).asConfirm("提示", "要删除这张照片吗？",
                "取消", "确定",
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        //移除当前图片刷新界面
                        mImageItems = adapter.getImages();
                        mImageItems.remove(mCurrentPosition);
                        selImageList.remove(mCurrentPosition);
                        if (mImageItems.size() >= 0) {
                            adapter.setImages(mImageItems);
                        } else {
//                    onBackPressed();
                        }
                        //
                        images = adapter.getImages();
                        set_vis_layout();

                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                }, false)
                .show();
        // old2
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("提示");
//        builder.setMessage("要删除这张照片吗？");
//        builder.setNegativeButton("取消", null);
//        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //移除当前图片刷新界面
//                mImageItems = adapter.getImages();
//                mImageItems.remove(mCurrentPosition);
//                selImageList.remove(mCurrentPosition);
//                if (mImageItems.size() >= 0) {
//                    adapter.setImages(mImageItems);
//                } else {
////                    onBackPressed();
//                }
//                //
//                images = adapter.getImages();
//                set_vis_layout();
//            }
//        });
//        builder.show();
    }

    private void set_vis_layout() {
        if (images == null || images.size() == 0) {
            ll1.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            tv1.setVisibility(View.GONE);
        } else {
            ll1.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            tv1.setVisibility(View.VISIBLE);
        }
    }

    // onActResult onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            //添加图片返回
            if (data != null && requestCode == REQUEST_CODE_SELECT) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                if (images != null) {
                    set_vis_layout();
                    selImageList.clear();
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                } else {
                    set_vis_layout();
                }
            }
        } else if (resultCode == ImagePicker.RESULT_CODE_BACK) {
            //预览图片返回
            if (data != null && requestCode == REQUEST_CODE_PREVIEW) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_ITEMS);
                if (images != null) {
                    set_vis_layout();
                    selImageList.clear();
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                } else {
                    set_vis_layout();
                }
            }
        }
    }

    @Override
    public void OnTouxiangsSuccess(HTouxiangBean1 hTouxiangBean1) {
        String imgStr = hTouxiangBean1.getUrls();
        List<String> list1 = new ArrayList<>();
        if (!TextUtils.isEmpty(imgStr)) {
            String[] str = imgStr.split(",");
            list1 = Arrays.asList(str);
        }
        presenter2.get_upload_img_urls(id, list1);
    }

    @Override
    public void OnTouxiangsNodata(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public void OnTouxiangsFail(String s) {
        ToastUtils.showLong(s);
    }

    @Override
    public String getIdentifier() {
        return SystemClock.currentThreadTimeMillis() + "";
    }

    @Override
    public void OnUploadurlsSuccess(String s) {
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassTest2Act");
                        intent.putExtra("id", id);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }, 500);
    }

    @Override
    public void OnUploadurlsNodata(final String s) {
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(s);
                    }
                });
            }
        }, 500);
    }

    @Override
    public void OnUploadurlsFail(final String s) {
        loadingPopup.postDelayed(new Runnable() {
            @Override
            public void run() {
//                        if(loadingPopup.isShow())
                loadingPopup.dismissWith(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(s);
                    }
                });
            }
        }, 500);
    }

    private void set_update() {
        configProgressbarHorizontalDialog();
        startProgress(true);
    }

    private void configProgressbarHorizontalDialog() {
        //新建一个Dialog
        mProgressBarDialog = new MProgressBarDialog.Builder(this)
                .setStyle(MProgressBarDialog.MProgressBarDialogStyle_Horizontal)
                .build();
    }

    //当前进度
    private int currentProgress;
    //是否开启动画：平滑过度，默认开启
    private boolean animal = true;
    private Handler mHandler = new Handler();
    private MProgressBarDialog mProgressBarDialog;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentProgress < 100) {
                mProgressBarDialog.showProgress(currentProgress, "当前进度为：" + currentProgress + "%", animal);
                currentProgress += 5;
                mHandler.postDelayed(runnable, 200);
            } else {
                mHandler.removeCallbacks(runnable);
                currentProgress = 0;
                mProgressBarDialog.showProgress(100, "完成", animal);
                //关闭
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarDialog.dismiss();
                    }
                }, 1000);
            }
        }
    };

    private void startProgress(boolean animal) {
        this.animal = animal;
        mHandler.post(runnable);
    }


}
