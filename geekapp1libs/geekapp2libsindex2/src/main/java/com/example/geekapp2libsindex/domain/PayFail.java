package com.example.geekapp2libsindex.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class PayFail implements Parcelable {
    private String id;

    public PayFail() {
    }

    public PayFail(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
    }

    protected PayFail(Parcel in) {
        this.id = in.readString();
    }

    public static final Creator<PayFail> CREATOR = new Creator<PayFail>() {
        @Override
        public PayFail createFromParcel(Parcel source) {
            return new PayFail(source);
        }

        @Override
        public PayFail[] newArray(int size) {
            return new PayFail[size];
        }
    };
}
