package com.example.geekapp2libsindex.widget;


public enum CollapsingToolbarLayoutState {
    EXPANDED,
    COLLAPSED,
    INTERNEDIATE
}