package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.lxj.xpopup.core.CenterPopupView;

public class CopyShareSuccessPopup extends CenterPopupView {
    private String content;

    public CopyShareSuccessPopup(@NonNull Context context, String content) {
        super(context);
        this.content = content;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_copy_share_success;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView contentTv = findViewById(R.id.contentTv);
        contentTv.setText(content);
    }

//    @Override
//    protected int getMaxHeight() {
//        return (int) (XPopupUtils.getWindowHeight(getContext()) * .6f);
//    }
}
