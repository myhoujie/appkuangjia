package com.example.geekapp2libsindex;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.biz3hxktindex.bean.HGengxinBean;
import com.example.biz3hxktindex.presenter.HGengxinPresenter;
import com.example.biz3hxktindex.view.HGengxinView;
import com.example.geekapp2libsindex.adapter.ShouyeFooterAdapter;
import com.example.geekapp2libsindex.domain.ShouyeFooterBean;
import com.example.geekapp2libsindex.fragment.demo.F1;
import com.example.geekapp2libsindex.fragment.demo.F2;
import com.example.geekapp2libsindex.fragment.demo.F3;
import com.example.geekapp2libsindex.fragment.demo.F4;
import com.example.geekapp2libsindex.fragment.demo.F5;
import com.example.slbappcomm.base.SlbBaseActivityNoDoubleClickOne;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.gensee.app.ApplicationUtil;
import com.github.commonlibs.libupdateapputils.util.UpdateAppReceiver;
import com.github.commonlibs.libupdateapputils.util.UpdateAppUtils;
import com.haier.cellarette.baselibrary.bannerview.Biaoge_listBean;
import com.haier.cellarette.baselibrary.common.BaseAppManager;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.AutoSizeCompat;

public class ShouyeActivity extends SlbBaseActivityNoDoubleClickOne implements HGengxinView {

    private RecyclerView recyclerView;
    private ShouyeFooterAdapter mAdapter;
    private List<Biaoge_listBean> mratings;
    private int current_pos = 0;
    private boolean firstIn = true;
    private String tag_ids;
    private FragmentManager mFragmentManager;
    public static final String id1 = "11";
    public static final String id2 = "22";
    public static final String id3 = "33";
    public static final String id4 = "44";
    public static final String id5 = "55";
    private List<ShouyeFooterBean> mList;
    private static final String LIST_TAG1 = "list11";
    private static final String LIST_TAG2 = "list22";
    private static final String LIST_TAG3 = "list33";
    private static final String LIST_TAG4 = "list44";
    private static final String LIST_TAG5 = "list55";
    //
    private UpdateAppReceiver updateAppReceiver;// 强制升级
    private HGengxinPresenter presenter;
    private String apkPath = "";
    private int serverVersionCode = 0;
    private String serverVersionName = "";
    private String updateInfoTitle = "";
    private String updateInfo = "";
    private String md5 = "";
    private String appPackageName = "";
    //
    private F1 mFragment1; //
    private F2 mFragment2; //
    private F3 mFragment3; //
    private F4 mFragment4; //
    private F5 mFragment5; //

    private String page;
    private String text;

    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("ShouyeActivity".equals(intent.getAction())) {
                    //点击item
                    int id1 = intent.getIntExtra("id", 0);
                    current_pos = id1;
                    footer_onclick();
                }
            } catch (Exception e) {
            }
        }
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        //
//        // ATTENTION: This was auto-generated to handle app links.
//        aaaa = getIntent().getStringExtra("page");
//        bbbb = getIntent().getStringExtra("text");
//        getWindow().getDecorView().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (TextUtils.equals(aaaa, "1")) {
//                    // 跳转到意见反馈
//                    startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.FeedbackActivity"));
//                }
//            }
//        }, 100);
//    }

    @Override
    protected void onResume() {
        updateAppReceiver.setBr(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (updateAppReceiver != null) {
            updateAppReceiver.desBr(this);
        }
        if (presenter != null) {
            presenter.onDestory();
        }
        LocalBroadcastManagers.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private int isfirst;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 避免从桌面启动程序后，会重新实例化入口类的activity
        if (!this.isTaskRoot()) {
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                    finish();
                    return;
                }
            }
        }
        //
        apkPath = "";
        serverVersionCode = AppUtils.getAppVersionCode();
        serverVersionName = AppUtils.getAppVersionName();
        appPackageName = AppUtils.getAppPackageName();
        updateInfoTitle = "";
        updateInfo = "";
        md5 = AppUtils.getAppSignatureMD5(appPackageName);
        presenter = new HGengxinPresenter();
        presenter.onCreate(this);
        presenter.get_gengxin("android", serverVersionCode + "", appPackageName, serverVersionName);
        updateAppReceiver = new UpdateAppReceiver();
        //
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("ShouyeActivity");
        LocalBroadcastManagers.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver, filter);
        //初始化G直播
        ApplicationUtil.init(this);
        //
//        hideSoftKeyboard();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        //
        mFragmentManager = getSupportFragmentManager();
        // 解决fragment布局重叠错乱
        if (savedInstanceState != null) {
            mFragment1 = (F1) mFragmentManager.findFragmentByTag(LIST_TAG1);
            mFragment2 = (F2) mFragmentManager.findFragmentByTag(LIST_TAG2);
            mFragment3 = (F3) mFragmentManager.findFragmentByTag(LIST_TAG3);
            mFragment4 = (F4) mFragmentManager.findFragmentByTag(LIST_TAG4);
            mFragment5 = (F5) mFragmentManager.findFragmentByTag(LIST_TAG5);
        }
        doNetWork();
        //
//        page = getIntent().getStringExtra("page");
//        text = getIntent().getStringExtra("text");
//        if (TextUtils.equals(page, "1")) {
//            // 跳转到意见反馈
//            Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.FeedbackActivity");
//            intent1.putExtra("page", page);
//            intent1.putExtra("text", text);
//            startActivity(intent1);
//        }
    }

    private void doNetWork() {
        mList = new ArrayList<>();
        addList();
        recyclerView.setLayoutManager(new GridLayoutManager(this, mList.size(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();
        current_pos = 0;
        footer_onclick();
    }

    //点击item
    private void footer_onclick() {
        final ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(current_pos);
        if (model.isEnselect()) {
            // 不切换当前的item点击 刷新当前页面
            showFragment(model.getText_id(), true);
        } else {
            // 切换到另一个item
            if (model.getText_id().equalsIgnoreCase(id2)) {
//                SlbLoginUtil2.get().loginTowhere(ShouyeActivity.this, new Runnable() {
//                    @Override
//                    public void run() {
//                        set_footer_change(model);
//                        showFragment(model.getText_id(), false);
//                    }
//                });
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            } else {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            }
        }
    }

    private void set_footer_change(ShouyeFooterBean model) {
        //设置为选中
        initList();
        model.setEnselect(true);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private long exitTime;

    private void exit() {
        ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(0);
        if (model != null && !tag_ids.equals(model.getText_id())) {
            set_footer_change(model);
            showFragment(model.getText_id(), false);
        } else {
            if ((System.currentTimeMillis() - exitTime) < 1500) {
//                RichText.recycle();
                BaseAppManager.getInstance().closeApp();
            } else {
                Toasty.normal(getApplicationContext(), "再次点击退出程序哟 ~").show();
                exitTime = System.currentTimeMillis();
            }
        }
    }

    private void findview() {
        recyclerView = findViewById(R.id.recycler_view1);
        mAdapter = new ShouyeFooterAdapter(this);

    }

    private void onclick() {
        mAdapter.setOnItemClickLitener(new ShouyeFooterAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item
                current_pos = position;
                footer_onclick();
            }
        });
    }

    private void addList() {
        mList.add(new ShouyeFooterBean(id1, "面具", R.drawable.f_icon1, R.drawable.f_icon1, true));
        mList.add(new ShouyeFooterBean(id2, "识别", R.drawable.f_icon2, R.drawable.f_icon2, false));
        mList.add(new ShouyeFooterBean(id3, "检测", R.drawable.f_icon3, R.drawable.f_icon3, false));
        mList.add(new ShouyeFooterBean(id4, "美颜", R.drawable.f_icon4, R.drawable.f_icon4, false));
        mList.add(new ShouyeFooterBean(id5, "拍照", R.drawable.f_icon5, R.drawable.f_icon5, false));
    }

    private void initList() {
        for (int i = 0; i < mList.size(); i++) {
            ShouyeFooterBean item = mList.get(i);
            if (item.isEnselect()) {
                item.setEnselect(false);
            }
        }
    }

    private void showFragment(final String tag, final boolean isrefresh) {
        tag_ids = tag;
        //pifubufen
//        pifu(id2);
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragments(transaction);

        if (tag.equalsIgnoreCase("-1")) { //
//            if (mFragment1 == null) {
//                mFragment1 = new FragmentContent1();
//                transaction.add(R.id.container, mFragment1, LIST_TAG0);
//            } else {
//                transaction.show(mFragment1);
//                mFragment1.initData();
//            }
        } else if (tag.equalsIgnoreCase(id1)) {
            if (mFragment1 == null) {
                mFragment1 = new F1();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment1.setArguments(args);
                transaction.add(R.id.container, mFragment1, LIST_TAG1);
            } else {
                transaction.show(mFragment1);
                mFragment1.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id2)) {
            if (mFragment2 == null) {
                mFragment2 = new F2();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment2.setArguments(args);
                transaction.add(R.id.container, mFragment2, LIST_TAG2);
            } else {
                transaction.show(mFragment2);
                mFragment2.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id3)) {
            if (mFragment3 == null) {
                mFragment3 = new F3();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment3.setArguments(args);
                transaction.add(R.id.container, mFragment3, LIST_TAG3);
            } else {
                transaction.show(mFragment3);
                mFragment3.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id4)) {
            if (mFragment4 == null) {
                mFragment4 = new F4();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment4.setArguments(args);
                transaction.add(R.id.container, mFragment4, LIST_TAG4);
            } else {
                transaction.show(mFragment4);
                mFragment4.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id5)) {
            if (mFragment5 == null) {
                mFragment5 = new F5();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment5.setArguments(args);
                transaction.add(R.id.container, mFragment5, LIST_TAG5);
            } else {
                transaction.show(mFragment5);
                mFragment5.getCate(tag, isrefresh);
            }
        }

        transaction.commitAllowingStateLoss();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mFragment1 != null) {
            transaction.hide(mFragment1);
            mFragment1.give_id(tag_ids);
        }
        if (mFragment2 != null) {
            transaction.hide(mFragment2);
            mFragment2.give_id(tag_ids);
        }
        if (mFragment3 != null) {
            transaction.hide(mFragment3);
            mFragment3.give_id(tag_ids);
        }
        if (mFragment4 != null) {
            transaction.hide(mFragment4);
            mFragment4.give_id(tag_ids);
        }
        if (mFragment5 != null) {
            transaction.hide(mFragment5);
            mFragment5.give_id(tag_ids);
        }

    }

    /**
     * fragment间通讯bufen
     *
     * @param value 要传递的值
     * @param tag   要通知的fragment的tag
     */
    public void callFragment(Object value, String... tag) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment;
        for (String item : tag) {
            if (TextUtils.isEmpty(item)) {
                continue;
            }

            fragment = fm.findFragmentByTag(item);
            if (fragment instanceof SlbBaseIndexFragment) {
                ((SlbBaseIndexFragment) fragment).call(value);
            }
        }
    }


    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 375, true);//如果有自定义需求就用这个方法
        return super.getResources();
    }


    /**
     * 升级bufen
     *
     * @param hGengxinBean
     */
    @Override
    public void OnGengxinSuccess(HGengxinBean hGengxinBean) {
        apkPath = hGengxinBean.getApkUrl();
        serverVersionCode = Integer.valueOf(hGengxinBean.getVersionNo());
        serverVersionName = hGengxinBean.getVersionName();
        updateInfoTitle = hGengxinBean.getTitle();
        updateInfo = hGengxinBean.getUpgradeInfo();
        if (TextUtils.equals(hGengxinBean.getForceFlag(), "1")) {
            // 检查更新bufen
            UpdateAppUtils.from(ShouyeActivity.this)
                    .serverVersionCode(serverVersionCode)
                    .serverVersionName(serverVersionName)
                    .downloadPath("apk/" + getPackageName() + ".apk")
                    .showProgress(true)
                    .isForce(true)
                    .apkPath(apkPath)
                    .downloadBy(UpdateAppUtils.DOWNLOAD_BY_APP)    //default
                    .checkBy(UpdateAppUtils.CHECK_BY_VERSION_CODE) //default
                    .updateInfoTitle(updateInfoTitle)
                    .updateInfo(updateInfo.replace("|", "\n"))
//                    .showNotification(true)
//                    .needFitAndroidN(true)
//                    .updateInfoTitle("新版本已准备好")
//                    .updateInfo("版本：1.01" + "    " + "大小：10.41M\n" + "1.修改已知问题\n2.加入动态绘本\n3.加入小游戏等你来学习升级")
                    .update();
        } else {
            // 检查更新bufen
            UpdateAppUtils.from(ShouyeActivity.this)
                    .serverVersionCode(serverVersionCode)
                    .serverVersionName(serverVersionName)
                    .downloadPath("apk/" + getPackageName() + ".apk")
                    .showProgress(true)
                    .apkPath(apkPath)
                    .downloadBy(UpdateAppUtils.DOWNLOAD_BY_APP)    //default
                    .checkBy(UpdateAppUtils.CHECK_BY_VERSION_CODE) //default
                    .updateInfoTitle(updateInfoTitle)
                    .updateInfo(updateInfo.replace("|", "\n"))
//                    .showNotification(true)
//                    .needFitAndroidN(true)
//                    .updateInfoTitle("新版本已准备好")
//                    .updateInfo("版本：1.01" + "    " + "大小：10.41M\n" + "1.修改已知问题\n2.加入动态绘本\n3.加入小游戏等你来学习升级")
                    .update();
        }
    }

    @Override
    public void OnGengxinNodata(String s) {

    }

    @Override
    public void OnGengxinFail(String s) {

    }
}
