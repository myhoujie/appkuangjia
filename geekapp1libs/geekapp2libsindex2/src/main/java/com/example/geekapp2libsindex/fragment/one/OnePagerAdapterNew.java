package com.example.geekapp2libsindex.fragment.one;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import java.util.Objects;

//import androidx.core.view.PagerAdapter;

/**
 * 首页viewpager adapter
 */
public class OnePagerAdapterNew extends PagerAdapter {

    private Context context;
    private int count = 0;

    public OnePagerAdapterNew(Context context,int count){
        this.context = context;
        this.count = count;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int layoutId =0;
        if (position==0){
            layoutId = context.getResources().getIdentifier(OneFactory1New.PAGE_LAYOUT_ID + 0, "layout", Objects.requireNonNull(context).getPackageName());
        }else {
            layoutId = context.getResources().getIdentifier(OneFactory1New.PAGE_LAYOUT_ID + 1, "layout", Objects.requireNonNull(context).getPackageName());
        }
        if (layoutId == 0) {
            throw new UnsupportedOperationException("layout not found!");
        }
        View itemLayout = LayoutInflater.from(context).inflate(layoutId, container, false);
        container.addView(itemLayout);
        MyLogUtil.e("--geekyun--",layoutId+"");
        return itemLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}