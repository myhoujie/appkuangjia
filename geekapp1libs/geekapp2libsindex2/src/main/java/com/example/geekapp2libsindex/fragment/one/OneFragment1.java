package com.example.geekapp2libsindex.fragment.one;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.collection.SparseArrayCompat;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.presenter.HLunbotuPresenter;
import com.example.biz3hxktindex.view.HLunbotuView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.BanjiPopup;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapterType;
import com.example.geekapp2libsindex.pop.banji.BanjiCommonbean;
import com.example.geekapp2libsindex.pop.banji.BanjiModel;
import com.example.geekapp2libsindex.pop.banji.BanjiModel2;
import com.example.geekapp2libsindex.widget.CollapsingToolbarLayoutState;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.assetsfitandroid.fileassets.GetAssetsFileMP3TXTJSONAPKUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.qcode.ExpandViewRect;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerChangeAdapter;
import com.haier.cellarette.baselibrary.tablayout.ViewPagerSlide;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class OneFragment1 extends SlbBaseIndexFragment implements HLunbotuView {

    private String ids;
    private TextView tv1;
    private BasePopupView loadingPopup;
    private TabLayout tablayoutMy;
    private ViewPagerSlide mViewPager;
    private String id1 = "1";//tablayout
    private String id2 = "2";
    private String id3 = "3";
    private String id4 = "4";
    private String current_id = id1;
    private SmartRefreshLayout refreshLayout1;
    private ClassicsHeader smartHeader1;
    private EmptyViewNew1 emptyview;
    //    private EmptyViewNew1 emptyview1;
    private NestedScrollView scrollView1;
    private CoordinatorLayout ll_view1;
    private AppBarLayout app_bar;
    private CollapsingToolbarLayoutState state;
    //    private LinearLayout ll_view1;
    private int mOffset = 0;
    private int mScrollY = 0;
    private int scrolly = 0;
    private boolean is_dingbu = true;
    //
    private HLunbotuBean mListbanner1;
    private HLunbotuPresenter presenter1;
    private boolean once;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
    }

    @Override
    public void onDestroyView() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        super.onDestroyView();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one1;
    }

    @Override
    protected void setup(View rootView, Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        tv1 = rootView.findViewById(R.id.tv1);
        // 初始化
        ExpandViewRect.expandViewTouchDelegate(tv1, 10, 10, 10, 10);
        BanjiCommonbean banjiCommonbean = MmkvUtils.getInstance().get_common_json(CommonUtils.BANJI_ID, BanjiCommonbean.class);
        if (banjiCommonbean == null || TextUtils.equals(banjiCommonbean.getText2(), BanjiPopup.TAG_CONTENT)) {
            tv1.setText(BanjiPopup.TAG_CONTENT);
        } else {
            tv1.setText(banjiCommonbean.getText2());
        }

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 年级
                // shuju
                List<BanjiAdapterType> mList1 = new ArrayList<>();
                String gson_url = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).getJson(getActivity(), "jsonbean/banjibean.json");
                BanjiModel bean_gson = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).JsonToObject(gson_url, BanjiModel.class);
                for (int i = 0; i < bean_gson.getList().size(); i++) {
                    BanjiModel2 bean = bean_gson.getList().get(i);
                    for (int j = 0; j < bean.getList().size(); j++) {
                        if (j == 0) {
                            BanjiCommonbean banjiCommonbean = new BanjiCommonbean();
                            banjiCommonbean.setText2(bean.getText1());
                            banjiCommonbean.setText1("-1");
//                            mList1.add(new BanjiAdapterType(BanjiAdapterType.style1, banjiCommonbean));
                        }
//                        mList1.add(new BanjiAdapterType(BanjiAdapterType.style2, bean.getList().get(j)));
                    }
                }
                loadingPopup = new XPopup.Builder(getContext())
                        .popupAnimation(PopupAnimation.ScaleAlphaFromLeftTop)
                        .asCustom(new BanjiPopup(Objects.requireNonNull(getActivity()), new BanjiPopup.Ondismiss() {
                            @Override
                            public void OnDismiss(final String content) {
//                                loadingPopup.dismiss();
                                loadingPopup.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
//                                            if (loadingPopup.isShow())
                                        loadingPopup.dismissWith(new Runnable() {
                                            @Override
                                            public void run() {
                                                tv1.setText(TextUtils.equals(content,BanjiPopup.TAG_CONTENT)?"年级":content);
                                                is_zhu = true;
                                                retryData();
                                            }
                                        });
                                    }
                                }, 100);
                            }
                        }, mList1))
                        .show();
            }
        });
        tablayoutMy = rootView.findViewById(R.id.tab);
        mViewPager = rootView.findViewById(R.id.viewpager_my);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1);
        smartHeader1 = rootView.findViewById(R.id.smart_header1);
        emptyview = rootView.findViewById(R.id.emptyview1);
//        emptyview1 = rootView.findViewById(R.id.emptyview11);
//        scrollView1 = rootView.findViewById(R.id.scroll_view1);
        ll_view1 = rootView.findViewById(R.id.ll_view1);
        app_bar = rootView.findViewById(R.id.appbarlayout1);
        //
        initsmart();
        //
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                // 分布局
                is_zhu = false;
                retryData();
            }
        });
//        emptyview.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");
//        emptyview1.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");
        emptyview.bind(ll_view1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 主布局
                is_zhu = true;
                retryData();
            }
        });
//        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
//            @Override
//            public void retry() {
//                // 分布局
//                is_zhu = false;
//                retryData();
//            }
//        });
        //
        init_viewpager();
        setupFragments();
//        TabUtils.setIndicatorWidth(tablayoutMy, 80);
        //
        presenter1 = new HLunbotuPresenter();
        presenter1.onCreate(this);
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        is_zhu = true;
        retryData();
    }

    private void retryData() {
        if (is_zhu) {
            emptyview.loading();
        } else {
//            refreshLayout1.autoLoadmore(3000);
//            emptyview1.loading();
        }
        presenter1.get_lunbotu();

    }

    private void initsmart() {
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                //计算进度百分比
                float percent = Float.valueOf(Math.abs(verticalOffset)) / Float.valueOf(appBarLayout.getTotalScrollRange());
                //根据百分比做你想做的
                if (verticalOffset == 0) {
                    if (state != CollapsingToolbarLayoutState.EXPANDED) {
                        state = CollapsingToolbarLayoutState.EXPANDED;//修改状态标记为展开
//                        collapsingToolbarLayout.setTitle("EXPANDED");//设置title为EXPANDED
                        MyLogUtil.e("--geekyun---", "1");
                        is_dingbu = true;
                    }
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (state != CollapsingToolbarLayoutState.COLLAPSED) {
//                        collapsingToolbarLayout.setTitle("");//设置title不显示
//                        iv_icon2.setVisibility(View.VISIBLE);//隐藏播放按钮
                        state = CollapsingToolbarLayoutState.COLLAPSED;//修改状态标记为折叠
                        MyLogUtil.e("--geekyun---", "2");
                        is_dingbu = false;
                    }
                } else {
                    if (state != CollapsingToolbarLayoutState.INTERNEDIATE) {
                        if (state == CollapsingToolbarLayoutState.COLLAPSED) {
//                            iv_icon2.setVisibility(View.GONE);//由折叠变为中间状态时隐藏播放按钮
                            MyLogUtil.e("--geekyun---", "3");
                        }
//                        collapsingToolbarLayout.setTitle("INTERNEDIATE");//设置title为INTERNEDIATE
                        state = CollapsingToolbarLayoutState.INTERNEDIATE;//修改状态标记为中间
                        MyLogUtil.e("--geekyun---", "4");
                        is_dingbu = false;
                    }
                }
            }
        });

//        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            private int lastScrollY = 0;
//            private int h = DensityUtil.dp2px(170);
//
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                scrolly = scrollY;
//                if (lastScrollY < h) {
//                    scrollY = Math.min(h, scrollY);
//                    mScrollY = scrollY > h ? h : scrollY;
//                }
//                lastScrollY = scrollY;
//            }
//        });
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
//        if (scrolly == 0) {
        if (is_dingbu) {
//            if (refreshLayout1 != null) {
//                refreshLayout1.autoRefresh();
//            }
            donetwork();
        } else {
//            scrollView1.smoothScrollTo(0, 0);
            CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) app_bar.getLayoutParams()).getBehavior();
            if (behavior instanceof AppBarLayout.Behavior) {
                AppBarLayout.Behavior appBarLayoutBehavior = (AppBarLayout.Behavior) behavior;
                int topAndBottomOffset = appBarLayoutBehavior.getTopAndBottomOffset();
                if (topAndBottomOffset != 0) {
                    appBarLayoutBehavior.setTopAndBottomOffset(0);
                }
            }
            //
            Intent msgIntent = new Intent();
            msgIntent.setAction("OneFragment1ToOneFragment11");
            LocalBroadcastManagers.getInstance(getActivity()).sendBroadcast(msgIntent);
        }

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }


    private void init_viewpager() {
        mViewPager.setScroll(true);
        mViewPager.setOffscreenPageLimit(OneFactory1.PAGE_COUNT);
        mViewPager.setAdapter(new OnePagerAdapter(getActivity()));
        //给ViewPager添加事件监听
        mViewPager.addOnPageChangeListener(new ViewPagerChangeAdapter() {
            @Override
            public void onPageSelected(int position) {
                // 此处不能做统计数据bufen

            }
        });
        mViewPager.setCurrentItem(OneFactory1.DEFAULT_PAGE_INDEX);//设置当前显示标签页为第一页
    }


    private void init_tablayout() {
        List<OneBean1> mDataTablayout = new ArrayList<>();
        mDataTablayout.add(new OneBean1(id1, "精选", false));
        mDataTablayout.add(new OneBean1(id2, "数学", false));
        mDataTablayout.add(new OneBean1(id3, "语文", false));
        mDataTablayout.add(new OneBean1(id4, "英语", false));
        tablayoutMy.setupWithViewPager(mViewPager);
        tablayoutMy.removeAllTabs();
        for (OneBean1 item : mDataTablayout) {
            tablayoutMy.addTab(tablayoutMy.newTab()
                    .setTag(item.getTab_id()).setText(item.getTab_name()));
        }
        TabUtils.setIndicatorWidth(tablayoutMy, 50);
//        tablayoutMy.clearAnimation();
        tablayoutMy.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TabUtils.tabSelect(tablayoutMy, tab);
                String tag = (String) tab.getTag();
                current_id = tag;
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (TextUtils.isEmpty(tag)) {
                    return;
                }
                MyLogUtil.e("-进了几次tablayout--geekyun--", "进了几次tablayout");
                showFragment(current_id);
            }
        });
    }

    private void showFragment(String tag) {
        if (tag.equals(id1)) {
            changeView(0);
            callFragment(tag, OneFragment11.class.getName());
        } else if (tag.equals(id2)) {
            changeView(1);
            callFragment(tag, OneFragment12.class.getName());
        } else if (tag.equals(id3)) {
            changeView(2);
            callFragment(tag, OneFragment13.class.getName());
        } else if (tag.equals(id4)) {
            changeView(3);
            callFragment(tag, OneFragment14.class.getName());
        }
    }

    /**
     * fragment间通讯bufen
     *
     * @param value 要传递的值
     * @param tag   要通知的fragment的tag
     */
    public void callFragment(Object value, String... tag) {
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        Fragment fragment;
        for (String item : tag) {
            if (TextUtils.isEmpty(item)) {
                continue;
            }

            fragment = fm.findFragmentByTag(item);
            if (fragment instanceof SlbBaseIndexFragment) {
                ((SlbBaseIndexFragment) fragment).call(value);
            }
        }
    }


    //手动设置ViewPager要显示的视图
    public void changeView(int desTab) {
        mViewPager.setCurrentItem(desTab, true);
    }

    /**
     * 初始化首页fragments
     */
    private void setupFragments() {
        // 使用HierarchyChangeListener的目的是防止在viewpager的itemview还没有准备好就去inflateFragment
        // 带来的问题
        mViewPager.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (((ViewGroup) parent).getChildCount() < OneFactory1.PAGE_COUNT) {
                    return;
                }

                FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
                SparseArrayCompat<Class<? extends Fragment>> array = OneFactory1.get();//一个版本模式bufen
                int size = array.size();
                Fragment item;
                for (int i = 0; i < size; i++) {
                    item = FragmentHelper.newFragment(array.valueAt(i), null);
                    ft.replace(array.keyAt(i), item, item.getClass().getName());
                }
                ft.commitAllowingStateLoss();
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {

            }
        });
    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    private boolean is_zhu;// ceshi

    @Override
    public void OnLunbotuSuccess(HLunbotuBean hLunbotuBean) {
        if (is_zhu) {
            emptyview.success();
            init_tablayout();
        } else {
            refreshLayout1.finishRefresh(0);
//            emptyview1.success();
        }
        showFragment(current_id);
    }

    @Override
    public void OnLunbotuNodata(String s) {
        if (is_zhu) {
            emptyview.nodata();
        } else {
            refreshLayout1.finishRefresh(false);
//            emptyview1.nodata();
        }
//        Toasty.normal(getActivity(), s).show();
    }

    @Override
    public void OnLunbotuFail(String s) {
        if (is_zhu) {
            emptyview.errorNet();
        } else {
            refreshLayout1.finishRefresh(false);
//            emptyview1.errorNet();
        }
//        Toasty.normal(getActivity(), s).show();
    }
}
