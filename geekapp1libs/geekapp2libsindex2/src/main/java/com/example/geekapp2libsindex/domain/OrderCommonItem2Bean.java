package com.example.geekapp2libsindex.domain;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by geek on 2016/2/25.
 */
public class OrderCommonItem2Bean implements Parcelable {

    private String id;
    private String text1;
    private boolean text2;

    public OrderCommonItem2Bean() {
    }

    public OrderCommonItem2Bean(String id, String text1, boolean text2) {
        this.id = id;
        this.text1 = text1;
        this.text2 = text2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public boolean isText2() {
        return text2;
    }

    public void setText2(boolean text2) {
        this.text2 = text2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.text1);
        dest.writeByte(this.text2 ? (byte) 1 : (byte) 0);
    }

    protected OrderCommonItem2Bean(Parcel in) {
        this.id = in.readString();
        this.text1 = in.readString();
        this.text2 = in.readByte() != 0;
    }

    public static final Parcelable.Creator<OrderCommonItem2Bean> CREATOR = new Parcelable.Creator<OrderCommonItem2Bean>() {
        @Override
        public OrderCommonItem2Bean createFromParcel(Parcel source) {
            return new OrderCommonItem2Bean(source);
        }

        @Override
        public OrderCommonItem2Bean[] newArray(int size) {
            return new OrderCommonItem2Bean[size];
        }
    };
}
