package com.example.geekapp2libsindex.common;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Html;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.OrderCommonItemBean;

public class OrderCommonAdapter extends BaseQuickAdapter<OrderCommonItemBean, BaseViewHolder> {

    public OrderCommonAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderCommonItemBean item) {
        TextView tv10 = helper.itemView.findViewById(R.id.tv10);
        TextView tv11 = helper.itemView.findViewById(R.id.tv11);
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        TextView tv6 = helper.itemView.findViewById(R.id.tv6);
        TextView tv7 = helper.itemView.findViewById(R.id.tv7);
        TextView tv8 = helper.itemView.findViewById(R.id.tv8);
        TextView tv9 = helper.itemView.findViewById(R.id.tv9);

        //
        GradientDrawable drawable = new GradientDrawable();
        //设置圆角大小
        drawable.setCornerRadius(5);
        //设置边缘线的宽以及颜色
        drawable.setStroke(1, Color.parseColor("#ff0000"));
        //设置shape背景色
        drawable.setColor(Color.parseColor("#ff0000"));
        //设置到TextView中
        tv1.setBackground(drawable);
        tv1.setPadding(10, 5, 10, 5);
        tv1.setTextColor(Color.parseColor("#ffffff"));

        startTime(1000 * 1000, tv10, item, helper.getAdapterPosition());
        helper.addOnClickListener(R.id.tv7);
        helper.addOnClickListener(R.id.tv8);
        helper.addOnClickListener(R.id.tv9);
    }

    private CountDownTimer timer;
    /**
     * 从x开始倒计时
     *
     * @param x
     */
    public void startTime(long x, final TextView tv1, final OrderCommonItemBean item, final int pos) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        CountDownTimer timer = new CountDownTimer(x, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int remainTime = (int) (millisUntilFinished / 1000L);// 182
                int hour = remainTime / 60;
                int second = remainTime % 60;
                String text = hour + "分" + second + "秒";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text)));
                }
            }

            @Override
            public void onFinish() {
                //
                cancel();
                tv1.setText(item.getText_content());
                remove_pos(item, pos);
            }
        };
        timer.start();
    }

    private void remove_pos(OrderCommonItemBean item, int pos) {
        if (pos == 0 || pos >= getItemCount()) {
            return;
        }
        getData().remove(item);
        notifyItemRemoved(pos);
    }


}