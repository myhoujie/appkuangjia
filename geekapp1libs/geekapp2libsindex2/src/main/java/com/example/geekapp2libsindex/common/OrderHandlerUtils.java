package com.example.geekapp2libsindex.common;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Html;
import android.widget.TextView;

import com.example.biz3hxktindex.bean.HMyorderBean1;
import com.example.geekapp2libsindex.R;
import com.haier.cellarette.baselibrary.zothers.DaojishiUtil;

import java.lang.ref.WeakReference;

public class OrderHandlerUtils {

    private H mHandler;
    private static final int MSG_RUN = 189;
    private static final int DELAY_MILLIS = 1000;
    private long mCurrentTime;
    private Context mContext;
    private TextView tv1;
    private HMyorderBean1 item;
    private int pos;

    public interface OnfinishClick {
        void onfinish(TextView tv1, HMyorderBean1 item, int pos);
    }

    public OnfinishClick onfinishClick;

    public void set_data(Context mContext, TextView tv1, HMyorderBean1 item, int pos, long mCurrentTime, OnfinishClick onfinishClick) {
        this.mContext = mContext;
        this.tv1 = tv1;
        this.item = item;
        this.pos = pos;
        this.mCurrentTime = mCurrentTime;
        this.onfinishClick = onfinishClick;
        decrTime(0);
    }

    public OrderHandlerUtils() {
        mHandler = new H(mContext);
        startTime();
    }

    public void startTime() {
        handler_chushihua();
        //启动handlerbufen
//        mHandler2.sendEmptyMessageDelayed(MSG_RUN, 0);
        mHandler.sendEmptyMessage(MSG_RUN);
    }

    public class H<T> extends Handler {
        private WeakReference<T> mActivity;
        private long mNextTime;

        public H(T activity) {
            mActivity = new WeakReference<T>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            T activity = mActivity.get();
            if (activity == null || msg.what != MSG_RUN) {
                return;
            }
            if (decrTime(SystemClock.uptimeMillis() - mNextTime)) {
                mNextTime = SystemClock.uptimeMillis();
                sendEmptyMessageDelayed(MSG_RUN, DELAY_MILLIS);
            }
        }
    }

    /**
     * 倒计时
     *
     * @param ms
     * @return true 继续倒计时， false停止倒计时
     */
    public boolean decrTime(long ms) {
        mCurrentTime -= ms;
        if (mCurrentTime <= 0) {
//        if (mCurrentTime - 1000 <= 1000) {
            // 已结束
            doCancel();
            return false;
        }
//        int remainTime = (int) (mCurrentTime / 1000L);// 182
//        int hour = remainTime / 60;
//        int second = remainTime % 60;
        long[] times = DaojishiUtil.compute(mCurrentTime);
        String text = DaojishiUtil.time_change_one(times[2]) + "分" + DaojishiUtil.time_change_one(times[3]) + "秒";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text)));
        }
        return true;
    }

    public void doCancel() {
        mHandler.removeMessages(MSG_RUN);
        mCurrentTime = 0;
        if (onfinishClick != null) {
            onfinishClick.onfinish(tv1, item, pos);
        }
//        setTime(0, 0, 0);
//        tp_hour.setText("00");
//        tp_min.setText("00");
//        tp_sec.setText("00");
//        tv1.setText("00 : 00 : 00");
    }

    public void handler_chushihua() {
        mHandler.removeMessages(MSG_RUN);
        mHandler.mNextTime = SystemClock.uptimeMillis();
//        decrTime(0);
    }

}
