package com.example.geekapp2libsindex.fragment.three;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.blankj.utilcode.util.AppUtils;
import com.example.biz3hxktindex.bean.HMyaddressBean;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.example.biz3hxktindex.presenter.HUserInfoPresenter;
import com.example.biz3hxktindex.view.HUserInfoView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.SlbLoginUtil2;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.scwang.smartrefresh.layout.util.DensityUtil;


public class OneFragment33 extends SlbBaseIndexFragment implements HUserInfoView {

    private String ids;
    private SmartRefreshLayout refreshLayout1;
    private ClassicsHeader smartHeader1;
    private NestedScrollView scrollView1;
    private RelativeLayout rl1;
    private GlideImageView iv1;
    private TextView tv1;
    private TextView tv2;
    private TextView tv_yhq1;
    private LinearLayout ll1;
    private LinearLayout ll2;
    private LinearLayout ll3;
    private LinearLayout ll4;
    private LinearLayout ll5;

    private int mOffset = 0;
    private int mScrollY = 0;

    private HUserInfoPresenter presenter1;
    private String tablayoutId;


    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
        retryData();
    }

    @Override
    public void onDestroy() {
        presenter1.onDestory();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen
        set_res();
//        donetwork();
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one33;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1);
        smartHeader1 = rootView.findViewById(R.id.smart_header1);
        scrollView1 = rootView.findViewById(R.id.scroll_view1);
        rl1 = rootView.findViewById(R.id.rl1);
        iv1 = rootView.findViewById(R.id.iv1);
        tv1 = rootView.findViewById(R.id.tv1);
        tv2 = rootView.findViewById(R.id.tv2);
        ll1 = rootView.findViewById(R.id.ll1);
        ll2 = rootView.findViewById(R.id.ll2);
        ll3 = rootView.findViewById(R.id.ll3);
        tv_yhq1 = rootView.findViewById(R.id.tv_yhq1);
        ll4 = rootView.findViewById(R.id.ll4);
        ll5 = rootView.findViewById(R.id.ll5);
        //
        initsmart();
        //
        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 编辑个人资料
                if (SlbLoginUtil2.get().isUserLogin()) {
                    startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.UserInfoAct"));
                } else {
                    SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                        @Override
                        public void run() {
//                        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.UserInfoAct"));
                        }
                    });
                }
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 我的订单
                if (SlbLoginUtil2.get().isUserLogin()) {
                    startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderAct"));
                } else {
                    SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                        @Override
                        public void run() {
//                        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderAct"));
                        }
                    });
                }
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 关于我们
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.AboutUsAct"));
                HiosHelper.resolveAd(getActivity(), getActivity(), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_aboutUs));
//                HiosHelper.resolveAd(getActivity(), getActivity(), "http://10.0.0.141:8080/#/about");
//                HiosHelper.resolveAd(getActivity(), getActivity(), "hios://com.sairobo.hexiangketang.slbapp.ad.web.page.part2{act}");

            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 设置
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SettingAct"));

            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 意见反馈
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.FeedbackActivity"));
                    }
                });
            }
        });
        ll5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 我的优惠券
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.CouponsAct"));
                    }
                });
            }
        });
        //
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                retryData();
            }
        });

        //
        // 接口
        presenter1 = new HUserInfoPresenter();
        presenter1.onCreate(this);
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        if (TextUtils.isEmpty(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_TOKEN))) {
//            return;
//        }
        presenter1.get_userinfo();

    }

    private void initsmart() {
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
            }
        });
        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            private int lastScrollY = 0;
            private int h = DensityUtil.dp2px(170);

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                scrolly = scrollY;
                if (lastScrollY < h) {
                    scrollY = Math.min(h, scrollY);
                    mScrollY = scrollY > h ? h : scrollY;
                }
                lastScrollY = scrollY;
            }
        });
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        if (scrolly == 0) {
            if (refreshLayout1 != null) {
                refreshLayout1.autoRefresh();
            }
        } else {
            scrollView1.smoothScrollTo(0, 0);
        }
    }

    private int scrolly = 0;

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    @Override
    public void OnUserInfoSuccess(HUserInfoBean bean) {
        refreshLayout1.finishRefresh(0);
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_SEX, bean.getChildGender());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_IMG, bean.getChildAvatar());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_NAME, bean.getNikeName());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_TEL, bean.getPhone());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_ALL, bean.getAddressDtl());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_CLASS, bean.getChildGradeName());

        HMyaddressBean myaddressBean = bean.getUserAddress();
        if (myaddressBean != null) {
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_NAME, myaddressBean.getUserName());
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_TEL, myaddressBean.getUserPhone());
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHENG, myaddressBean.getProvince());
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_SHI, myaddressBean.getCity());
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_QU, myaddressBean.getDistrict());
            MmkvUtils.getInstance().set_common(CommonUtils.MMKV_ADDRESS_DETAIL, myaddressBean.getAddressDtl());
        }

        iv1.loadImage(bean.getChildAvatar(), R.drawable.head_zanwu11);
//        LoginImgUtils.getInstance(getActivity()).get_head_icon_new(iv1, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_SEX));
        if (TextUtils.isEmpty(bean.getNikeName())) {
            tv1.setText("注册/登录");
        } else {
            tv1.setText(bean.getNikeName());
        }
        if (TextUtils.isEmpty(bean.getChildGradeName())) {
            tv2.setText("编辑个人资料");
        } else {
            tv2.setText(bean.getChildGradeName());
        }
        tv_yhq1.setText(bean.getCouponDesc());
    }

    @Override
    public void OnUserInfoNodata(String s) {
        refreshLayout1.finishRefresh(false);
        set_res();
        tv_yhq1.setText("");
    }

    @Override
    public void OnUserInfoFail(String s) {
        refreshLayout1.finishRefresh(false);
        set_res();
        tv_yhq1.setText("");
    }

    private void set_res() {
        iv1.loadImage(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_IMG), R.drawable.head_zanwu11);
//        LoginImgUtils.getInstance(getActivity()).get_head_icon_new(iv1, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_SEX));
        if (TextUtils.isEmpty(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_NAME))) {
            tv1.setText("注册/登录");
        } else {
            tv1.setText(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_NAME));
        }
        if (TextUtils.isEmpty(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_CLASS))) {
            tv2.setText("编辑个人资料");
        } else {
            tv2.setText(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_CLASS));
        }
    }
}
