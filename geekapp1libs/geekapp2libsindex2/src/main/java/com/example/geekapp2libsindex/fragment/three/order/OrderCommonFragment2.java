package com.example.geekapp2libsindex.fragment.three.order;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.example.biz3hxktindex.bean.HLunbotuBean1;
import com.example.biz3hxktindex.bean.HMyorderBean;
import com.example.biz3hxktindex.bean.HMyorderBeanNew;
import com.example.biz3hxktindex.bean.HMyorderBeanNew1;
import com.example.biz3hxktindex.presenter.HMyorderPresenter;
import com.example.biz3hxktindex.presenter.HMyorderdeletePresenter;
import com.example.biz3hxktindex.view.HMyorderView;
import com.example.biz3hxktindex.view.HMyorderdeleteView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.common.OrderCommonAdapter3;
import com.example.geekapp2libsindex.common.SlbBaseOrderFragment2;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


public class OrderCommonFragment2 extends SlbBaseOrderFragment2 implements HMyorderView {

    private List<HLunbotuBean1> mListbanner1;
    private HMyorderPresenter presenter1;
    private HMyorderdeletePresenter presenter2;
    //
    private OrderCommonAdapter3 mAdapter1;
    private List<HMyorderBeanNew1> mList1;
    private String tablayoutId;

    @Override
    public void call(Object value) {
        tablayoutId = (String) value;
        Toasty.normal(getActivity(), tablayoutId).show();
        //
//        retryData();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("id");
            MyLogUtil.e("---tablayoutId----", tablayoutId);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_order1_common3;
    }

    @Override
    protected void donetwork1() {
        emptyview1.loading();
        donetwork1_init(mAdapter1);
        //
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("OrderAct");
        filter.addAction("OrderAct1");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
//        if (getArguments() != null) {
//            tablayoutId = getArguments().getString("id");
//        }
//        MyLogUtil.e("---tablayoutId----", tablayoutId);
    }

    @Override
    protected void retryData1() {
        if (presenter1 == null) {
            mListbanner1 = new ArrayList<>();
            presenter1 = new HMyorderPresenter();
            presenter1.onCreate(this);
        }
        String limit = PAGE_SIZE + "";
        String page = mNextRequestPage + "";
        String orderStatus = tablayoutId;
        MyLogUtil.e("---tablayoutId1----", tablayoutId);
        presenter1.get_myorderlist2(page, limit, orderStatus);
    }

    @Override
    protected void onclick1() {
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                donetwork1_init(mAdapter1);
                mAdapter1.setNewData(null);
                mAdapter1.cancle_timer_all();
                retryData1();
            }
        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                donetwork1_init(mAdapter1);
                mAdapter1.setNewData(null);
                mAdapter1.cancle_timer_all();
                retryData1();
            }
        });
        mAdapter1.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HMyorderBeanNew1 bean = (HMyorderBeanNew1) adapter.getData().get(position);
//                if (view.getId() == R.id.tv0) {
//                    return;
//                }
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun---", bean.getId());
                //订单详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderDetailActivity");
                intent.putExtra("orderType", bean.getOrderType());
                intent.putExtra("orderId", bean.getId());
                startActivity(intent);
            }
        });
        mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, final int position) {
                final HMyorderBeanNew1 bean = (HMyorderBeanNew1) adapter.getData().get(position);
                int i = view.getId();
//                Toasty.normal(SlbBaseOrderAct.this, bean.getmBean().getText1()).show();
                MyLogUtil.e("--geekyun1---", bean.getId());
                if (i == R.id.tv77) {
                    // ceshi
//                    mAdapter1.remove_pos(bean, position);
//                    if (mAdapter1.getData().size() == 0) {
//                        emptyview1.nodata();
//                    }
                    new XPopup.Builder(getActivity())
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                            .setPopupCallback(new SimpleCallback() {
                                @Override
                                public void onCreated() {
                                    Log.e("tag", "弹窗创建了");
                                }

                                @Override
                                public void onShow() {
                                    Log.e("tag", "onShow");
                                }

                                @Override
                                public void onDismiss() {
                                    Log.e("tag", "onDismiss");
                                }

                                //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                                @Override
                                public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                                    return true;
                                }
                            }).asConfirm("提示", "确定要删除订单吗？",
                            "取消", "确定",
                            new OnConfirmListener() {
                                @Override
                                public void onConfirm() {
                                    //
                                    // 接口
                                    if (presenter2 == null) {
                                        presenter2 = new HMyorderdeletePresenter();
                                        presenter2.onCreate(new HMyorderdeleteView() {
                                            @Override
                                            public void OnMyorderdeleteSuccess(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                                // 删除订单
                                                mAdapter1.remove_pos(bean, position);
                                                if (mAdapter1.getData().size() == 0) {
                                                    emptyview1.nodata();
                                                }
                                            }

                                            @Override
                                            public void OnMyorderdeleteNodata(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                            }

                                            @Override
                                            public void OnMyorderdeleteFail(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                            }

                                            @Override
                                            public String getIdentifier() {
                                                return SystemClock.currentThreadTimeMillis() + "";
                                            }
                                        });
                                        presenter2.get_myorderlist_delete2(bean.getId(), bean.getOrderType());
                                    }
                                }
                            }, new OnCancelListener() {
                                @Override
                                public void onCancel() {

                                }
                            }, false)
                            .show();
                } else if (i == R.id.tv777) {
                    // ceshi
//                    mAdapter1.remove_pos(bean, position);
//                    if (mAdapter1.getData().size() == 0) {
//                        emptyview1.nodata();
//                    }
                    new XPopup.Builder(getActivity())
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                            .setPopupCallback(new SimpleCallback() {
                                @Override
                                public void onCreated() {
                                    Log.e("tag", "弹窗创建了");
                                }

                                @Override
                                public void onShow() {
                                    Log.e("tag", "onShow");
                                }

                                @Override
                                public void onDismiss() {
                                    Log.e("tag", "onDismiss");
                                }

                                //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                                @Override
                                public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                                    return true;
                                }
                            }).asConfirm("提示", "确定要取消订单吗？",
                            "取消", "确定",
                            new OnConfirmListener() {
                                @Override
                                public void onConfirm() {
                                    //
                                    // 接口
                                    if (presenter2 == null) {
                                        presenter2 = new HMyorderdeletePresenter();
                                        presenter2.onCreate(new HMyorderdeleteView() {
                                            @Override
                                            public void OnMyorderdeleteSuccess(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                                // 取消订单
                                                mAdapter1.remove_pos(bean, position);
                                                if (mAdapter1.getData().size() == 0) {
                                                    emptyview1.nodata();
                                                }
                                            }

                                            @Override
                                            public void OnMyorderdeleteNodata(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                            }

                                            @Override
                                            public void OnMyorderdeleteFail(String s) {
                                                ToastUtils.showLong(s);
                                                presenter2.onDestory();
                                                presenter2 = null;
                                            }

                                            @Override
                                            public String getIdentifier() {
                                                return SystemClock.currentThreadTimeMillis() + "";
                                            }
                                        });
                                        presenter2.get_myorderlist_delete(bean.getId(), bean.getOrderType());
                                    }
                                }
                            }, new OnCancelListener() {
                                @Override
                                public void onCancel() {

                                }
                            }, false)
                            .show();
                } else if (i == R.id.tv88) {
                    // 继续支付
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity");
//                    intent.putExtra("courseId", bean.getLiveCourseId());
                    intent.putExtra("orderId", bean.getId());
                    intent.putExtra("orderType", bean.getOrderType());
                    startActivity(intent);
                } else if (i == R.id.tv99) {
                    // 入学通知书
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.NoticeDetailActivity");
//                    intent.putExtra("courseId", bean.getLiveCourseId());
                    intent.putExtra("orderType", bean.getOrderType());
                    intent.putExtra("orderId", bean.getId());
                    startActivity(intent);
                }


            }
        });
        mAdapter1.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
//                is_zhu = false;
                loadMore();
                MyLogUtil.e("--geekyun--", "loadMore");
            }
        }, recyclerView1);
    }

    @Override
    protected void findview1() {
        emptyview1.notices("暂无订单，去选课中心看看吧…", "没有网络了,检查一下吧", "正在加载....", "");
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        mList1 = new ArrayList<>();
        mAdapter1 = new OrderCommonAdapter3();
        recyclerView1.setAdapter(mAdapter1);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        if (presenter1 != null) {
            presenter1.onDestory();
        }
        if (presenter2 != null) {
            presenter2.onDestory();
        }
        super.onDestroy ();
    }

    @Override
    public void onDestroyView() {
        mAdapter1.cancle_timer_all();
        MyLogUtil.e("sssssss", "onDestroy");
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen
        MyLogUtil.e("sssssss", "onResume");
        super.onResume();
    }

    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("OrderAct".equals(intent.getAction())) {
                    OrderCommonItem2Bean bean = intent.getParcelableExtra("OrderAct1");
//                    MyLogUtil.e("--geekyun-updateUI-tttttt", bean.getId());
                    if (bean != null) {
                        tablayoutId = bean.getId();
                        if (TextUtils.equals(tablayoutId, "0")) {
                            // 刷新
                            donetwork1_init(mAdapter1);
                            mAdapter1.setNewData(null);
                            retryData1();
                        }
                        if (TextUtils.equals(tablayoutId, "1")) {
                            // 滚动到顶部
                            getCate(bean);
                        }
                    } else {
                        // 其他页面通知更新订单状态
                        String id = intent.getStringExtra("id");
                        if (!TextUtils.isEmpty(id)) {
//                        emptyview1.loading();
                            donetwork1_init(mAdapter1);
                            mAdapter1.setNewData(null);
                            retryData1();
                        }
                    }
//                    int pos = 0;
//                    if (!TextUtils.isEmpty(id)) {
//                        // 更新订单状态
//                        List<HMyorderBean1> mlist = mAdapter1.getData();
//                        for (int i = 0; i < mlist.size(); i++) {
//                            if (TextUtils.equals(mlist.get(i).getId(), id)) {
//                                pos = i;
//                                mlist.get(pos).setButtonCancel("");
//                                mlist.get(pos).setButtonPay("");
//                            }
//                        }
//                        mAdapter1.setNewData(mlist);
//                        mAdapter1.notifyItemChanged(pos);
                }
            } catch (
                    Exception e) {
            }
        }

    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    private ViewSkeletonScreen skeletonScreen;

    @Override
    public void OnMyorderSuccess(HMyorderBean hMyorderBean) {

    }

    @Override
    public void OnMyorderNodata(String s) {

    }

    @Override
    public void OnMyorderFail(String s) {

    }

    @Override
    public void OnMyorderNewSuccess(HMyorderBeanNew hMyorderBean) {
        OnOrderSuccess();
        mList1 = new ArrayList<>();
        mList1 = hMyorderBean.getList();
        if (mNextRequestPage == 1) {
            setData(mAdapter1, true, mList1);
            if (mList1.size() == 0) {
                emptyview1.nodata();
            }
        } else {
            setData(mAdapter1, false, mList1);
        }
//        //
//        skeletonScreen = Skeleton.bind(refreshLayout1)
//                .load(R.layout.skeleton_view_layout)
//                .shimmer(true)
//                .angle(20)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .show();
//        //
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                skeletonScreen.hide();
//            }
//        }, 1500);
    }

    @Override
    public void OnMyorderNewNodata(String s) {
        OnOrderNodata();
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            emptyview1.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }

    @Override
    public void OnMyorderNewFail(String s) {
        OnOrderFail();
        if (mNextRequestPage == 1) {
            mAdapter1.setEnableLoadMore(true);
            // 根据需求修改bufen
            mAdapter1.setNewData(null);
            emptyview1.errorNet();
        } else {
            mAdapter1.loadMoreFail();
        }
    }
}
