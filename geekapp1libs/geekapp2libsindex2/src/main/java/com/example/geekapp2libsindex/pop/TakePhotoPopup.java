package com.example.geekapp2libsindex.pop;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.PicHelper;
import com.lxj.xpopup.core.BottomPopupView;

public class TakePhotoPopup extends BottomPopupView {
    private Context context;
    private TextView cameraTv;
    private TextView xcTv;
    private TextView closeTv;

    public TakePhotoPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_takephoto;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        cameraTv = findViewById(R.id.cameraTv);
        xcTv = findViewById(R.id.xcTv);
        closeTv = findViewById(R.id.closeTv);
        closeTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        cameraTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PicHelper.getInstance().toOpenCamera((Activity) getContext());
                dismiss();
            }
        });
        xcTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PicHelper.getInstance().toSelectPic((Activity) getContext(), false);
                dismiss();
            }
        });
    }
}
