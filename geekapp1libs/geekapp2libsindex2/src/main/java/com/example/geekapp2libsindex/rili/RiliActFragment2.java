package com.example.geekapp2libsindex.rili;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HlistItemTodayBean1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.RiliActFragment2Adapter;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;

import java.util.ArrayList;
import java.util.List;

public class RiliActFragment2 extends SlbBaseIndexFragment {

    private XRecyclerView recycler_view1;
    private String ids;
    private EmptyViewNew1 emptyview1_order;
    private RiliActFragment2Adapter riliActFragment2Adapter;
    private List<HlistItemTodayBean1> mList1;

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_riliactfragment2;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        rootView.findViewById(R.id.tv1).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SendToFragment("demo1的fragment1页面");
//            }
//        });
        emptyview1_order = rootView.findViewById(R.id.emptyview1_order);
        recycler_view1 = rootView.findViewById(R.id.recycler_view1);
        //
        emptyview1_order.bind(recycler_view1);
        emptyview1_order.notices("今日无课程，去选课中心看看吧…", "没有网络了,检查一下吧", "正在加载....", "");
        emptyview1_order.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                //重试
                donetwork();
            }
        });
        //
        recycler_view1.setLayoutManager(new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false));
        riliActFragment2Adapter = new RiliActFragment2Adapter();
        recycler_view1.setAdapter(riliActFragment2Adapter);
        //
        donetwork();
    }

    private void donetwork() {
        mList1 = new ArrayList<>();
        mList1.add(new HlistItemTodayBean1());
        mList1.add(new HlistItemTodayBean1());
        mList1.add(new HlistItemTodayBean1());
        mList1.add(new HlistItemTodayBean1());
        mList1.add(new HlistItemTodayBean1());
        riliActFragment2Adapter.setNewData(mList1);
    }


}
