package com.example.geekapp2libsindex.recycleview.commonlist;

import java.io.Serializable;
import java.util.List;

public class CommonClassClassCommonbean implements Serializable {
    private String text1;
    private List<CommonClassClassCommonbean3> text1_list;
    private String text2;
    private String text3;
    private String text4;
    private String text5;
    private String text6;
    private String text7;
    private String text8;
    private List<CommonClassClassCommonbean2> list;

    public CommonClassClassCommonbean() {
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public List<CommonClassClassCommonbean3> getText1_list() {
        return text1_list;
    }

    public void setText1_list(List<CommonClassClassCommonbean3> text1_list) {
        this.text1_list = text1_list;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText4() {
        return text4;
    }

    public void setText4(String text4) {
        this.text4 = text4;
    }

    public String getText5() {
        return text5;
    }

    public void setText5(String text5) {
        this.text5 = text5;
    }

    public String getText6() {
        return text6;
    }

    public void setText6(String text6) {
        this.text6 = text6;
    }

    public String getText7() {
        return text7;
    }

    public void setText7(String text7) {
        this.text7 = text7;
    }

    public String getText8() {
        return text8;
    }

    public void setText8(String text8) {
        this.text8 = text8;
    }

    public List<CommonClassClassCommonbean2> getList() {
        return list;
    }

    public void setList(List<CommonClassClassCommonbean2> list) {
        this.list = list;
    }
}
