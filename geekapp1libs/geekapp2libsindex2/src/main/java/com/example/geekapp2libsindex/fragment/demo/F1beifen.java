package com.example.geekapp2libsindex.fragment.demo;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.jiechangtu.DownloadShareImgUtil;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.CommonUtils;

import java.io.IOException;


public class F1beifen extends SlbBaseIndexFragment implements SurfaceHolder.Callback, SurfaceView.OnClickListener, Camera.PictureCallback {

    private String ids;
    private String tablayoutId;
    private ImageView iv_back1_order;
    private TextView tv_center_content;
    private SurfaceView mSurfaceView;
    private RelativeLayout rl1;
    private SurfaceHolder mSurfaceHolder;
    private Camera mCamera;
    private DownloadShareImgUtil downloadShareImgUtil;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.demo_f1;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        iv_back1_order = rootView.findViewById(R.id.iv_back1_order);
        rl1 = rootView.findViewById(R.id.rl1);
        mSurfaceView = rootView.findViewById(R.id.surfaceViewOverlap);
        mSurfaceView.setFocusable(true);
        mSurfaceView.setFocusableInTouchMode(true);
        mSurfaceView.setClickable(true);
        mSurfaceView.setOnClickListener(this);
        mSurfaceHolder = mSurfaceView.getHolder();
        //设置该SurfaceView是一个"推送"类型的SurfaceView
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mSurfaceHolder.addCallback(this);
        iv_back1_order.setVisibility(View.GONE);
        tv_center_content = rootView.findViewById(R.id.tv_center_content);
        tv_center_content.setText("保存到相册");
        tv_center_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bitmap bitmap = ShareImgCutUtil.getRelativeLayoutBitmap(rl1);
//                if (bitmap != null) {
//                    String aaaa = SystemClock.currentThreadTimeMillis() + ".png";
//                    downloadShareImgUtil.downloadPicture(true, getActivity(), bitmap, CommonUtils.img_file_url, aaaa);
//                }
            }
        });
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
        downloadShareImgUtil = new DownloadShareImgUtil();
        mSurfaceView.getHolder().addCallback(this);
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }

    //处理SurfaceView的点击事件
    @Override
    public void onClick(View v) {
        mCamera.takePicture(null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        //将图片保存至相册
        ContentResolver resolver = getActivity().getContentResolver();
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//        MediaStore.Images.Media.insertImage(resolver, bitmap, "t", "des");
        String aaaa = SystemClock.currentThreadTimeMillis() + ".JPEG";
        downloadShareImgUtil.downloadPicture(true, getActivity(), bitmap, CommonUtils.img_file_url, aaaa);

        //拍照后重新开始预览
        camera.startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
//        camera.setDisplayOrientation(90);
//        try {
//            camera.setPreviewDisplay(holder);
//            camera.startPreview();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        RelativeLayout.LayoutParams ll_param = (RelativeLayout.LayoutParams) rl1.getLayoutParams();
//        ll_param.width = RelativeLayout.LayoutParams.MATCH_PARENT;
//        ll_param.height = RelativeLayout.LayoutParams.MATCH_PARENT;
//
////        Double w = new Double((ScreenUtils.getScreenWidth()));
////        Double h = new Double((ScreenUtils.getScreenHeight()));
//        Double w = (double) (ScreenUtils.getScreenWidth());
//        Double h = (double) (ScreenUtils.getScreenHeight());
//        final int LARGEST_WIDTH = w.intValue();
//        final int LARGEST_HEIGHT = h.intValue();
        final int LARGEST_WIDTH = getActivity().getWindowManager().getDefaultDisplay().getWidth();
        final int LARGEST_HEIGHT = 500;
        int bestWidth = 0;
        int bestHeight = 0;
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
//        mCamera = Camera.open();
        Camera.Parameters parameters = mCamera.getParameters();

        //操作1 为相机设置某种特效
//        List<String> colorEffects=parameters.getSupportedColorEffects();
//        Iterator<String> iterator1=colorEffects.iterator();
//        while (iterator1.hasNext()) {
//            String effect = (String) iterator1.next();
//            if (effect.equals(Camera.Parameters.EFFECT_SOLARIZE)) {
//                //若支持过度曝光效果,则设置该效果
//                parameters.setColorEffect(Camera.Parameters.EFFECT_SOLARIZE);
//                break;
//            }
//        }
        //操作2 改变SurfaceView的大小
//        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
//        if (previewSizes.size() > 1) {
//            Iterator<Camera.Size> iterator2 = previewSizes.iterator();
//            while (iterator2.hasNext()) {
//                Camera.Size size = (Camera.Size) iterator2.next();
//                if (size.width > bestWidth && size.width <= LARGEST_WIDTH &&
//                        size.height > bestHeight && size.height <= LARGEST_HEIGHT) {
//                    bestWidth = size.width;
//                    bestHeight = size.height;
//                }
//            }
//            if (bestWidth != 0 && bestHeight != 0) {
//                parameters.setPreviewSize(bestWidth, bestHeight);
//                mSurfaceView.setLayoutParams
//                        (new RelativeLayout.LayoutParams(bestWidth, bestHeight));
//            }
//        }
        //操作3 当屏幕变化时,旋转角度.否则不对
        mCamera.setDisplayOrientation(90);

        //操作结束
        try {
            //将摄像头的预览显示设置为mSurfaceHolder
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            mCamera.release();
        }
        //设置输出格式
        parameters.setPictureFormat(PixelFormat.JPEG);
        //设置摄像头的参数.否则前面的设置无效
        mCamera.setParameters(parameters);
        //摄像头开始预览
        mCamera.startPreview();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
    }

    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */


}
