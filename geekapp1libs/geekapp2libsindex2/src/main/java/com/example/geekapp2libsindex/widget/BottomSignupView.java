package com.example.geekapp2libsindex.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.haier.cellarette.libutils.SlbLoginUtil2;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.umeng.analytics.MobclickAgent;

public class BottomSignupView extends FrameLayout {
    private Context context;
    private LinearLayout ll;
    private TextView freeTv;
    private TextView priceTv;
    private TextView signUpTv;
    private TextView bottomCenterTv;
    public final String SYSTEM = "system";
    public final String EXPERIENCE = "experience";
    private OnListener onListener;
    private Ondonetwork ondonetworkListener;


    public interface OnListener {
        void toOrder();
    }

    public interface Ondonetwork {
        void onFinish();
    }

    public void setOnListener(OnListener onListener) {
        this.onListener = onListener;
    }

    public Ondonetwork getOndonetworkListener() {
        return ondonetworkListener;
    }

    public void setOndonetworkListener(Ondonetwork ondonetworkListener) {
        this.ondonetworkListener = ondonetworkListener;
    }

    public BottomSignupView(@NonNull Context context) {
        super(context);
        initView(context);
    }

    public BottomSignupView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public BottomSignupView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.layout_bottom_signup, this);
        ll = findViewById(R.id.ll);
        freeTv = findViewById(R.id.freeTv);
        priceTv = findViewById(R.id.priceTv);
        signUpTv = findViewById(R.id.signUpTv);
        bottomCenterTv = findViewById(R.id.bottomCenterTv);
    }

    private CountDownTimer timer;
    private CourseBean courseBean;
    private String orderId;
    private String orderType;
    private boolean stop;

    public void setData(final CourseResponseBean responseBean) {
        this.courseBean = responseBean.getCourse();
        this.orderId = responseBean.getOrderId();
        this.orderType = responseBean.getOrderType();
        if (TextUtils.equals(courseBean.getClassAttribute(), EXPERIENCE)) {//体验班
            if (TextUtils.equals(courseBean.getSigned(), "1")) {//已预约
                bottomCenterTv.setText("已预约，去上课");
                bottomCenterTv.setVisibility(VISIBLE);
                bottomCenterTv.setEnabled(true);
                bottomCenterTv.setBackgroundColor(Color.parseColor("#FA9E49"));
                ll.setVisibility(GONE);
                bottomCenterTv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toClass();
                    }
                });
            } else {//未预约
                freeTv.setVisibility(VISIBLE);
                priceTv.setVisibility(View.GONE);
                signUpTv.setText("立即预约");
                bottomCenterTv.setVisibility(GONE);
                ll.setVisibility(VISIBLE);
                signUpTv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MobclickAgent.onEvent(context, "courseselection_systemfreeclass_joinbutton");
                        if (onListener != null) {
                            onListener.toOrder();
                        }
                    }
                });
            }
        } else {//系统班
            //////////////////////////////////////////////
            if (TextUtils.equals(courseBean.getSigned(), "1")) {//已报名
                destory();
                bottomCenterTv.setText("已报名，去上课");
                bottomCenterTv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toClass();
                    }
                });
                bottomCenterTv.setVisibility(VISIBLE);
                bottomCenterTv.setEnabled(true);
                bottomCenterTv.setBackgroundColor(Color.parseColor("#FA9E49"));
                ll.setVisibility(GONE);
            } else {//未报名
                if (TextUtils.equals("0", courseBean.getChargeFlag())) {//免费课
                    freeTv.setVisibility(VISIBLE);
                    priceTv.setVisibility(View.GONE);
                    signUpTv.setText("立即报名");
                    bottomCenterTv.setVisibility(GONE);
                    ll.setVisibility(VISIBLE);
                    signUpTv.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (TextUtils.equals("1", courseBean.getChargeFlag())) {
                                MobclickAgent.onEvent(context, "courseselection_systemclass_paybutton");
                            } else {
                                MobclickAgent.onEvent(context, "courseselection_systemfreeclass_joinbutton");
                            }
                            toPay();
                        }
                    });
                } else {//收费课
                    if (TextUtils.equals(courseBean.getOrderStatus(), "paid")) {//已付款
                        destory();
                        ll.setVisibility(GONE);
                        bottomCenterTv.setVisibility(VISIBLE);
                        bottomCenterTv.setEnabled(true);
                        bottomCenterTv.setBackgroundColor(Color.parseColor("#FA9E49"));
                        bottomCenterTv.setText("已报名，去上课");
                        bottomCenterTv.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toClass();
                            }
                        });
                    } else if (TextUtils.equals(courseBean.getOrderStatus(), "unpaid")) {//未付款
                        bottomCenterTv.setVisibility(VISIBLE);
                        bottomCenterTv.setEnabled(true);
                        bottomCenterTv.setBackgroundColor(Color.parseColor("#FA9E49"));
                        ll.setVisibility(GONE);
                        // 倒计时
                        MobclickAgent.onEvent(context, "courseselection_systemclass_notpaypage");
                        timer = new CountDownTimer(Long.valueOf(courseBean.getCountdownMills()), 1000) {
                            @Override
                            public void onTick(long l) {
                                long minute = l / (1000 * 60);
                                long second = l / 1000 % 60;

                                String m = minute + "";
                                String s = second + "";
                                if (minute < 10) {
                                    m = "0" + m;
                                }
                                if (second < 10) {
                                    s = "0" + s;
                                }
                                if (!stop) {
                                    bottomCenterTv.setText("待付款：剩余" + m + "分" + s + "秒");
                                }
                            }

                            @Override
                            public void onFinish() {
                                if (!stop) {
                                    //倒计时为0时执行此方法
                                    bottomCenterTv.setText("待付款：剩余0分0秒");
                                    bottomCenterTv.setEnabled(false);
                                    bottomCenterTv.setBackgroundColor(Color.parseColor("#cccccc"));
                                }
                            }
                        };
                        timer.start();

                        bottomCenterTv.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toPay();
                            }
                        });
                    } else {//未报名
                        freeTv.setVisibility(GONE);
                        priceTv.setVisibility(View.VISIBLE);
                        signUpTv.setText("立即报名");
                        bottomCenterTv.setVisibility(GONE);
                        ll.setVisibility(VISIBLE);

                        String price = "合计：¥" + courseBean.getPricePayable();
                        SpannableString nPriceSpannableString = new SpannableString(price);
                        nPriceSpannableString.setSpan(new AbsoluteSizeSpan(24, true), 4, price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        nPriceSpannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorFF4E08)), 3, price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        priceTv.setText(nPriceSpannableString);

                        signUpTv.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (TextUtils.equals("1", courseBean.getChargeFlag())) {
                                    MobclickAgent.onEvent(context, "courseselection_systemclass_paybutton");
                                } else {
                                    MobclickAgent.onEvent(context, "courseselection_systemfreeclass_joinbutton");
                                }
                                toPay();
                            }
                        });
                    }
                }
            }
        }
    }

    public void destory() {
        stop = true;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void toPay() {//跳支付

        if (SlbLoginUtil2.get().isUserLogin()) {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity");
//                intent.putExtra("courseId", courseBean.getId());
//                context.startActivity(intent);
//        hios://com.sairobo.hexiangketang.hs.act.slbapp.BooksListActivity{act}?id={s}2&name={s}geek&condition=login
//        String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity" + "{act}?courseId={s}" + courseBean.getId() + "&condition=login";

            String param;
            if (TextUtils.equals("unpaid", courseBean.getOrderStatus())) {
                param = "orderId={s}" + orderId + "&orderType={s}" + orderType;
            } else {
                param = "courseId={s}" + courseBean.getId();
            }
            String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity" + "{act}?" + param;
            HiosHelper.resolveAd((Activity) context, (Activity) context, url);
        } else {
            SlbLoginUtil2.get().loginTowhere((Activity) context, new Runnable() {
                @Override
                public void run() {

                    if (ondonetworkListener != null) {
                        ondonetworkListener.onFinish();
                    }
                }
            });
        }

    }

    private void toClass() {//跳课程
//        String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity" + "{act}?courseId={s}" + courseBean.getId() + "&condition=login";
//        HiosHelper.resolveAd((Activity) context, (Activity) context, url);
        if (SlbLoginUtil2.get().isUserLogin()) {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity");
//                intent.putExtra("courseId", courseBean.getId());
//                context.startActivity(intent);
//        hios://com.sairobo.hexiangketang.hs.act.slbapp.BooksListActivity{act}?id={s}2&name={s}geek&condition=login
//        String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity" + "{act}?courseId={s}" + courseBean.getId() + "&condition=login";
            String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity" + "{act}?courseId={s}" + courseBean.getId();
            HiosHelper.resolveAd((Activity) context, (Activity) context, url);
        } else {
            SlbLoginUtil2.get().loginTowhere((Activity) context, new Runnable() {
                @Override
                public void run() {

                    if (ondonetworkListener != null) {
                        ondonetworkListener.onFinish();
                    }
                }
            });
        }
//        SlbLoginUtil2.get().loginTowhere((Activity) context, new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
//                intent.putExtra("courseId", courseBean.getId());
//                context.startActivity(intent);
//            }
//        });
    }
}
