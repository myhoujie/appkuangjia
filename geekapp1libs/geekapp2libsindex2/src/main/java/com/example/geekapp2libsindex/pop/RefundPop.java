package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.lxj.xpopup.core.CenterPopupView;

public class RefundPop extends CenterPopupView {

    public RefundPop(@NonNull Context context) {
        super(context);
    }


    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_refund;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView okTv = findViewById(R.id.okTv);
        okTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}