package com.example.geekapp2libsindex.coupons;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.geek.libglide47.base.util.DisplayUtil;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 16);
            tbSpace = DisplayUtil.dip2px(context, 14);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = tbSpace;
            }
            outRect.bottom = tbSpace;
            outRect.left = outRect.right = lrSpace;
        }
    }