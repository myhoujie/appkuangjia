package com.example.geekapp2libsindex.rili;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.collection.SparseArrayCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.base.SlbBaseFragment;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;

public class RiliAct extends SlbBaseActivity implements View.OnClickListener {
    private ImageView ivBack1Order;
    private TextView tvCenterContent;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_riliact;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("课程表");
        setupFragments();
    }

    private void onclick() {
        ivBack1Order.setOnClickListener(this);
    }

    private void findview() {
        ivBack1Order = findViewById(R.id.iv_back1_order);
        tvCenterContent = findViewById(R.id.tv_center_content);

    }

    /**
     * 初始化首页fragments
     */
    private void setupFragments() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        SparseArrayCompat<Class<? extends SlbBaseFragment>> array = RiliActFragmentFactory.get();//一个版本模式bufen
        int size = array.size();
        SlbBaseFragment item;
        for (int i = 0; i < size; i++) {
            item = FragmentHelper.newFragment(array.valueAt(i), null);
            ft.replace(array.keyAt(i), item, item.getClass().getName());
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.iv_back1_order) {
            onBackPressed();
        } else if (i == R.id.tv_center_content) {
            //
        } else {

        }
    }

    /**
     * fragment间通讯bufen
     *
     * @param value 要传递的值
     * @param tag   要通知的fragment的tag
     */
    public void callFragment(Object value, String... tag) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment;
        for (String item : tag) {
            if (TextUtils.isEmpty(item)) {
                continue;
            }

            fragment = fm.findFragmentByTag(item);
            if (fragment instanceof SlbBaseIndexFragment) {
                ((SlbBaseIndexFragment) fragment).call(value);
            }
        }
    }
}
