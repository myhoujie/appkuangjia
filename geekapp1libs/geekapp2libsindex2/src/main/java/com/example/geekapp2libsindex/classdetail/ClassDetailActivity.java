package com.example.geekapp2libsindex.classdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.ClassDetailTeacherAdapter;
import com.example.geekapp2libsindex.adapter.ClassSection2Adapter;
import com.example.geekapp2libsindex.domain.ClassSectionStatusBean;
import com.example.geekapp2libsindex.widget.RVMoveToPosition;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.fanli.biz3hxktdetail.bean.ClassSectionBean;
import com.fanli.biz3hxktdetail.bean.ClassSectionResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.presenter.ClassSectionPresenter;
import com.fanli.biz3hxktdetail.presenter.DetailPresenter;
import com.fanli.biz3hxktdetail.presenter.GenseePresenter;
import com.fanli.biz3hxktdetail.view.ClassSectionView;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.fanli.biz3hxktdetail.view.GenseeSdkParamsView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class ClassDetailActivity extends SlbBaseActivity implements CourseDetailView, ClassSectionView, GenseeSdkParamsView {
    private TextView titleTv;
    private TextView dateTv;
    private TextView classHourTv;
    private XRecyclerView sectionRv;
    private ClassSection2Adapter sessionAdapter;
    private ClassDetailTeacherAdapter teacherAdapter;
    private DetailPresenter presenter;
    private String courseId;
    private RecyclerView teacherRv;
    private ClassSectionPresenter classSectionPresenter;
    // 分页
    private static int PAGE_SIZE = 10;
    private int mNextRequestPage = 1;

    private GenseePresenter genseePresenter;
    private ClassSectionBean classSectionBean;
    private EmptyViewNew1 emptyView;
    private View errorView;
    private View nodataView;
    private int toSectionPosition = -1;

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        if (classSectionPresenter != null) {
            classSectionPresenter.onDestory();
        }
        if (genseePresenter != null) {
            genseePresenter.onDestory();
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_classdetail;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        super.setup(savedInstanceState);
        MobclickAgent.onEvent(this, "learning_lessons");
        courseId = getIntent().getStringExtra("courseId");
        String orderNo = getIntent().getStringExtra("toSectionPosition");
        if (!TextUtils.isEmpty(orderNo)) {
            toSectionPosition = Integer.valueOf(orderNo);
        }
        if (toSectionPosition > PAGE_SIZE) {
            PAGE_SIZE = toSectionPosition;
        }
//        toSectionPosition = 40;
        findview();
        onclick();
        presenter = new DetailPresenter();
        presenter.onCreate(this);

        classSectionPresenter = new ClassSectionPresenter();
        classSectionPresenter.onCreate(this);

        genseePresenter = new GenseePresenter();
        genseePresenter.onCreate(this);

        doNetWork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void doNetWork() {
        emptyView.loading();
        presenter.getCourseDetail4Learn(courseId);
    }


    private void findview() {
        errorView = LayoutInflater.from(this).inflate(R.layout.empty_error, null);
        nodataView = LayoutInflater.from(this).inflate(R.layout.empty_nodata, null);

        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("课程详情");
        View ll_wrap = findViewById(R.id.ll_wrap);
        emptyView = new EmptyViewNew1(this);
        emptyView.attach(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        titleTv = findViewById(R.id.titleTv);
        dateTv = findViewById(R.id.dateTv);
        classHourTv = findViewById(R.id.classHourTv);
        teacherRv = findViewById(R.id.teacherRv);
        teacherRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        teacherRv.addItemDecoration(new SpaceItemDecoration2(this, 40));
        teacherAdapter = new ClassDetailTeacherAdapter(this);
        teacherRv.setAdapter(teacherAdapter);

        sectionRv = findViewById(R.id.sectionRv);
        sessionAdapter = new ClassSection2Adapter(toSectionPosition);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        sectionRv.setLayoutManager(linearLayoutManager);
        sectionRv.addItemDecoration(new SpaceItemDecoration(this, 0.5f));
        sectionRv.setAdapter(sessionAdapter);
        sectionRv.setHasFixedSize(true);
        sectionRv.setNestedScrollingEnabled(false);
        sectionRv.setFocusable(false);
        sessionAdapter.setEnableLoadMore(false);
    }

    private ValueAnimator animator;

    @Override
    protected void onStop() {
        super.onStop();
        if (animator != null) {
            animator.cancel();
            animator = null;
        }
    }

    private void onclick() {
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });
        sessionAdapter.setOn_huxi(new ClassSection2Adapter.On_huxi() {
            @Override
            public void OnFinish(final ConstraintLayout cl) {
                cl.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animator = ObjectAnimator.ofInt(cl, "backgroundColor", 0xfff7f7f7, 0xffffffff);//对背景色颜色进行改变，操作的属性为"backgroundColor",此处必须这样写，不能全小写,后面的颜色为在对应颜色间进行渐变
                        animator.setDuration(2000);
                        animator.setEvaluator(new ArgbEvaluator());//如果要颜色渐变必须要ArgbEvaluator，来实现颜色之间的平滑变化，否则会出现颜色不规则跳动
                        animator.start();
                    }
                }, 2000);

            }
        });
        sessionAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                classSectionBean = (ClassSectionBean) adapter.getItem(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassSectionActivity");
//                intent.putExtra("classSectionBean", classSectionBean);
                intent.putExtra("position", position);
                intent.putExtra("id", classSectionBean.getId());
                startActivity(intent);
            }
        });
        sessionAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getClassSectionList();
            }
        }, sectionRv);
    }

    @Override
    public void OnCourseSuccess(CourseResponseBean responseBean) {
        emptyView.success();
        CourseBean courseBean = responseBean.getCourse();
        //标题
        String title = "";
        boolean hasSubject = courseBean.getSubjectNames() != null && courseBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : courseBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + courseBean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < courseBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(this, TextUtils.equals(courseBean.getClassAttribute(), "experience") ? Color.parseColor("#666666") : Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        dateTv.setText(courseBean.getCourseTimeRange());
        classHourTv.setText(courseBean.getItemCount() + "课节");
        teacherAdapter.setNewData(courseBean.getTeacherList());

        getClassSectionList();
    }

    private void getClassSectionList() {
        classSectionPresenter.getClassSectionList(courseId, PAGE_SIZE + "", "" + mNextRequestPage);
    }

    @Override
    public void OnCourseNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OnCourseFail(String s) {
        emptyView.errorNet();
    }

    @Override
    public void OnGetShareInfoSuccess(ShareInfoBean shareInfoBean) {

    }

    @Override
    public void OnGetShareInfoNodata(String s) {

    }

    @Override
    public void OnGetShareInfoFail(String s) {

    }

    @Override
    public void OnClassSectionBeanSuccess(ClassSectionResponseBean classSectionResponseBean) {
        List<ClassSectionBean> classSectionBeanList = classSectionResponseBean.getList();
        if (mNextRequestPage == 1) {
            if (classSectionBeanList.size() == 0) {
                sessionAdapter.setEmptyView(nodataView);
            } else {
                sessionAdapter.setNewData(classSectionBeanList);
            }
        } else {
            sessionAdapter.addData(classSectionBeanList);
            emptyView.success();
        }
        mNextRequestPage++;
        if (classSectionBeanList.size() < PAGE_SIZE) {
            sessionAdapter.setEnableLoadMore(false);
        } else {
            sessionAdapter.loadMoreComplete();
        }
        if (toSectionPosition != -1) {
            sectionRv.post(new Runnable() {
                @Override
                public void run() {
                    RVMoveToPosition.createRVMoveToPosition(sectionRv, toSectionPosition).smoothMoveToPosition();
                    toSectionPosition = -1;
                }
            });
        }

    }

    @Override
    public void OnClassSectionBeanNodata(String s) {
        if (mNextRequestPage == 1) {
            sessionAdapter.setEmptyView(nodataView);
        } else {
            Toasty.normal(this, s).show();
        }
    }

    @Override
    public void OnClassSectionBeanFail(String s) {
        if (mNextRequestPage == 1) {
            sessionAdapter.setEmptyView(errorView);
        } else {
            sessionAdapter.loadMoreFail();
            Toasty.normal(this, s).show();
        }
    }

    @Override
    public void OnGenseeSuccess(GenseeBean genseeBean) {
        switch (classSectionBean.getItemStatus()) {
            case "1":
                MobclickAgent.onEvent(ClassDetailActivity.this, "lessons_live");
                LiveUtil.startLive(this, genseeBean);
                break;
            case "2":
                MobclickAgent.onEvent(ClassDetailActivity.this, "lessons_playback");
                LiveUtil.startVod(this, genseeBean);
                break;
        }
    }

    @Override
    public void OnGenseeNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnGenseeFail(String s) {
        Toasty.normal(this, s).show();
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int mSpace;

        public SpaceItemDecoration(Context context, float dpValue) {
            mSpace = DisplayUtil.dip2px(context, dpValue);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            outRect.bottom = mSpace;
        }
    }

    public class SpaceItemDecoration2 extends RecyclerView.ItemDecoration {

        private int mSpace;

        public SpaceItemDecoration2(Context context, int dpValue) {
            mSpace = DisplayUtil.dip2px(context, dpValue);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = mSpace;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void classSectionChange(ClassSectionStatusBean sectionStatusBean) {
        int position = sectionStatusBean.getPosition();
        if (position >= 0 && position < sessionAdapter.getData().size()) {
            sessionAdapter.getData().get(position).setExerciseStatus(sectionStatusBean.getExerciseStatus());
//            sessionAdapter.getData(sessionAdapter.getData().set(postion, ))
            sessionAdapter.notifyItemChanged(position);
        }
    }
}
