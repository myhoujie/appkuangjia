package com.example.geekapp2libsindex.classdetail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.ClassDetailServerTipAdapter;
import com.example.geekapp2libsindex.adapter.ClassDetailTeacherAdapter;
import com.example.geekapp2libsindex.domain.PayFail;
import com.example.geekapp2libsindex.domain.PaySuccess;
import com.example.geekapp2libsindex.listener.InitSuccessListener;
import com.example.geekapp2libsindex.pop.AboutWxQrcodePopup;
import com.example.geekapp2libsindex.pop.ClassDetailServerPopup;
import com.example.geekapp2libsindex.pop.CopyShareSuccessPopup;
import com.example.geekapp2libsindex.widget.BottomSignupView;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.pop.share.PopForShare;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseServiceBean;
import com.fanli.biz3hxktdetail.bean.ExpressForm;
import com.fanli.biz3hxktdetail.bean.PayBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.presenter.DetailPresenter;
import com.fanli.biz3hxktdetail.presenter.PayPresenter;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.fanli.biz3hxktdetail.view.PayView;
import com.geek.libglide47.base.GlideImageView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.google.android.material.tabs.TabLayout;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.tablayout.TabSelectAdapter;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.widget.LxLinearLayout;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.SlbLoginUtil2;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupAnimation;
import com.umeng.analytics.MobclickAgent;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class ClassDetailFromMainActivity extends SlbBaseActivity implements CourseDetailView, PayView, InitSuccessListener {

    private ImageView shareIv;
    private GlideImageView topIv;
    private TextView titleTv;
    private TextView dateTv;
    private TextView classHourTv;
    private TextView nPriceTv;
    private TextView oPriceTv;
    private TextView numTv;
    private RecyclerView teacherRv;
    private LxLinearLayout ll_server;
    private RecyclerView serverRv;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;
    private BottomSignupView bottomSignupView;
    private ClassDetailTeacherAdapter teacherAdapter;
    private DetailPresenter presenter;
    private ClassDetailFragment classDetailFragment;
    private ClassDetailServerTipAdapter classDetailServerTipAdapter;
    private String courseId;
    private boolean firstIn = true;
    private PayPresenter payPresenter;
    private EmptyViewNew1 emptyView;

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        if (payPresenter != null) {
            payPresenter.onDestory();
        }
        bottomSignupView.destory();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_classdetail_from_main;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.setup(savedInstanceState);
        MobclickAgent.onEvent(this, "courseselection_details");
        courseId = getIntent().getStringExtra("courseId");
        findview();
        onclick();
        presenter = new DetailPresenter();
        presenter.onCreate(this);

        payPresenter = new PayPresenter();
        payPresenter.onCreate(this);

        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(), getTitleList(), getFragmentList());
        viewPager.setAdapter(viewPageAdapter);

        TabUtils.setIndicatorWidth(tabLayout, 100);
        tabLayout.setTabIndicatorFullWidth(false);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);
//        emptyView.loading();
//        doNetWork();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!firstIn) {
//            doNetWork();
//        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void update(PaySuccess paySuccess) {
        doNetWork();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void update(PayFail payFail) {
        doNetWork();
    }

    private void doNetWork() {
//        String courseId = getIntent().getStringExtra("courseId");
//        courseId = "12";
        emptyView.loading();
        firstIn = false;
        presenter.getCourseDetail(courseId);
        presenter.getShareInfo(courseId);
    }

    private List<String> getTitleList() {
        List<String> titleList = new ArrayList();
        titleList.add("课程详情");
        titleList.add("课程表");
        return titleList;
    }

    private List<Fragment> getFragmentList() {
        List<Fragment> fragmentList = new ArrayList();
        classDetailFragment = ClassDetailFragment.newInstance();
        classDetailFragment.setInitSuccessListener(this);
        fragmentList.add(classDetailFragment);
        fragmentList.add(ClassScheduleFragment.newInstance(courseId));
        return fragmentList;
    }


    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("课程详情");
        View ll_wrap = findViewById(R.id.ll_wrap);
//        emptyView = new EmptyViewNew1(this);
//        emptyView.attach(ll_wrap);

        emptyView = findViewById(R.id.emptyview1);
        emptyView.bind(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        shareIv = findViewById(R.id.shareIv);
        shareIv.setVisibility(View.VISIBLE);
        topIv = findViewById(R.id.topIv);
        titleTv = findViewById(R.id.titleTv);
        dateTv = findViewById(R.id.dateTv);
        classHourTv = findViewById(R.id.classHourTv);
        nPriceTv = findViewById(R.id.nPriceTv);
        oPriceTv = findViewById(R.id.oPriceTv);
        numTv = findViewById(R.id.numTv);

        teacherRv = findViewById(R.id.teacherRv);
        teacherRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        teacherRv.addItemDecoration(new SpaceItemDecoration(this, 40));
        teacherAdapter = new ClassDetailTeacherAdapter(this);
        teacherRv.setAdapter(teacherAdapter);

        ll_server = findViewById(R.id.ll_server);
        ll_server.setTouch(true);
        serverRv = findViewById(R.id.serverRv);
        serverRv.setLayoutManager(new GridLayoutManager(this, 4));
        serverRv.addItemDecoration(new ServerTipItemDecoration(this));
        classDetailServerTipAdapter = new ClassDetailServerTipAdapter();
        serverRv.setAdapter(classDetailServerTipAdapter);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewpager);

        oPriceTv.getPaint().setAntiAlias(true);//抗锯齿
        oPriceTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);  // 设置中划线并加清晰

        bottomSignupView = findViewById(R.id.bottomSignupView);
    }

    private void onclick() {
        ll_server.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showServerPop();
            }
        });
//        classDetailServerTipAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                showServerPop();
//            }
//        });
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });
        shareIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });
        tabLayout.addOnTabSelectedListener(new TabSelectAdapter() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TabUtils.tabSelect(tabLayout, tab);
            }
        });
    }

    private void showServerPop() {
        MobclickAgent.onEvent(ClassDetailFromMainActivity.this, "courseselection_details_servicepop");
        new XPopup.Builder(ClassDetailFromMainActivity.this)
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .asCustom(new ClassDetailServerPopup(ClassDetailFromMainActivity.this, courseBean == null ? new ArrayList<CourseServiceBean>() : courseBean.getServiceTagList())/*.enableDrag(false)*/)
                .show();
    }

    // 试读完成弹窗bufen
    private PopForShare popSharePay;
    private String share_title = "";
    private String share_name = "";
    private String share_img = "";
    private String share_imgUrl = "";
    private String share_imgUrl2 = "";
    private String share_url = "";

    private void share() {
        MobclickAgent.onEvent(this, "courseselection_details_sharebutton");
        popSharePay = new PopForShare(this, share_title, share_name, share_img, share_imgUrl, share_imgUrl2, share_url, "courseselection_details_sharepage_wechatfriendsbutton", "courseselection_details_sharepage_friendsbutton", "courseselection_details_sharepage_linkbutton", "courseselection_details_sharepage_cancelbutton", new PopForShare.OnFinishResultClickListener() {
            @Override
            public void onWeChat1() {
                // 微信小程序

            }

            @Override
            public void onWeChat2() {
                // 朋友圈
                new XPopup.Builder(ClassDetailFromMainActivity.this)
                        .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                        .autoOpenSoftInput(true)
                        .asCustom(new CopyShareSuccessPopup(ClassDetailFromMainActivity.this, "分享成功")).show();
            }
        });

    }

    private CourseBean courseBean;
    private CourseResponseBean responseBean;

    @Override
    public void OnCourseSuccess(CourseResponseBean responseBean) {
        emptyView.success();
        this.responseBean = responseBean;
        courseBean = responseBean.getCourse();
        topIv.loadImage(courseBean.getCoverImg(), R.drawable.default_teather);

        //标题
        String title = "";
        boolean hasSubject = courseBean.getSubjectNames() != null && courseBean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : courseBean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + courseBean.getCourseName());
        if (hasSubject) {
            for (int i = 0; i < courseBean.getSubjectNames().size(); i++) {
                spannableString.setSpan(new RoundBackgroundColorSpan(this, TextUtils.equals(courseBean.getClassAttribute(), "experience") ? Color.parseColor("#666666") : Color.parseColor("#F78451"), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        titleTv.setText(spannableString);

        dateTv.setText(courseBean.getCourseTimeRange());
        classHourTv.setText(courseBean.getItemCount() + "课节");
        if (TextUtils.equals("0", courseBean.getChargeFlag())) {//免费
            nPriceTv.setText("免费");
        } else {
            SpannableString nPriceSpannableString = new SpannableString("¥" + courseBean.getPricePayable());
            AbsoluteSizeSpan relativeSizeSpan = new AbsoluteSizeSpan(14, true);
            nPriceSpannableString.setSpan(relativeSizeSpan, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            nPriceTv.setText(nPriceSpannableString);
        }
        if (Double.valueOf(courseBean.getPriceMarked()) == 0) {
            oPriceTv.setVisibility(View.GONE);
        } else {
            oPriceTv.setText("¥" + courseBean.getPriceMarked());
            oPriceTv.setVisibility(View.VISIBLE);
        }
        numTv.setText(courseBean.getItemCount() + "人已报名");
        numTv.setVisibility(View.GONE);

        teacherAdapter.setNewData(courseBean.getTeacherList());

//        if (courseBean != null && courseBean.getServiceTagList() != null) {
//            int size = courseBean.getServiceTagList().size();
//            if (size > 0) {
//                int position = 0;
//                int maxLength = courseBean.getServiceTagList().get(0).getName().length();
//                for (int i = 1; i < size; i++) {
//                    if (courseBean.getServiceTagList().get(i).getName().length() > maxLength) {
//                        position = i;
//                    }
//                }
//                classDetailServerTipAdapter.setTextSizeByPosition(position);
//            }
//        }

        classDetailServerTipAdapter.setNewData(courseBean.getServiceTagList());
        classDetailFragment.onDetailReceive(courseBean);

        bottomSignupView.setData(responseBean);
        bottomSignupView.setOnListener(new BottomSignupView.OnListener() {
            @Override
            public void toOrder() {//预约公开课
                SlbLoginUtil2.get().loginTowhere(ClassDetailFromMainActivity.this, new Runnable() {
                    @Override
                    public void run() {
                        payPresenter.signUp(new ExpressForm(), courseBean.getId(), courseBean.getLiveOrderId(), courseBean.getExpressFlag());
                    }
                });
            }
        });
        bottomSignupView.setOndonetworkListener(new BottomSignupView.Ondonetwork() {
            @Override
            public void onFinish() {
                doNetWork();
            }
        });
    }


    @Override
    public void OnCourseNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OnCourseFail(String s) {
        emptyView.errorNet();
    }

    @Override
    public void OnGetShareInfoSuccess(ShareInfoBean shareInfoBean) {
        share_title = shareInfoBean.getTitle();
        share_name = shareInfoBean.getDesc();
        share_img = shareInfoBean.getImgUrl();
        share_imgUrl = shareInfoBean.getUrlForFriend();
        share_imgUrl2 = shareInfoBean.getUrlForMoments();
        share_url = shareInfoBean.getShareUrl();
    }

    @Override
    public void OnGetShareInfoNodata(String s) {

    }

    @Override
    public void OnGetShareInfoFail(String s) {

    }

    @Override
    public void onSuccess() {
        doNetWork();
    }

    @Override
    public void OnPaySuccess(PayBean payBean) {
        new XPopup.Builder(this)
                .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                .asCustom(new AboutWxQrcodePopup(this, "预约成功", "关注公众号微信", true, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_hxEduWechatImg), MmkvUtils.getInstance().get_common(CommonUtils.MMKV_hxEduWechatNum))/*.enableDrag(false)*/)
                .show();
        if (courseBean != null) {
            courseBean.setSigned("1");
            bottomSignupView.setData(responseBean);
        } else {
            presenter.getCourseDetail(courseId);
        }
    }

    @Override
    public void OnPayNodata(String s) {
        if (SlbLoginUtil2.get().isUserLogin()) {
            doNetWork();
        } else {
            Toasty.normal(this, s).show();
        }
    }

    @Override
    public void OnPayFail(String s) {
        Toasty.normal(this, s).show();
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int mSpace;

        public SpaceItemDecoration(Context context, int dpValue) {
            mSpace = DisplayUtil.dip2px(context, dpValue);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = mSpace;
        }
    }

    public class ServerTipItemDecoration extends RecyclerView.ItemDecoration {
        private int lrGap;
        private int tbGap;
        private Paint mPaint;
        private int radius;
        private Paint mPaint2;
        private Paint mPaint3;

        public ServerTipItemDecoration(Context context) {
            lrGap = DisplayUtil.dip2px(context, 14);
            tbGap = DisplayUtil.dip2px(context, 10);
            radius = DisplayUtil.dip2px(context, 2);
            mPaint = new Paint();
            mPaint.setColor(Color.parseColor("#d8d8d8"));
            mPaint2 = new Paint();
            mPaint2.setColor(Color.YELLOW);
            mPaint3 = new Paint();
            mPaint3.setColor(Color.RED);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
//            if ((position + 1) % 4 == 1) {
//                outRect.right = lrGap;
//            } else if ((position + 1) % 4 == 0) {
//                outRect.left = lrGap;
//            } else {
//                outRect.left = outRect.right = lrGap;
//            }
            outRect.left = outRect.right = lrGap;
            if (position < 4) {
                outRect.top = tbGap;
            }
            outRect.bottom = tbGap;
        }

        @Override
        public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.onDraw(c, parent, state);
            int childCount = parent.getChildCount();
            for (int i = 1; i < childCount; i++) {
                if (i % 4 != 0) {
                    final View child = parent.getChildAt(i - 1);
                    c.drawCircle(child.getRight() + lrGap, (child.getBottom() + child.getTop()) / 2, radius, mPaint);
                }

            }
        }

//        @Override
//        public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
//            super.onDrawOver(c, parent, state);
//            int childCount = parent.getChildCount();
//            for (int i = 1; i < childCount; i++) {
//                if (i % 4 == 1) {
//                    View child = parent.getChildAt(i - 1);
//                    c.drawRect(child.getLeft(), child.getTop(), child.getRight(), child.getBottom(), mPaint2);
//                }
//                if (i % 4 == 2) {
//                    View child = parent.getChildAt(i - 1);
//                    c.drawRect(child.getLeft(), child.getTop(), child.getRight(), child.getBottom(), mPaint3);
//                }
//            }
//
//        }
    }


    @Override
    public void back(View v) {
        MobclickAgent.onEvent(this, "courseselection_details_backbutton");
        super.back(v);
    }
}
