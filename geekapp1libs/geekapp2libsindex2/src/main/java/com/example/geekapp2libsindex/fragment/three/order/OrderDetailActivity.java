package com.example.geekapp2libsindex.fragment.three.order;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.DiscountAdapter;
import com.example.geekapp2libsindex.adapter.OrderClassAdapter;
import com.example.geekapp2libsindex.domain.PaySuccess;
import com.example.geekapp2libsindex.pop.TwoBtnPop;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.fanli.biz3hxktdetail.bean.LiveExpressBean;
import com.fanli.biz3hxktdetail.bean.LivePOrderBean;
import com.fanli.biz3hxktdetail.bean.LiveSonOrderBean;
import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.fanli.biz3hxktdetail.presenter.OrderOrCourseDetailPresenter;
import com.fanli.biz3hxktdetail.presenter.OrderPresenter;
import com.fanli.biz3hxktdetail.view.OrderDetailView;
import com.fanli.biz3hxktdetail.view.OrderOrCourseDetailView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class OrderDetailActivity extends SlbBaseActivity implements OrderDetailView, OrderOrCourseDetailView {
    private TextView topTipTv;
    private View ll_countdown;
    private TextView countdownTv;
    private TextView receiveInfoTipTv;
    private TextView receiveInfoTv;
    private TextView orderNumTv;
    private TextView orderTimeTv;
    private View ll_bottom;
    private TextView cancelOrderTv;
    private TextView payTv;
    private OrderPresenter presenter;
    private OrderOrCourseDetailPresenter orderOrCourseDetailPresenter;
    private String orderId;
    private String orderType;
    private EmptyViewNew1 emptyView;
    private boolean cancelSuccess;
    private OrderClassAdapter orderClassAdapter;
    private TextView classTitleTv;
    private RecyclerView orderClassRv;
    private RecyclerView discountRv;
    private DiscountAdapter discountAdapter;
    private TextView finalPriceTv;
    private TextView delOrderTv;
    private View ll_delOrder;
    private TextView orderStatusTimeTipTv;
    private TextView orderStatusTimeTv;
    private View line1V;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        orderId = getIntent().getStringExtra("orderId");
        orderType = getIntent().getStringExtra("orderType");
        presenter = new OrderPresenter();
        presenter.onCreate(this);

        orderOrCourseDetailPresenter = new OrderOrCourseDetailPresenter();
        orderOrCourseDetailPresenter.onCreate(this);
        findview();
        onclick();
        doNetWork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void update(PaySuccess paySuccess) {
        doNetWork();
    }

    private void doNetWork() {
        emptyView.loading();
        orderOrCourseDetailPresenter.get_orderDetail(orderId, orderType);
    }

    private void cancelOrder() {
        if (popupView != null) {
            popupView.dismiss();
        }
        presenter.cancelOrder(orderId, orderType);
    }

    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("订单详情");
        View ll_wrap = findViewById(R.id.ll_wrap);
        emptyView = new EmptyViewNew1(this);
        emptyView.attach(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        topTipTv = findViewById(R.id.topTipTv);
        ll_countdown = findViewById(R.id.ll_countdown);
        countdownTv = findViewById(R.id.countdownTv);
        receiveInfoTipTv = findViewById(R.id.receiveInfoTipTv);
        receiveInfoTv = findViewById(R.id.receiveInfoTv);
        orderNumTv = findViewById(R.id.orderNumTv);
        orderTimeTv = findViewById(R.id.orderTimeTv);
        ll_bottom = findViewById(R.id.ll_bottom);
        orderStatusTimeTipTv = findViewById(R.id.orderStatusTimeTipTv);
        orderStatusTimeTv = findViewById(R.id.orderStatusTimeTv);
        cancelOrderTv = findViewById(R.id.cancelOrderTv);
        payTv = findViewById(R.id.payTv);
        line1V = findViewById(R.id.line1V);

        classTitleTv = findViewById(R.id.classTitleTv);
        orderClassRv = findViewById(R.id.orderClassRv);
        orderClassAdapter = new OrderClassAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        orderClassRv.setNestedScrollingEnabled(false);
        orderClassRv.setLayoutManager(linearLayoutManager);
//        orderClassRv.addItemDecoration(new SpaceItemDecoration(this));
        orderClassRv.setAdapter(orderClassAdapter);

        discountRv = findViewById(R.id.discountRv);
        discountAdapter = new DiscountAdapter();
        LinearLayoutManager discountLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        discountRv.setNestedScrollingEnabled(false);
        discountRv.setLayoutManager(discountLayoutManager);
//        discountRv.addItemDecoration(new SpaceItemDecoration(this));
        discountRv.setAdapter(discountAdapter);

        finalPriceTv = findViewById(R.id.finalPriceTv);
        ll_delOrder = findViewById(R.id.ll_delOrder);
        delOrderTv = findViewById(R.id.delOrderTv);
    }

    private BasePopupView popupView;

    private void onclick() {
        cancelOrderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(OrderDetailActivity.this, "courseselection_systemclass_notpay_cancelorderbutton");
                popupView = new XPopup.Builder(OrderDetailActivity.this)
                        .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                        .asCustom(new TwoBtnPop(OrderDetailActivity.this, "提示", "确认要取消订单吗?", "取消", "确定", new TwoBtnPop.OnBtnListener() {
                            @Override
                            public void onLeft() {
                                popupView.dismiss();
                            }

                            @Override
                            public void onRight() {
                                cancelOrder();
                            }
                        })).show();
            }
        });
        payTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(OrderDetailActivity.this, "courseselection_systemclass_notpay_continuepaybutton");
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.PayActivity");
                intent.putExtra("orderType", orderType);
                intent.putExtra("orderId", orderId);
                startActivity(intent);
            }
        });
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });
        delOrderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDelOrderPopView();
            }
        });
        orderClassAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //首页选课进来的课程详情
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
                intent.putExtra("courseId", ((LiveSonOrderBean) adapter.getItem(position)).getLiveCourseId());
                startActivity(intent);
            }
        });
    }

    private String getOrderStatusName(String orderStatus) {
        //unpaid-待支付,paid-已支付,cancel-已取消,refund-已退款
        String ret = "";
        switch (orderStatus) {
            case "unpaid":
                ret = "待支付";
                break;
            case "paid":
                ret = "已支付";
                break;
            case "cancel":
                ret = "已取消";
                break;
            case "refund":
                ret = "已退款";
                break;
        }
        return ret;
    }

    private CountDownTimer timer;

    @Override
    public void OrderOrCourseDetailSuccess(OrderOrCourseDetailResponseBean responseBean) {
        emptyView.success();
        LivePOrderBean orderBean = responseBean.getLivePOrder();
        topTipTv.setText(getOrderStatusName(orderBean.getOrderStatus()));
        if (!TextUtils.isEmpty(orderBean.getPkgTitle())) {
            classTitleTv.setText(orderBean.getPkgTitle());
            classTitleTv.setVisibility(View.VISIBLE);
        } else {
            classTitleTv.setVisibility(View.GONE);
        }
        if (TextUtils.equals(orderBean.getOrderStatus(), "cancel")) {
            ll_delOrder.setVisibility(View.VISIBLE);
            orderStatusTimeTipTv.setText("取消时间：");
            orderStatusTimeTv.setText(orderBean.getOrderCancelTime());
        } else {
            ll_delOrder.setVisibility(View.GONE);
            if (!TextUtils.equals(orderBean.getOrderStatus(), "unpaid")) {
                orderStatusTimeTipTv.setText("支付时间：");
                orderStatusTimeTv.setText(orderBean.getOrderPayTime());
            }
        }
        if (TextUtils.equals(orderBean.getOrderStatus(), "unpaid")) {//未付款
            ll_bottom.setVisibility(View.VISIBLE);
            ll_countdown.setVisibility(View.VISIBLE);
            long countDownSecond = Long.valueOf(orderBean.getCountDownSecond());
            if (countDownSecond > 0) {
                ll_bottom.setVisibility(View.VISIBLE);
                ll_countdown.setVisibility(View.VISIBLE);

                timer = new CountDownTimer(countDownSecond, 1000) {
                    @Override
                    public void onTick(long l) {
                        long minute = l / (1000 * 60);
                        long second = l / 1000 % 60;
                        String m = minute + "";
                        String s = second + "";
                        if (minute < 10) {
                            m = "0" + m;
                        }
                        if (second < 10) {
                            s = "0" + s;
                        }
                        countdownTv.setText(m + "分" + s + "秒");
                    }

                    @Override
                    public void onFinish() {
                        countdownTv.setText("0分0秒");
                        orderOrCourseDetailPresenter.get_orderDetail(orderId, orderType);
                    }
                };
                timer.start();
            } else {
                countdownTv.setText("0分0秒");
            }
        } else {
            ll_bottom.setVisibility(View.GONE);
            ll_countdown.setVisibility(View.GONE);
        }
        if (responseBean.getLiveExpress() == null || TextUtils.isEmpty(responseBean.getLiveExpress().getAddressDtl())) {
            receiveInfoTipTv.setVisibility(View.GONE);
            receiveInfoTv.setVisibility(View.GONE);
        } else {
            receiveInfoTipTv.setVisibility(View.VISIBLE);
            receiveInfoTv.setVisibility(View.VISIBLE);
            LiveExpressBean liveExpressBean = responseBean.getLiveExpress();
            String receiveInfo = liveExpressBean.getUserName() + " " + liveExpressBean.getUserPhone() + "\n" + liveExpressBean.getProvince() + liveExpressBean.getCity() + liveExpressBean.getDistrict() + liveExpressBean.getAddressDtl();
            receiveInfoTv.setText(receiveInfo);
        }
        orderNumTv.setText(orderBean.getOrderNo());
        orderTimeTv.setText(orderBean.getOrderCreateTime());
        if(!ListUtil.isEmpty(orderBean.getSons())&&orderBean.getSons().size()>1){
            line1V.setVisibility(View.VISIBLE);
        }else{
            line1V.setVisibility(View.GONE);
        }
        orderClassAdapter.setNewData(orderBean.getSons());
        if(ListUtil.isEmpty(orderBean.getDiscounts())){
            discountRv.setVisibility(View.GONE);
        }else{
            discountAdapter.setNewData(orderBean.getDiscounts());
            discountRv.setVisibility(View.VISIBLE);
        }
        String str = "应付：¥";
        String payPrice = TextUtils.isEmpty(orderBean.getAmountPayable()) ? "0" : orderBean.getAmountPayable();
        if (TextUtils.equals(orderBean.getOrderStatus(), "paid") || TextUtils.equals(orderBean.getOrderStatus(), "refund")) {
            str = "实付：¥";
            payPrice = TextUtils.isEmpty(orderBean.getAmountFinal()) ? "0" : orderBean.getAmountFinal();
        }
        String price = str + payPrice;
        SpannableString priceSpannableString = new SpannableString(price);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(14, true), 3, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        priceSpannableString.setSpan(new AbsoluteSizeSpan(18, true), 4, price.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        priceSpannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_333333)), 3, price.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        priceSpannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 3, price.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        finalPriceTv.setText(priceSpannableString);
    }

    @Override
    public void OrderOrCourseDetailNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OrderOrCourseDetailFail(String s) {
        emptyView.errorNet();
    }


    @Override
    public void OnOrdeCancelSuccess(Object o) {
        cancelSuccess = true;
        orderOrCourseDetailPresenter.get_orderDetail(orderId, orderType);
    }

    @Override
    public void OnOrderCancelNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnOrderCancelFail(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnOrderDeleteSuccess(Object o) {
        Toasty.normal(this, "删除成功").show();
        cancelSuccess = true;
        finish();
    }

    @Override
    public void OnOrderDeleteNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnOrderDeleteFail(String s) {
        Toasty.normal(this, s).show();
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        int space;

        public SpaceItemDecoration(Context context) {
            space = DisplayUtil.dip2px(context, 1);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = space;
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = space;
            }
        }
    }

    private BasePopupView delOrderPopupView;

    private void showDelOrderPopView() {
        delOrderPopupView = new XPopup.Builder(this)
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .asCustom(new TwoBtnPop(this, "提示", "确认要删除订单吗?", "取消", "确定", new TwoBtnPop.OnBtnListener() {
                    @Override
                    public void onLeft() {
                        delOrderPopupView.dismiss();
                    }

                    @Override
                    public void onRight() {
                        delOrder();
                    }
                })).show();
    }

    private void delOrder() {
        if (delOrderPopupView != null) {
            delOrderPopupView.dismiss();
        }
        presenter.delOrder(orderId, orderType);
    }


    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        if (orderOrCourseDetailPresenter != null) {
            orderOrCourseDetailPresenter.onDestory();
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (popupView != null) {
            popupView.dismiss();
        }
        if (delOrderPopupView != null && delOrderPopupView.isShow()) {
            delOrderPopupView.dismiss();
        }
        EventBus.getDefault().unregister(this);
        if (cancelSuccess) {
            Intent msgIntent = new Intent();
            msgIntent.setAction("OrderAct");
            msgIntent.putExtra("id", orderId);
            LocalBroadcastManagers.getInstance(getApplicationContext()).sendBroadcast(msgIntent);
        }
//        Intent msgIntent = new Intent();
//        msgIntent.setAction("OrderAct");
//        msgIntent.putExtra("id", "11226");
//        LocalBroadcastManagers.getInstance(getApplicationContext()).sendBroadcast(msgIntent);
        super.onDestroy();
    }
}
