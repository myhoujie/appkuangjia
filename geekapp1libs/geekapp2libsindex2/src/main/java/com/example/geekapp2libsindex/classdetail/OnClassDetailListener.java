package com.example.geekapp2libsindex.classdetail;

import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;

public interface OnClassDetailListener {
    void onDetailReceive(CourseBean courseBean);
    void onTeacherReceive(TeacherBean teacherBean);
}
