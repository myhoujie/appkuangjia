package com.example.geekapp2libsindex.common;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HMyorderBean1;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.widget.RoundBackgroundColorSpan2;
import com.haier.cellarette.baselibrary.flowlayout.FlowLayout;
import com.haier.cellarette.baselibrary.flowlayout.TagAdapter;
import com.haier.cellarette.baselibrary.flowlayout.TagFlowLayout;

import java.util.List;

public class OrderCommonAdapter2 extends BaseQuickAdapter<HMyorderBean1, BaseViewHolder> {

    public OrderCommonAdapter2() {
        super(R.layout.recycleview_hxkt_order_item1);
    }

    @Override
    protected void convert(BaseViewHolder helper, HMyorderBean1 item) {
        TextView tv10 = helper.itemView.findViewById(R.id.tv10);
        TextView tv11 = helper.itemView.findViewById(R.id.tv11);
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        TextView tv4 = helper.itemView.findViewById(R.id.tv4);
        TextView tv5 = helper.itemView.findViewById(R.id.tv5);
        TextView tv6 = helper.itemView.findViewById(R.id.tv6);
        TextView tv7 = helper.itemView.findViewById(R.id.tv7);
        TextView tv8 = helper.itemView.findViewById(R.id.tv8);
        TextView tv9 = helper.itemView.findViewById(R.id.tv9);
        TagFlowLayout tagflowlayout10 = helper.itemView.findViewById(R.id.tagflowlayout10);
        //
//        get_flowlayout0(tagflowlayout10, item.getSubjectNames());
//        tv2.setText(mContext.getResources().getString(R.string.slb_tips1, item.getOrderName()));
        set_flowlayout(tv2, item);
        if (item.getOrderStatusName().contains("待")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_F58585, mContext.getTheme()));
            } else {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_F58585));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_999999, mContext.getTheme()));
            } else {
                tv11.setTextColor(mContext.getResources().getColor(R.color.color_999999));
            }
        }
        tv11.setText(item.getOrderStatusName());
        tv3.setText(item.getCourseTimeRange());
        tv4.setText(item.getLecturerName());
        tv6.setText(item.getAmountPayable());

        CountDownTimer countDownTimer = timer_list.get(tv10.hashCode());
        if (countDownTimer != null) {
            //将复用的倒计时清除
            countDownTimer.cancel();
            countDownTimer = null;
        }

        if (item.getCurrentCountDownSecond() == 0) {
            item.setCurrentCountDownSecond(System.currentTimeMillis() + Long.valueOf(item.getCountDownSecond()));
        }

        if (item.getCurrentCountDownSecond() - System.currentTimeMillis() > 0) {
            startTime(item.getCurrentCountDownSecond() - System.currentTimeMillis(), tv10, item, helper.getAdapterPosition());
        } else {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.CHINA);
//            String dates = TimeUtils.millis2String(item.getCountDownSecond(), dateFormat);
            tv10.setText(item.getCreateTimeName());
        }
        if (TextUtils.isEmpty(item.getButtonCancel())) {
            tv7.setVisibility(View.GONE);
        } else {
            tv7.setVisibility(View.VISIBLE);
            tv7.setText(item.getButtonCancel());
        }
        if (TextUtils.isEmpty(item.getButtonPay())) {
            tv8.setVisibility(View.GONE);
        } else {
            tv8.setVisibility(View.VISIBLE);
            tv8.setText(item.getButtonPay());
        }
        if (TextUtils.isEmpty(item.getButtonInfo())) {
            tv9.setVisibility(View.GONE);
        } else {
            tv9.setVisibility(View.VISIBLE);
            tv9.setText(item.getButtonInfo());
        }
        helper.addOnClickListener(R.id.tv7);
        helper.addOnClickListener(R.id.tv8);
        helper.addOnClickListener(R.id.tv9);
    }

    private void set_flowlayout(TextView tv1, HMyorderBean1 bean) {
        //标题
        String title = "";
        boolean hasSubject = bean.getSubjectNames() != null && bean.getSubjectNames().size() > 0;
        if (hasSubject) {
            for (String subject : bean.getSubjectNames()) {
                title += subject;
            }
        }
        SpannableString spannableString = new SpannableString(title + bean.getOrderName());
        if (hasSubject) {
            for (int i = 0; i < bean.getSubjectNames().size(); i++) {
                String colorString = "#F78451";
                if (TextUtils.equals("system", bean.getClassAttribute())) {
                    colorString = "#F78451";
                } else if (TextUtils.equals("experience", bean.getClassAttribute())) {
                    colorString = "#666666";
                }
                spannableString.setSpan(new RoundBackgroundColorSpan2(mContext, Color.parseColor(colorString), Color.parseColor("#FFFFFF")), i, i + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new AbsoluteSizeSpan(11, true), i, i + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }
        tv1.setText(spannableString);
    }


    private SparseArray<CountDownTimer> timer_list = new SparseArray<>();

    public void cancle_timer_all() {
        for (int i = 0, length = timer_list.size(); i < length; i++) {
            CountDownTimer cdt = timer_list.get(timer_list.keyAt(i));
            if (cdt != null) {
                cdt.cancel();
            }
        }
    }

    /**
     * 从x开始倒计时
     *
     * @param x
     */
    public void startTime(long x, final TextView tv1, final HMyorderBean1 item, final int pos) {
        CountDownTimer timer = new CountDownTimer(x, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int remainTime = (int) (millisUntilFinished / 1000L);// 182
                int hour = remainTime / 60;
                int second = remainTime % 60;
                String text = hour + "分" + second + "秒";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    tv1.setText(Html.fromHtml(mContext.getResources().getString(R.string.slb_order_tip1, text)));
                }
                item.setCurrentCountDownSecond(System.currentTimeMillis() + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                remove_pos(item, pos);
                this.cancel();
            }
        };
        timer.start();
        timer_list.put(tv1.hashCode(), timer);
    }

    public void remove_pos(HMyorderBean1 item, int pos) {
        if (pos < 0 || pos >= getItemCount()) {
            return;
        }
        //
        getData().remove(item);
        notifyItemRemoved(pos);
    }

//    public void cancel_handler(){
//        orderHandlerUtils.doCancel();
//    }

    private void get_flowlayout0(final TagFlowLayout tagflowlayout1, List<String> list) {
        //mFlowLayout.setMaxSelectCount(3);
        final LayoutInflater mInflater = LayoutInflater.from(mContext);
        tagflowlayout1.setAdapter(new TagAdapter(list) {
            @Override
            public View getView(FlowLayout parent, int position, Object bean) {
                View view = mInflater.inflate(R.layout.recycleview_hxkt_class_img_item2,
                        tagflowlayout1, false);
                TextView tv1 = view.findViewById(R.id.tv1);
                String bean1 = (String) bean;
                //
                GradientDrawable drawable = new GradientDrawable();
                //设置圆角大小
                drawable.setCornerRadius(5);
                //设置边缘线的宽以及颜色
                drawable.setStroke(1, Color.parseColor("#F78451"));
                //设置shape背景色
                drawable.setColor(Color.parseColor("#F78451"));
                //设置到TextView中
                tv1.setBackground(drawable);
                tv1.setPadding(10, 5, 10, 5);
                tv1.setTextColor(Color.parseColor("#ffffff"));
                tv1.setText(bean1);
                return view;
            }
        });
    }
}