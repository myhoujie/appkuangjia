package com.example.geekapp2libsindex.fragment.three.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;

public class AboutUsAct extends SlbBaseActivity implements View.OnClickListener {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private GlideImageView iv1;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;
    private String tip1 = "https://s2.51cto.com//wyfs02/M01/89/BA/wKioL1ga-u7QnnVnAAAfrCiGnBQ946_middle.jpg";
    private String tip4 = "400-0737365";


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_aboutus;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("关于我们");
        iv1.loadImage("", R.drawable.slb_logo3);
        tv2.setText("版本号:" + AppUtils.getAppVersionName());
        tv3.setText(getResources().getString(R.string.slb_aboutus_tip1));
        tv6.setText(getResources().getString(R.string.slb_aboutus_tip2, tip4));

    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);
        tv6.setOnClickListener(this);
    }

    private void findview() {
        ivBack = (ImageView) findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) findViewById(R.id.tv_center_content);
        iv1 = (GlideImageView) findViewById(R.id.iv1);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else if (view.getId() == R.id.tv4) {
            // 用户协议
            HiosHelper.resolveAd(AboutUsAct.this, AboutUsAct.this, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_serviceProtocol));

        } else if (view.getId() == R.id.tv5) {
            // 隐私政策
            HiosHelper.resolveAd(AboutUsAct.this, AboutUsAct.this, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_privacyPolicy));
        } else if (view.getId() == R.id.tv6) {
            // 拨打电话
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + tip4));
            startActivity(intent);
        } else {

        }
    }


}
