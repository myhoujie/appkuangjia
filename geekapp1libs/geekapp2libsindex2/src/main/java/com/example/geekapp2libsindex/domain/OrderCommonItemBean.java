package com.example.geekapp2libsindex.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by geek on 2016/2/25.
 */
public class OrderCommonItemBean implements Serializable, Parcelable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String text_id;
    private String text_content;

    public OrderCommonItemBean() {
    }

    public OrderCommonItemBean(String text_id, String text_content) {
        this.text_id = text_id;
        this.text_content = text_content;
    }

    public String getText_id() {
        return text_id;
    }

    public void setText_id(String text_id) {
        this.text_id = text_id;
    }

    public String getText_content() {
        return text_content;
    }

    public void setText_content(String text_content) {
        this.text_content = text_content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text_id);
        dest.writeString(this.text_content);
    }

    protected OrderCommonItemBean(Parcel in) {
        this.text_id = in.readString();
        this.text_content = in.readString();
    }

    public static final Parcelable.Creator<OrderCommonItemBean> CREATOR = new Parcelable.Creator<OrderCommonItemBean>() {
        @Override
        public OrderCommonItemBean createFromParcel(Parcel source) {
            return new OrderCommonItemBean(source);
        }

        @Override
        public OrderCommonItemBean[] newArray(int size) {
            return new OrderCommonItemBean[size];
        }
    };
}
