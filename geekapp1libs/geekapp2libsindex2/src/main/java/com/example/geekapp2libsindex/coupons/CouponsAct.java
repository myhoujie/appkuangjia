package com.example.geekapp2libsindex.coupons;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCouponsBean;
import com.example.biz3hxktindex.presenter.HCouponsPresenter;
import com.example.biz3hxktindex.view.HCouponsView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.common.SlbBaseOrderAct2;
import com.example.geekapp2libsindex.domain.OrderCommonItem2Bean;
import com.example.geekapp2libsindex.fragment.one.OneBean1;
import com.haier.cellarette.baselibrary.tablayout.TabUtils;
import com.haier.cellarette.baselibrary.yanzheng.LocalBroadcastManagers;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.FragmentHelper;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;

import java.util.ArrayList;
import java.util.List;

public class CouponsAct extends SlbBaseOrderAct2 implements View.OnClickListener, HCouponsView {

    private List<HCategoryBean> mListbanner1;
    private HCouponsPresenter presenter1;

    @Override
    protected void onDestroy() {
        presenter1.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_coupons1;
    }

    @Override
    protected void donetwork1() {
        tvCenterContent.setText("我的优惠券");
        presenter1 = new HCouponsPresenter();
        presenter1.onCreate(this);

    }

    @Override
    protected void retryData1() {
        // 接口
        emptyview.loading();
        mListbanner1 = new ArrayList<>();
        presenter1.get_coupons();
    }

    @Override
    protected void onclick1() {
        tvCenterContent.setOnClickListener(this);
        tv_right.setOnClickListener(this);
        emptyview.notices("你还没有优惠券哦", "没有网络了,检查一下吧", "正在加载....", "");
    }


    @Override
    protected void findview1() {
        //
//        skeletonScreen = Skeleton.bind(ll_refresh1)
//                .load(R.layout.skeleton_view_layout)
//                .shimmer(true)
//                .angle(20)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .show();
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText("");
        tv_right.setBackgroundResource(R.drawable.hxkt_icon_sm1);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_center_content) {
            OrderCommonItem2Bean bean = null;
            //
            Intent msgIntent = new Intent();
            msgIntent.setAction("OrderAct");
//            if (getScrolly() == 0) {
            if (!isEnscrolly()) {
                //
//                retryData1();
                // fragment传值 刷新
                bean = new OrderCommonItem2Bean("0", current_id, true);
//                EventBus.getDefault().post(bean);
                msgIntent.putExtra("OrderAct1", bean);
                LocalBroadcastManagers.getInstance(CouponsAct.this).sendBroadcast(msgIntent);
            } else {
                // fragment传值 滚动
                bean = new OrderCommonItem2Bean("1", current_id, true);
                msgIntent.putExtra("OrderAct1", bean);
                LocalBroadcastManagers.getInstance(CouponsAct.this).sendBroadcast(msgIntent);
            }
        } else if (view.getId() == R.id.tv_right) {
            // 用户协议
            HiosHelper.resolveAd(CouponsAct.this, this, MmkvUtils.getInstance().get_common(CommonUtils.MMKV_couponstips));
        } else {

        }
    }

    @Override
    public void OnCouponsSuccess(HCouponsBean hCouponsBean) {
        OnOrderSuccess();
        List<OneBean1> mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCouponsBean.getList().size(); i++) {
            mDataTablayout.add(new OneBean1(hCouponsBean.getList().get(i).getKey1(), hCouponsBean.getList().get(i).getValue1(), false));
        }
        // viewpager
        current_id = mDataTablayout.get(0).getTab_id();
//        init_tablayout(mDataTablayout);
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mDataTablayout.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mDataTablayout.get(i).getTab_id());
            CouponsCommonFragment fragment1 = FragmentHelper.newFragment(CouponsCommonFragment.class, bundle);
            mFragmentList.add(fragment1);
        }
        init_tablayout(mDataTablayout, mFragmentList);


        TabUtils.setIndicatorWidth(tablayoutMy, 30, 680);

//        skeletonScreen.hide();
    }


    @Override
    public void OnCouponsNodata(String s) {
        OnOrderNodata();
    }

    @Override
    public void OnCouponsFail(String s) {
        OnOrderFail();
    }
}
