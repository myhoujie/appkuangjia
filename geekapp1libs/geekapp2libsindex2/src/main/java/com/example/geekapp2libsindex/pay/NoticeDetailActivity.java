package com.example.geekapp2libsindex.pay;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.NoticeDetailTaskAdapter;
import com.example.geekapp2libsindex.domain.Task;
import com.example.geekapp2libsindex.pop.AboutWxQrcodePopup;
import com.example.geekapp2libsindex.pop.EditNamePopup;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.pop.bottompay.wechatutils.WeChat1Utils;
import com.fanli.biz3hxktdetail.bean.TaskBean;
import com.fanli.biz3hxktdetail.presenter.TaskPresenter;
import com.fanli.biz3hxktdetail.presenter.UpdateUserRealNamePresenter;
import com.fanli.biz3hxktdetail.view.CommonView;
import com.fanli.biz3hxktdetail.view.TaskView;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.CommonUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

public class NoticeDetailActivity extends SlbBaseActivity implements TaskView, EditNamePopup.OnSubmitListener, CommonView {
    private RecyclerView taskRv;
    private NoticeDetailTaskAdapter taskAdapter;
    private TaskPresenter presenter;
    private TaskBean taskBean;
    private UpdateUserRealNamePresenter updateUserRealNamePresenter;
    private TextView addTeacherWxTv;

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        if (updateUserRealNamePresenter != null) {
            updateUserRealNamePresenter.onDestory();
        }
        super.onDestroy();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_notice_detail;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        MobclickAgent.onEvent(this, "noticepage");
        findview();
        onclick();

        doNetWork();
    }

    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("入学通知书");
        addTeacherWxTv = findViewById(R.id.addTeacherWxTv);
        taskRv = findViewById(R.id.taskRv);
        taskAdapter = new NoticeDetailTaskAdapter();
        taskRv.setLayoutManager(new LinearLayoutManager(this));
        taskRv.setAdapter(taskAdapter);
        List<Task> taskList = new ArrayList<>();
        taskList.add(new Task(1, "录入真实姓名", "方便老师认识学生", true));
        taskList.add(new Task(2, "添加老师微信", "和老师一起学习", true));
        taskList.add(new Task(3, "关注公众号微信", "及时收到上课练习通知", true));
        taskAdapter.setNewData(taskList);
    }

    private void onclick() {
        taskAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.btnTv) {
                    btnClick(position);
                }
            }
        });

        addTeacherWxTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wxId = taskBean == null ? "" : taskBean.getTeacherWechatId();
                if (copy(wxId)) {
                    Toasty.normal(NoticeDetailActivity.this, "老师微信号已复制").show();
                    if (WeChat1Utils.getInstance(getApplicationContext()).isWeChatAppInstalled()) {
                        addTeacherWxTv.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                openWxClient();
                            }
                        }, 2500);
                    }
                }
            }
        });
    }

    private void openWxClient() {
        Intent lan = getPackageManager().getLaunchIntentForPackage("com.tencent.mm");
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(lan.getComponent());
        startActivity(intent);
    }

    private void doNetWork() {
        presenter = new TaskPresenter();
        presenter.onCreate(this);
        presenter.getTaskInfo(getIntent().getStringExtra("orderId"));

        updateUserRealNamePresenter = new UpdateUserRealNamePresenter();
        updateUserRealNamePresenter.onCreate(this);
    }

    @Override
    public void OnSuccess(TaskBean taskBean) {
        this.taskBean = taskBean;
        Task task = new Task(1, "录入真实姓名", "方便老师认识学生", TextUtils.equals(taskBean.getFinishFlag(), "0"));
        taskAdapter.setData(0, task);
    }

    @Override
    public void OnSuccess(Object o) {
        Task task = new Task(1, "录入真实姓名", "方便老师认识学生", false);
        taskAdapter.setData(0, task);
        editPop.dismiss();
    }

    @Override
    public void OnNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnFail(String s) {
        Toasty.normal(this, s).show();
    }

    private BasePopupView editPop;

    public void btnClick(int position) {
        String qrUrl = "";
        String qrId = "";
        if (position == 0) {
            MobclickAgent.onEvent(this, "noticepage_name");
            editPop = new XPopup.Builder(this)
                    .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                    .autoOpenSoftInput(true)
                    .asCustom(new EditNamePopup(this, this)).show();
        } else if (position == 1) {
            MobclickAgent.onEvent(this, "noticepage_teacherwechatbutton");
            if (taskBean != null) {
                qrUrl = taskBean.getTeacherWechatQr();
                qrId = taskBean.getTeacherWechatId();
            }
            new XPopup.Builder(this)
                    .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                    .asCustom(new AboutWxQrcodePopup(this, "添加老师微信", "和老师一起学习", false, qrUrl, qrId, "teacherwechat_savebutton", "teacherwechat_copybutton")/*.enableDrag(false)*/)
                    .show();
        } else if (position == 2) {
            MobclickAgent.onEvent(this, "noticepage_publicmumberbutton");
            if (taskBean != null) {
                qrUrl = taskBean.getSubscriptionQr();
                qrId = taskBean.getSubscriptionId();
            } else {
                qrUrl = CommonUtils.MMKV_hxEduWechatImg;
            }
            new XPopup.Builder(this)
                    .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
                    .asCustom(new AboutWxQrcodePopup(this, "关注公众号微信", "获取上课提醒，领取学习资料", false, qrUrl, qrId, "publicmumber_savebutton", "publicmumber_copybutton", true)/*.enableDrag(false)*/)
                    .show();
        }

    }

    @Override
    public void onSubimt(String name) {
        updateUserRealNamePresenter.updateUserRealName(name);
    }

    private boolean copy(String copyStr) {
        try {
            if (TextUtils.isEmpty(copyStr)) {
                return false;
            }
            //获取剪贴板管理器
            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            // 创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("Label", copyStr);
            // 将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
