package com.example.geekapp2libsindex.adapter;

import android.text.TextUtils;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.fanli.biz3hxktdetail.bean.DiscountBean;

public class DiscountAdapter extends BaseQuickAdapter<DiscountBean, BaseViewHolder> {

    public DiscountAdapter() {
        super(R.layout.adapter_discount);
    }

    @Override
    protected void convert(BaseViewHolder helper, DiscountBean discount) {
        TextView titleTv = helper.itemView.findViewById(R.id.titleTv);
        TextView priceTv = helper.itemView.findViewById(R.id.priceTv);
        if (TextUtils.equals(discount.getDiscountType(),"1")) {
            titleTv.setText("优惠券优惠");
        } else {
            titleTv.setText(discount.getDiscountName());
        }
        priceTv.setText("￥" + discount.getAmountDiscount());
    }
}
