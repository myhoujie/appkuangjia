package com.example.geekapp2libsindex.fragment.one;

import androidx.collection.SparseArrayCompat;
import androidx.fragment.app.Fragment;

public class OneFactory1New {

    /**
     * viewpager页大小
     */
    public static int PAGE_COUNT = 4;
    /**
     * viewpager每页的itemview id
     */
    public static String PAGE_LAYOUT_ID = "fragment_hxkt_one_layout_pager_item_";
    /**
     * 默认显示第几页
     */
    public static int DEFAULT_PAGE_INDEX = 0;

    private static SparseArrayCompat<Class<? extends Fragment>> sIndexFragments = new SparseArrayCompat<>();

    static {

        //sIndexFragments.put(R.id.fragment_one_pager_index_0_0, OneFragment11.class);//模块1
        //sIndexFragments.put(R.id.fragment_one_pager_index_1_0, OneFragment12.class);//模块2

    }

    public static Class<? extends Fragment> get(int id) {
        if (sIndexFragments.indexOfKey(id) < 0) {
            throw new UnsupportedOperationException("cannot find fragment by " + id);
        }
        return sIndexFragments.get(id);
    }

    public static SparseArrayCompat<Class<? extends Fragment>> get() {
        return sIndexFragments;
    }

}
