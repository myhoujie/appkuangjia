package com.example.geekapp2libsindex.classdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.adapter.TeacherSessionAdatper;
import com.example.slbappcomm.base.SlbBaseFragment;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;
import com.fanli.biz3hxktdetail.presenter.TeacherPresenter;
import com.fanli.biz3hxktdetail.view.TeacherView;
import com.geek.libglide47.base.util.DisplayUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;

import java.util.List;


public class TeacherSessionListFragment extends SlbBaseFragment implements TeacherView {
    private RecyclerView sessionRv;
    private TeacherSessionAdatper teacherSessionAdatper;
    //    private SmartRefreshLayout refreshLayout;
    // 分页
    private static final int PAGE_SIZE = 10;
    private int mNextRequestPage = 1;
    private String teacherId;
    private TeacherPresenter presenter;
    private EmptyViewNew1 emptyView;

    public static TeacherSessionListFragment newInstance(String teacherId) {
        TeacherSessionListFragment fragment = new TeacherSessionListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("teacherId", teacherId);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_teacher_session;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        refreshLayout = rootView.findViewById(R.id.smartRefreshLayout);
        sessionRv = rootView.findViewById(R.id.scheduleRv);
        sessionRv.setBackgroundResource(R.color.f5f5f5);
        teacherSessionAdatper = new TeacherSessionAdatper();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        sessionRv.setNestedScrollingEnabled(true);
        sessionRv.setLayoutManager(linearLayoutManager);
        sessionRv.addItemDecoration(new SpaceItemDecoration(getContext()));
        sessionRv.setAdapter(teacherSessionAdatper);
        teacherSessionAdatper.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CourseBean bean = (CourseBean) adapter.getItem(position);

                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailFromMainActivity");
                intent.putExtra("courseId", bean.getId());
                startActivity(intent);
            }
        });

        emptyView = rootView.findViewById(R.id.emptyview1);
        emptyView.bind(sessionRv);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                getData();
            }
        });

//        refreshLayout.setEnableRefresh(false);
//        refreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
//            @Override
//            public void onLoadmore(RefreshLayout refreshlayout) {
//                getData();
//            }
//
//            @Override
//            public void onRefresh(RefreshLayout refreshlayout) {
//
//            }
//        });
        teacherId = getArguments().getString("teacherId");
        presenter = new TeacherPresenter();
        presenter.onCreate(this);

        getData();
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        super.onDestroy();
    }

    private void getData() {
        presenter.getTeacherCourseList(teacherId, mNextRequestPage, PAGE_SIZE);
    }


    @Override
    public void OnTeacherInfoSuccess(TeacherBean teacherBean) {

    }

    @Override
    public void OnTeacherInfoNodata(String s) {

    }

    @Override
    public void OnTeacherInfoFail(String s) {

    }

    @Override
    public void OnTeacherSessionListSuccess(List<CourseBean> list) {
        if (list.size() == 0) {
            emptyView.nodata();
        } else {
            teacherSessionAdatper.setNewData(list);
        }

//        refreshLayout.finishLoadmore();
//        if (mNextRequestPage == 1) {
//            if (list.size() == 0) {
//                emptyView.nodata();
//            } else {
//                teacherSessionAdatper.setNewData(list);
//            }
//        } else {
//            teacherSessionAdatper.addData(list);
//            emptyView.success();
//        }
//        mNextRequestPage++;
//        if (list.size() < PAGE_SIZE) {
//            refreshLayout.setEnableLoadmore(false);
//        }
    }

    @Override
    public void OnTeacherSessionListNodata(String s) {
//        if (mNextRequestPage == 1) {
//            emptyView.nodata();
//        } else {
//            Toasty.normal(getContext(), s).show();
//        }

        emptyView.nodata();
    }

    @Override
    public void OnTeacherSessionListFail(String s) {
//        if (mNextRequestPage == 1) {
//            emptyView.errorNet();
//        } else {
//            Toasty.normal(getContext(), s).show();
//        }

        emptyView.errorNet();
    }

    public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 16);
            tbSpace = DisplayUtil.dip2px(context, 12);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if (position == 0) {
                outRect.top = tbSpace;
            }
            outRect.bottom = tbSpace;
            outRect.left = outRect.right = lrSpace;
        }
    }
}