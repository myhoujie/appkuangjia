package com.example.geekapp2libsindex.fragment.three;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapter;
import com.example.geekapp2libsindex.pop.banji.BanjiAdapterType;
import com.example.geekapp2libsindex.pop.banji.BanjiCommonbean;
import com.example.geekapp2libsindex.pop.banji.BanjiModel;
import com.example.geekapp2libsindex.pop.banji.BanjiModel2;
import com.haier.cellarette.libutils.CommonUtils;
import com.example.slbappcomm.base.SlbBaseIndexFragment;
import com.example.slbappcomm.pop.share.PopForShare;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.geek.libglide47.glide48.GlideAppUtils;
import com.haier.cellarette.baselibrary.assetsfitandroid.fileassets.GetAssetsFileMP3TXTJSONAPKUtil;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.style.cityjd.JDCityConfig;
import com.lljjcoder.style.cityjd.JDCityPicker;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.shaohui.bottomdialog.BottomDialog;


public class OneFragment3 extends SlbBaseIndexFragment {

    private String ids;
    private SmartRefreshLayout refreshLayout1;
    private ClassicsHeader smartHeader1;
    private EmptyViewNew1 emptyview1;
    private NestedScrollView scrollView1;
    private View parallax;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;
    private TextView tv7;
    private TextView tv8;
    private TextView tv9;
    private ImageView iv9;
    private int mOffset = 0;
    private int mScrollY = 0;
    //
//    private List<SLBBannerBean1> mListbanner1;
//    private SLBBannerPresenter presenter1;
    private String tablayoutId;
    private BasePopupView loadingPopup;

    @Override
    public void call(Object value) {
        ids = (String) value;
        Toasty.normal(getActivity(), ids).show();
        //
//        retryData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        // 从缓存中拿出头像bufen

        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
//        Bundle arg = getArguments();
        if (getArguments() != null) {
            tablayoutId = getArguments().getString("tablayoutId");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_hxkt_one3;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1);
        smartHeader1 = rootView.findViewById(R.id.smart_header1);
        emptyview1 = rootView.findViewById(R.id.emptyview1);
        scrollView1 = rootView.findViewById(R.id.scroll_view1);
        parallax = rootView.findViewById(R.id.parallax);
        tv1 = rootView.findViewById(R.id.tv1);
        tv2 = rootView.findViewById(R.id.tv2);
        tv3 = rootView.findViewById(R.id.tv3);
        tv4 = rootView.findViewById(R.id.tv4);
        tv5 = rootView.findViewById(R.id.tv5);
        tv6 = rootView.findViewById(R.id.tv6);
        tv7 = rootView.findViewById(R.id.tv7);
        tv8 = rootView.findViewById(R.id.tv8);
        tv9 = rootView.findViewById(R.id.tv9);
        iv9 = rootView.findViewById(R.id.iv9);
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 省市区
                JDCityPicker cityPicker = new JDCityPicker();
                JDCityConfig jdCityConfig = new JDCityConfig.Builder().build();

                jdCityConfig.setShowType(JDCityConfig.ShowType.PRO_CITY_DIS);
                cityPicker.init(getActivity());
                cityPicker.setConfig(jdCityConfig);
                cityPicker.setOnCityItemClickListener(new OnCityItemClickListener() {
                    @Override
                    public void onSelected(ProvinceBean province, CityBean city, DistrictBean district) {
                        tv1.setText("城市选择结果：\n" + province.getName() + "(" + province.getId() + ")\n"
                                + city.getName() + "(" + city.getId() + ")\n"
                                + district.getName() + "(" + district.getId() + ")");
                    }

                    @Override
                    public void onCancel() {
                    }
                });
                cityPicker.showCityPicker();
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 性别
                new XPopup.Builder(getContext())
//                        .enableDrag(false)
                        .asCenterList("", new String[]{"男", "女"},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        Toasty.normal(Objects.requireNonNull(getActivity()), text).show();
                                    }
                                })
                        .show();
            }
        });
        // 初始化
        if (TextUtils.isEmpty(SPUtils.getInstance().getString(CommonUtils.BANJI_ID))) {
            tv3.setText("不限");
        } else {
            tv3.setText(SPUtils.getInstance().getString(CommonUtils.BANJI_ID));
        }
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 年级
                loadingPopup = new XPopup.Builder(getContext())
                        .popupAnimation(PopupAnimation.ScaleAlphaFromLeftTop)
                        .asCustom(new CustomPopup(Objects.requireNonNull(getActivity())))
                        .show();

            }
        });
        tv4.setText("测试");
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 年级2
                final BottomDialog bottomDialog = BottomDialog.create(getActivity().getSupportFragmentManager());
                bottomDialog.setViewListener(new BottomDialog.ViewListener() {
                    private String content = "";

                    @Override
                    public void bindView(View v) {
                        // // You can do any of the necessary the operation with the view
                        TextView tv1 = v.findViewById(R.id.tv1);
                        TextView tv2 = v.findViewById(R.id.tv2);
                        final WheelPicker wheelPicker = v.findViewById(R.id.wheelview11);
//            FrameLayout flContainer = findViewById(R.id.framelayout1);
//            WheelPicker wheelPicker = new WheelPicker(context);
//            wheelPicker.setCurtainColor(ContextCompat.getColor(context, R.color.color6592D3));
//            FrameLayout.LayoutParams flParams = new FrameLayout.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            flParams.gravity = Gravity.BOTTOM;
//            flContainer.addView(wheelPicker, flParams);
                        //
                        List<String> data = new ArrayList<>();
                        data.add("一年级");
                        data.add("二年级");
                        data.add("三年级");
                        data.add("四年级");
                        data.add("五年级");
                        wheelPicker.setData(data);
                        //
                        tv1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                bottomDialog.dismiss();
                            }
                        });
                        //
                        tv2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                bottomDialog.dismiss();
                                if (TextUtils.isEmpty(content)) {
                                    content = wheelPicker.getData().get(0).toString();
                                    tv4.setText(content);
                                } else {
                                    tv4.setText(content);
                                }
                            }
                        });
                        //
                        wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(WheelPicker picker, Object data, int position) {
                                content = data.toString();
                            }
                        });
                    }
                })
                        .setLayoutRes(R.layout.pop_hxkt_banji)
                        .setDimAmount(0.2f)            // Dialog window dim amount(can change window background color）, range：0 to 1，default is : 0.2f
                        .setCancelOutside(false)     // click the external area whether is closed, default is : true
                        .setTag("pop_hxkt_banji")     // setting the DialogFragment tag
                        .show();
            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 弹窗1
                final BasePopupView loadingPopup = new XPopup.Builder(getActivity())
                        .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                        .asCustom(new CustomPopup(Objects.requireNonNull(getActivity())))
//                        .asLoading("")
                        .show();
                loadingPopup.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        if(loadingPopup.isShow())
                        loadingPopup.dismissWith(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.normal(getActivity(), "传值去~").show();
                            }
                        });
                    }
                }, 2000);
            }
        });
        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 上传图片
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassTest1Act"));// ClassTest1Act
            }
        });
        tv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetworkUtils.isConnected()) {
                    ToastUtils.showLong("网络异常，请检查网络连接！");
                    return;
                }
                get_share_pop();
            }
        });
        tv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent("hs.act.slbapp.VideoPlayActivity"));
            }
        });
        tv9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
//                GlideAppUtils glideAppUtils = new GlideAppUtils();
//                glideAppUtils.setglide10(getActivity(), iv9, R.drawable.ic_def_loading);
                GlideAppUtils glideAppUtils = new GlideAppUtils();
                glideAppUtils.setglide11(getActivity(), "https://s2.51cto.com/wyfs02/M01/89/BA/wKioL1ga-u7QnnVnAAAfrCiGnBQ946_middle.jpg");
            }
        });
        //
        initsmart();
        //
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                retryData();
            }
        });
        emptyview1.notices(CommonUtils.TIPS_WUSHUJU, CommonUtils.TIPS_WUWANG, "小象正奔向故事里...", "");
        emptyview1.bind(scrollView1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                //
                retryData();
            }
        });
//        //
//        mListbanner1 = new ArrayList<>();
//        // 接口
//        presenter1 = new SLBBannerPresenter();
//        presenter1.onCreate(this);
        donetwork();
    }


    /**
     * 第一次进来加载bufen
     */
    private void donetwork() {
        emptyview1.loading();
        retryData();
    }

    private void retryData() {
//        mEmptyView.loading();
//        presenter1.getLBBannerData("0");
        refreshLayout1.finishRefresh();
        emptyview1.success();
    }

    private void initsmart() {
        smartHeader1.setEnableLastTime(false);
        refreshLayout1.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
                parallax.setTranslationY(mOffset - mScrollY);
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
                parallax.setTranslationY(mOffset - mScrollY);
            }
        });
        scrollView1.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            private int lastScrollY = 0;
            private int h = DensityUtil.dp2px(170);

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                scrolly = scrollY;
                if (lastScrollY < h) {
                    scrollY = Math.min(h, scrollY);
                    mScrollY = scrollY > h ? h : scrollY;
                    parallax.setTranslationY(mOffset - mScrollY);
                }
                lastScrollY = scrollY;
            }
        });
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {

        if (!isrefresh) {
            // 从缓存中拿出头像bufen

            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
        if (scrolly == 0) {
            if (refreshLayout1 != null) {
                refreshLayout1.autoRefresh();
            }
        } else {
            scrollView1.smoothScrollTo(0, 0);
        }
    }

    private int scrolly = 0;

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();

    }


    /**
     * --------------------------------业务逻辑分割线----------------------------------
     */

    public class CustomPopup extends CenterPopupView {

        public void set_select(BaseQuickAdapter mAdapter1, int pos) {
            List<BanjiAdapterType> list = mAdapter1.getData();
            for (int i = 0; i < list.size(); i++) {
                BanjiAdapterType bean = list.get(i);
                bean.getmBean().setText3(false);
            }
            list.get(pos).getmBean().setText3(true);
//            SPUtils.getInstance().put(CommonUtils.BANJI_ID, list.get(pos).getmBean().getText2());
            mAdapter1.setNewData(list);
            loadingPopup.dismiss();
        }

        public void get_select(BaseQuickAdapter mAdapter1, List<BanjiAdapterType> mList1, String id) {
//            List<BanjiAdapterType> list = mAdapter1.getData();
            for (int i = 0; i < mList1.size(); i++) {
                BanjiAdapterType bean = mList1.get(i);
//                if (TextUtils.equals(bean.getmBean().getText2(), SPUtils.getInstance().getString(CommonUtils.BANJI_ID))) {
//                    bean.getmBean().setText3(true);
//                } else {
//                    bean.getmBean().setText3(false);
//                }
            }
            mAdapter1.setNewData(mList1);
        }

        private void set_tv1_background(TextView tv1, boolean is_press) {
            if (is_press) {
//            tv1.setPressed(true);
                tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_press_round);
                tv1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
//            tv1.setPressed(false);
                tv1.setBackgroundResource(R.drawable.pop_tv_common_blue_enpress_round);
                tv1.setTextColor(ContextCompat.getColor(getActivity(), R.color.color2F3D57));
            }
        }

        public CustomPopup(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.pop_hxkt_nianji;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            XRecyclerView recyclerView1 = findViewById(R.id.recycler_view1);
            final TextView tv1 = findViewById(R.id.tv1);

            recyclerView1.setHasFixedSize(true);
            recyclerView1.setNestedScrollingEnabled(false);
            recyclerView1.setFocusable(false);
            recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false));
//            recyclerView1.addItemDecoration(new GridSpacingItemDecoration(3, (int) (DisplayUtil.getScreenWidth(getActivity()) * 8f / 375), true));
            //
            List<BanjiAdapterType> mList1 = new ArrayList<>();
            final BanjiAdapter mAdapter1 = new BanjiAdapter(mList1);
            mAdapter1.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
                @Override
                public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
//                int type = mList1.get(position).type;
                    int type = Objects.requireNonNull(mAdapter1.getItem(position)).type;
                    if (type == BanjiAdapterType.style1) {
                        return 3;
                    } else if (type == BanjiAdapterType.style2) {
                        return 1;
                    } else {
                        return 3;
                    }
                }
            });
            recyclerView1.setAdapter(mAdapter1);

            mAdapter1.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                @Override
                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                    BanjiAdapterType bean = (BanjiAdapterType) adapter.getData().get(position);
                    int i = view.getId();
//                    Toasty.normal(getActivity(), bean.getmBean().getText2()).show();
//                    MyLogUtil.e("--geekyun---", bean.getmBean().getText2());
                    tv1.setPressed(false);
                    set_select(adapter, position);
//                    tv3.setText(bean.getmBean().getText2());
                }
            });
            tv1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    // 不限
                    SPUtils.getInstance().put(CommonUtils.BANJI_ID, "");
                    tv3.setText("不限");
                    set_tv1_background(tv1, true);
                    List<BanjiAdapterType> list = mAdapter1.getData();
                    get_select(mAdapter1, list, SPUtils.getInstance().getString(CommonUtils.BANJI_ID));
                    loadingPopup.dismiss();
                }
            });
            // shuju
            mList1 = new ArrayList<>();
            String gson_url = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).getJson(getActivity(), "jsonbean/banjibean.json");
            BanjiModel bean_gson = GetAssetsFileMP3TXTJSONAPKUtil.getInstance(getActivity()).JsonToObject(gson_url, BanjiModel.class);
            for (int i = 0; i < bean_gson.getList().size(); i++) {
                BanjiModel2 bean = bean_gson.getList().get(i);
                for (int j = 0; j < bean.getList().size(); j++) {
                    if (j == 0) {
                        BanjiCommonbean banjiCommonbean = new BanjiCommonbean();
                        banjiCommonbean.setText2(bean.getText1());
                        banjiCommonbean.setText1("-1");
//                        mList1.add(new BanjiAdapterType(BanjiAdapterType.style1, banjiCommonbean));
                    }
//                    mList1.add(new BanjiAdapterType(BanjiAdapterType.style2, bean.getList().get(j)));
                }
            }
//            mAdapter1.setNewData(mList1);
            get_select(mAdapter1, mList1, SPUtils.getInstance().getString(CommonUtils.BANJI_ID));

            // 初始化
            if (TextUtils.isEmpty(SPUtils.getInstance().getString(CommonUtils.BANJI_ID))) {
                tv3.setText("不限");
                set_tv1_background(tv1, true);
            } else {
                tv3.setText(SPUtils.getInstance().getString(CommonUtils.BANJI_ID));
                set_tv1_background(tv1, false);
            }


        }

        protected void onShow() {
            super.onShow();
        }

//        @Override
//        protected int getMaxHeight() {
//            return 200;
//        }
//
        //返回0表示让宽度撑满window，或者你可以返回一个任意宽度
//        @Override
//        protected int getMaxWidth() {
//            return 1200;
//        }
    }

    //
//    @Override
//    public void OnLBBannerSuccess(SLBBannerBean bean) {
//        refreshLayout1.finishRefresh();
//        emptyview1.success();
//
//    }
//
//    @Override
//    public void OnLBBannerNodata(String s) {
//        refreshLayout1.finishRefresh(false);
//        emptyview1.nodata();
////        Toasty.normal(getActivity(), s).show();
//    }
//
//    @Override
//    public void OnLBBannerFail(String s) {
//        refreshLayout1.finishRefresh(false);
//        emptyview1.errorNet();
////        Toasty.normal(getActivity(), s).show();
//    }

    // 试读完成弹窗bufen
    private PopForShare popSharePay;
    private String share_title = "";
    private String share_name = "";
    private String share_img = "";
    private String share_imgUrl = "";
    private String share_imgUrl2 = "";

    private void get_share_pop() {
        popSharePay = new PopForShare(getActivity(), share_title, share_name, share_img, share_imgUrl, share_imgUrl2, new PopForShare.OnFinishResultClickListener() {
            @Override
            public void onWeChat1() {
                // 微信小程序

            }

            @Override
            public void onWeChat2() {
                // 朋友圈

            }
        });

    }

}
