package com.example.geekapp2libsindex.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class PaySuccessFlagBean implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public PaySuccessFlagBean() {
    }

    protected PaySuccessFlagBean(Parcel in) {
    }

    public static final Parcelable.Creator<PaySuccessFlagBean> CREATOR = new Parcelable.Creator<PaySuccessFlagBean>() {
        @Override
        public PaySuccessFlagBean createFromParcel(Parcel source) {
            return new PaySuccessFlagBean(source);
        }

        @Override
        public PaySuccessFlagBean[] newArray(int size) {
            return new PaySuccessFlagBean[size];
        }
    };
}
