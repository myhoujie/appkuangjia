package com.example.geekapp2libsindex.pop;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.geekapp2libsindex.R;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.lxj.xpopup.core.CenterPopupView;

public class EditNamePopup extends CenterPopupView {
    private OnSubmitListener onSubmitListener;

    public EditNamePopup(@NonNull Context context, OnSubmitListener onSubmitListener) {
        super(context);
        this.onSubmitListener = onSubmitListener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_edit_name;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        final EditText nameEt = findViewById(R.id.nameEt);
        TextView saveTv = findViewById(R.id.saveTv);
        saveTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameEt.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    Toasty.normal(getContext(), "请输入姓名");
                } else {
                    if (onSubmitListener != null) {
                        onSubmitListener.onSubimt(name);
                    }
                }
            }
        });
        findViewById(R.id.closeIv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnSubmitListener {
        void onSubimt(String name);
    }

}