package com.example.geekapp2libsindex.domain;

import java.io.Serializable;

public class Task implements Serializable {
    private int num;
    private String title;
    private String content;
    private boolean enable;

    public Task(int num, String title, String content, boolean enable) {
        this.num = num;
        this.title = title;
        this.content = content;
        this.enable = enable;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
