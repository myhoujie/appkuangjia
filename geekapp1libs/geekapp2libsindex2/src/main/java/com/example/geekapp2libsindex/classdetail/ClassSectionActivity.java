package com.example.geekapp2libsindex.classdetail;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.fanli.biz3hxktdetail.bean.ClassSectionInfoBean;
import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.fanli.biz3hxktdetail.presenter.ClassSectionInfoPresenter;
import com.fanli.biz3hxktdetail.presenter.GenseePresenter;
import com.fanli.biz3hxktdetail.view.ClassSectionInfoView;
import com.fanli.biz3hxktdetail.view.GenseeSdkParamsView;
import com.haier.cellarette.baselibrary.emptyview.EmptyView;
import com.haier.cellarette.baselibrary.emptyview.EmptyViewNew1;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.umeng.analytics.MobclickAgent;


public class ClassSectionActivity extends SlbBaseActivity implements ClassSectionInfoView, GenseeSdkParamsView {
    private TextView titleTv;
    private TextView dateTv;
    private TextView nameTv;
    private View cl_khlx;
    private TextView khlxStatusTv;
    private View cl_baogao;
    private TextView baogaoStatusTv;
    private TextView bottomTv;
    private int position;
    private View arrowIv1, arrowIv2;
    private String itemId;
    private ClassSectionInfoPresenter presenter;
    private ClassSectionInfoBean classSectionInfoBean;
    private EmptyViewNew1 emptyView;
    private GenseePresenter genseePresenter;

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestory();
        }
        if (genseePresenter != null) {
            genseePresenter.onDestory();
        }
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_class_section;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        presenter = new ClassSectionInfoPresenter();
        presenter.onCreate(this);

        genseePresenter = new GenseePresenter();
        genseePresenter.onCreate(this);
        position = getIntent().getIntExtra("position", 0);
        itemId = getIntent().getStringExtra("id");
        findview();
        onclick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        doNetWork();
    }

    private void findview() {
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("课程详情");

        View ll_wrap = findViewById(R.id.ll_wrap);
        emptyView = new EmptyViewNew1(this);
        emptyView.attach(ll_wrap);
        emptyView.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");

        titleTv = findViewById(R.id.titleTv);
        dateTv = findViewById(R.id.dateTv);
        nameTv = findViewById(R.id.nameTv);
        cl_khlx = findViewById(R.id.cl_khlx);
        khlxStatusTv = findViewById(R.id.khlxStatusTv);
        cl_baogao = findViewById(R.id.cl_baogao);
        baogaoStatusTv = findViewById(R.id.baogaoStatusTv);
        bottomTv = findViewById(R.id.bottomTv);
        arrowIv1 = findViewById(R.id.arrowIv1);
        arrowIv2 = findViewById(R.id.arrowIv2);
    }


    private void onclick() {
        emptyView.setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                doNetWork();
            }
        });
        cl_khlx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classSectionInfoBean == null) {
                    return;
                }
                String status = classSectionInfoBean.getExerciseStatus();
                //练习状态(0, "未布置"), (1, "待提交"), (2, "批改0中"), (3, "已批改")
                if (TextUtils.equals(status, "1")) {
                    String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassTest1Act{act}?id={s}" + classSectionInfoBean.getId() + "&position={i}" + position;
                    HiosHelper.resolveAd(ClassSectionActivity.this, ClassSectionActivity.this, url);
                } else if (TextUtils.equals(status, "2") || TextUtils.equals(status, "3")) {
                    String url = "hios://" + AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassTest3Act{act}?id={s}" + classSectionInfoBean.getId();
                    HiosHelper.resolveAd(ClassSectionActivity.this, ClassSectionActivity.this, url);
                }
            }
        });
        cl_baogao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classSectionInfoBean == null) {
                    return;
                }
                if (TextUtils.equals("1", classSectionInfoBean.getHasReport())) {
                    MyLogUtil.e("reportUrl:", classSectionInfoBean.getReportUrl() + MmkvUtils.getInstance().get_common(CommonUtils.MMKV_TOKEN));
                    MobclickAgent.onEvent(ClassSectionActivity.this, "lessons_studyreport");
                    HiosHelper.resolveAd(ClassSectionActivity.this, ClassSectionActivity.this, classSectionInfoBean.getReportUrl() + MmkvUtils.getInstance().get_common(CommonUtils.MMKV_TOKEN));
                }
            }
        });
        bottomTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(classSectionInfoBean.getItemStatus(), "1") || TextUtils.equals(classSectionInfoBean.getItemStatus(), "2")) {
                    genseePresenter.getSdkParam(classSectionInfoBean.getId(), classSectionInfoBean.getItemStatus());
                }
            }
        });
    }

    private void doNetWork() {
        emptyView.loading();
        presenter.getClassSectionInfo(itemId);
    }

    @Override
    public void OnClassSectionInfoBeanSuccess(ClassSectionInfoBean classSectionInfoBean) {
        this.classSectionInfoBean = classSectionInfoBean;
        emptyView.success();
        titleTv.setText("第" + classSectionInfoBean.getOrders() + "讲：" + classSectionInfoBean.getItemTitle());
        String str;
        if (TextUtils.equals("1", classSectionInfoBean.getTodayFlag())) {
            str = "今天" + classSectionInfoBean.getItemTimeRangeNoDay();
        } else {
            str = classSectionInfoBean.getItemTimeRange();
        }
        dateTv.setText(str);
        nameTv.setText(classSectionInfoBean.getLecturerName());

        //播放状态(0, "未开始"), (1, "直播中"), (2, "看回放"), (3, "已结束");
        switch (classSectionInfoBean.getItemStatus()) {
            case "0":
                bottomTv.setText("观看直播");
                bottomTv.setEnabled(false);
                bottomTv.setBackgroundColor(Color.parseColor("#C7E3FF"));
                break;
            case "1":
                bottomTv.setText("观看直播");
                bottomTv.setEnabled(true);
                bottomTv.setBackgroundColor(Color.parseColor("#55AAFF"));
                break;
            case "2":
                bottomTv.setText("观看回放");
                bottomTv.setEnabled(true);
                bottomTv.setBackgroundColor(Color.parseColor("#55AAFF"));
                break;
            case "3":
                bottomTv.setText("观看回放");
                bottomTv.setEnabled(false);
                bottomTv.setBackgroundColor(Color.parseColor("#C7E3FF"));
                break;
        }

        //练习状态(0, "未布置"), (1, "待提交"), (2, "批改中"), (3, "已批改")
        switch (classSectionInfoBean.getExerciseStatus()) {
            case "0":
                khlxStatusTv.setText("未布置");
                cl_khlx.setEnabled(false);
                arrowIv1.setVisibility(View.INVISIBLE);
                break;
            case "1":
                khlxStatusTv.setText("待提交");
                cl_khlx.setEnabled(true);
                arrowIv1.setVisibility(View.VISIBLE);
                break;
            case "2":
                khlxStatusTv.setText("批改中");
                cl_khlx.setEnabled(true);
                arrowIv1.setVisibility(View.VISIBLE);
                break;
            case "3":
                khlxStatusTv.setText("已批改");
                cl_khlx.setEnabled(true);
                arrowIv1.setVisibility(View.VISIBLE);
                break;
        }

        //是否有报告(0:没有,1:有)
        switch (classSectionInfoBean.getHasReport()) {
            case "0":
                baogaoStatusTv.setText("未生成");
                cl_baogao.setEnabled(false);
                arrowIv2.setVisibility(View.INVISIBLE);
                break;
            case "1":
                baogaoStatusTv.setText("可查看");
                cl_baogao.setEnabled(true);
                arrowIv2.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void OnClassSectionInfoBeanNodata(String s) {
        emptyView.nodata();
    }

    @Override
    public void OnClassSectionInfoBeanFail(String s) {
        emptyView.errorNet();
    }

    @Override
    public void OnGenseeSuccess(GenseeBean genseeBean) {
        if (classSectionInfoBean != null) {
            switch (classSectionInfoBean.getItemStatus()) {
                case "1":
                    MobclickAgent.onEvent(this, "lessons_live");
                    LiveUtil.startLive(this, genseeBean);
                    break;
                case "2":
                    MobclickAgent.onEvent(this, "lessons_playback");
                    LiveUtil.startVod(this, genseeBean);
                    break;
            }
        }
    }

    @Override
    public void OnGenseeNodata(String s) {
        Toasty.normal(this, s).show();
    }

    @Override
    public void OnGenseeFail(String s) {
        Toasty.normal(this, s).show();
    }
}
