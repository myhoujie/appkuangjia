package com.example.geekapp2libsindex.classtest;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HZuoyeinfoBean;
import com.example.biz3hxktindex.presenter.HZuoyeinfoPresenter;
import com.example.biz3hxktindex.view.HZuoyeinfoView;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.ClassTest2ItemBean;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.widgets.recyclerviewnice.XRecyclerView;
import com.geek.libglide47.base.GlideImageView;
import com.haier.cellarette.libutils.ConstantsUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import cc.shinichi.library.ImagePreview;
import cc.shinichi.library.bean.ImageInfo;

public class ClassTest2Act extends SlbBaseActivity implements View.OnClickListener, HZuoyeinfoView {

    private ImageView ivBack;
    private TextView tvCenterContent;
    private XRecyclerView mRecyclerView;
    private ClassTest2Adapter mAdapter;
    private List<ClassTest2ItemBean> mList;
    private HZuoyeinfoPresenter presenter;

    private TextView tv1;
    private LinearLayout ll1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv_13;
    private String id;

    @Override
    protected void onDestroy() {
        presenter.onDestory();
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_classtest2;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        donetwork();
    }

    private void donetwork() {
        tvCenterContent.setText("课后练习");
        id = getIntent().getStringExtra("id");
        //
        MobclickAgent.onEvent(this, "lessons_practice_myresponses");
        //
        presenter = new HZuoyeinfoPresenter();
        presenter.onCreate(this);
        presenter.get_zuoyeinfo(id);
        //
//        mList = new ArrayList<>();
//        mList = getSampleData(10);
//        mAdapter.setNewData(mList);
//        tv1.setVisibility(View.GONE);
//        ll1.setVisibility(View.VISIBLE);
//        String aaa = "1、第1题还可以再补充一点\n2、第2题做的不错,但有个错别字";
//        String content2 = aaa.replace("|", "\n");
//        tv2.setText(content2);
//        //
//        List<String> list = new ArrayList<>();
//        list.add("主讲老师:王老师");
//        list.add("辅导老师:李老师");
//        StringBuilder content3 = new StringBuilder();
//        for (int i = 0; i < list.size(); i++) {
//            content3.append(list.get(i)).append("\n");
//        }
//        tv3.setText(content3.toString());
    }

    public List<ClassTest2ItemBean> getSampleData(int lenth) {
        List<ClassTest2ItemBean> list = new ArrayList<>();
        for (int i = 0; i < lenth; i++) {
            ClassTest2ItemBean status = new ClassTest2ItemBean();
            status.setText_id(i + "");
            status.setText_content("https://avatars1.githubusercontent.com/u/7698209?v=3&s=460");
            list.add(status);
        }
        return list;
    }

    private void onclick() {
        ivBack.setOnClickListener(this);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                //item click
                ClassTest2ItemBean bean = (ClassTest2ItemBean) adapter.getItem(position);
//                ToastUtils.showLong(bean.getText_id());
                // 打开网络图片并下载到指定路径
                List<ImageInfo> imageInfoList = new ArrayList<>();
                for (int i = 0; i < mList.size(); i++) {
                    ImageInfo imageInfo = new ImageInfo();
                    imageInfo.setOriginUrl(mList.get(i).getText_content());
                    imageInfo.setThumbnailUrl(mList.get(i).getText_content());
                    imageInfoList.add(imageInfo);
                }
                ImagePreview
                        .getInstance()
                        .setContext(ClassTest2Act.this)
                        .setIndex(position)
                        .setImageInfoList(imageInfoList)
                        .setShowDownButton(true)
                        .setLoadStrategy(ImagePreview.LoadStrategy.Default)
                        .setFolderName(ConstantsUtils.gen_img)
                        .setScaleLevel(1, 3, 8)
                        .setZoomTransitionDuration(500)
                        .setShowCloseButton(true)
                        .setEnableDragClose(true)// 是否启用上拉/下拉关闭，默认不启用
                        .setEnableClickClose(true)// 是否启用点击图片关闭，默认启用
                        .start();
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                ClassTest2ItemBean bean = (ClassTest2ItemBean) adapter.getItem(position);
                int i1 = view.getId();
//                ToastUtils.showLong(bean.getText_id());
                // 打开网络图片并下载到指定路径
                List<ImageInfo> imageInfoList = new ArrayList<>();
                for (int i = 0; i < mList.size(); i++) {
                    ImageInfo imageInfo = new ImageInfo();
                    imageInfo.setOriginUrl(mList.get(i).getText_content());
                    imageInfo.setThumbnailUrl(mList.get(i).getText_content());
                    imageInfoList.add(imageInfo);
                }
                ImagePreview
                        .getInstance()
                        .setContext(ClassTest2Act.this)
                        .setIndex(position)
                        .setImageInfoList(imageInfoList)
                        .setShowDownButton(true)
                        .setLoadStrategy(ImagePreview.LoadStrategy.Default)
                        .setFolderName(ConstantsUtils.gen_img)
                        .setScaleLevel(1, 3, 8)
                        .setZoomTransitionDuration(500)
                        .setShowCloseButton(true)
                        .setEnableDragClose(true)// 是否启用上拉/下拉关闭，默认不启用
                        .setEnableClickClose(true)// 是否启用点击图片关闭，默认启用
                        .start();
            }
        });
    }

    private void findview() {
        ivBack = findViewById(R.id.iv_back1_order);
        tvCenterContent = findViewById(R.id.tv_center_content);
        mRecyclerView = findViewById(R.id.recycler_view1);
        tv1 = findViewById(R.id.tv1);
        ll1 = findViewById(R.id.ll1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv_13 = findViewById(R.id.tv_13);
        //
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4, RecyclerView.VERTICAL, false));
        mAdapter = new ClassTest2Adapter(R.layout.uploadimg2_item23);
//        mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
//        mAdapter.setNotDoAnimationCount(3);// mFirstPageItemCount
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back1_order) {
            onBackPressed();
        } else {

        }
    }

    @Override
    public void OnZuoyeinfoSuccess(HZuoyeinfoBean hZuoyeinfoBean) {
        mList = new ArrayList<>();
        for (String url : hZuoyeinfoBean.getPics()) {
            ClassTest2ItemBean bean = new ClassTest2ItemBean();
            bean.setText_id(url);
            bean.setText_content(url);
            mList.add(bean);
        }
        mAdapter.setNewData(mList);
        //
        if (TextUtils.equals(hZuoyeinfoBean.getExerciseStatus(), "2")) {
            tv1.setVisibility(View.VISIBLE);
            tv_13.setVisibility(View.GONE);
            ll1.setVisibility(View.GONE);
            tv1.setText("等待老师批改中");
//        } else if (TextUtils.equals(hZuoyeinfoBean.getExerciseStatus(), "3")) {
        } else if (TextUtils.equals(hZuoyeinfoBean.getExerciseStatus(), "3")) {
            tv1.setVisibility(View.GONE);
            ll1.setVisibility(View.VISIBLE);
            tv_13.setVisibility(View.VISIBLE);
            String content2 = hZuoyeinfoBean.getExerciseSuggest().replace("|", "\n");
            tv2.setText(content2);
            StringBuilder content3 = new StringBuilder();
            for (int i = 0; i < hZuoyeinfoBean.getTeachers().size(); i++) {
                content3.append(hZuoyeinfoBean.getTeachers().get(i)).append("\n");
            }
            tv3.setText(content3.toString());
        }
    }

    @Override
    public void OnZuoyeinfoNodata(String s) {

    }

    @Override
    public void OnZuoyeinfoFail(String s) {

    }

    class ClassTest2Adapter extends BaseQuickAdapter<ClassTest2ItemBean, BaseViewHolder> {

        public ClassTest2Adapter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, ClassTest2ItemBean item) {
            GlideImageView iv1 = helper.itemView.findViewById(R.id.iv1);
            iv1.loadImage(item.getText_content(), R.drawable.ic_def_loading);
            helper.addOnClickListener(R.id.iv1);
        }


    }

}
