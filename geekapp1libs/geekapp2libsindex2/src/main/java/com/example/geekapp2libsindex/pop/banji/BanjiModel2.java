package com.example.geekapp2libsindex.pop.banji;

import java.io.Serializable;
import java.util.List;

public class BanjiModel2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String text1;// title
    private List<BanjiCommonbean> list;// text

    public BanjiModel2() {
    }

    public BanjiModel2(String text1, List<BanjiCommonbean> list) {
        this.text1 = text1;
        this.list = list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public List<BanjiCommonbean> getList() {
        return list;
    }

    public void setList(List<BanjiCommonbean> list) {
        this.list = list;
    }
}
