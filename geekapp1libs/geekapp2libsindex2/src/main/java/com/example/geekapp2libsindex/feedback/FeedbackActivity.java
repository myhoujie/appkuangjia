package com.example.geekapp2libsindex.feedback;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.bean.HfeedBackReqBean;
import com.example.biz3hxktindex.presenter.HTouxiangsPresenter;
import com.example.biz3hxktindex.presenter.HfeedBackPresenter;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.example.biz3hxktindex.view.HfeedBackView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.uploadimg3.FullyGridLayoutManager;
import com.example.slbappcomm.uploadimg3.GridImageAdapter;
import com.haier.cellarette.baselibrary.common.BaseAppManager;
import com.haier.cellarette.baselibrary.statusbar.StatusBarUtil;
import com.haier.cellarette.baselibrary.toasts2.Toasty;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.permissions.RxPermissions;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.lzy.imagepicker.util.Utils;
import com.lzy.imagepicker.view.GridSpacingItemDecoration;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.jessyan.autosize.AutoSizeCompat;


public class FeedbackActivity extends AppCompatActivity implements HfeedBackView, HTouxiangsView {
    private EditText contentEt;
    private EditText contactEt;
    private TextView numTv;
    private TextView submitTv;
    private HfeedBackPresenter feedBackPresenter;
    private HTouxiangsPresenter uploadPresenter;
    private List<LocalMedia> selectList;
    private int themeId;
    private int chooseMode = PictureMimeType.ofImage();
    private int maxSelectNum = 3;
    private int mCurrentPosition;

    private ImageView cameraIv;
    private RecyclerView recyclerView;
    private GridImageAdapter adapter;
    private BasePopupView loadingPopup;

    private String page;
    private String text;


    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 667, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        BaseAppManager.getInstance().add(this);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        StatusBarUtil.setLightMode(this);

        findview();
        onclick();
        donetwork();
        //
        page = getIntent().getStringExtra("page");
        text = getIntent().getStringExtra("text");
        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        if (appLinkIntent!=null){
//            String appLinkAction = appLinkIntent.getAction();
//            if (appLinkAction!=null){
//                Uri appLinkData = appLinkIntent.getData();
//                if (appLinkData!=null){
//                    aaaa = appLinkData.getQueryParameter("query1");
//                    bbbb = appLinkData.getQueryParameter("query2");
//                }
//            }
//        }
    }

    private void findview() {
        selectList = new ArrayList<>();
        themeId = R.style.picture_white_style;

        ImageView iv_back1_order = findViewById(R.id.iv_back1_order);
        iv_back1_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView tv_center_content = findViewById(R.id.tv_center_content);
        tv_center_content.setText("意见反馈");

        contentEt = findViewById(R.id.contentEt);
        contactEt = findViewById(R.id.contactEt);
        numTv = findViewById(R.id.numTv);
        submitTv = findViewById(R.id.submitTv);
        submitTv.setEnabled(false);
        cameraIv = findViewById(R.id.cameraIv);
        numTv = findViewById(R.id.numTv);

        recyclerView = findViewById(R.id.recyclerView);
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, Utils.dp2px(this, 20), false));
        adapter = new GridImageAdapter(this, onAddPicClickListener);
        adapter.setList(selectList);
        adapter.setSelectMax(maxSelectNum);
        recyclerView.setAdapter(adapter);
    }

    private void donetwork() {
        feedBackPresenter = new HfeedBackPresenter();
        feedBackPresenter.onCreate(this);

        uploadPresenter = new HTouxiangsPresenter();
        uploadPresenter.onCreate(this);
    }

    private int MAX_TEXT = 100;
    private TextWatcher mTextWatcher = new TextWatcher() {
        private CharSequence temp;
        private int editStart;
        private int editEnd;

        @Override
        public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            temp = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            numTv.setText(s.length() + "/100字");
            setSubmitTvStatus();
        }

        @Override
        public void afterTextChanged(Editable s) {
            editStart = contentEt.getSelectionStart();
            editEnd = contentEt.getSelectionEnd();
            if (temp.length() > MAX_TEXT) {
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                contentEt.setText(s);
                contentEt.setSelection(tempSelection);
            }
        }
    };

    private void onclick() {
        KeyboardVisibilityEvent.setEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                // some code depending on keyboard visiblity status
                if (isOpen) {
                    // 此处为得到焦点时的处理内容
                    submitTv.setVisibility(View.GONE);
                } else {
                    // 此处为失去焦点时的处理内容
                    submitTv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            submitTv.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                }
            }
        });
        submitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectList.size() > 0) {
                    List<File> list = new ArrayList<>();
                    for (int i = 0; i < selectList.size(); i++) {
                        if (selectList.get(i).isCompressed()) {
                            File file = new File(selectList.get(i).getCompressPath());
                            list.add(file);
                        } else {
                            File file = new File(selectList.get(i).getPath());
                            list.add(file);
                        }
                    }
                    loadingPopup = new XPopup.Builder(FeedbackActivity.this)
                            .dismissOnBackPressed(true)
                            .dismissOnTouchOutside(false)
                            .asLoading("上传中...")
                            .show();
                    uploadPresenter.get_touxiang3(list);
                } else {
                    feedback(new ArrayList<String>());
                }
            }
        });
        cameraIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                set_pop();
            }
        });
        contentEt.addTextChangedListener(mTextWatcher);
        contactEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setSubmitTvStatus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        adapter.setOnItemClickListener(new GridImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (selectList.size() > 0) {
                    LocalMedia media = selectList.get(position);
                    String pictureType = media.getPictureType();
                    int mediaType = PictureMimeType.pictureToVideo(pictureType);
                    switch (mediaType) {
                        case 1:
                            // 预览图片 可自定长按保存路径
                            //PictureSelector.create(MainActivity.this).themeStyle(themeId).externalPicturePreview(position, "/custom_file", selectList);
                            PictureSelector.create(FeedbackActivity.this).themeStyle(themeId).openExternalPreview(position, selectList);
                            break;
                        case 2:
                            // 预览视频
                            PictureSelector.create(FeedbackActivity.this).externalPictureVideo(media.getPath());
                            break;
                        case 3:
                            // 预览音频
                            PictureSelector.create(FeedbackActivity.this).externalPictureAudio(media.getPath());
                            break;
                    }
                }
            }
        });
        adapter.setOnItemClickListener2(new GridImageAdapter.OnItemClickListener2() {
            @Override
            public void onItemClick2(int position, View v) {
                mCurrentPosition = position;
                showDeleteDialog();
            }
        });
    }

    private void feedback(List<String> urls) {
        String phone = contactEt.getText().toString().trim();
        if (!TextUtils.isEmpty(phone) && !RegexUtils.isMobileSimple(phone)) {
            ToastUtils.showShort("手机号格式不正确");
            return;
        }
        HfeedBackReqBean bean = new HfeedBackReqBean(BanbenUtils.getInstance().getVersion(), phone, contentEt.getText().toString().trim(), BanbenUtils.getInstance().getImei(), "android", urls);
        feedBackPresenter.get_feedBack(bean);
    }

    private void set_pop() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(this)
                .openGallery(chooseMode)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(maxSelectNum)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(false)// 是否可预览视频
                .enablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .compressSavePath(getPath())//压缩图片保存地址
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(16, 9)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(true)// 是否显示gif图片
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                //.isDragFrame(false)// 是否可拖动裁剪框(固定)
//                        .videoMaxSecond(15)
//                        .videoMinSecond(10)
                //.previewEggs(false)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                //.rotateEnabled(true) // 裁剪是否可旋转图片
                //.scaleEnabled(true)// 裁剪是否可放大缩小图片
                //.videoQuality()// 视频录制质量 0 or 1
                //.videoSecond()//显示多少秒以内的视频or音频也可适用
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    /**
     * 是否删除此张图片
     */
    private void showDeleteDialog() {
        new XPopup.Builder(this)
//                         .dismissOnTouchOutside(false)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onCreated() {
                        Log.e("tag", "弹窗创建了");
                    }

                    @Override
                    public void onShow() {
                        Log.e("tag", "onShow");
                    }

                    @Override
                    public void onDismiss() {
                        Log.e("tag", "onDismiss");
                    }

                    //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                    @Override
                    public boolean onBackPressed() {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                        return true;
                    }
                }).asConfirm("提示", "要删除这张照片吗？",
                "取消", "确定",
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        //移除当前图片刷新界面
                        selectList.remove(mCurrentPosition);
                        adapter.notifyItemRemoved(mCurrentPosition);
                        adapter.notifyItemRangeChanged(mCurrentPosition, selectList.size());
                        set_vis_layout();

                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                }, false)
                .show();
    }

    private void set_vis_layout() {
        if (selectList.size() == 0) {
            cameraIv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            submitTv.setVisibility(View.GONE);
        } else {
            cameraIv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        setSubmitTvStatus();
    }

    private void setSubmitTvStatus() {
        if (contentEt.getText().toString().trim().length() > 0) {
            submitTv.setEnabled(true);
        } else {
            submitTv.setEnabled(false);
        }
    }

    @Override
    public void OnTouxiangsSuccess(HTouxiangBean1 hTouxiangBean1) {
        String imgStr = hTouxiangBean1.getUrls();
        List<String> list1 = new ArrayList<>();
        if (!TextUtils.isEmpty(imgStr)) {
            String[] str = imgStr.split(",");
            list1 = Arrays.asList(str);
        }
        feedback(list1);
    }

    @Override
    public void OnTouxiangsNodata(final String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        ToastUtils.showLong(s);
    }

    @Override
    public void OnTouxiangsFail(final String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        ToastUtils.showLong(s);
    }

    @Override
    public void OnfeedBackSuccess(String s) {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
        }
        Toasty.normal(this, "反馈成功").show();
        set_clear_img_lujing();
        finish();
    }

    @Override
    public void OnfeedBackNodata(String s) {
        Toasty.normal(this, "反馈失败，请稍后重试~").show();
    }

    @Override
    public void OnfeedBackFail(String s) {
        Toasty.normal(this, "反馈失败，请稍后重试~").show();
    }

    private void set_clear_img_lujing() {
        // 清空图片缓存，包括裁剪、压缩后的图片 注意:必须要在上传完成后调用 必须要获取权限
        RxPermissions permissions = new RxPermissions(this);
        permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PictureFileUtils.deleteCacheDirFile(FeedbackActivity.this);
                } else {
                    Toast.makeText(FeedbackActivity.this,
                            getString(R.string.picture_jurisdiction), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }

    /**
     * 自定义压缩存储地址
     *
     * @return
     */
    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        if (file.mkdirs()) {
            return path;
        }
        return path;
    }

    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            set_pop();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    selectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
                    for (LocalMedia media : selectList) {
                        Log.i("ssss", "压缩---->" + media.getCompressPath());
                        Log.i("ssss", "原图---->" + media.getPath());
                        Log.i("ssss", "裁剪---->" + media.getCutPath());
                    }
                    adapter.setList(selectList);
                    adapter.notifyDataSetChanged();
                    set_vis_layout();
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
        uploadPresenter.onDestory();
        feedBackPresenter.onDestory();
        BaseAppManager.getInstance().remove(this);
//        ToastUtils.showLong("" + BaseAppManager.getInstance().getAll().size());
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.equals(page, "1")) {
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouyeActivity"));
        }
        super.onBackPressed();
    }

    @Override
    public String getIdentifier() {
        return SystemClock.currentThreadTimeMillis() + "";
    }

}
