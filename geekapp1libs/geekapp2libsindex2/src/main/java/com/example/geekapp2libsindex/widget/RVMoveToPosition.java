package com.example.geekapp2libsindex.widget;

import androidx.recyclerview.widget.RecyclerView;

public class RVMoveToPosition {
    //目标项是否在最后一个可见项之后
    private boolean mShouldScroll;
    //记录目标项位置
    private int position;
    private RecyclerView mRecyclerView;

    public RVMoveToPosition(RecyclerView mRecyclerView, final int position, OnScrollStateChanged onScrollStateChanged) {
        this.mRecyclerView = mRecyclerView;
        this.position = position;
        this.onScrollStateChanged = onScrollStateChanged;
    }

    public static RVMoveToPosition createRVMoveToPosition(RecyclerView mRecyclerView, final int position) {
        return new RVMoveToPosition(mRecyclerView, position, null);
    }

    public static RVMoveToPosition createRVMoveToPosition(RecyclerView mRecyclerView, final int position, OnScrollStateChanged onScrollStateChanged) {
        return new RVMoveToPosition(mRecyclerView, position, onScrollStateChanged);
    }

    /**
     * 滑动到指定位置
     */

    public void smoothMoveToPosition() {
//        setListener(onScrollStateChanged);
        // 第一个可见位置
        int firstItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(0));
        // 最后一个可见位置
        int lastItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(mRecyclerView.getChildCount() - 1));
        if (position < firstItem) {
            // 第一种可能:跳转位置在第一个可见位置之前
            mRecyclerView.smoothScrollToPosition(position);
        } else if (position <= lastItem) {
            // 第二种可能:跳转位置在第一个可见位置之后
            int movePosition = position - firstItem;
            if (movePosition >= 0 && movePosition < mRecyclerView.getChildCount()) {
                int top = mRecyclerView.getChildAt(movePosition).getTop();
                mRecyclerView.smoothScrollBy(0, top);
            }
        } else {
            // 第三种可能:跳转位置在最后可见项之后
            mRecyclerView.smoothScrollToPosition(position);

            mShouldScroll = true;
        }
    }

    public void setListener(final OnScrollStateChanged onScrollStateChanged) {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (onScrollStateChanged != null) {
                    onScrollStateChanged.onScrollStateChanged(recyclerView, newState);
                }
                if (mShouldScroll && RecyclerView.SCROLL_STATE_IDLE == newState) {
                    mShouldScroll = false;
                    smoothMoveToPosition();
                }
            }
        });
    }

    private OnScrollStateChanged onScrollStateChanged;

    public interface OnScrollStateChanged {
        void onScrollStateChanged(RecyclerView recyclerView, int newState);
    }

}