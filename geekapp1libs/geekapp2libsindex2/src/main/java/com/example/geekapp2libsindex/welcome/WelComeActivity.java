package com.example.geekapp2libsindex.welcome;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.example.biz3hxktindex.bean.HSettingBean;
import com.example.biz3hxktindex.presenter.HSettingPresenter;
import com.example.biz3hxktindex.view.HSettingView;
import com.example.geekapp2libsindex.R;
import com.example.slbappcomm.base.SlbBaseActivity;
import com.example.slbappcomm.nativendk.JNIUtils2;
import com.example.slbappcomm.nativendk.KeyReflectUtils;
import com.haier.cellarette.libutils.CommonUtils;
import com.haier.cellarette.libutils.SlbLoginUtil2;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;
import com.haier.cellarette.libutils.utilslib.data.MmkvUtils;
import com.haier.cellarette.libutils.utilslib.data.SpUtils;
import com.haier.cellarette.libwebview.hois2.HiosHelper;

import java.net.URLDecoder;

//import android.support.annotation.Nullable;


public class WelComeActivity extends SlbBaseActivity implements HSettingView {

    private int delayMillis = 4 * 1000;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            if (key_token == -1) {
//                SpUtils.getInstance(WelComeActivity.this).put("key_token", 0);
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SplshActivity"));
//                finish();
//            } else {
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouyeActivity"));
//                finish();
//            }
//            GenseeLiveHelper.startLive(WelComeActivity.this,"","","","");
        }
    };
    private int key_token;
    private TextView tv1;
    private HSettingPresenter hSettingPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 避免从桌面启动程序后，会重新实例化入口类的activity
//        if (!this.isTaskRoot()) {
//            Intent intent = getIntent();
//            if (intent != null) {
//                String action = intent.getAction();
//                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
//                    finish();
//                    return;
//                }
//            }
//        }
        //虚拟键
//        if (NavigationBarUtil.hasNavigationBar(this)) {
////            NavigationBarUtil.initActivity(getWindow().getDecorView());
//            NavigationBarUtil.hideBottomUIMenu(this);
//        }
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        if (appLinkIntent != null) {
//            String appLinkAction = appLinkIntent.getAction();
//            if (appLinkAction != null) {
//                Uri appLinkData = appLinkIntent.getData();
//                if (appLinkData != null) {
//                    aaaa = appLinkData.getQueryParameter("query1");
//                    bbbb = appLinkData.getQueryParameter("query2");
//                }
//            }
//        }
        setContentView(R.layout.activity_hxkt_welcome);
        tv1 = findViewById(R.id.tv1);
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.postDelayed(runnable, 0);
            }
        });
        key_token = (int) SpUtils.getInstance(this).get("key_token", -1);
//        handler.postDelayed(runnable, 0);
//        JumpWhere();

//        GetAssetsFileMP3TXTJSONAPKUtil.getInstance(this).playMusic(BaseApp.get(), true, "android.resource://" + getPackageName() + "/" + R.raw.appstart);
        configNDK();
        JNIUtils2 jniUtils = new JNIUtils2();
        MyLogUtil.e("--JNIUtils2--", jniUtils.stringFromJNI());
        //

//        startActivity(new Intent("com.example.geekapp2libzhibo.GenseeLiveActivity"));
//        finish();
        hSettingPresenter = new HSettingPresenter();
        hSettingPresenter.onCreate(this);
        hSettingPresenter.get_setting("android");
    }

    private String aaaa;
    private String bbbb;
    private String cccc;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_welcome;
    }

//    @Override
//    protected void setup(@Nullable Bundle savedInstanceState) {
//        super.setup(savedInstanceState);
//        tv1 = findViewById(R.id.tv1);
//        tv1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                handler.postDelayed(runnable, 0);
//            }
//        });
//        key_token = (int) SpUtils.getInstance(this).get("key_token", -1);
////        handler.postDelayed(runnable, 0);
////        JumpWhere();
//
////        GetAssetsFileMP3TXTJSONAPKUtil.getInstance(this).playMusic(BaseApp.get(), true, "android.resource://" + getPackageName() + "/" + R.raw.appstart);
//        configNDK();
//        JNIUtils2 jniUtils = new JNIUtils2();
//        MyLogUtil.e("--JNIUtils2--", jniUtils.stringFromJNI());
//        //
//
////        startActivity(new Intent("com.example.geekapp2libzhibo.GenseeLiveActivity"));
////        finish();
//        hSettingPresenter = new HSettingPresenter();
//        hSettingPresenter.onCreate(this);
//        hSettingPresenter.get_setting("android");
//
//    }

    private void configNDK() {
        try {
//            String aa = (String) KeyReflectUtils.getInstance().invokeMethod("com.sairobo.alty.JNIUtils", "stringFromJNI",
            String aa = (String) KeyReflectUtils.getInstance().invokeMethod(AppUtils.getAppPackageName() + ".JNIUtils", "stringFromJNI",
                    new Object[]{});
            MyLogUtil.e("--JNIUtils-反射-", aa);
        } catch (Exception e) {
            e.printStackTrace();
            MyLogUtil.e("--JNIUtils-反射-", e.getMessage());
        }
    }

    CountDownTimer countDownTimer;

    private void JumpWhere() {
        //联网 MobileUtils.isNetworkConnected(this)
        countDownTimer = new CountDownTimer(delayMillis, 1000) {
            @Override
            public void onTick(long l) {
                String value = String.valueOf((int) (l / 1000));
                tv1.setText(String.format(getResources().getString(R.string.djs), value));
            }

            @Override
            public void onFinish() {
//                tv1.setText("");
                handler.postDelayed(runnable, 0);
            }
        };
        countDownTimer.start();
    }

    @Override
    protected void onDestroy() {
        if (null != countDownTimer) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        if (hSettingPresenter != null) {
            hSettingPresenter.onDestory();
        }
        handler.removeCallbacks(runnable);
        super.onDestroy();


    }

    @Override
    public void OnSettingSuccess(HSettingBean hSettingBean) {
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_forceLogin, hSettingBean.getForceLogin());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_startBgImg, hSettingBean.getStartBgImg());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_aboutUs, hSettingBean.getAboutUs());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_how2Teach, hSettingBean.getHow2Teach());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_startSound, hSettingBean.getStartSound());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_aboutHxEdu, hSettingBean.getAboutHxEdu());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_serviceProtocol, hSettingBean.getServiceProtocol());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_privacyPolicy, hSettingBean.getPrivacyPolicy());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_hxEduWechatImg, hSettingBean.getHxEduWechatImg());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_hxEduWechatNum, hSettingBean.getHxEduWechatNum());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_contactInfo, hSettingBean.getContactInfo());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_admission, hSettingBean.getContactInfo());
        MmkvUtils.getInstance().set_common(CommonUtils.MMKV_couponstips, hSettingBean.getCouponDescUrl());

        if (key_token == -1) {
            SpUtils.getInstance(WelComeActivity.this).put("key_token", 0);
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SplshActivity"));
            finish();
        } else {
            if (TextUtils.equals(hSettingBean.getForceLogin(), "1")) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.LoginActivity"));
            } else {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouyeActivity");
                // ATTENTION: This was auto-generated to handle app links.
                Intent appLinkIntent = getIntent();
                if (appLinkIntent != null) {
                    String appLinkAction = appLinkIntent.getAction();
                    if (appLinkAction != null) {
                        Uri appLinkData = appLinkIntent.getData();
                        if (appLinkData != null) {
                            aaaa = appLinkData.getQueryParameter("query1");
                            bbbb = appLinkData.getQueryParameter("query2");
                            MyLogUtil.e("sssssssss",bbbb);
                            cccc = appLinkData.getQueryParameter("query3");
//                            intent.putExtra("page", aaaa);
//                            intent.putExtra("text", bbbb);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            // 业务逻辑
                            if (TextUtils.equals(aaaa, "shouye")) {
                                // 跳转到首页
                                Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouyeActivity");
                                intent1.putExtra("text", bbbb);
                                startActivity(intent1);
                                finish();
                                return;
                            } else if (TextUtils.equals(aaaa, "classdetail")) {
                                SlbLoginUtil2.get().loginTowhere(WelComeActivity.this, new Runnable() {
                                    @Override
                                    public void run() {
                                        // 跳转到我的课程—课程详情页
                                        Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassDetailActivity");
                                        intent1.putExtra("courseId", bbbb);
                                        intent1.putExtra("toSectionPosition ", cccc);
                                        startActivity(intent1);
                                        finish();
                                        return;
                                    }
                                });
                            } else if (TextUtils.equals(aaaa, "webview")) {
                                SlbLoginUtil2.get().loginTowhere(WelComeActivity.this, new Runnable() {
                                    @Override
                                    public void run() {
                                        // 跳转到学习报告详情页
                                        HiosHelper.resolveAd(WelComeActivity.this, WelComeActivity.this, URLDecoder.decode(bbbb) + MmkvUtils.getInstance().get_common(CommonUtils.MMKV_TOKEN));
                                        finish();
                                        return;
                                    }
                                });
                            } else if (TextUtils.equals(aaaa, "myorder")) {
                                // 跳转到我的—我的订单
                                SlbLoginUtil2.get().loginTowhere(WelComeActivity.this, new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.OrderAct"));
                                        finish();
                                        return;
                                    }
                                });
                            } else {
                                startActivity(intent);
                                finish();
                            }
                            return;
                        }
                    }
                }
                startActivity(intent);
                finish();

            }
        }
    }


    @Override
    public void finish() {
        overridePendingTransition(0, 0);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void OnSettingNodata(String s) {

    }

    @Override
    public void OnSettingFail(String s) {
        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.MeiyanAct"));
        finish();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }
}