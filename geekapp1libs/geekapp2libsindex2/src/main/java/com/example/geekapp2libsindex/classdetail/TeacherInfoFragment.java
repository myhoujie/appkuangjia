package com.example.geekapp2libsindex.classdetail;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.listener.InitSuccessListener;
import com.example.slbappcomm.base.SlbBaseFragment;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;

public class TeacherInfoFragment extends SlbBaseFragment implements OnClassDetailListener {
    private TextView text1Tv;
    private TextView text2Tv;
    private TextView text3Tv;
    private InitSuccessListener initSuccessListener;
    private TextView tip1Tv;
    private TextView tip2Tv;
    private TextView tip3Tv;
    public static TeacherInfoFragment newInstance() {
        TeacherInfoFragment fragment = new TeacherInfoFragment();
        return fragment;
    }

    public void setInitSuccessListener(InitSuccessListener initSuccessListener) {
        this.initSuccessListener = initSuccessListener;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_teacherinfo;
    }

    @Override
    protected void setup(final View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        text1Tv = rootView.findViewById(R.id.text1Tv);
        text2Tv = rootView.findViewById(R.id.text2Tv);
        text3Tv = rootView.findViewById(R.id.text3Tv);
        tip1Tv = rootView.findViewById(R.id.tip1Tv);
        tip2Tv = rootView.findViewById(R.id.tip2Tv);
        tip3Tv = rootView.findViewById(R.id.tip3Tv);
        if (initSuccessListener != null) {
            initSuccessListener.onSuccess();
        }
    }


    @Override
    public void onDetailReceive(CourseBean courseBean) {

    }

    @Override
    public void onTeacherReceive(TeacherBean teacherBean) {
        if(TextUtils.isEmpty(teacherBean.getCertificate())){
            tip1Tv.setVisibility(View.GONE);
            text1Tv.setVisibility(View.GONE);
        }else{
            tip1Tv.setVisibility(View.VISIBLE);
            text1Tv.setVisibility(View.VISIBLE);
            text1Tv.setText(teacherBean.getCertificate());
        }
        if(TextUtils.isEmpty(teacherBean.getIntroduction())){
            tip2Tv.setVisibility(View.GONE);
            text2Tv.setVisibility(View.GONE);
        }else{
            tip2Tv.setVisibility(View.VISIBLE);
            text2Tv.setVisibility(View.VISIBLE);
            text2Tv.setText(teacherBean.getIntroduction());
        }
        if(TextUtils.isEmpty(teacherBean.getAchievement())){
            tip3Tv.setVisibility(View.GONE);
            text3Tv.setVisibility(View.GONE);
        }else{
            tip3Tv.setVisibility(View.VISIBLE);
            text3Tv.setVisibility(View.VISIBLE);
            text3Tv.setText(teacherBean.getAchievement());
        }
    }

}