package com.example.geekapp2libsindex.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;
import com.example.geekapp2libsindex.domain.UnionPicBean;
import com.example.geekapp2libsindex.widget.PicHelper;
import com.example.slbappcomm.videoplay.bt.ListUtil;
import com.geek.libglide47.base.GlideImageView;
import com.luck.picture.lib.entity.LocalMedia;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;

import java.util.ArrayList;
import java.util.List;

import cc.shinichi.library.bean.ImageInfo;

public class UploadExercisePicAdapter extends BaseQuickAdapter<UnionPicBean, BaseViewHolder> {
    private int maxPicsCount = 9;
    private Activity activity;
    private String exerciseStatus;//练习状态(1-未提交,2-待批改,3-已批改 )

    public UploadExercisePicAdapter(int layoutResId, Activity activity, String exerciseStatus) {
        super(layoutResId);
        this.activity = activity;
        this.exerciseStatus = exerciseStatus;
    }

    @Override
    protected void convert(BaseViewHolder helper, UnionPicBean item) {
        final int position = helper.getAdapterPosition();
        View rl_pic = helper.itemView.findViewById(R.id.rl_pic);
        GlideImageView picIv = helper.itemView.findViewById(R.id.picIv);
        ImageView deleteIv = helper.itemView.findViewById(R.id.deleteIv);
        ImageView passStatusIv = helper.itemView.findViewById(R.id.passStatusIv);
        ImageView cameraIv = helper.itemView.findViewById(R.id.cameraIv);

        if (!TextUtils.isEmpty(item.getId())) {//已提交过的图片，直接展示
            cameraIv.setVisibility(View.GONE);
            rl_pic.setVisibility(View.VISIBLE);
            deleteIv.setVisibility(View.GONE);
            if (TextUtils.equals(exerciseStatus, "3")) {
                passStatusIv.setVisibility(View.VISIBLE);
                passStatusIv.setImageResource(TextUtils.equals("0", item.getCorrectFlag()) ? R.drawable.not_pass : R.drawable.pass);
            } else {
                passStatusIv.setVisibility(View.GONE);
            }
            picIv.loadImage(item.getPicUrl(), R.drawable.ic_def_loading);
            picIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PicHelper.getInstance().previewUrl(activity, position, createImageInfoList());
                }
            });
        } else {//本地选择的图片
            if (TextUtils.isEmpty(item.getPath()) && position == getData().size() - 1) {
                cameraIv.setVisibility(View.VISIBLE);
                rl_pic.setVisibility(View.INVISIBLE);
            } else {
                cameraIv.setVisibility(View.INVISIBLE);
                rl_pic.setVisibility(View.VISIBLE);
                deleteIv.setVisibility(View.VISIBLE);
                passStatusIv.setVisibility(View.GONE);

                String path;
                if (item.isCut() && !item.isCompressed()) {
                    // 裁剪过
                    path = item.getCutPath();
                } else if (item.isCompressed() || (item.isCut() && item.isCompressed())) {
                    // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
                    path = item.getCompressPath();
                } else {
                    // 原图
                    path = item.getPath();
                }
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(com.example.slbappcomm.R.color.color_f6)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                Glide.with(mContext)
                        .load(path)
                        .apply(options)
                        .into(picIv);

                picIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PicHelper.getInstance().previewLocal(activity, position);
                    }
                });
            }
        }
        deleteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(position);
            }
        });
        cameraIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PicHelper.getInstance().toSelectPic(activity);
            }
        });
    }

    private List<ImageInfo> createImageInfoList() {
        List<ImageInfo> imageInfoList = new ArrayList<>();
        for (UnionPicBean bean : getData()) {
            ImageInfo imageInfo = new ImageInfo();
            imageInfo.setOriginUrl(bean.getPicUrl());
            imageInfo.setThumbnailUrl(bean.getPicUrl());
            imageInfoList.add(imageInfo);
        }
        return imageInfoList;
    }

    private BasePopupView basePopupView;

    /**
     * 是否删除此张图片
     */
    private void showDeleteDialog(final int position) {
        basePopupView = new XPopup.Builder(mContext).asConfirm("提示", "要删除这张照片吗？",
                "取消", "确定",
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        //如果之前9张全是图片，需要在删除图片后再添加一个拍照图片
                        boolean isAllPic = !TextUtils.isEmpty(getData().get(getData().size() - 1).getPath());
                        List<LocalMedia> list = PicHelper.getInstance().getSelectedList();
                        if (!ListUtil.isEmpty(list) && position < list.size()) {
                            list.remove(position);
                        }
                        getData().remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getData().size() - position);
                        if (isAllPic) {
                            getData().add(new UnionPicBean());
                            notifyItemInserted(getData().size() - 1);
                        }
                        basePopupView.dismiss();
//                        notifyDataSetChanged();
                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        basePopupView.dismiss();
                    }
                }, false).show();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (basePopupView != null && basePopupView.isShow()) {
            basePopupView.dismiss();
            basePopupView = null;
        }
    }

//    @Override
//    public int getItemCount() {
//        if (mData.size() == 0) {
//            return 0;
//        } else {
//            if (!TextUtils.isEmpty(mData.get(0).getId())) {//已提交过
//                return mData.size();
//            } else {
//                if (mData.size() < maxPicsCount) {
//                    return mData.size() + 1;
//                } else {
//                    return mData.size();
//                }
//            }
//        }
//    }
}
