package com.example.geekapp2libsindex.adapter;

import android.content.Context;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.geekapp2libsindex.R;

public class Huifangdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private Context context;

    public Huifangdapter(Context context) {
        super(R.layout.adapter_huifang);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        TextView numTv = helper.itemView.findViewById(R.id.numTv);
        TextView urlTv = helper.itemView.findViewById(R.id.urlTv);

        numTv.setText("第" + (helper.getAdapterPosition() + 1) + "段");
        urlTv.setText(item);
    }


}
