package com.example.geekapp2libsindex.search.part3;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.biz3hxktindex.bean.HSearchBean1;
import com.example.geekapp2libsindex.R;
import com.geek.libglide47.base.GlideImageView;

public class SearchListAdapter extends BaseQuickAdapter<HSearchBean1, BaseViewHolder> {

    public SearchListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, HSearchBean1 item) {
        GlideImageView iv = helper.itemView.findViewById(R.id.iv1);
        iv.loadImage(item.getCoverImg(), R.drawable.ic_def_loading);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            helper.setText(R.id.tv1, Html.fromHtml(item.getTitleHtml(), Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            helper.setText(R.id.tv1, Html.fromHtml(item.getTitleHtml()));
//        }
        helper.setText(R.id.tv1, item.getBookId());
        helper.setText(R.id.tv2, item.getBookName());
        helper.setText(R.id.tv3, item.getBookId());
        helper.setText(R.id.tv4, item.getBookId());

        helper.addOnClickListener(R.id.iv1);
        helper.addOnClickListener(R.id.tv1);
    }
}
