package com.example.geekapp2libsindex.classdetail;

import android.os.Parcelable;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.example.slbappcomm.videoplay.bt.ListUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewPageAdapter extends FragmentStatePagerAdapter {
    private List<String> titleList = new ArrayList<>();
    private List<Fragment> fragmentList = new ArrayList<>();

    public ViewPageAdapter(FragmentManager fm, List<String> titleList, List<Fragment> fragmentList) {
        super(fm);
        if (!ListUtil.isEmpty(titleList))
            this.titleList = titleList;
        if (!ListUtil.isEmpty(fragmentList))
            this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
    @Override
    public Parcelable saveState() {
        return null;
    }
    public void addFragment(Fragment fragment) {
        fragmentList.add(fragment);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (ListUtil.isEmpty(titleList))
            return "";
        if (position >= titleList.size()) {
            return "";
        }
        return titleList.get(position);
    }
    @Override
    public int getItemPosition(Object object) {
// TODO Auto-generated method stub
        return PagerAdapter.POSITION_NONE;
    }
}
