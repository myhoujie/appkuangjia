package com.example.geekapp2libsindex.recycleview.commonlist;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.geekapp2libsindex.R;

public class CommonClassProvider1 extends BaseItemProvider<CommonClassAdapterType, BaseViewHolder> {

    @Override
    public int viewType() {
        return CommonClassAdapter.STYLE_ONE;
    }

    @Override
    public int layout() {
        return R.layout.recycleview_hxkt_classprovider_item1;
    }

    @Override
    public void convert(BaseViewHolder helper, CommonClassAdapterType data, int position) {
        helper.setText(R.id.tv0, data.getmBean().getCourseName());
    }
}
