package com.example.shining.libglin.glin.chan;


import com.example.shining.libglin.glin.GlinContext;

public abstract class ChanNode {
    private ChanNode mNext;
    private GlinContext mGlinContext;

    private boolean beforeCall;

    public final void beforeCall(boolean invoke) {
        beforeCall = invoke;
    }

    public final boolean isBeforeCall() {
        return beforeCall;
    }

    public final void exec(GlinContext ctx) {
        mGlinContext = ctx;
        run(ctx);
    }

    public final ChanNode nextChanNode() {
        return mNext;
    }

    public final void nextChanNode(ChanNode chanNode) {
        mNext = chanNode;
    }

    protected final void next() {
        if (mNext != null) {
            mNext.exec(mGlinContext);
            return;
        }

        if (beforeCall) { mGlinContext.getCall().exec();}
    }

    protected final void cancel() {
        mNext = null;
        if (beforeCall) { mGlinContext.getCall().cancel();}
    }

    public abstract void run(GlinContext ctx);
}
