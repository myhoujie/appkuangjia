package com.example.shining.libglin.glin;


import com.example.shining.libglin.glin.chan.ChanNode;

public abstract class Callback<T> {

    private GlinContext mGlinContext;
    private ChanNode mAfterChanNode;

    public final void attach(GlinContext ctx, ChanNode afterChanNode) {
        mGlinContext = ctx;
        mAfterChanNode = afterChanNode;
    }

    public final void afterResponse(Result<T> result, RawResult rawResult) {
        mGlinContext.setRawResult(rawResult);
        mGlinContext.setResult(result);

        if (mAfterChanNode != null) {
            mAfterChanNode.exec(mGlinContext);
        }
    }

    public abstract void onResponse(Result<T> result);
}

