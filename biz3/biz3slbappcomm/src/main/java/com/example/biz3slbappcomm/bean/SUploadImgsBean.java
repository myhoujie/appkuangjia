package com.example.biz3slbappcomm.bean;

import java.io.Serializable;

public class SUploadImgsBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String url;

    public SUploadImgsBean() {
    }

    public SUploadImgsBean(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
