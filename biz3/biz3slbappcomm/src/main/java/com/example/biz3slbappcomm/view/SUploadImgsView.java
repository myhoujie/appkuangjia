package com.example.biz3slbappcomm.view;

import com.example.biz3slbappcomm.bean.SUploadImgsBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface SUploadImgsView extends IView {

    void OnUploadImgsSuccess(SUploadImgsBean bean);
    void OnUploadImgsNodata(String bean);
    void OnUploadImgsFail(String msg);

}
