package com.example.biz3slbappcomm.presenter;

import com.example.biz3slbappcomm.api.SCommonApi;
import com.example.biz3slbappcomm.bean.SUploadImgsBean;
import com.example.biz3slbappcomm.view.SUploadImgsView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SUploadImgsPresenter extends Presenter<SUploadImgsView> {


    // file方式上传bufen
    public void getUploadUserImgData(List<File> fileList) {
        Map<String, RequestBody> params = new HashMap<>();
        //以下参数是，参数需要换成自己服务器支持的
        params.put("uid", convertToRequestBody("1701"));
        params.put("token", convertToRequestBody("E6C4C1B581A506F2F4D6748B3649AD3C"));
        params.put("source", convertToRequestBody("android"));
        params.put("appVersion", convertToRequestBody("101"));

//        JSONObject requestData = new JSONObject();
//        requestData.put("nickName", "");
//        RequestBody requestBody1 = RequestBody.create(MediaType.parse("text/plain"), requestData.toString());
        List<MultipartBody.Part> partList = filesToMultipartBodyParts(fileList);
        RetrofitNetNew.build(SCommonApi.class, getIdentifier()).get_my_uploadimgs(BanbenUtils.getInstance().getVersion(), BanbenUtils.getInstance().getImei(), BanbenUtils.getInstance().getToken(), params, partList).enqueue(new Callback<ResponseSlbBean<SUploadImgsBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<SUploadImgsBean>> call, Response<ResponseSlbBean<SUploadImgsBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnUploadImgsNodata(response.body().getMsg());
                    return;
                }
                getView().OnUploadImgsSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<SUploadImgsBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnUploadImgsFail(string);
                call.cancel();
            }
        });

    }

    // Base64上传bufen
//    public void getUploadUserImgData2(String base64) {
//        JSONObject requestData = new JSONObject();
//        requestData.put("base64", base64);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
//        RetrofitNetNew.build(SMyApi.class, getIdentifier()).get_my_uploadimg2(BanbenUtils.getInstance().getVersion(), BanbenUtils.getInstance().getImei(), BanbenUtils.getInstance().getToken(), requestBody).enqueue(new Callback<ResponseSlbBean<SUserImgBean>>() {
//            @Override
//            public void onResponse(Call<ResponseSlbBean<SUserImgBean>> call, Response<ResponseSlbBean<SUserImgBean>> response) {
//                if (!hasView()) {
//                    return;
//                }
//                if (response.body() == null) {
//                    return;
//                }
//                if (response.body().getCode() != 0) {
//                    getView().OnUploadUserImgNodata(response.body().getMsg());
//                    return;
//                }
//                getView().OnUploadUserImgSuccess(response.body().getData());
//                call.cancel();
//            }
//
//            @Override
//            public void onFailure(Call<ResponseSlbBean<SUserImgBean>> call, Throwable t) {
//                if (!hasView()) {
//                    return;
//                }
////                String string = t.toString();
//                String string = BanbenUtils.getInstance().net_tips;
//                getView().OnUploadUserImgFail(string);
//                call.cancel();
//            }
//        });
//
//    }

    private RequestBody convertToRequestBody(String param) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), param);
        return requestBody;
    }


    private List<MultipartBody.Part> filesToMultipartBodyParts(List<File> files) {
        List<MultipartBody.Part> parts = new ArrayList<>(files.size());
        for (File file : files) {
            //设置文件的类型
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            //file就是上传文件的参数类型,后面的file.getName()就是你上传的文件,首先要拿到文件的地址
            MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
            parts.add(part);
        }
        return parts;
    }
}
