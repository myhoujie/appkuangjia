package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HGradecategoryBean2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;//
    private String name;//
    private int enable;//
    private boolean text3;// is_xuanzhong

    public HGradecategoryBean2() {
    }

    public HGradecategoryBean2(String code, String name, int enable, boolean text3) {
        this.code = code;
        this.name = name;
        this.enable = enable;
        this.text3 = text3;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public boolean isText3() {
        return text3;
    }

    public void setText3(boolean text3) {
        this.text3 = text3;
    }
}
