package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HGradecategoryBean;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HGradecategoryView extends IView {

    void OnGradecategorySuccess(HGradecategoryBean bean);
    void OnGradecategoryNodata(String bean);
    void OnGradecategoryFail(String msg);

}
