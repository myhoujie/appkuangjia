package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HZuoyeinfoBean;
import com.example.biz3hxktindex.view.HZuoyeinfoView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HZuoyeinfoPresenter extends Presenter<HZuoyeinfoView> {

    public void get_zuoyeinfo(String itemId) {
        JSONObject requestData = new JSONObject();
        requestData.put("itemId", itemId);//
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier()).get_zuoyeinfo(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HZuoyeinfoBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<HZuoyeinfoBean>> call, Response<ResponseSlbBean<HZuoyeinfoBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnZuoyeinfoNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnZuoyeinfoSuccess(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<HZuoyeinfoBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnZuoyeinfoFail(string);
                        call.cancel();
                    }
                });

    }

}
