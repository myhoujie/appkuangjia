package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HClasscategoryBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String courseTypeCode;
    private String courseTypeName;
    private List<HClasscategoryBean2> courseList;//

    public HClasscategoryBean1() {
    }

    public HClasscategoryBean1(String courseTypeCode, String courseTypeName, List<HClasscategoryBean2> courseList) {
        this.courseTypeCode = courseTypeCode;
        this.courseTypeName = courseTypeName;
        this.courseList = courseList;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }

    public List<HClasscategoryBean2> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<HClasscategoryBean2> courseList) {
        this.courseList = courseList;
    }
}
