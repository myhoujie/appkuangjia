package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HMyaddressBean;
import com.example.biz3hxktindex.view.HMyaddressView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HMyaddressPresenter extends Presenter<HMyaddressView> {

    public void get_myaddress() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_myaddress().enqueue(new Callback<ResponseSlbBean<HMyaddressBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HMyaddressBean>> call, Response<ResponseSlbBean<HMyaddressBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnMyaddressNodata(response.body().getMsg());
                    return;
                }
                getView().OnMyaddressSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HMyaddressBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnMyaddressFail(string);
                call.cancel();
            }
        });

    }

}
