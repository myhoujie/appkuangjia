package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.view.HTouxiangView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTouxiangPresenter extends Presenter<HTouxiangView> {


    // Base64上传bufen
    public void get_touxiang1(String base64) {
        JSONObject requestData = new JSONObject();
        requestData.put("base64", base64);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier()).get_touxiang1(requestBody).enqueue(new Callback<ResponseSlbBean<HTouxiangBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HTouxiangBean>> call, Response<ResponseSlbBean<HTouxiangBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTouxiangNodata(response.body().getMsg());
                    return;
                }
                getView().OnTouxiangSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HTouxiangBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTouxiangFail(string);
                call.cancel();
            }
        });

    }

    // file方式上传bufen
    public void get_touxiang2(File imgfile) {
//        JSONObject requestData = new JSONObject();
//        requestData.put("nickName", "");
//        RequestBody requestBody1 = RequestBody.create(MediaType.parse("text/plain"), requestData.toString());
        MultipartBody.Part requestBody = MultipartBody.Part.createFormData("file", imgfile.getName(), RequestBody.create(MediaType.parse("image/*"), imgfile));
        RetrofitNetNew.build(Api.class, getIdentifier()).get_touxiang2(requestBody).enqueue(new Callback<ResponseSlbBean<HTouxiangBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HTouxiangBean>> call, Response<ResponseSlbBean<HTouxiangBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTouxiangNodata(response.body().getMsg());
                    return;
                }
                getView().OnTouxiangSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HTouxiangBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTouxiangFail(string);
                call.cancel();
            }
        });

    }

}
