package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HClasscategoryBean;
import com.example.biz3hxktindex.view.HClasscategoryView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HClasscategoryPresenter extends Presenter<HClasscategoryView> {

    public void get_classcategorylist(String grade, String subjectCode) {
        JSONObject requestData = new JSONObject();
        requestData.put("grade", grade);//
        requestData.put("subjectCode", subjectCode);// 是否需要全部:0-否,1-是
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_classcategorylist(requestBody).enqueue(new Callback<ResponseSlbBean<HClasscategoryBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HClasscategoryBean>> call, Response<ResponseSlbBean<HClasscategoryBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnClasscategoryNodata(response.body().getMsg());
                    return;
                }
                getView().OnClasscategorySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HClasscategoryBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeee:"+t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnClasscategoryFail(string);
                call.cancel();
            }
        });

    }

}
