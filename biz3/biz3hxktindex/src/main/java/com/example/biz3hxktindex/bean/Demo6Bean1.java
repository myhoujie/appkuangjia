package com.example.biz3hxktindex.bean;

import java.io.Serializable;

//{
//            "bjxmg": "string",
//            "createTime": "2020-04-22 19:04:46",
//            "createUserID": "1",
//            "createUserName": "stylefeng",
//            "dataStatus": 1,
//            "jk_name": "rtp://123.23.213.123",
//            "jkdID": "32CAD86B-DD68-4B3D-B5C1-342028CF860F",
//            "jkdMC": "string",
//            "modifyTime": "2020-04-22 19:04:46",
//            "objID": "79B3B63F-AC0C-4CE4-99C5-49A43C96DC29"
//          }
public class Demo6Bean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String bjxmg;
    private String createTime;
    private String createUserID;
    private String createUserName;
    private String dataStatus;
    private String jk_name;
    private String jkdID;
    private String jkdMC;
    private String modifyTime;
    private String objID;

    public Demo6Bean1() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBjxmg() {
        return bjxmg;
    }

    public void setBjxmg(String bjxmg) {
        this.bjxmg = bjxmg;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getJk_name() {
        return jk_name;
    }

    public void setJk_name(String jk_name) {
        this.jk_name = jk_name;
    }

    public String getJkdID() {
        return jkdID;
    }

    public void setJkdID(String jkdID) {
        this.jkdID = jkdID;
    }

    public String getJkdMC() {
        return jkdMC;
    }

    public void setJkdMC(String jkdMC) {
        this.jkdMC = jkdMC;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }
}
