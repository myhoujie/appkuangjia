package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HCouponsListBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * amount : 9.99
     * canSelected : 1
     * collectTime :
     * couponInfoState : 0
     * couponName :
     * couponRangeDesc :
     * couponTypeDesc :
     * liveCouponId : 0
     * liveCouponInfoId : 0
     * strShrink : 0
     * swUserId : 0
     * timeEnd :
     * timeStart :
     * useRangeType :
     * useTime :
     * useTimeRange : 2019.09.10-2020.09.10
     */

    private String amount;
    private String canSelected;
    private String collectTime;
    private String couponInfoState;
    private String couponName;
    private String couponRangeDesc;
    private String couponTypeDesc;
    private String liveCouponId;
    private String liveCouponInfoId;
    private String strShrink;
    private String swUserId;
    private String timeEnd;
    private String timeStart;
    private String useRangeType;
    private String useTime;
    private String useTimeRange;


    public HCouponsListBean1() {
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCanSelected() {
        return canSelected;
    }

    public void setCanSelected(String canSelected) {
        this.canSelected = canSelected;
    }

    public String getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(String collectTime) {
        this.collectTime = collectTime;
    }

    public String getCouponInfoState() {
        return couponInfoState;
    }

    public void setCouponInfoState(String couponInfoState) {
        this.couponInfoState = couponInfoState;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponRangeDesc() {
        return couponRangeDesc;
    }

    public void setCouponRangeDesc(String couponRangeDesc) {
        this.couponRangeDesc = couponRangeDesc;
    }

    public String getCouponTypeDesc() {
        return couponTypeDesc;
    }

    public void setCouponTypeDesc(String couponTypeDesc) {
        this.couponTypeDesc = couponTypeDesc;
    }

    public String getLiveCouponId() {
        return liveCouponId;
    }

    public void setLiveCouponId(String liveCouponId) {
        this.liveCouponId = liveCouponId;
    }

    public String getLiveCouponInfoId() {
        return liveCouponInfoId;
    }

    public void setLiveCouponInfoId(String liveCouponInfoId) {
        this.liveCouponInfoId = liveCouponInfoId;
    }

    public String getStrShrink() {
        return strShrink;
    }

    public void setStrShrink(String strShrink) {
        this.strShrink = strShrink;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getUseRangeType() {
        return useRangeType;
    }

    public void setUseRangeType(String useRangeType) {
        this.useRangeType = useRangeType;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getUseTimeRange() {
        return useTimeRange;
    }

    public void setUseTimeRange(String useTimeRange) {
        this.useTimeRange = useTimeRange;
    }
}
