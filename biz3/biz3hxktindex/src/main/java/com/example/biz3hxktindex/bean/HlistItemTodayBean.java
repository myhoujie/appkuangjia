package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HlistItemTodayBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HlistItemTodayBean1> items;//

    public HlistItemTodayBean() {
    }

    public HlistItemTodayBean(List<HlistItemTodayBean1> items) {
        this.items = items;
    }

    public List<HlistItemTodayBean1> getItems() {
        return items;
    }

    public void setItems(List<HlistItemTodayBean1> items) {
        this.items = items;
    }
}
