package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HTouxiangBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String urls;

    public HTouxiangBean1() {
    }

    public HTouxiangBean1(String urls) {
        this.urls = urls;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }
}
