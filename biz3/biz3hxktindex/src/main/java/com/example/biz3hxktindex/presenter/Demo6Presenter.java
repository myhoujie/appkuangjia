package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.ResponseDemoBean;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.Demo6Bean;
import com.example.biz3hxktindex.view.Demo6View;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Demo6Presenter extends Presenter<Demo6View> {

    public void get_Demo6(String end_time,String jk_id, String start_time, String pageNum, String pageSize) {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_demo6(end_time, jk_id,start_time, pageNum, pageSize)
                .enqueue(new Callback<ResponseDemoBean<Demo6Bean>>() {
                    @Override
                    public void onResponse(Call<ResponseDemoBean<Demo6Bean>> call, Response<ResponseDemoBean<Demo6Bean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnDemo6Nodata(response.body().getMsg());
                            return;
                        }
                        getView().OnDemo6Success(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseDemoBean<Demo6Bean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnDemo6Fail(string);
                        call.cancel();
                    }
                });

    }

}
