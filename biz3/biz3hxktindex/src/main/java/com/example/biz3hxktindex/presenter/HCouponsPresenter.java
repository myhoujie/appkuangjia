package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCouponsBean;
import com.example.biz3hxktindex.view.HCategoryView;
import com.example.biz3hxktindex.view.HCouponsView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HCouponsPresenter extends Presenter<HCouponsView> {

    public void get_coupons() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_coupons().enqueue(new Callback<ResponseSlbBean<HCouponsBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HCouponsBean>> call, Response<ResponseSlbBean<HCouponsBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnCouponsNodata(response.body().getMsg());
                    return;
                }
                getView().OnCouponsSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HCouponsBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnCouponsFail(string);
                call.cancel();
            }
        });

    }

}
