package com.example.biz3hxktindex;

public class ResponseDemoBean<T> {
    private int code;
    private String message;
    private T data;

    public ResponseDemoBean() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String msg) {
        this.message = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("code: ").append(this.code).append("\n");
        sb.append("message: ").append(this.message).append("\n");
        sb.append("data: ").append(this.data).append("\n");
        return sb.toString();
    }
}
