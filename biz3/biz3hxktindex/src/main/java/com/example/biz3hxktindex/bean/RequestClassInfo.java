package com.example.biz3hxktindex.bean;

public class RequestClassInfo {


    /**
     * limit : 3
     * page : 1
     * keyword : {"classifyId":1,"courseSource":0}
     */

    private String limit;
    private String page;
    private KeywordBean keyword;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public KeywordBean getKeyword() {
        return keyword;
    }

    public void setKeyword(KeywordBean keyword) {
        this.keyword = keyword;
    }

    public static class KeywordBean {

        private String orderStatus;

        public KeywordBean() {
        }

        public KeywordBean(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }
    }
}
