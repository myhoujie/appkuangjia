package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HCouponsListBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String allCount;
    private String currPage;
    private String pageSize;
    private String todayCount;
    private String totalCount;
    private String totalPage;
    private List<HCouponsListBean1> list;//

    public HCouponsListBean() {
    }

    public HCouponsListBean(List<HCouponsListBean1> list) {
        this.list = list;
    }

    public List<HCouponsListBean1> getList() {
        return list;
    }

    public void setList(List<HCouponsListBean1> list) {
        this.list = list;
    }
}
