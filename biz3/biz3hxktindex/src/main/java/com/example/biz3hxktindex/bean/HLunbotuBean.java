package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HLunbotuBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HLunbotuBean1> bannerList;

    public HLunbotuBean() {
    }

    public HLunbotuBean(List<HLunbotuBean1> bannerList) {
        this.bannerList = bannerList;
    }

    public List<HLunbotuBean1> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<HLunbotuBean1> bannerList) {
        this.bannerList = bannerList;
    }
}
