package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HMonthSheduleBean;
import com.example.biz3hxktindex.view.HMonthSheduleView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HMonthShedulePresenter extends Presenter<HMonthSheduleView> {

    public void get_MonthShedule(String month) {
        JSONObject requestData = new JSONObject();
        requestData.put("month", month);//
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier()).get_MonthShedule(requestBody).enqueue(new Callback<ResponseSlbBean<HMonthSheduleBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HMonthSheduleBean>> call, Response<ResponseSlbBean<HMonthSheduleBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnMonthSheduleNodata(response.body().getMsg());
                    return;
                }
                getView().OnMonthSheduleSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HMonthSheduleBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnMonthSheduleFail(string);
                call.cancel();
            }
        });

    }

}
