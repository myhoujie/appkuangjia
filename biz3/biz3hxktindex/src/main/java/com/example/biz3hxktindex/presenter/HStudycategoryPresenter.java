package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HClasscategoryBean;
import com.example.biz3hxktindex.bean.HStudycategoryBean;
import com.example.biz3hxktindex.view.HClasscategoryView;
import com.example.biz3hxktindex.view.HStudycategoryView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HStudycategoryPresenter extends Presenter<HStudycategoryView> {

    public void get_studycategorylist(String courseId, String limit, String page, String sheduleStatus) {
        JSONObject requestData = new JSONObject();
//        requestData.put("courseId", courseId);//
        requestData.put("limit", limit);//
        requestData.put("page", page);//
        requestData.put("sheduleStatus", sheduleStatus);//
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_studycategorylist(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HStudycategoryBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HStudycategoryBean>> call, Response<ResponseSlbBean<HStudycategoryBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnStudycategoryNodata(response.body().getMsg());
                    return;
                }
                getView().OnStudycategorySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HStudycategoryBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnStudycategoryFail(string);
                call.cancel();
            }
        });

    }

}
