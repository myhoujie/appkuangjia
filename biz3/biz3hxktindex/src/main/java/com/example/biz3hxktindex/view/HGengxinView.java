package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HGengxinBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HGengxinView extends IView {

    void OnGengxinSuccess(HGengxinBean bean);
    void OnGengxinNodata(String bean);
    void OnGengxinFail(String msg);

}
