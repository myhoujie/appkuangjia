package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSON;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HfeedBackReqBean;
import com.example.biz3hxktindex.view.HfeedBackView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HfeedBackPresenter extends Presenter<HfeedBackView> {

    public void get_feedBack(HfeedBackReqBean bean) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JSON.toJSONString(bean));
        RetrofitNetNew.build(Api.class, getIdentifier()).get_feedBack(requestBody).enqueue(new Callback<ResponseSlbBean<Object>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<Object>> call, Response<ResponseSlbBean<Object>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnfeedBackNodata(response.body().getMsg());
                    return;
                }
                getView().OnfeedBackSuccess(response.body().getMsg());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<Object>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnfeedBackFail(string);
                call.cancel();
            }
        });

    }

}
