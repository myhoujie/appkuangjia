package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HMyorderBean;
import com.example.biz3hxktindex.bean.HMyorderBeanNew;
import com.example.biz3hxktindex.bean.RequestClassInfo;
import com.example.biz3hxktindex.view.HMyorderView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HMyorderPresenter extends Presenter<HMyorderView> {

    public void get_myorderlist(String page, String limit, String id) {
        RequestClassInfo requestClassInfo = new RequestClassInfo();
        requestClassInfo.setPage(page);
        requestClassInfo.setLimit(limit);
        requestClassInfo.setKeyword(new RequestClassInfo.KeywordBean(id));
        String requestData = JSON.toJSONString(requestClassInfo);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData);

        RetrofitNetNew.build(Api.class, getIdentifier()).get_myorderlist(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HMyorderBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<HMyorderBean>> call, Response<ResponseSlbBean<HMyorderBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnMyorderNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnMyorderSuccess(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<HMyorderBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnMyorderFail(string);
                        call.cancel();
                    }
                });

    }

    public void get_myorderlist2(String page, String limit, String orderStatus) {
        JSONObject requestData = new JSONObject();
        requestData.put("page", page);//
        requestData.put("limit", limit);//
        requestData.put("orderStatus", orderStatus);//

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_myorderlist2(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HMyorderBeanNew>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<HMyorderBeanNew>> call, Response<ResponseSlbBean<HMyorderBeanNew>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnMyorderNewNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnMyorderNewSuccess(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<HMyorderBeanNew>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnMyorderNewFail(string);
                        call.cancel();
                    }
                });

    }

}
