package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HMonthSheduleBean;
import com.example.biz3hxktindex.bean.HlistItemTodayBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HMonthSheduleView extends IView {

    void OnMonthSheduleSuccess(HMonthSheduleBean bean);

    void OnMonthSheduleNodata(String bean);

    void OnMonthSheduleFail(String msg);

}
