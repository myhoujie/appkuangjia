package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HCouponsListBean;
import com.example.biz3hxktindex.view.HCouponsListView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HCouponsListPresenter extends Presenter<HCouponsListView> {

    public void get_coupons_list(String couponInfoState, String limit, String page) {
        JSONObject requestData = new JSONObject();
        requestData.put("couponInfoState", couponInfoState);//
        requestData.put("limit", limit);//
        requestData.put("page", page);//
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier()).get_coupons_list(requestBody).enqueue(new Callback<ResponseSlbBean<HCouponsListBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HCouponsListBean>> call, Response<ResponseSlbBean<HCouponsListBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnCouponsListNodata(response.body().getMsg());
                    return;
                }
                getView().OnCouponsListSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HCouponsListBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnCouponsListFail(string);
                call.cancel();
            }
        });

    }

}
