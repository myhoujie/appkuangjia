package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HUploadurlsView extends IView {

    void OnUploadurlsSuccess(String bean);

    void OnUploadurlsNodata(String bean);

    void OnUploadurlsFail(String msg);

}
