package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HGradecategoryBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HGradecategoryBean1> gradeModels;//

    public HGradecategoryBean() {
    }

    public HGradecategoryBean(List<HGradecategoryBean1> gradeModels) {
        this.gradeModels = gradeModels;
    }

    public List<HGradecategoryBean1> getGradeModels() {
        return gradeModels;
    }

    public void setGradeModels(List<HGradecategoryBean1> gradeModels) {
        this.gradeModels = gradeModels;
    }
}
