package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HfeedBackView extends IView {

    void OnfeedBackSuccess(String bean);

    void OnfeedBackNodata(String bean);

    void OnfeedBackFail(String msg);

}
