package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HCategoryBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HCategoryView extends IView {

    void OnCategorySuccess(HCategoryBean bean);
    void OnCategoryNodata(String bean);
    void OnCategoryFail(String msg);

}
