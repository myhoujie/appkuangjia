package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HLunbotuView extends IView {

    void OnLunbotuSuccess(HLunbotuBean bean);
    void OnLunbotuNodata(String bean);
    void OnLunbotuFail(String msg);

}
