package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HExerciseResponseBean;
import com.example.biz3hxktindex.view.HExerciseView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HExerciseInfoPresenter extends Presenter<HExerciseView> {
    public void get_exerciseInfo(String itemId) {
        JSONObject requestData = new JSONObject();
        requestData.put("itemId", itemId);//
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier()).get_exerciseInfo(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HExerciseResponseBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<HExerciseResponseBean>> call, Response<ResponseSlbBean<HExerciseResponseBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnExerciseInfoNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnExerciseInfoSuccess(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<HExerciseResponseBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        System.out.println("eeeeeeeee:" + t.toString());
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnExerciseInfoFail(string);
                        call.cancel();
                    }
                });

    }

}
