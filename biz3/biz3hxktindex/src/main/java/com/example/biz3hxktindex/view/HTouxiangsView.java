package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HTouxiangsView extends IView {

    void OnTouxiangsSuccess(HTouxiangBean1 bean);

    void OnTouxiangsNodata(String bean);

    void OnTouxiangsFail(String msg);

}
