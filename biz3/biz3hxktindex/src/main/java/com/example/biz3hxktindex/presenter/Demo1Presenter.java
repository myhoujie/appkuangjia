package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.ResponseDemoBean;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.Demo1Bean;
import com.example.biz3hxktindex.view.Demo1View;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Demo1Presenter extends Presenter<Demo1View> {

    public void get_Demo1(String pageNum, String pageSize, String searchKey) {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_demo1(pageNum, pageSize, searchKey)
                .enqueue(new Callback<ResponseDemoBean<Demo1Bean>>() {
                    @Override
                    public void onResponse(Call<ResponseDemoBean<Demo1Bean>> call, Response<ResponseDemoBean<Demo1Bean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnDemo1Nodata(response.body().getMsg());
                            return;
                        }
                        getView().OnDemo1Success(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseDemoBean<Demo1Bean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnDemo1Fail(string);
                        call.cancel();
                    }
                });

    }

}
