package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HMyorderBean;
import com.example.biz3hxktindex.bean.HMyorderBeanNew;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HMyorderView extends IView {

    void OnMyorderSuccess(HMyorderBean bean);

    void OnMyorderNodata(String bean);

    void OnMyorderFail(String msg);

    void OnMyorderNewSuccess(HMyorderBeanNew bean);

    void OnMyorderNewNodata(String bean);

    void OnMyorderNewFail(String msg);

}
