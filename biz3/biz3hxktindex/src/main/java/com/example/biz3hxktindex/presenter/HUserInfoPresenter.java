package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.example.biz3hxktindex.view.HTuichudengluView;
import com.example.biz3hxktindex.view.HUserInfoView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HUserInfoPresenter extends Presenter<HUserInfoView> {

    public void get_userinfo() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_userinfo().enqueue(new Callback<ResponseSlbBean<HUserInfoBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HUserInfoBean>> call, Response<ResponseSlbBean<HUserInfoBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnUserInfoNodata(response.body().getMsg());
                    return;
                }
                getView().OnUserInfoSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HUserInfoBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnUserInfoFail(string);
                call.cancel();
            }
        });

    }

}
