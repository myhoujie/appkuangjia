package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HMyorderBeanNew1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String amountFinal;
    private String amountPayable;
    private String countDownSecond;
    private String  id;
    private String orderCreateTime;
    private String orderIdStr;
    private String pkgTitle;
    private String orderName;
    private String orderNo;
    private String orderStatus;
    private String orderStatusName;
    private String orderType;
    private String buttonCancel;
    private String buttonDelete;
    private String buttonInfo;
    private String buttonPay;
    private String courseNum;
    private List<HMyorderBean1> sons;
    private long currentCountDownSecond;

    public HMyorderBeanNew1() {
    }

    public String getPkgTitle() {
        return pkgTitle;
    }

    public void setPkgTitle(String pkgTitle) {
        this.pkgTitle = pkgTitle;
    }

    public String getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    public long getCurrentCountDownSecond() {
        return currentCountDownSecond;
    }

    public void setCurrentCountDownSecond(long currentCountDownSecond) {
        this.currentCountDownSecond = currentCountDownSecond;
    }

    public String getAmountFinal() {
        return amountFinal;
    }

    public void setAmountFinal(String amountFinal) {
        this.amountFinal = amountFinal;
    }

    public String getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(String amountPayable) {
        this.amountPayable = amountPayable;
    }

    public String getCountDownSecond() {
        return countDownSecond;
    }

    public void setCountDownSecond(String countDownSecond) {
        this.countDownSecond = countDownSecond;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderIdStr() {
        return orderIdStr;
    }

    public void setOrderIdStr(String orderIdStr) {
        this.orderIdStr = orderIdStr;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonDelete() {
        return buttonDelete;
    }

    public void setButtonDelete(String buttonDelete) {
        this.buttonDelete = buttonDelete;
    }

    public String getButtonInfo() {
        return buttonInfo;
    }

    public void setButtonInfo(String buttonInfo) {
        this.buttonInfo = buttonInfo;
    }

    public String getButtonPay() {
        return buttonPay;
    }

    public void setButtonPay(String buttonPay) {
        this.buttonPay = buttonPay;
    }

    public List<HMyorderBean1> getSons() {
        return sons;
    }

    public void setSons(List<HMyorderBean1> sons) {
        this.sons = sons;
    }
}
