package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.view.HTuichudengluView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTuichudengluPresenter extends Presenter<HTuichudengluView> {

    public void get_tuichudenglu() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_tuichudenglu().enqueue(new Callback<ResponseSlbBean<Object>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<Object>> call, Response<ResponseSlbBean<Object>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTuichudengluNodata(response.body().getMsg());
                    return;
                }
                getView().OnTuichudengluSuccess(response.body().getMsg());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<Object>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTuichudengluFail(string);
                call.cancel();
            }
        });

    }

}
