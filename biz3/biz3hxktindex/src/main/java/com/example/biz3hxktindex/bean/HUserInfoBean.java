package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HUserInfoBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String childAvatar;// 宝宝-头像url
    private String childGender;// 宝宝-性别 1男 2女 0未知
    private String childGenderName;// 宝宝-性别 1男 2女 0未知
    private String childGrade;// 宝宝-年级
    private String childGradeName;// 宝宝-年级
    private String childNikeName;// 宝宝-昵称
    private String nikeName;// 昵称
    private String phone;// 手机号
    private String swChildId;// 宝宝-swChildId
    private String swUserId;// 主键id
    private String addressDtl;// 地址
    private String couponDesc;// 地址
    private HMyaddressBean userAddress;

    public HUserInfoBean() {
    }

    public HUserInfoBean(String childAvatar, String childGender, String childGenderName, String childGrade, String childGradeName, String childNikeName, String nikeName, String phone, String swChildId, String swUserId, String addressDtl) {
        this.childAvatar = childAvatar;
        this.childGender = childGender;
        this.childGenderName = childGenderName;
        this.childGrade = childGrade;
        this.childGradeName = childGradeName;
        this.childNikeName = childNikeName;
        this.nikeName = nikeName;
        this.phone = phone;
        this.swChildId = swChildId;
        this.swUserId = swUserId;
        this.addressDtl = addressDtl;
    }

    public HUserInfoBean(String childAvatar, String childGender, String childGenderName, String childGrade, String childGradeName, String childNikeName, String nikeName, String phone, String swChildId, String swUserId, String addressDtl, String couponDesc, HMyaddressBean userAddress) {
        this.childAvatar = childAvatar;
        this.childGender = childGender;
        this.childGenderName = childGenderName;
        this.childGrade = childGrade;
        this.childGradeName = childGradeName;
        this.childNikeName = childNikeName;
        this.nikeName = nikeName;
        this.phone = phone;
        this.swChildId = swChildId;
        this.swUserId = swUserId;
        this.addressDtl = addressDtl;
        this.couponDesc = couponDesc;
        this.userAddress = userAddress;
    }

    public HMyaddressBean getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(HMyaddressBean userAddress) {
        this.userAddress = userAddress;
    }

    public String getChildAvatar() {
        return childAvatar;
    }

    public void setChildAvatar(String childAvatar) {
        this.childAvatar = childAvatar;
    }

    public String getChildGender() {
        return childGender;
    }

    public void setChildGender(String childGender) {
        this.childGender = childGender;
    }

    public String getChildGenderName() {
        return childGenderName;
    }

    public void setChildGenderName(String childGenderName) {
        this.childGenderName = childGenderName;
    }

    public String getChildGrade() {
        return childGrade;
    }

    public void setChildGrade(String childGrade) {
        this.childGrade = childGrade;
    }

    public String getChildGradeName() {
        return childGradeName;
    }

    public void setChildGradeName(String childGradeName) {
        this.childGradeName = childGradeName;
    }

    public String getChildNikeName() {
        return childNikeName;
    }

    public void setChildNikeName(String childNikeName) {
        this.childNikeName = childNikeName;
    }

    public String getNikeName() {
        return nikeName;
    }

    public void setNikeName(String nikeName) {
        this.nikeName = nikeName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSwChildId() {
        return swChildId;
    }

    public void setSwChildId(String swChildId) {
        this.swChildId = swChildId;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getAddressDtl() {
        return addressDtl;
    }

    public void setAddressDtl(String addressDtl) {
        this.addressDtl = addressDtl;
    }

    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }
}
