package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo4Bean1;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo4View extends IView {

    void OnDemo4Success(Demo4Bean1 bean);

    void OnDemo4Nodata(String bean);

    void OnDemo4Fail(String msg);

}
