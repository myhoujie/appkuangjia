package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HClasscategoryBean;
import com.example.biz3hxktindex.bean.HGradecategoryBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HClasscategoryView extends IView {

    void OnClasscategorySuccess(HClasscategoryBean bean);
    void OnClasscategoryNodata(String bean);
    void OnClasscategoryFail(String msg);

}
