package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.view.HUserInfoupdateView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HUserInfoupdatePresenter extends Presenter<HUserInfoupdateView> {

    public void get_myaddressupdate(String childGender, String childGrade, String childName, String swChildId, String swUserId, String addressDtl) {
        JSONObject requestData = new JSONObject();
        requestData.put("childGender", childGender);// 性别 1男 2女 0未知
        requestData.put("childGrade", childGrade);// 年纪
        requestData.put("childName", childName);// 学员姓名
        requestData.put("swChildId", swChildId);// swChildId
        requestData.put("swUserId", swUserId);// swUserId
        requestData.put("addressDtl", addressDtl);// 地址

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_userinfoupdate(requestBody).enqueue(new Callback<ResponseSlbBean<Object>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<Object>> call, Response<ResponseSlbBean<Object>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnUserInfoUpdateNodata(response.body().getMsg());
                    return;
                }
                getView().OnUserInfoUpdateSuccess(response.body().getMsg());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<Object>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnUserInfoUpdateFail(string);
                call.cancel();
            }
        });

    }

}
