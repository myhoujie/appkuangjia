package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.view.HTouxiangView;
import com.example.biz3hxktindex.view.HTouxiangsView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTouxiangsPresenter extends Presenter<HTouxiangsView> {

    // file方式上传bufen
    public void get_touxiang3(List<File> fileList) {
        Map<String, RequestBody> params = new HashMap<>();
        //以下参数是，参数需要换成自己服务器支持的
        params.put("uid", convertToRequestBody("1701"));
        params.put("token", convertToRequestBody("E6C4C1B581A506F2F4D6748B3649AD3C"));
        params.put("source", convertToRequestBody("android"));
        params.put("appVersion", convertToRequestBody("101"));

//        JSONObject requestData = new JSONObject();
//        requestData.put("nickName", "");
//        RequestBody requestBody1 = RequestBody.create(MediaType.parse("text/plain"), requestData.toString());
        List<MultipartBody.Part> partList = filesToMultipartBodyParts(fileList);
        RetrofitNetNew.build(Api.class, getIdentifier()).get_touxiang3(params, partList)
                .enqueue(new Callback<ResponseSlbBean<HTouxiangBean1>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HTouxiangBean1>> call, Response<ResponseSlbBean<HTouxiangBean1>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTouxiangsNodata(response.body().getMsg());
                    return;
                }
                getView().OnTouxiangsSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HTouxiangBean1>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTouxiangsFail(string);
                call.cancel();
            }
        });

    }

    private RequestBody convertToRequestBody(String param) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), param);
        return requestBody;
    }


    private List<MultipartBody.Part> filesToMultipartBodyParts(List<File> files) {
        List<MultipartBody.Part> parts = new ArrayList<>(files.size());
        for (File file : files) {
            //设置文件的类型
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            //file就是上传文件的参数类型,后面的file.getName()就是你上传的文件,首先要拿到文件的地址
            MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), requestBody);
            parts.add(part);
        }
        return parts;
    }
}
