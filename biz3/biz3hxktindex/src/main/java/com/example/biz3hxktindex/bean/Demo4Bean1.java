package com.example.biz3hxktindex.bean;

import java.io.Serializable;

//         {
//            "createTime": "2020-04-22 19:25:04",
//            "createUserID": "1",
//            "createUserName": "stylefeng",
//            "dataStatus": 1,
//            "dc": 0,
//            "fwmc": "string",
//            "hb": "string",
//            "jd": "string",
//            "modifyTime": "2020-04-22 19:25:04",
//            "objID": "64B6AA73-E239-4608-9452-AFE48F34A393",
//            "read": "",
//            "sblx": 0,
//            "sblxqtmc": "string",
//            "wd": "string",
//            "wxsl": 0,
//            "xxxx": "string",
//            "ztmc": "string"
//          }
public class Demo4Bean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String createTime;
    private String createUserID;
    private String createUserName;
    private String dataStatus;
    private String dc;
    private String hb;
    private String jd;
    private String modifyTime;
    private String objID;
    private String read;
    private String sblx;
    private String sblxqtmc;
    private String wd;
    private String wxsl;
    private String xxxx;
    private String ztmc;

    public Demo4Bean1() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getHb() {
        return hb;
    }

    public void setHb(String hb) {
        this.hb = hb;
    }

    public String getJd() {
        return jd;
    }

    public void setJd(String jd) {
        this.jd = jd;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getSblx() {
        return sblx;
    }

    public void setSblx(String sblx) {
        this.sblx = sblx;
    }

    public String getSblxqtmc() {
        return sblxqtmc;
    }

    public void setSblxqtmc(String sblxqtmc) {
        this.sblxqtmc = sblxqtmc;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }

    public String getWxsl() {
        return wxsl;
    }

    public void setWxsl(String wxsl) {
        this.wxsl = wxsl;
    }

    public String getXxxx() {
        return xxxx;
    }

    public void setXxxx(String xxxx) {
        this.xxxx = xxxx;
    }

    public String getZtmc() {
        return ztmc;
    }

    public void setZtmc(String ztmc) {
        this.ztmc = ztmc;
    }
}
