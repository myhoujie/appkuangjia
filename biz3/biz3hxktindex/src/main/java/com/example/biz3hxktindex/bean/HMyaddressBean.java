package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HMyaddressBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String addressDtl;// 详细地址
    private String city;// 市
    private String district;// 区
    private String id;//
    private String province;// 省
    private String remark;// 备注
    private String swUserId;//
    private String userName;// 收货人
    private String userPhone;// 收货手机号
    private String zipcode;// 收货人的邮编

    public HMyaddressBean() {
    }

    public HMyaddressBean(String addressDtl, String city, String district, String id, String province, String remark, String swUserId, String userName, String userPhone, String zipcode) {
        this.addressDtl = addressDtl;
        this.city = city;
        this.district = district;
        this.id = id;
        this.province = province;
        this.remark = remark;
        this.swUserId = swUserId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.zipcode = zipcode;
    }

    public String getAddressDtl() {
        return addressDtl;
    }

    public void setAddressDtl(String addressDtl) {
        this.addressDtl = addressDtl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
