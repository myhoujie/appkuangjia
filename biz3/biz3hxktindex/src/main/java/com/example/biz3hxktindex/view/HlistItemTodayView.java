package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HlistItemTodayBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HlistItemTodayView extends IView {

    void OnlistItemTodaySuccess(HlistItemTodayBean bean);

    void OnlistItemTodayNodata(String bean);

    void OnlistItemTodayFail(String msg);

}
