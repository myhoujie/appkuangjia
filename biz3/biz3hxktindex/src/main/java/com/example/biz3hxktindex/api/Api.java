package com.example.biz3hxktindex.api;

import com.example.biz3hxktindex.ResponseDemoBean;
import com.example.biz3hxktindex.bean.Demo1Bean;
import com.example.biz3hxktindex.bean.Demo2Bean;
import com.example.biz3hxktindex.bean.Demo3Bean;
import com.example.biz3hxktindex.bean.Demo4Bean1;
import com.example.biz3hxktindex.bean.Demo6Bean;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HClasscategoryBean;
import com.example.biz3hxktindex.bean.HCouponsBean;
import com.example.biz3hxktindex.bean.HCouponsListBean;
import com.example.biz3hxktindex.bean.HExerciseResponseBean;
import com.example.biz3hxktindex.bean.HGengxinBean;
import com.example.biz3hxktindex.bean.HGradecategoryBean;
import com.example.biz3hxktindex.bean.HLoginBean;
import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.bean.HMonthSheduleBean;
import com.example.biz3hxktindex.bean.HMyaddressBean;
import com.example.biz3hxktindex.bean.HMyorderBean;
import com.example.biz3hxktindex.bean.HMyorderBeanNew;
import com.example.biz3hxktindex.bean.HSearchBean;
import com.example.biz3hxktindex.bean.HSettingBean;
import com.example.biz3hxktindex.bean.HStudycategoryBean;
import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.example.biz3hxktindex.bean.HTouxiangBean1;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.example.biz3hxktindex.bean.HZuoyeinfoBean;
import com.example.biz3hxktindex.bean.HlistItemTodayBean;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;
import com.haier.cellarette.libvariants.NetConfig;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface Api {

    // app差量更新
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/appUpgrade")
    Call<ResponseSlbBean<HGengxinBean>> get_gengxin(@Body RequestBody body);

    // 所有分类的列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/enum/liveEnumLists")
    Call<ResponseSlbBean<HCategoryBean>> get_category(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_category2(@Body RequestBody body);

    // 获取二维码
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/sendloginVCode2")
    Call<ResponseSlbBean<Object>> get_erweima(@Body RequestBody body);

    // 上传用户头像信息1
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/uploadBase64")
    Call<ResponseSlbBean<HTouxiangBean>> get_touxiang1(@Body RequestBody body);

    // 上传用户头像信息2
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @Multipart
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/upload/avatar")
    Call<ResponseSlbBean<HTouxiangBean>> get_touxiang2(/*@Part("userinfo") RequestBody body1,*/
            @Part MultipartBody.Part body2);

    // 多图上传
    // 上传用户头像信息2
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @Multipart
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/multiUpload")
    Call<ResponseSlbBean<HTouxiangBean1>> get_touxiang3(@PartMap Map<String, RequestBody> map,
                                                        @Part List<MultipartBody.Part> parts);

    // setting信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/dict/getDicts")
    Call<ResponseSlbBean<HSettingBean>> get_setting(@Body RequestBody body);

    // 用户登录
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/login")
    Call<ResponseSlbBean<HLoginBean>> get_yonghudenglu(@Body RequestBody body);

    // 退出登录
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/logout")
    Call<ResponseSlbBean<Object>> get_tuichudenglu();

    // 获取个人资料信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/getUserInfoNew")
    Call<ResponseSlbBean<HUserInfoBean>> get_userinfo();

    // 我的收货地址
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/getMyAddressInfo")
    Call<ResponseSlbBean<HMyaddressBean>> get_myaddress();

    // 更新收货地址
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/updateUserAddress")
    Call<ResponseSlbBean<Object>> get_myaddressupdate(@Body RequestBody body);

    // 编辑个人资料
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/updateUserInfo")
    Call<ResponseSlbBean<Object>> get_userinfoupdate(@Body RequestBody body);


    // 轮播图
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/banner/banner")
    Call<ResponseSlbBean<HLunbotuBean>> get_lunbotu();


    // 首页年级分类
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/enum/gradeInfo")
    Call<ResponseSlbBean<HGradecategoryBean>> get_gradecategory();

    // 首页选课分类列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/course/list")
    Call<ResponseSlbBean<HClasscategoryBean>> get_classcategorylist(@Body RequestBody body);


    // 首页学习分类列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/list")
    Call<ResponseSlbBean<HStudycategoryBean>> get_studycategorylist(@Body RequestBody body);


    // 我的订单列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order/myOrder")
    Call<ResponseSlbBean<HMyorderBean>> get_myorderlist(@Body RequestBody body);

    // 我的订单列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/myOrder/parent")
//    @POST("http://aic.sairobo.cn:8082/liveApp/order2/myOrder/parent")
    Call<ResponseSlbBean<HMyorderBeanNew>> get_myorderlist2(@Body RequestBody body);


    // 我的订单列表->删除
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/cancel")
    Call<ResponseSlbBean<Object>> get_myorderlist_delete(@Body RequestBody body);


    // 我的订单列表->删除2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/delete")
    Call<ResponseSlbBean<Object>> get_myorderlist_delete2(@Body RequestBody body);


    // 获取图片地址提交给data
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/saveExercise")
    Call<ResponseSlbBean<Object>> get_upload_img_urls(@Body RequestBody body);


    // 获取作业信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/getExerciseInfo")
    Call<ResponseSlbBean<HZuoyeinfoBean>> get_zuoyeinfo(@Body RequestBody body);

    // 获取作业信息(1.4.1)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/getExerciseInfo2")
    Call<ResponseSlbBean<HExerciseResponseBean>> get_exerciseInfo(@Body RequestBody body);


    // 今日直播课节列表(不分页)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/listItemToday")
    Call<ResponseSlbBean<HlistItemTodayBean>> get_listItemToday();


    // 按月查询排课日期
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/getMonthShedule")
    Call<ResponseSlbBean<HMonthSheduleBean>> get_MonthShedule(@Body RequestBody body);

    // 按天查询直播课节列表(不分页)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/listItemByDay")
    Call<ResponseSlbBean<HlistItemTodayBean>> get_listItemByDay(@Body RequestBody body);

    // 意见反馈
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/feedBack")
    Call<ResponseSlbBean<Object>> get_feedBack(@Body RequestBody body);


    // 优惠券分类列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/coupon/couponStateList")
//    @POST("http://aic.sairobo.cn:8082/liveApp/coupon/couponStateList")
    Call<ResponseSlbBean<HCouponsBean>> get_coupons();

    // 优惠券列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/coupon/couponList")
//    @POST("http://aic.sairobo.cn:8082/liveApp/coupon/couponList")
    Call<ResponseSlbBean<HCouponsListBean>> get_coupons_list(@Body RequestBody body);

    // 搜索
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "picbook/app/searchAll")
    Call<ResponseSlbBean<HSearchBean>> get_my_search(@Body RequestBody body);

    // 搜索热门标签
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "picbook/app/searchAll")
    Call<ResponseSlbBean<HSearchBean>> get_my_search2(@Body RequestBody body);

    String aaaaaaa = "";

    // demo 获取监控区域
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET(aaaaaaa + "area")
    Call<ResponseDemoBean<Demo1Bean>> get_demo1(@Query("pageNum") String pageNum, @Query("pageSize") String pageSize, @Query("searchKey") String searchKey);

    // demo 根据监控区域获取设备
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET(aaaaaaa + "device")
    Call<ResponseDemoBean<Demo2Bean>> get_demo2(@Query("areaID") String areaID, @Query("pageNum") String pageNum, @Query("pageSize") String pageSize, @Query("searchKey") String searchKey);

    // demo 获取信息列表
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET(aaaaaaa + "info")
    Call<ResponseDemoBean<Demo3Bean>> get_demo3(@Query("sblx") String sblx, @Query("pageNum") String pageNum, @Query("pageSize") String pageSize, @Query("searchKey") String searchKey);

    // demo 提交信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(aaaaaaa + "info")
    Call<ResponseDemoBean<Demo4Bean1>> get_demo4(@Query("sblx") String sblx, @Query("pageNum") String pageNum, @Query("pageSize") String pageSize, @Query("searchKey") String searchKey);

    // demo 获取信息详情
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET(aaaaaaa + "info/{infoID}")
    Call<ResponseDemoBean<Demo4Bean1>> get_demo5(@Query("infoID") String infoID);

    // demo 查询报警信息
//    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET(aaaaaaa + "warning")
    Call<ResponseDemoBean<Demo6Bean>> get_demo6(@Query("end_time") String end_time, @Query("jk_id") String jk_id
            , @Query("pageNum") String pageNum, @Query("pageSize") String pageSize, @Query("start_time") String start_time);


}
