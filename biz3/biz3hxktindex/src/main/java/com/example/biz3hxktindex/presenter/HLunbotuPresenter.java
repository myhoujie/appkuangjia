package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HLunbotuBean;
import com.example.biz3hxktindex.view.HLunbotuView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HLunbotuPresenter extends Presenter<HLunbotuView> {

    public void get_lunbotu() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_lunbotu().enqueue(new Callback<ResponseSlbBean<HLunbotuBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HLunbotuBean>> call, Response<ResponseSlbBean<HLunbotuBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnLunbotuNodata(response.body().getMsg());
                    return;
                }
                getView().OnLunbotuSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HLunbotuBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnLunbotuFail(string);
                call.cancel();
            }
        });

    }

}
