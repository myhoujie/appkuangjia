package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HLoginBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HYonghudengluView extends IView {

    void OnYonghudengluSuccess(HLoginBean bean);
    void OnYonghudengluNodata(String bean);
    void OnYonghudengluFail(String msg);

}
