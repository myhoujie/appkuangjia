package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.ResponseDemoBean;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.Demo2Bean;
import com.example.biz3hxktindex.bean.Demo3Bean;
import com.example.biz3hxktindex.view.Demo2View;
import com.example.biz3hxktindex.view.Demo3View;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Demo3Presenter extends Presenter<Demo3View> {

    public void get_Demo3(String sblx, String pageNum, String pageSize, String searchKey) {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_demo3(sblx, pageNum, pageSize, searchKey)
                .enqueue(new Callback<ResponseDemoBean<Demo3Bean>>() {
                    @Override
                    public void onResponse(Call<ResponseDemoBean<Demo3Bean>> call, Response<ResponseDemoBean<Demo3Bean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnDemo3Nodata(response.body().getMsg());
                            return;
                        }
                        getView().OnDemo3Success(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseDemoBean<Demo3Bean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnDemo3Fail(string);
                        call.cancel();
                    }
                });

    }

}
