package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo2Bean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo2View extends IView {

    void OnDemo2Success(Demo2Bean bean);

    void OnDemo2Nodata(String bean);

    void OnDemo2Fail(String msg);

}
