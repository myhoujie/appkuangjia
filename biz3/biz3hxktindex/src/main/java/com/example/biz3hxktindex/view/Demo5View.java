package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo6Bean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo5View extends IView {

    void OnDemo5Success(Demo6Bean bean);

    void OnDemo5Nodata(String bean);

    void OnDemo5Fail(String msg);

}
