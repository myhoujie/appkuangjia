package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.view.HMyaddressupdateView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HMyaddressupdatePresenter extends Presenter<HMyaddressupdateView> {

    public void get_myaddressupdate(String addressDtl, String city, String district, String province, String swUserId, String userName, String userPhone) {
        JSONObject requestData = new JSONObject();
        requestData.put("addressDtl", addressDtl);//
        requestData.put("city", city);//
        requestData.put("district", district);//
        requestData.put("province", province);//
        requestData.put("swUserId", swUserId);//
        requestData.put("userName", userName);//
        requestData.put("userPhone", userPhone);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_myaddressupdate(requestBody).enqueue(new Callback<ResponseSlbBean<Object>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<Object>> call, Response<ResponseSlbBean<Object>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnMyaddressupdateNodata(response.body().getMsg());
                    return;
                }
                getView().OnMyaddressupdateSuccess(response.body().getMsg());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<Object>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnMyaddressupdateFail(string);
                call.cancel();
            }
        });

    }

}
