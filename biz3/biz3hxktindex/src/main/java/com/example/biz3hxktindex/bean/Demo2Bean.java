package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class Demo2Bean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String current;
    private String pages;
    private String searchCount;
    private String size;
    private String total;
//    private List<String> orders;//
    private List<Demo1Bean1> records;//

    public Demo2Bean() {
    }

    public Demo2Bean(String current, String pages, String searchCount, String size, String total, List<Demo1Bean1> records) {
        this.current = current;
        this.pages = pages;
        this.searchCount = searchCount;
        this.size = size;
        this.total = total;
        this.records = records;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(String searchCount) {
        this.searchCount = searchCount;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Demo1Bean1> getRecords() {
        return records;
    }

    public void setRecords(List<Demo1Bean1> records) {
        this.records = records;
    }
}
