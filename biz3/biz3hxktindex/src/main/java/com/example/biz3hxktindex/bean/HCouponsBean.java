package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HCouponsBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HCouponsBean1> list;//

    public HCouponsBean() {
    }

    public HCouponsBean(List<HCouponsBean1> list) {
        this.list = list;
    }

    public List<HCouponsBean1> getList() {
        return list;
    }

    public void setList(List<HCouponsBean1> list) {
        this.list = list;
    }
}
