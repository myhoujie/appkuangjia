package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HfeedBackReqBean implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * appVersion :
     * contact :
     * content :
     * imei :
     * imgs : []
     * userAgent :
     */

    private String appVersion;
    private String contact;
    private String content;
    private String imei;
    private String userAgent;
    private List<String> imgs;

    public HfeedBackReqBean() {
    }

    public HfeedBackReqBean(String appVersion, String contact, String content, String imei, String userAgent, List<String> imgs) {
        this.appVersion = appVersion;
        this.contact = contact;
        this.content = content;
        this.imei = imei;
        this.userAgent = userAgent;
        this.imgs = imgs;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }
}
