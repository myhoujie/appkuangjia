package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HSettingBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String startBgImg;
    private String aboutUs;
    private String forceLogin;
    private String how2Teach;
    private String aboutHxEdu;
    private String startSound;
    private String serviceProtocol;
    private String privacyPolicy;
    private String hxEduWechatImg;
    private String contactInfo;
    private String hxEduWechatNum;
    private String admission;
    private String couponDescUrl;

    public HSettingBean() {
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getHxEduWechatNum() {
        return hxEduWechatNum;
    }

    public void setHxEduWechatNum(String hxEduWechatNum) {
        this.hxEduWechatNum = hxEduWechatNum;
    }

    public String getStartBgImg() {
        return startBgImg;
    }

    public void setStartBgImg(String startBgImg) {
        this.startBgImg = startBgImg;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getForceLogin() {
        return forceLogin;
    }

    public void setForceLogin(String forceLogin) {
        this.forceLogin = forceLogin;
    }

    public String getHow2Teach() {
        return how2Teach;
    }

    public void setHow2Teach(String how2Teach) {
        this.how2Teach = how2Teach;
    }

    public String getAboutHxEdu() {
        return aboutHxEdu;
    }

    public void setAboutHxEdu(String aboutHxEdu) {
        this.aboutHxEdu = aboutHxEdu;
    }

    public String getStartSound() {
        return startSound;
    }

    public void setStartSound(String startSound) {
        this.startSound = startSound;
    }

    public String getServiceProtocol() {
        return serviceProtocol;
    }

    public void setServiceProtocol(String serviceProtocol) {
        this.serviceProtocol = serviceProtocol;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public String getHxEduWechatImg() {
        return hxEduWechatImg;
    }

    public void setHxEduWechatImg(String hxEduWechatImg) {
        this.hxEduWechatImg = hxEduWechatImg;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getCouponDescUrl() {
        return couponDescUrl;
    }

    public void setCouponDescUrl(String couponDescUrl) {
        this.couponDescUrl = couponDescUrl;
    }
}
