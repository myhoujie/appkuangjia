package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HUploadurlsBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String itemId;
    private List<String> pics;

    public HUploadurlsBean() {
    }

    public HUploadurlsBean(String itemId, List<String> pics) {
        this.itemId = itemId;
        this.pics = pics;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }
}
