package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HMonthSheduleBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<String> timeStamps;//

    public HMonthSheduleBean() {
    }

    public HMonthSheduleBean(List<String> timeStamps) {
        this.timeStamps = timeStamps;
    }

    public List<String> getTimeStamps() {
        return timeStamps;
    }

    public void setTimeStamps(List<String> timeStamps) {
        this.timeStamps = timeStamps;
    }
}
