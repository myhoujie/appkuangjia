package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HUserInfoupdateView extends IView {

    void OnUserInfoUpdateSuccess(String bean);
    void OnUserInfoUpdateNodata(String bean);
    void OnUserInfoUpdateFail(String msg);

}
