package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HStudycategoryBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HStudycategoryView extends IView {

    void OnStudycategorySuccess(HStudycategoryBean bean);

    void OnStudycategoryNodata(String bean);

    void OnStudycategoryFail(String msg);

}
