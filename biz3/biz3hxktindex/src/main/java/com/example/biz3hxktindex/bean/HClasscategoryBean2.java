package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HClasscategoryBean2 implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * chargeFlag : 1
     * courseType : system
     * releaseTime : 1567741822000
     * coverImg : http://edu-ae-test.oss-cn-hangzhou.aliyuncs.com/20190906/2db9d3c00be043aebe6d54763f5f8a14.png
     * subject : |1|2|
     * liveLecturerId : 2
     * remark :
     * classAttribute : system
     * delFlag : 0
     * initSignUpNum : 0
     * releaseStatus : 1
     * id : 1
     * recommendFlag : 1
     * courseService : |4|
     * lecturer : {"nikeName":"admin2","sysUserId":"2","avatar":""}
     * techSysUserId : 196
     * signUpCount : 0
     * tutor : {"nikeName":"泽学家","sysUserId":"196","avatar":""}
     * itemCount : 0
     * courseName : 【2019-暑】五年级升六年级数学北师引航班全国版
     * hideFlag : 0
     * pricePayable : 30
     * createTime : 1567741821000
     * grade : |1|
     * priceMarked : 100
     * releaseWay : immediate
     * orders : 999
     * courseDetail : <p>课程详情</p>
     */

    private int chargeFlag;
    private String courseType;
    private List<String> subjectNames;
    private String courseTimeRange;
    private String releaseTime;
    private String coverImg;
    private String subject;
    private String liveLecturerId;
    private String remark;
    private String classAttribute;
    private int delFlag;
    private int initSignUpNum;
    private int releaseStatus;
    private String id;
    private int recommendFlag;
    private String courseService;
    private LecturerBean lecturer;
    private String techSysUserId;
    private int signUpCount;
    private TutorBean tutor;
    private int itemCount;
    private String courseName;
    private int hideFlag;
    private String pricePayable;
    private String createTime;
    private String grade;
    private String priceMarked;
    private String releaseWay;
    private int orders;
    private String courseDetail;
    private List<HClasscategoryBean3> teacherList;


    public HClasscategoryBean2() {
    }

    public int getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(int chargeFlag) {
        this.chargeFlag = chargeFlag;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLiveLecturerId() {
        return liveLecturerId;
    }

    public void setLiveLecturerId(String liveLecturerId) {
        this.liveLecturerId = liveLecturerId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public int getInitSignUpNum() {
        return initSignUpNum;
    }

    public void setInitSignUpNum(int initSignUpNum) {
        this.initSignUpNum = initSignUpNum;
    }

    public int getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(int releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRecommendFlag() {
        return recommendFlag;
    }

    public void setRecommendFlag(int recommendFlag) {
        this.recommendFlag = recommendFlag;
    }

    public String getCourseService() {
        return courseService;
    }

    public void setCourseService(String courseService) {
        this.courseService = courseService;
    }

    public LecturerBean getLecturer() {
        return lecturer;
    }

    public void setLecturer(LecturerBean lecturer) {
        this.lecturer = lecturer;
    }

    public String getTechSysUserId() {
        return techSysUserId;
    }

    public void setTechSysUserId(String techSysUserId) {
        this.techSysUserId = techSysUserId;
    }

    public int getSignUpCount() {
        return signUpCount;
    }

    public void setSignUpCount(int signUpCount) {
        this.signUpCount = signUpCount;
    }

    public TutorBean getTutor() {
        return tutor;
    }

    public void setTutor(TutorBean tutor) {
        this.tutor = tutor;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getHideFlag() {
        return hideFlag;
    }

    public void setHideFlag(int hideFlag) {
        this.hideFlag = hideFlag;
    }

    public String getPricePayable() {
        return pricePayable;
    }

    public void setPricePayable(String pricePayable) {
        this.pricePayable = pricePayable;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPriceMarked() {
        return priceMarked;
    }

    public void setPriceMarked(String priceMarked) {
        this.priceMarked = priceMarked;
    }

    public String getReleaseWay() {
        return releaseWay;
    }

    public void setReleaseWay(String releaseWay) {
        this.releaseWay = releaseWay;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public String getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(String courseDetail) {
        this.courseDetail = courseDetail;
    }

    public static class LecturerBean {
        /**
         * nikeName : admin2
         * sysUserId : 2
         * avatar :
         */

        private String nikeName;
        private String sysUserId;
        private String avatar;

        public String getNikeName() {
            return nikeName;
        }

        public void setNikeName(String nikeName) {
            this.nikeName = nikeName;
        }

        public String getSysUserId() {
            return sysUserId;
        }

        public void setSysUserId(String sysUserId) {
            this.sysUserId = sysUserId;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

    public static class TutorBean {
        /**
         * nikeName : 泽学家
         * sysUserId : 196
         * avatar :
         */

        private String nikeName;
        private String sysUserId;
        private String avatar;

        public String getNikeName() {
            return nikeName;
        }

        public void setNikeName(String nikeName) {
            this.nikeName = nikeName;
        }

        public String getSysUserId() {
            return sysUserId;
        }

        public void setSysUserId(String sysUserId) {
            this.sysUserId = sysUserId;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

    public List<HClasscategoryBean3> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<HClasscategoryBean3> teacherList) {
        this.teacherList = teacherList;
    }
}
