package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HCouponsBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String key1;//
    private String value1;//

    public HCouponsBean1() {
    }

    public HCouponsBean1(String key1, String value1) {
        this.key1 = key1;
        this.value1 = value1;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }
}
