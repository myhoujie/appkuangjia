package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class Demo1Bean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String createTime;//
    private String createUserID;//
    private String createUserName;//
    private String dataStatus;//
    private String modifyTime;//
    private String name;//
    private String objID;//

    public Demo1Bean1() {
    }

    public Demo1Bean1(String createTime, String createUserID, String createUserName, String dataStatus, String modifyTime, String name, String objID) {
        this.createTime = createTime;
        this.createUserID = createUserID;
        this.createUserName = createUserName;
        this.dataStatus = dataStatus;
        this.modifyTime = modifyTime;
        this.name = name;
        this.objID = objID;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }
}
