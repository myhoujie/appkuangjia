package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HErweimaView extends IView {

    void OnErweimaSuccess(String bean);
    void OnErweimaNodata(String bean);
    void OnErweimaFail(String msg);

}
