package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HMyaddressupdateView extends IView {

    void OnMyaddressupdateSuccess(String bean);
    void OnMyaddressupdateNodata(String bean);
    void OnMyaddressupdateFail(String msg);

}
