package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HMyorderBeanNew implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HMyorderBeanNew1> list;

    public HMyorderBeanNew() {
    }

    public HMyorderBeanNew(List<HMyorderBeanNew1> list) {
        this.list = list;
    }

    public List<HMyorderBeanNew1> getList() {
        return list;
    }

    public void setList(List<HMyorderBeanNew1> list) {
        this.list = list;
    }
}
