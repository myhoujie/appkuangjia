package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HClasscategoryBean3 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nikeName;
    private String sysUserId;
    private String avatar;
    private String teacherType;

    public HClasscategoryBean3() {
    }

    public HClasscategoryBean3(String nikeName, String sysUserId, String avatar, String teacherType) {
        this.nikeName = nikeName;
        this.sysUserId = sysUserId;
        this.avatar = avatar;
        this.teacherType = teacherType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNikeName() {
        return nikeName;
    }

    public void setNikeName(String nikeName) {
        this.nikeName = nikeName;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType;
    }
}
