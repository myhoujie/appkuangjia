package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.view.HUploadurlsView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HUploadurlsPresenter extends Presenter<HUploadurlsView> {

    public void get_upload_img_urls(String itemId, List<String> pics) {
        JSONObject requestData = new JSONObject();
        requestData.put("itemId", itemId);//
        requestData.put("pics", pics);// 是否需要全部:0-否,1-是
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_upload_img_urls(requestBody)
                .enqueue(new Callback<ResponseSlbBean<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<Object>> call, Response<ResponseSlbBean<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnUploadurlsNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnUploadurlsSuccess(response.body().getMsg());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnUploadurlsFail(string);
                        call.cancel();
                    }
                });

    }

}
