package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HExerciseResponseBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String exerciseStatus;
    private List<HZuoyeinfoBean> exercises;
    private String liveCourseId;
    private String liveCourseItemId;
    private String secondHandFlag;
    private String swUserId;
    private List<String> teachers;

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public List<HZuoyeinfoBean> getExercises() {
        return exercises;
    }

    public void setExercises(List<HZuoyeinfoBean> exercises) {
        this.exercises = exercises;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getLiveCourseItemId() {
        return liveCourseItemId;
    }

    public void setLiveCourseItemId(String liveCourseItemId) {
        this.liveCourseItemId = liveCourseItemId;
    }

    public String getSecondHandFlag() {
        return secondHandFlag;
    }

    public void setSecondHandFlag(String secondHandFlag) {
        this.secondHandFlag = secondHandFlag;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public List<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<String> teachers) {
        this.teachers = teachers;
    }
}
