package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo6Bean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo6View extends IView {

    void OnDemo6Success(Demo6Bean bean);

    void OnDemo6Nodata(String bean);

    void OnDemo6Fail(String msg);

}
