package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HCouponsBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HCouponsView extends IView {

    void OnCouponsSuccess(HCouponsBean bean);
    void OnCouponsNodata(String bean);
    void OnCouponsFail(String msg);

}
