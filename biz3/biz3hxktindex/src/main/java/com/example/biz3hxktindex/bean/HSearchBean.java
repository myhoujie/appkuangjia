package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;


public class HSearchBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HSearchBean1> result;

    public HSearchBean() {
    }

    public HSearchBean(List<HSearchBean1> result) {
        this.result = result;
    }

    public List<HSearchBean1> getResult() {
        return result;
    }

    public void setResult(List<HSearchBean1> result) {
        this.result = result;
    }
}
