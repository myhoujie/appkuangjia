package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class TodayCourseBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String courseItemTimeRange;
    private String courseName;
    private String exerciseStatus;
    private String itemStatus;
    private String itemTitle;
    private String liveCourseId;
    private String liveCourseItemId;
    private String orders;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCourseItemTimeRange() {
        return courseItemTimeRange;
    }

    public void setCourseItemTimeRange(String courseItemTimeRange) {
        this.courseItemTimeRange = courseItemTimeRange;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getLiveCourseItemId() {
        return liveCourseItemId;
    }

    public void setLiveCourseItemId(String liveCourseItemId) {
        this.liveCourseItemId = liveCourseItemId;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }
}
