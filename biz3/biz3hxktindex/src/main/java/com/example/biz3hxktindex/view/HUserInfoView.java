package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HUserInfoView extends IView {

    void OnUserInfoSuccess(HUserInfoBean bean);
    void OnUserInfoNodata(String bean);
    void OnUserInfoFail(String msg);

}
