package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HMyaddressBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HMyaddressView extends IView {

    void OnMyaddressSuccess(HMyaddressBean bean);
    void OnMyaddressNodata(String bean);
    void OnMyaddressFail(String msg);

}
