package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HGengxinBean;
import com.example.biz3hxktindex.view.HGengxinView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HGengxinPresenter extends Presenter<HGengxinView> {

    public void get_gengxin(String clientType, String versionNo, String appPkgName, String versionName) {
        JSONObject requestData = new JSONObject();
        requestData.put("clientType", clientType);// android
        requestData.put("versionNo", versionNo);//
        requestData.put("appPkgName", appPkgName);//
        requestData.put("versionName", versionName);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_gengxin(requestBody).enqueue(new Callback<ResponseSlbBean<HGengxinBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HGengxinBean>> call, Response<ResponseSlbBean<HGengxinBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnGengxinNodata(response.body().getMsg());
                    return;
                }
                getView().OnGengxinSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HGengxinBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnGengxinFail(string);
                call.cancel();
            }
        });

    }

}
