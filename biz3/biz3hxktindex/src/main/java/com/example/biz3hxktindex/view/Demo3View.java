package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo3Bean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo3View extends IView {

    void OnDemo3Success(Demo3Bean bean);

    void OnDemo3Nodata(String bean);

    void OnDemo3Fail(String msg);

}
