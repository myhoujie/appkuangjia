package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HClasscategoryBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HClasscategoryBean1> list;//
    private TodayCourseBean todayCourseModel;

    public HClasscategoryBean() {
    }

    public TodayCourseBean getTodayCourseModel() {
        return todayCourseModel;
    }

    public void setTodayCourseModel(TodayCourseBean todayCourseModel) {
        this.todayCourseModel = todayCourseModel;
    }

    public HClasscategoryBean(List<HClasscategoryBean1> list) {
        this.list = list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<HClasscategoryBean1> getList() {
        return list;
    }

    public void setList(List<HClasscategoryBean1> list) {
        this.list = list;
    }
}
