package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HSearchBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HSearchView extends IView {

    void OnHSearchSuccess(HSearchBean bean, int which);

    void OnHSearchNodata(String bean, int which);

    void OnHSearchFail(String msg, int which);

}
