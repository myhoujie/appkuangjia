package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HMyorderBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HMyorderBean1> list;

    public HMyorderBean() {
    }

    public HMyorderBean(List<HMyorderBean1> list) {
        this.list = list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<HMyorderBean1> getList() {
        return list;
    }

    public void setList(List<HMyorderBean1> list) {
        this.list = list;
    }
}
