package com.example.biz3hxktindex.bean;

import java.io.Serializable;

//         {
//            "areaID": "",
//            "areaName": "",
//            "createTime": "2020-04-26 08:05:13",
//            "createUserID": "1",
//            "createUserName": "stylefeng",
//            "dataStatus": 1,
//            "modifyTime": "2020-04-26 08:05:13",
//            "name": "rtp://123.23.213.123",
//            "objID": "32CAD86B-DD68-4B3D-B5C1-342028CF860F",
//            "rtspURL": ""
//          }
public class Demo2Bean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String areaID;
    private String areaName;
    private String createTime;
    private String createUserID;
    private String createUserName;
    private String dataStatus;
    private String modifyTime;
    private String name;
    private String objID;
    private String rtspURL;

    public Demo2Bean1() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAreaID() {
        return areaID;
    }

    public void setAreaID(String areaID) {
        this.areaID = areaID;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }

    public String getRtspURL() {
        return rtspURL;
    }

    public void setRtspURL(String rtspURL) {
        this.rtspURL = rtspURL;
    }
}
