package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.Demo1Bean;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface Demo1View extends IView {

    void OnDemo1Success(Demo1Bean bean);
    void OnDemo1Nodata(String bean);
    void OnDemo1Fail(String msg);

}
