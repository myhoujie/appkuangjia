package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HZuoyeinfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HZuoyeinfoView extends IView {

    void OnZuoyeinfoSuccess(HZuoyeinfoBean bean);

    void OnZuoyeinfoNodata(String bean);

    void OnZuoyeinfoFail(String msg);

}
