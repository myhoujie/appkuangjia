package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HTouxiangBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HTouxiangView extends IView {

    void OnTouxiangSuccess(HTouxiangBean bean);
    void OnTouxiangNodata(String bean);
    void OnTouxiangFail(String msg);

}
