package com.example.biz3hxktindex.presenter;

import com.blankj.utilcode.util.Utils;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HGradecategoryBean;
import com.example.biz3hxktindex.view.HGradecategoryView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HGradecategoryPresenter extends Presenter<HGradecategoryView> {

    public void get_gradecategory() {
//        JSONObject requestData = new JSONObject();
//        requestData.put("enumType", enumType);//
//        requestData.put("needAll", needAll);// 是否需要全部:0-否,1-是
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_gradecategory().enqueue(new Callback<ResponseSlbBean<HGradecategoryBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HGradecategoryBean>> call, Response<ResponseSlbBean<HGradecategoryBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnGradecategoryNodata(response.body().getMsg());
                    return;
                }
                getView().OnGradecategorySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HGradecategoryBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnGradecategoryFail(string);
                call.cancel();
            }
        });

    }

}
