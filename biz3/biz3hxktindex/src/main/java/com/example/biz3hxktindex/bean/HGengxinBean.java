package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HGengxinBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String apkUrl;// apk的url
    private String clientType;// 移动APP客户端: ios/android
    private String forceFlag;// 是否强制更新(1是,0否)
    private String id;//
    private String state;// 是否上下架(1上架,0下架)
    private String title;// 更新标题
    private String upgradeInfo;// 更新说明
    private String versionNo;// app版本号
    private String appPkgName;// app版本包名
    private String versionName;// app版本名

    public HGengxinBean() {
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getForceFlag() {
        return forceFlag;
    }

    public void setForceFlag(String forceFlag) {
        this.forceFlag = forceFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpgradeInfo() {
        return upgradeInfo;
    }

    public void setUpgradeInfo(String upgradeInfo) {
        this.upgradeInfo = upgradeInfo;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getAppPkgName() {
        return appPkgName;
    }

    public void setAppPkgName(String appPkgName) {
        this.appPkgName = appPkgName;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
