package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HGradecategoryBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String stageCode;
    private String stageName;
    private List<HGradecategoryBean2> grades;//

    public HGradecategoryBean1() {
    }

    public HGradecategoryBean1(String stageCode, String stageName, List<HGradecategoryBean2> grades) {
        this.stageCode = stageCode;
        this.stageName = stageName;
        this.grades = grades;
    }

    public String getStageCode() {
        return stageCode;
    }

    public void setStageCode(String stageCode) {
        this.stageCode = stageCode;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public List<HGradecategoryBean2> getGrades() {
        return grades;
    }

    public void setGrades(List<HGradecategoryBean2> grades) {
        this.grades = grades;
    }
}
