package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HMyorderBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String liveCourseId;
    private String countDownSecond;
    private String orderStatusName;
    private List<String> subjectNames;
    private String orderName;
    private String courseTimeRange;
    private String lecturerName;
    private String amountPayable;
    private String createTimeName;
    private String buttonCancel;
    private String buttonPay;
    private String buttonInfo;
    private String courseTypeCode;
    private String courseTypeName;
    private long currentCountDownSecond;
    private String classAttribute;

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }

    public HMyorderBean1() {
    }

    public long getCurrentCountDownSecond() {
        return currentCountDownSecond;
    }

    public void setCurrentCountDownSecond(long currentCountDownSecond) {
        this.currentCountDownSecond = currentCountDownSecond;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getCountDownSecond() {
        return countDownSecond;
    }

    public void setCountDownSecond(String countDownSecond) {
        this.countDownSecond = countDownSecond;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(String amountPayable) {
        this.amountPayable = amountPayable;
    }

    public String getCreateTimeName() {
        return createTimeName;
    }

    public void setCreateTimeName(String createTimeName) {
        this.createTimeName = createTimeName;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonPay() {
        return buttonPay;
    }

    public void setButtonPay(String buttonPay) {
        this.buttonPay = buttonPay;
    }

    public String getButtonInfo() {
        return buttonInfo;
    }

    public void setButtonInfo(String buttonInfo) {
        this.buttonInfo = buttonInfo;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }
}
