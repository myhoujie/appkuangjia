package com.example.biz3hxktindex.bean;

import java.io.Serializable;

public class HTouxiangBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String url;

    public HTouxiangBean() {
    }

    public HTouxiangBean(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
