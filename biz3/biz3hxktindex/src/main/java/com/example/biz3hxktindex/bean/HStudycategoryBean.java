package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HStudycategoryBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HStudycategoryBean1> list;//

    public HStudycategoryBean() {
    }

    public HStudycategoryBean(List<HStudycategoryBean1> list) {
        this.list = list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<HStudycategoryBean1> getList() {
        return list;
    }

    public void setList(List<HStudycategoryBean1> list) {
        this.list = list;
    }
}
