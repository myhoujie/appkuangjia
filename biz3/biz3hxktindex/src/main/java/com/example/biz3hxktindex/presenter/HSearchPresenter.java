package com.example.biz3hxktindex.presenter;

import com.alibaba.fastjson.JSONObject;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HSearchBean;
import com.example.biz3hxktindex.view.HSearchView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HSearchPresenter extends Presenter<HSearchView> {

    public static String TAG = HSearchPresenter.class.getSimpleName();

    public void getSearchData(String keyword, String limit, final int which) {
        JSONObject requestData = new JSONObject();
        requestData.put("keyword", keyword);
        requestData.put("limit", limit);

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_my_search(requestBody).enqueue(new Callback<ResponseSlbBean<HSearchBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HSearchBean>> call, Response<ResponseSlbBean<HSearchBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnHSearchNodata(response.body().getMsg(), which);
                    return;
                }
                getView().OnHSearchSuccess(response.body().getData(), which);
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HSearchBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnHSearchFail(string, which);
                call.cancel();
            }
        });

    }
}
