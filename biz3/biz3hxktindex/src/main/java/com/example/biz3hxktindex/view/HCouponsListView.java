package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HCouponsBean;
import com.example.biz3hxktindex.bean.HCouponsListBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HCouponsListView extends IView {

    void OnCouponsListSuccess(HCouponsListBean bean);
    void OnCouponsListNodata(String bean);
    void OnCouponsListFail(String msg);

}
