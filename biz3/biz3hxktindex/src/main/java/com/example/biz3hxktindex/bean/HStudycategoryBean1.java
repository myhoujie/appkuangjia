package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HStudycategoryBean1 implements Serializable {
    private static final long serialVersionUID = 1L;

    private String courseName;
    private String courseTypeCode;
    private String courseTypeName;
    private List<String> subjectNames;
    private String courseTimeRange;
    private List<HClasscategoryBean3> teacherList;
    private int itemCount;
    private String nextTime;
    private String statusDesc;
    private String statusInt;
    private int id;
    private String classAttribute;

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }
    public HStudycategoryBean1() {
    }

    public HStudycategoryBean1(String courseName, String courseTypeCode, String courseTypeName, List<String> subjectNames, String courseTimeRange, List<HClasscategoryBean3> teacherList, int itemCount, String nextTime, String statusDesc, String statusInt, int id) {
        this.courseName = courseName;
        this.courseTypeCode = courseTypeCode;
        this.courseTypeName = courseTypeName;
        this.subjectNames = subjectNames;
        this.courseTimeRange = courseTimeRange;
        this.teacherList = teacherList;
        this.itemCount = itemCount;
        this.nextTime = nextTime;
        this.statusDesc = statusDesc;
        this.statusInt = statusInt;
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public List<HClasscategoryBean3> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<HClasscategoryBean3> teacherList) {
        this.teacherList = teacherList;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getNextTime() {
        return nextTime;
    }

    public void setNextTime(String nextTime) {
        this.nextTime = nextTime;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusInt() {
        return statusInt;
    }

    public void setStatusInt(String statusInt) {
        this.statusInt = statusInt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
