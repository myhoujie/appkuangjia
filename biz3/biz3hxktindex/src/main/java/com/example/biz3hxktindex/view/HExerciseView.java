package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HExerciseResponseBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HExerciseView extends IView {

    void OnExerciseInfoSuccess(HExerciseResponseBean bean);

    void OnExerciseInfoNodata(String bean);

    void OnExerciseInfoFail(String msg);

}
