package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HZuoyeinfoBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String exerciseStatus;
    private String exerciseSuggest;
    private String id;
    private String liveCourseId;
    private String liveCourseItemId;
    private String swUserId;
    private List<String> pics;
    private List<String> teachers;
    private List<PicBean> picModels;

    public HZuoyeinfoBean() {
    }

    public HZuoyeinfoBean(String exerciseStatus, String exerciseSuggest, String id, String liveCourseId, String liveCourseItemId, String swUserId, List<String> pics, List<String> teachers) {
        this.exerciseStatus = exerciseStatus;
        this.exerciseSuggest = exerciseSuggest;
        this.id = id;
        this.liveCourseId = liveCourseId;
        this.liveCourseItemId = liveCourseItemId;
        this.swUserId = swUserId;
        this.pics = pics;
        this.teachers = teachers;
    }

    public List<PicBean> getPicModels() {
        return picModels;
    }

    public void setPicModels(List<PicBean> picModels) {
        this.picModels = picModels;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public String getExerciseSuggest() {
        return exerciseSuggest;
    }

    public void setExerciseSuggest(String exerciseSuggest) {
        this.exerciseSuggest = exerciseSuggest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getLiveCourseItemId() {
        return liveCourseItemId;
    }

    public void setLiveCourseItemId(String liveCourseItemId) {
        this.liveCourseItemId = liveCourseItemId;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public List<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<String> teachers) {
        this.teachers = teachers;
    }
}
