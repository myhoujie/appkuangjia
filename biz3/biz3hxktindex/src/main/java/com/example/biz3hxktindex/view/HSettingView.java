package com.example.biz3hxktindex.view;

import com.example.biz3hxktindex.bean.HSettingBean;
import com.example.biz3hxktindex.bean.HUserInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HSettingView extends IView {

    void OnSettingSuccess(HSettingBean bean);
    void OnSettingNodata(String bean);
    void OnSettingFail(String msg);

}
