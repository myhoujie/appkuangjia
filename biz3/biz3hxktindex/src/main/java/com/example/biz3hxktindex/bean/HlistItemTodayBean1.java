package com.example.biz3hxktindex.bean;

import java.io.Serializable;
import java.util.List;

public class HlistItemTodayBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * courseName :
     * endTime :
     * exerciseStatus :
     * hasReport : 0
     * id : 0
     * itemStatus : 0
     * itemTimeRange : 09月01日 09:00-11:00
     * itemTitle :
     * orders : 0
     * reportUrl :
     * startTime :
     * subjectNames : []
     * todayFlag : 0
     */

    private String courseName;
    private String endTime;
    private String exerciseStatus;
    private String hasReport;
    private String id;
    private String itemStatus;
    private String itemTimeRange;
    private String itemTitle;
    private String orders;
    private String reportUrl;
    private String startTime;
    private String todayFlag;
    private String courseType;
    private List<String> subjectNames;
    private String liveCourseId;
    private String classAttribute;

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }
    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public HlistItemTodayBean1() {
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public String getHasReport() {
        return hasReport;
    }

    public void setHasReport(String hasReport) {
        this.hasReport = hasReport;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemTimeRange() {
        return itemTimeRange;
    }

    public void setItemTimeRange(String itemTimeRange) {
        this.itemTimeRange = itemTimeRange;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTodayFlag() {
        return todayFlag;
    }

    public void setTodayFlag(String todayFlag) {
        this.todayFlag = todayFlag;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }
}
