package com.example.biz3hxktindex.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface HMyorderdeleteView extends IView {

    void OnMyorderdeleteSuccess(String bean);

    void OnMyorderdeleteNodata(String bean);

    void OnMyorderdeleteFail(String msg);

}
