package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.ResponseDemoBean;
import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.Demo2Bean;
import com.example.biz3hxktindex.view.Demo2View;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Demo2Presenter extends Presenter<Demo2View> {

    public void get_Demo2(String areaID, String pageNum, String pageSize, String searchKey) {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_demo2(areaID, pageNum, pageSize, searchKey)
                .enqueue(new Callback<ResponseDemoBean<Demo2Bean>>() {
                    @Override
                    public void onResponse(Call<ResponseDemoBean<Demo2Bean>> call, Response<ResponseDemoBean<Demo2Bean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnDemo2Nodata(response.body().getMsg());
                            return;
                        }
                        getView().OnDemo2Success(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseDemoBean<Demo2Bean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnDemo2Fail(string);
                        call.cancel();
                    }
                });

    }

}
