package com.example.biz3hxktindex.presenter;

import com.example.biz3hxktindex.api.Api;
import com.example.biz3hxktindex.bean.HCategoryBean;
import com.example.biz3hxktindex.bean.HlistItemTodayBean;
import com.example.biz3hxktindex.view.HlistItemTodayView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HlistItemTodayPresenter extends Presenter<HlistItemTodayView> {

    public void get_listItemToday() {
        RetrofitNetNew.build(Api.class, getIdentifier()).get_listItemToday().enqueue(new Callback<ResponseSlbBean<HlistItemTodayBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HlistItemTodayBean>> call, Response<ResponseSlbBean<HlistItemTodayBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnlistItemTodayNodata(response.body().getMsg());
                    return;
                }
                getView().OnlistItemTodaySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HlistItemTodayBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnlistItemTodayFail(string);
                call.cancel();
            }
        });

    }

}
