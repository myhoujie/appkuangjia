package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface CourseDetailView extends IView {

    void OnCourseSuccess(CourseResponseBean bean);

    void OnCourseNodata(String bean);

    void OnCourseFail(String msg);


    void OnGetShareInfoSuccess(ShareInfoBean bean);

    void OnGetShareInfoNodata(String bean);

    void OnGetShareInfoFail(String msg);
}
