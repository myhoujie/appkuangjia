package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.ClassSectionInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface ClassSectionInfoView extends IView {
    void OnClassSectionInfoBeanSuccess(ClassSectionInfoBean var1);

    void OnClassSectionInfoBeanNodata(String var1);

    void OnClassSectionInfoBeanFail(String var1);
}