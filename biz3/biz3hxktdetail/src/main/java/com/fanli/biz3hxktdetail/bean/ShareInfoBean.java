package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class ShareInfoBean implements Serializable {
    private String desc;
    private String imgUrl;
    private String title;
    private String urlForFriend;
    private String urlForMoments;
    private String shareUrl;

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlForFriend() {
        return urlForFriend;
    }

    public void setUrlForFriend(String urlForFriend) {
        this.urlForFriend = urlForFriend;
    }

    public String getUrlForMoments() {
        return urlForMoments;
    }

    public void setUrlForMoments(String urlForMoments) {
        this.urlForMoments = urlForMoments;
    }
}
