package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class ClassSectionInfoBean implements Serializable {
    private String courseName;
    private String courseType;
    private String dayChn;
    private String endTime;
    private String exerciseStatus;
    private String hasReport;
    private String id;
    private String itemStatus;
    private String itemTimeRange;
    private String itemTimeRangeNoDay;
    private String itemTitle;
    private String liveCourseId;
    private String orders;
    private String reportUrl;
    private String startTime;
    private List<String> subjectNames;
    private String todayFlag;
    private String weekChn;
    private String lecturerName;

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getDayChn() {
        return dayChn;
    }

    public void setDayChn(String dayChn) {
        this.dayChn = dayChn;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public String getHasReport() {
        return hasReport;
    }

    public void setHasReport(String hasReport) {
        this.hasReport = hasReport;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemTimeRange() {
        return itemTimeRange;
    }

    public void setItemTimeRange(String itemTimeRange) {
        this.itemTimeRange = itemTimeRange;
    }

    public String getItemTimeRangeNoDay() {
        return itemTimeRangeNoDay;
    }

    public void setItemTimeRangeNoDay(String itemTimeRangeNoDay) {
        this.itemTimeRangeNoDay = itemTimeRangeNoDay;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }


    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getTodayFlag() {
        return todayFlag;
    }

    public void setTodayFlag(String todayFlag) {
        this.todayFlag = todayFlag;
    }

    public String getWeekChn() {
        return weekChn;
    }

    public void setWeekChn(String weekChn) {
        this.weekChn = weekChn;
    }
}
