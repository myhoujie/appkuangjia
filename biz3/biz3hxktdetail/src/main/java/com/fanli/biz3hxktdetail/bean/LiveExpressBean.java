package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class LiveExpressBean implements Serializable {
    private String addressDtl;
    private String city;
    private String deliveryTime;
    private String district;
    private String expressCompany;
    private String expressNo;
    private String id;
    private String isSend;
    private String liveOrderDistributorId;
    private String liveOrderId;
    private String liveOrderParentId;
    private String province;
    private String recvTime;
    private String remark;
    private String swUserId;
    private String userName;
    private String userPhone;

    public String getAddressDtl() {
        return addressDtl;
    }

    public void setAddressDtl(String addressDtl) {
        this.addressDtl = addressDtl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

    public String getLiveOrderDistributorId() {
        return liveOrderDistributorId;
    }

    public void setLiveOrderDistributorId(String liveOrderDistributorId) {
        this.liveOrderDistributorId = liveOrderDistributorId;
    }

    public String getLiveOrderId() {
        return liveOrderId;
    }

    public void setLiveOrderId(String liveOrderId) {
        this.liveOrderId = liveOrderId;
    }

    public String getLiveOrderParentId() {
        return liveOrderParentId;
    }

    public void setLiveOrderParentId(String liveOrderParentId) {
        this.liveOrderParentId = liveOrderParentId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRecvTime() {
        return recvTime;
    }

    public void setRecvTime(String recvTime) {
        this.recvTime = recvTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
