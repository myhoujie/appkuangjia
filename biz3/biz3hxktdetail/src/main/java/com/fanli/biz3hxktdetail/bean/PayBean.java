package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class PayBean implements Serializable {
    private String alipayRetStr;
    private String code;
    private String codeUrl;
    private String msg;
    private String prepayId;
    private WxRetBean wxResp;
    private String liveCourseId;
    private String liveOrderId;
    private String orderStatus;
    private String orderId;
    private String orderType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getLiveOrderId() {
        return liveOrderId;
    }

    public void setLiveOrderId(String liveOrderId) {
        this.liveOrderId = liveOrderId;
    }

    public String getAlipayRetStr() {
        return alipayRetStr;
    }

    public void setAlipayRetStr(String alipayRetStr) {
        this.alipayRetStr = alipayRetStr;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeUrl() {
        return codeUrl;
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public WxRetBean getWxResp() {
        return wxResp;
    }

    public void setWxResp(WxRetBean wxResp) {
        this.wxResp = wxResp;
    }
}
