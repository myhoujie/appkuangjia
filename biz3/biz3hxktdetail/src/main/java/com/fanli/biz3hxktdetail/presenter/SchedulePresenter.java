package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.ScheduleResponseBean;
import com.fanli.biz3hxktdetail.view.SchdeuleView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchedulePresenter extends Presenter<SchdeuleView> {

    public void getCourseItemPage(String courseId, int page, int limit) {
        JSONObject requestData = new JSONObject();
        requestData.put("courseId", courseId);
        requestData.put("limit", limit);
        requestData.put("page", page);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_courseItemPage(requestBody).enqueue(new Callback<ResponseSlbBean<ScheduleResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<ScheduleResponseBean>> call, Response<ResponseSlbBean<ScheduleResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnNodata(response.body().getMsg());
                    return;
                }
                getView().OnSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<ScheduleResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnFail(string);
                call.cancel();
            }
        });

    }

}
