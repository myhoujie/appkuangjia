package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class LivePOrderBean implements Serializable {
    private String amountFinal;
    private String amountPayable;
    private String buttonCancel;
    private String buttonDelete;
    private String buttonPay;
    private String countDownSecond;
    private String courseNum;
    private String createTimeName;
    private String id;
    private String needButton;
    private String needToPost;
    private String orderCreateTime;
    private String orderIdStr;
//    private List[] orderIds;
    private String orderName;
    private String orderNo;
    private String orderStatus;
    private String orderType;
    private String amountOrigin;
    private List<DiscountBean> discounts;
    private String liveCoursePkgId;
    private List<LiveSonOrderBean> sons;
    private String orderPayTime;
    private String orderCancelTime;
    private String pkgTitle;

    public String getPkgTitle() {
        return pkgTitle;
    }

    public void setPkgTitle(String pkgTitle) {
        this.pkgTitle = pkgTitle;
    }

    public String getOrderPayTime() {
        return orderPayTime;
    }

    public void setOrderPayTime(String orderPayTime) {
        this.orderPayTime = orderPayTime;
    }

    public String getOrderCancelTime() {
        return orderCancelTime;
    }

    public void setOrderCancelTime(String orderCancelTime) {
        this.orderCancelTime = orderCancelTime;
    }

    public String getLiveCoursePkgId() {
        return liveCoursePkgId;
    }

    public void setLiveCoursePkgId(String liveCoursePkgId) {
        this.liveCoursePkgId = liveCoursePkgId;
    }

    public String getNeedToPost() {
        return needToPost;
    }

    public void setNeedToPost(String needToPost) {
        this.needToPost = needToPost;
    }

    public List<DiscountBean> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<DiscountBean> discounts) {
        this.discounts = discounts;
    }

    public String getAmountOrigin() {
        return amountOrigin;
    }

    public void setAmountOrigin(String amountOrigin) {
        this.amountOrigin = amountOrigin;
    }

    public String getAmountFinal() {
        return amountFinal;
    }

    public void setAmountFinal(String amountFinal) {
        this.amountFinal = amountFinal;
    }

    public String getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(String amountPayable) {
        this.amountPayable = amountPayable;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonDelete() {
        return buttonDelete;
    }

    public void setButtonDelete(String buttonDelete) {
        this.buttonDelete = buttonDelete;
    }

    public String getButtonPay() {
        return buttonPay;
    }

    public void setButtonPay(String buttonPay) {
        this.buttonPay = buttonPay;
    }

    public String getCountDownSecond() {
        return countDownSecond;
    }

    public void setCountDownSecond(String countDownSecond) {
        this.countDownSecond = countDownSecond;
    }

    public String getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    public String getCreateTimeName() {
        return createTimeName;
    }

    public void setCreateTimeName(String createTimeName) {
        this.createTimeName = createTimeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNeedButton() {
        return needButton;
    }

    public void setNeedButton(String needButton) {
        this.needButton = needButton;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderIdStr() {
        return orderIdStr;
    }

    public void setOrderIdStr(String orderIdStr) {
        this.orderIdStr = orderIdStr;
    }

//    public List[] getOrderIds() {
//        return orderIds;
//    }
//
//    public void setOrderIds(List[] orderIds) {
//        this.orderIds = orderIds;
//    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public List<LiveSonOrderBean> getSons() {
        return sons;
    }

    public void setSons(List<LiveSonOrderBean> sons) {
        this.sons = sons;
    }
}
