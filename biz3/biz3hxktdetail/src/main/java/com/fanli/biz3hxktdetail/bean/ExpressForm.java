package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class ExpressForm implements Serializable {
    private String addressDtl;
    private String city;
    private String district;
    private String province;
    private String remark;
    private String userName;
    private String userPhone;

    public String getAddressDtl() {
        return addressDtl;
    }

    public void setAddressDtl(String addressDtl) {
        this.addressDtl = addressDtl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
