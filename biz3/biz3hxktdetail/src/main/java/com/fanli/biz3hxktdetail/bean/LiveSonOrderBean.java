package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class LiveSonOrderBean implements Serializable {
    private String amountCoupon;
    private String amountCourse;
    private String amountDiscount;
    private String amountFinal;
    private String amountMarked;
    private String amountOrder;
    private String amountPayable;
    private String buttonCancel;
    private String buttonInfo;
    private String buttonPay;
    private String cancelTime;
    private String client;
    private String countDownSecond;
    private String courseTimeRange;
    private String courseTypeCode;
    private String courseTypeName;
    private String createSysUserId;
    private String createTimeName;
    private String distributorId;
    private String divideAmount;
    private String firstItemStartTime;
    private String froms;
    private String giftFlag;
    private String id;
    private String itemCount;
    private String lastItemEndTime;
    private String lecturerAvatar;
    private String lecturerName;
    private String lecturerName1;
    private String liveCoupouInfoId;
    private String liveCourseId;
    private String liveLecturerId;
    private String liveOrderParentId;
    private boolean needButton;
    private String needToPost;
    private String orderCreateTime;
    private String orderImg;
    private String orderName;
    private String orderNo;
    private String orderStatus;
    private String orderStatusName;
    private String orderType;
    private String payTime;
    private String payWay;
    private String remark;
    private String subject;
    private List<String> subjectNames;
    private String swUserId;
    private String teacherAvatar;
//    private List<Lecturer> teacherList;
    private String teacherName;
    private String techSysUserId;
    private String wxPrepayId;
    private String classAttribute;

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }

    public String getAmountCoupon() {
        return amountCoupon;
    }

    public void setAmountCoupon(String amountCoupon) {
        this.amountCoupon = amountCoupon;
    }

    public String getAmountCourse() {
        return amountCourse;
    }

    public void setAmountCourse(String amountCourse) {
        this.amountCourse = amountCourse;
    }

    public String getAmountDiscount() {
        return amountDiscount;
    }

    public void setAmountDiscount(String amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public String getAmountFinal() {
        return amountFinal;
    }

    public void setAmountFinal(String amountFinal) {
        this.amountFinal = amountFinal;
    }

    public String getAmountMarked() {
        return amountMarked;
    }

    public void setAmountMarked(String amountMarked) {
        this.amountMarked = amountMarked;
    }

    public String getAmountOrder() {
        return amountOrder;
    }

    public void setAmountOrder(String amountOrder) {
        this.amountOrder = amountOrder;
    }

    public String getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(String amountPayable) {
        this.amountPayable = amountPayable;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonInfo() {
        return buttonInfo;
    }

    public void setButtonInfo(String buttonInfo) {
        this.buttonInfo = buttonInfo;
    }

    public String getButtonPay() {
        return buttonPay;
    }

    public void setButtonPay(String buttonPay) {
        this.buttonPay = buttonPay;
    }

    public String getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(String cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCountDownSecond() {
        return countDownSecond;
    }

    public void setCountDownSecond(String countDownSecond) {
        this.countDownSecond = countDownSecond;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }

    public String getCreateSysUserId() {
        return createSysUserId;
    }

    public void setCreateSysUserId(String createSysUserId) {
        this.createSysUserId = createSysUserId;
    }

    public String getCreateTimeName() {
        return createTimeName;
    }

    public void setCreateTimeName(String createTimeName) {
        this.createTimeName = createTimeName;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getDivideAmount() {
        return divideAmount;
    }

    public void setDivideAmount(String divideAmount) {
        this.divideAmount = divideAmount;
    }

    public String getFirstItemStartTime() {
        return firstItemStartTime;
    }

    public void setFirstItemStartTime(String firstItemStartTime) {
        this.firstItemStartTime = firstItemStartTime;
    }

    public String getFroms() {
        return froms;
    }

    public void setFroms(String froms) {
        this.froms = froms;
    }

    public String getGiftFlag() {
        return giftFlag;
    }

    public void setGiftFlag(String giftFlag) {
        this.giftFlag = giftFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getLastItemEndTime() {
        return lastItemEndTime;
    }

    public void setLastItemEndTime(String lastItemEndTime) {
        this.lastItemEndTime = lastItemEndTime;
    }

    public String getLecturerAvatar() {
        return lecturerAvatar;
    }

    public void setLecturerAvatar(String lecturerAvatar) {
        this.lecturerAvatar = lecturerAvatar;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getLecturerName1() {
        return lecturerName1;
    }

    public void setLecturerName1(String lecturerName1) {
        this.lecturerName1 = lecturerName1;
    }

    public String getLiveCoupouInfoId() {
        return liveCoupouInfoId;
    }

    public void setLiveCoupouInfoId(String liveCoupouInfoId) {
        this.liveCoupouInfoId = liveCoupouInfoId;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getLiveLecturerId() {
        return liveLecturerId;
    }

    public void setLiveLecturerId(String liveLecturerId) {
        this.liveLecturerId = liveLecturerId;
    }

    public String getLiveOrderParentId() {
        return liveOrderParentId;
    }

    public void setLiveOrderParentId(String liveOrderParentId) {
        this.liveOrderParentId = liveOrderParentId;
    }

    public boolean isNeedButton() {
        return needButton;
    }

    public void setNeedButton(boolean needButton) {
        this.needButton = needButton;
    }

    public String getNeedToPost() {
        return needToPost;
    }

    public void setNeedToPost(String needToPost) {
        this.needToPost = needToPost;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getTeacherAvatar() {
        return teacherAvatar;
    }

    public void setTeacherAvatar(String teacherAvatar) {
        this.teacherAvatar = teacherAvatar;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTechSysUserId() {
        return techSysUserId;
    }

    public void setTechSysUserId(String techSysUserId) {
        this.techSysUserId = techSysUserId;
    }

    public String getWxPrepayId() {
        return wxPrepayId;
    }

    public void setWxPrepayId(String wxPrepayId) {
        this.wxPrepayId = wxPrepayId;
    }
}
