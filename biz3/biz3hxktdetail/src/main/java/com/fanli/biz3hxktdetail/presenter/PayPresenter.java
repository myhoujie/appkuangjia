package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ExpressForm;
import com.fanli.biz3hxktdetail.bean.PayBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.fanli.biz3hxktdetail.view.PayView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayPresenter extends Presenter<PayView> {

    public void pay(ExpressForm expressForm, String liveCoupouInfoId, String liveCourseId, String liveOrderId, String money, String needToPost, String payType) {
        JSONObject requestData = new JSONObject();
        requestData.put("client", "android");
        requestData.put("expressForm", expressForm);
        requestData.put("liveCoupouInfoId", liveCoupouInfoId);
        requestData.put("liveCourseId", liveCourseId);
        requestData.put("liveOrderId", liveOrderId);
        requestData.put("money", money);
        requestData.put("needToPost", needToPost);
        requestData.put("payType", payType);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).pay(requestBody).enqueue(new Callback<ResponseSlbBean<PayBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<PayBean>> call, Response<ResponseSlbBean<PayBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnPayNodata(response.body().getMsg());
                    return;
                }
                getView().OnPaySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<PayBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnPayFail(string);
                call.cancel();
            }
        });

    }

    public void signUp(ExpressForm expressForm, String liveCourseId, String liveOrderId, String needToPost) {
        JSONObject requestData = new JSONObject();
        requestData.put("client", "android");
        requestData.put("expressForm", expressForm);
        requestData.put("liveCourseId", liveCourseId);
        requestData.put("liveOrderId", liveOrderId);
        requestData.put("needToPost", needToPost);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).signUp(requestBody).enqueue(new Callback<ResponseSlbBean<PayBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<PayBean>> call, Response<ResponseSlbBean<PayBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnPayNodata(response.body().getMsg());
                    return;
                }
                getView().OnPaySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<PayBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnPayFail(string);
                call.cancel();
            }
        });

    }

    public void zeroPay(ExpressForm expressForm, String liveCoupouInfoId, String liveCourseId, String liveOrderId, String money, String needToPost, String payType) {
        JSONObject requestData = new JSONObject();
        requestData.put("client", "android");
        requestData.put("expressForm", expressForm);
        requestData.put("liveCoupouInfoId", liveCoupouInfoId);
        requestData.put("liveCourseId", liveCourseId);
        requestData.put("liveOrderId", liveOrderId);
        requestData.put("money", money);
        requestData.put("needToPost", needToPost);
        requestData.put("payType", payType);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).zeroPay(requestBody).enqueue(new Callback<ResponseSlbBean<PayBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<PayBean>> call, Response<ResponseSlbBean<PayBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnPayNodata(response.body().getMsg());
                    return;
                }
                getView().OnPaySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<PayBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnPayFail(string);
                call.cancel();
            }
        });

    }

    public void pay2(ExpressForm expressForm, List<String> liveCourseIds, String liveCoupouInfoId, String liveCoursePkgId, String liveOrderParentId, String money, String needToPost, String payType) {
        JSONObject requestData = new JSONObject();
        requestData.put("client", "android");
        requestData.put("expressForm", expressForm);
        requestData.put("liveCoupouInfoId", liveCoupouInfoId);
        requestData.put("liveCourseIds", liveCourseIds);
        requestData.put("liveCoursePkgId", liveCoursePkgId);
        requestData.put("liveOrderParentId", liveOrderParentId);
        requestData.put("money", money);
        requestData.put("needToPost", needToPost);
        requestData.put("payType", payType);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).pay2(requestBody).enqueue(new Callback<ResponseSlbBean<PayBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<PayBean>> call, Response<ResponseSlbBean<PayBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnPayNodata(response.body().getMsg());
                    return;
                }
                getView().OnPaySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<PayBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnPayFail(string);
                call.cancel();
            }
        });

    }

}
