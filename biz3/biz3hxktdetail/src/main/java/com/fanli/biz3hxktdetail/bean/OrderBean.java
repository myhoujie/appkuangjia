package com.fanli.biz3hxktdetail.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class OrderBean implements Parcelable {
    private String id;
    private String createTime;
    private String updateTime;
    private String delFlag;
    private String swUserId;
    private String liveCourseId;
    private String orderStatus;
    private String orderNo;
    private String orderName;
    private String orderImg;
    private String amountMarked;
    private String amountPayable;
    private String amountDiscount;
    private String needToPost;
    private String orderType;
    private String payWay;
    private String payTime;
    private String cancelTime;
    private String froms;
    private String client;
    private String remark;
    private String orderStatusName;
    private String firstItemStartTime;
    private String lastItemEndTime;
    private String courseTimeRange;
    private String lecturerName;
    private String subject;
    private List<String> subjectNames;
    private String countDownSecond;
    private String buttonCancel;
    private String buttonPay;
    private String buttonInfo;
    private String orderCreateTime;
    private String createTimeName;
    private String receiveInfo;
    private String expressId;
    private String userName;
    private String userPhone;
    private String province;
    private String city;
    private String district;
    private String addressDtl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    public String getAmountMarked() {
        return amountMarked;
    }

    public void setAmountMarked(String amountMarked) {
        this.amountMarked = amountMarked;
    }

    public String getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(String amountPayable) {
        this.amountPayable = amountPayable;
    }

    public String getAmountDiscount() {
        return amountDiscount;
    }

    public void setAmountDiscount(String amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public String getNeedToPost() {
        return needToPost;
    }

    public void setNeedToPost(String needToPost) {
        this.needToPost = needToPost;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(String cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getFroms() {
        return froms;
    }

    public void setFroms(String froms) {
        this.froms = froms;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String getFirstItemStartTime() {
        return firstItemStartTime;
    }

    public void setFirstItemStartTime(String firstItemStartTime) {
        this.firstItemStartTime = firstItemStartTime;
    }

    public String getLastItemEndTime() {
        return lastItemEndTime;
    }

    public void setLastItemEndTime(String lastItemEndTime) {
        this.lastItemEndTime = lastItemEndTime;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getCountDownSecond() {
        return countDownSecond;
    }

    public void setCountDownSecond(String countDownSecond) {
        this.countDownSecond = countDownSecond;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonPay() {
        return buttonPay;
    }

    public void setButtonPay(String buttonPay) {
        this.buttonPay = buttonPay;
    }

    public String getButtonInfo() {
        return buttonInfo;
    }

    public void setButtonInfo(String buttonInfo) {
        this.buttonInfo = buttonInfo;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getCreateTimeName() {
        return createTimeName;
    }

    public void setCreateTimeName(String createTimeName) {
        this.createTimeName = createTimeName;
    }

    public String getReceiveInfo() {
        return receiveInfo;
    }

    public void setReceiveInfo(String receiveInfo) {
        this.receiveInfo = receiveInfo;
    }

    public String getExpressId() {
        return expressId;
    }

    public void setExpressId(String expressId) {
        this.expressId = expressId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddressDtl() {
        return addressDtl;
    }

    public void setAddressDtl(String addressDtl) {
        this.addressDtl = addressDtl;
    }

    public static Creator<OrderBean> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.createTime);
        dest.writeString(this.updateTime);
        dest.writeString(this.delFlag);
        dest.writeString(this.swUserId);
        dest.writeString(this.liveCourseId);
        dest.writeString(this.orderStatus);
        dest.writeString(this.orderNo);
        dest.writeString(this.orderName);
        dest.writeString(this.orderImg);
        dest.writeString(this.amountMarked);
        dest.writeString(this.amountPayable);
        dest.writeString(this.amountDiscount);
        dest.writeString(this.needToPost);
        dest.writeString(this.orderType);
        dest.writeString(this.payWay);
        dest.writeString(this.payTime);
        dest.writeString(this.cancelTime);
        dest.writeString(this.froms);
        dest.writeString(this.client);
        dest.writeString(this.remark);
        dest.writeString(this.orderStatusName);
        dest.writeString(this.firstItemStartTime);
        dest.writeString(this.lastItemEndTime);
        dest.writeString(this.courseTimeRange);
        dest.writeString(this.lecturerName);
        dest.writeString(this.subject);
        dest.writeStringList(this.subjectNames);
        dest.writeString(this.countDownSecond);
        dest.writeString(this.buttonCancel);
        dest.writeString(this.buttonPay);
        dest.writeString(this.buttonInfo);
        dest.writeString(this.orderCreateTime);
        dest.writeString(this.createTimeName);
        dest.writeString(this.receiveInfo);
        dest.writeString(this.expressId);
        dest.writeString(this.userName);
        dest.writeString(this.userPhone);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeString(this.district);
        dest.writeString(this.addressDtl);
    }

    public OrderBean() {
    }

    protected OrderBean(Parcel in) {
        this.id = in.readString();
        this.createTime = in.readString();
        this.updateTime = in.readString();
        this.delFlag = in.readString();
        this.swUserId = in.readString();
        this.liveCourseId = in.readString();
        this.orderStatus = in.readString();
        this.orderNo = in.readString();
        this.orderName = in.readString();
        this.orderImg = in.readString();
        this.amountMarked = in.readString();
        this.amountPayable = in.readString();
        this.amountDiscount = in.readString();
        this.needToPost = in.readString();
        this.orderType = in.readString();
        this.payWay = in.readString();
        this.payTime = in.readString();
        this.cancelTime = in.readString();
        this.froms = in.readString();
        this.client = in.readString();
        this.remark = in.readString();
        this.orderStatusName = in.readString();
        this.firstItemStartTime = in.readString();
        this.lastItemEndTime = in.readString();
        this.courseTimeRange = in.readString();
        this.lecturerName = in.readString();
        this.subject = in.readString();
        this.subjectNames = in.createStringArrayList();
        this.countDownSecond = in.readString();
        this.buttonCancel = in.readString();
        this.buttonPay = in.readString();
        this.buttonInfo = in.readString();
        this.orderCreateTime = in.readString();
        this.createTimeName = in.readString();
        this.receiveInfo = in.readString();
        this.expressId = in.readString();
        this.userName = in.readString();
        this.userPhone = in.readString();
        this.province = in.readString();
        this.city = in.readString();
        this.district = in.readString();
        this.addressDtl = in.readString();
    }

    public static final Parcelable.Creator<OrderBean> CREATOR = new Parcelable.Creator<OrderBean>() {
        @Override
        public OrderBean createFromParcel(Parcel source) {
            return new OrderBean(source);
        }

        @Override
        public OrderBean[] newArray(int size) {
            return new OrderBean[size];
        }
    };
}
