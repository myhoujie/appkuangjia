package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class ScheduleResponseBean implements Serializable {
    private List<ScheduleBean> list;

    public List<ScheduleBean> getList() {
        return list;
    }

    public void setList(List<ScheduleBean> list) {
        this.list = list;
    }
}
