package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class OrderOrCourseDetailResponseBean implements Serializable {
    private LiveExpressBean liveExpress;
    private LivePOrderBean livePOrder;

    public LiveExpressBean getLiveExpress() {
        return liveExpress;
    }

    public void setLiveExpress(LiveExpressBean liveExpress) {
        this.liveExpress = liveExpress;
    }

    public LivePOrderBean getLivePOrder() {
        return livePOrder;
    }

    public void setLivePOrder(LivePOrderBean livePOrder) {
        this.livePOrder = livePOrder;
    }
}
