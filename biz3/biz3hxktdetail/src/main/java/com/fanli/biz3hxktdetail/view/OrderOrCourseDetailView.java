package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface OrderOrCourseDetailView extends IView {

    void OrderOrCourseDetailSuccess(OrderOrCourseDetailResponseBean bean);

    void OrderOrCourseDetailNodata(String bean);

    void OrderOrCourseDetailFail(String msg);
}