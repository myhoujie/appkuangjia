package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.ScheduleResponseBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface SchdeuleView extends IView {

    void OnSuccess(ScheduleResponseBean bean);
    void OnNodata(String bean);
    void OnFail(String msg);

}