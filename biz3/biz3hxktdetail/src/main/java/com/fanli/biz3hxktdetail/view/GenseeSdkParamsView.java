package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface GenseeSdkParamsView extends IView {

    void OnGenseeSuccess(GenseeBean bean);

    void OnGenseeNodata(String bean);

    void OnGenseeFail(String msg);

}
