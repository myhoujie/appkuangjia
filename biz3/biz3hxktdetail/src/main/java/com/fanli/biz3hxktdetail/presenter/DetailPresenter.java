package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.AvailableCouponInfoBean;
import com.fanli.biz3hxktdetail.bean.CouponPopResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPresenter extends Presenter<CourseDetailView> {

    public void getCourseDetail(String courseId) {
        JSONObject requestData = new JSONObject();
        requestData.put("courseId", courseId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_courseDetail(requestBody).enqueue(new Callback<ResponseSlbBean<CourseResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<CourseResponseBean>> call, Response<ResponseSlbBean<CourseResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnCourseNodata(response.body().getMsg());
                    return;
                }
                getView().OnCourseSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<CourseResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }    System.out.println("eeeeeeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnCourseFail(string);
                call.cancel();
            }
        });

    }
    public void getCourseDetail4Learn(String courseId) {
        JSONObject requestData = new JSONObject();
        requestData.put("courseId", courseId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_courseDtl4learn(requestBody).enqueue(new Callback<ResponseSlbBean<CourseResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<CourseResponseBean>> call, Response<ResponseSlbBean<CourseResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnCourseNodata(response.body().getMsg());
                    return;
                }
                getView().OnCourseSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<CourseResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }    System.out.println("eeeeeeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnCourseFail(string);
                call.cancel();
            }
        });

    }
    public void getShareInfo(String courseId) {
        JSONObject requestData = new JSONObject();
        requestData.put("liveCourseId", courseId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_shareInfo(requestBody).enqueue(new Callback<ResponseSlbBean<ShareInfoBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<ShareInfoBean>> call, Response<ResponseSlbBean<ShareInfoBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnGetShareInfoNodata(response.body().getMsg());
                    return;
                }
                getView().OnGetShareInfoSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<ShareInfoBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnGetShareInfoFail(string);
                call.cancel();
            }
        });

    }

}
