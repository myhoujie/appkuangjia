package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.ClassSectionInfoBean;
import com.fanli.biz3hxktdetail.view.ClassSectionInfoView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassSectionInfoPresenter extends Presenter<ClassSectionInfoView> {

    public void getClassSectionInfo(String itemId) {
        JSONObject requestData = new JSONObject();
        requestData.put("itemId", itemId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_classSectionInfo(requestBody).enqueue(new Callback<ResponseSlbBean<ClassSectionInfoBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<ClassSectionInfoBean>> call, Response<ResponseSlbBean<ClassSectionInfoBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnClassSectionInfoBeanNodata(response.body().getMsg());
                    return;
                }
                getView().OnClassSectionInfoBeanSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<ClassSectionInfoBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnClassSectionInfoBeanFail(string);
                call.cancel();
            }
        });
    }
}

