package com.fanli.biz3hxktdetail.api;

import com.fanli.biz3hxktdetail.bean.AvailableCouponInfoBean;
import com.fanli.biz3hxktdetail.bean.ClassSectionResponseBean;
import com.fanli.biz3hxktdetail.bean.CouponPopResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.ClassSectionInfoBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.fanli.biz3hxktdetail.bean.PayBean;
import com.fanli.biz3hxktdetail.bean.ScheduleResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.bean.TaskBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;
import com.haier.cellarette.libvariants.NetConfig;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {

    // 课程详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/course/courseDtl")
    Call<ResponseSlbBean<CourseResponseBean>> get_courseDetail(@Body RequestBody body);

    // 课程详情(我的课程)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/course/courseDtl4learn")
    Call<ResponseSlbBean<CourseResponseBean>> get_courseDtl4learn(@Body RequestBody body);

    //获取分享信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/course/getShareInfo")
    Call<ResponseSlbBean<ShareInfoBean>> get_shareInfo(@Body RequestBody body);

    // 子课节列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/course/courseItemPage")
    Call<ResponseSlbBean<ScheduleResponseBean>> get_courseItemPage(@Body RequestBody body);

    // 支付
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/pay/pay")
    Call<ResponseSlbBean<PayBean>> pay(@Body RequestBody body);

    // 零元订单支付接口-支付
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/pay/zeroPay")
    Call<ResponseSlbBean<PayBean>> zeroPay(@Body RequestBody body);

    // 支付
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/pay2/pay")
    Call<ResponseSlbBean<PayBean>> pay2(@Body RequestBody body);

    // 报名
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/pay/signUp")
    Call<ResponseSlbBean<PayBean>> signUp(@Body RequestBody body);

    // 任务清单
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order/taskList")
    Call<ResponseSlbBean<TaskBean>> getTaskInfo(@Body RequestBody body);

    // 添加真实姓名
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/updateUserRealName")
    Call<ResponseSlbBean<Object>> updateUserRealName(@Body RequestBody body);

    // 课节列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/listItem")
    Call<ResponseSlbBean<ClassSectionResponseBean>> get_classSectionList(@Body RequestBody body);

    // 课节详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/getCourseItemInfo")
    Call<ResponseSlbBean<ClassSectionInfoBean>> get_classSectionInfo(@Body RequestBody body);

    // 订单详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/info")
    Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> get_orderDetail(@Body RequestBody body);

    // 课程购买详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/buyinfo")
    Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> get_buyDetail(@Body RequestBody body);

    // 取消订单
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/cancel")
    Call<ResponseSlbBean<Object>> cancelOrder(@Body RequestBody body);

    // 删除订单
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/order2/delete")
    Call<ResponseSlbBean<Object>> delOrder(@Body RequestBody body);

    // 获取主讲老师信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/teacher/lecturer/info")
    Call<ResponseSlbBean<TeacherBean>> getTeacherInfo(@Body RequestBody body);

    // 获取主讲老师课程列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/teacher/lecturer/courseList")
    Call<ResponseSlbBean<List<CourseBean>>> getTeacherCourseList(@Body RequestBody body);

    // 获取sdk参数(直播/回放)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/learn/getSdkParam")
    Call<ResponseSlbBean<GenseeBean>> getSdkParam(@Body RequestBody body);

    //优惠券详情弹窗列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/coupon/couponDtlList")
    Call<ResponseSlbBean<CouponPopResponseBean>> getCouponDtlList(@Body RequestBody body);

    //优惠券详情弹窗列表(联报)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/coupon/couponDtlList/courseList")
    Call<ResponseSlbBean<CouponPopResponseBean>> getCouponDtlList2(@Body RequestBody body);

    //根据优惠券:重新计算订单应付金额
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(NetConfig.SERVER_ISERVICE + "liveApp/coupon/reCalOrderPrice")
    Call<ResponseSlbBean<AvailableCouponInfoBean>> reCalOrderPrice(@Body RequestBody body);
}
