package com.fanli.biz3hxktdetail.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface CommonView extends IView {

    void OnSuccess(Object bean);

    void OnNodata(String bean);

    void OnFail(String msg);

}
