package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class CouponPopResponseBean implements Serializable {
    private List<Coupon> couponDtlList;

    public List<Coupon> getCouponDtlList() {
        return couponDtlList;
    }

    public void setCouponDtlList(List<Coupon> couponDtlList) {
        this.couponDtlList = couponDtlList;
    }
}
