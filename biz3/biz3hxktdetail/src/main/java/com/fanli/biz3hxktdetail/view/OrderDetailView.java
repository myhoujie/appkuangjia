package com.fanli.biz3hxktdetail.view;

import com.haier.cellarette.libmvp.mvp.IView;

public interface OrderDetailView extends IView {

//    void OnOrderDetailSuccess(OrderOrCourseDetailResponseBean bean);
//
//    void OnOrderDetaiNodata(String bean);
//
//    void OnOrderDetaiFail(String msg);

    void OnOrdeCancelSuccess(Object bean);

    void OnOrderCancelNodata(String bean);

    void OnOrderCancelFail(String msg);

    void OnOrderDeleteSuccess(Object bean);

    void OnOrderDeleteNodata(String bean);

    void OnOrderDeleteFail(String msg);
}