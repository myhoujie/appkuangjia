package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class Lecturer implements Serializable {
    private String avatar;
    private String nikeName;
    private String sysUserId;
    private String teacherType;
    private String liveLecturerId;
    private String teacherTypeCode;

    public String getTeacherTypeCode() {
        return teacherTypeCode;
    }

    public void setTeacherTypeCode(String teacherTypeCode) {
        this.teacherTypeCode = teacherTypeCode;
    }

    public String getLiveLecturerId() {
        return liveLecturerId;
    }

    public void setLiveLecturerId(String liveLecturerId) {
        this.liveLecturerId = liveLecturerId;
    }

    public String getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNikeName() {
        return nikeName;
    }

    public void setNikeName(String nikeName) {
        this.nikeName = nikeName;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

}
