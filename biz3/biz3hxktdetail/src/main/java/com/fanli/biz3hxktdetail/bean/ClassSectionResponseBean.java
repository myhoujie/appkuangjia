package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;
import java.util.List;

public class ClassSectionResponseBean implements Serializable {
    private List<ClassSectionBean> list;

    public ClassSectionResponseBean() {
    }

    public List<ClassSectionBean> getList() {
        return list;
    }

    public void setList(List<ClassSectionBean> list) {
        this.list = list;
    }
}
