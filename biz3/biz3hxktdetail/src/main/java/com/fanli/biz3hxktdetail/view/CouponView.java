package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.AvailableCouponInfoBean;
import com.fanli.biz3hxktdetail.bean.CouponPopResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface CouponView extends IView {

    void OnGetPopCouponSuccess(CouponPopResponseBean bean);

    void OnGePopCouponNodata(String bean);

    void OnGetPopCouponFail(String msg);

    void OnReCalOrderPriceSuccess(AvailableCouponInfoBean bean);

    void OnReCalOrderPriceNodata(String bean);

    void OnReCalOrderPriceFail(String msg);
}
