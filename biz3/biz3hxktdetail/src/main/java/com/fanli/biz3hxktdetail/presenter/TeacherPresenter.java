package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.ScheduleResponseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;
import com.fanli.biz3hxktdetail.view.SchdeuleView;
import com.fanli.biz3hxktdetail.view.TeacherView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherPresenter extends Presenter<TeacherView> {
    public void getTeacherInfo(String teacherId) {
        JSONObject requestData = new JSONObject();
        requestData.put("teacherId", teacherId);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).getTeacherInfo(requestBody).enqueue(new Callback<ResponseSlbBean<TeacherBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<TeacherBean>> call, Response<ResponseSlbBean<TeacherBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTeacherInfoNodata(response.body().getMsg());
                    return;
                }
                getView().OnTeacherInfoSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<TeacherBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTeacherInfoFail(string);
                call.cancel();
            }
        });

    }
    public void getTeacherCourseList(String teacherId, int page, int limit) {
        JSONObject requestData = new JSONObject();
        requestData.put("teacherId", teacherId);
        requestData.put("limit", limit);
        requestData.put("page", page);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).getTeacherCourseList(requestBody).enqueue(new Callback<ResponseSlbBean<List<CourseBean>>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<List<CourseBean>>> call, Response<ResponseSlbBean<List<CourseBean>>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnTeacherSessionListNodata(response.body().getMsg());
                    return;
                }
                getView().OnTeacherSessionListSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<List<CourseBean>>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnTeacherSessionListFail(string);
                call.cancel();
            }
        });

    }

}
