package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class ClassSectionBean implements Serializable {
    private String endTime;
    private String exerciseStatus;
    private String hasReport;
    private String id;
    private String itemStatus;
    private String itemTitle;
    private String orders;
    private String startTime;
    private String reportUrl;
    private String itemTimeRange;
    private String todayFlag;
    private String weekChn;
    private String lecturerName;
    private String itemTimeRangeNoDay;
    private String dayChn;

    public String getDayChn() {
        return dayChn;
    }

    public void setDayChn(String dayChn) {
        this.dayChn = dayChn;
    }

    public String getItemTimeRangeNoDay() {
        return itemTimeRangeNoDay;
    }

    public void setItemTimeRangeNoDay(String itemTimeRangeNoDay) {
        this.itemTimeRangeNoDay = itemTimeRangeNoDay;
    }

    public String getTodayFlag() {
        return todayFlag;
    }

    public void setTodayFlag(String todayFlag) {
        this.todayFlag = todayFlag;
    }

    public String getWeekChn() {
        return weekChn;
    }

    public void setWeekChn(String weekChn) {
        this.weekChn = weekChn;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    public String getItemTimeRange() {
        return itemTimeRange;
    }

    public void setItemTimeRange(String itemTimeRange) {
        this.itemTimeRange = itemTimeRange;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExerciseStatus() {
        return exerciseStatus;
    }

    public void setExerciseStatus(String exerciseStatus) {
        this.exerciseStatus = exerciseStatus;
    }

    public String getHasReport() {
        return hasReport;
    }

    public void setHasReport(String hasReport) {
        this.hasReport = hasReport;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
