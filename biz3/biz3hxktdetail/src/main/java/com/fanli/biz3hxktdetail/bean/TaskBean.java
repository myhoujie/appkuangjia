package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class TaskBean implements Serializable {
    private String finishFlag;
    private String finishFlagName;
    private String subscriptionId;
    private String subscriptionQr;
    private String teacherWechatId;
    private String teacherWechatQr;

    public String getFinishFlag() {
        return finishFlag;
    }

    public void setFinishFlag(String finishFlag) {
        this.finishFlag = finishFlag;
    }

    public String getFinishFlagName() {
        return finishFlagName;
    }

    public void setFinishFlagName(String finishFlagName) {
        this.finishFlagName = finishFlagName;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionQr() {
        return subscriptionQr;
    }

    public void setSubscriptionQr(String subscriptionQr) {
        this.subscriptionQr = subscriptionQr;
    }

    public String getTeacherWechatId() {
        return teacherWechatId;
    }

    public void setTeacherWechatId(String teacherWechatId) {
        this.teacherWechatId = teacherWechatId;
    }

    public String getTeacherWechatQr() {
        return teacherWechatQr;
    }

    public void setTeacherWechatQr(String teacherWechatQr) {
        this.teacherWechatQr = teacherWechatQr;
    }
}
