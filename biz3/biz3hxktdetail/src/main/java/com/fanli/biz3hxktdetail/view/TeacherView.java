package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.CourseBean;
import com.fanli.biz3hxktdetail.bean.TeacherBean;
import com.haier.cellarette.libmvp.mvp.IView;

import java.util.List;

public interface TeacherView  extends IView {

    void OnTeacherInfoSuccess(TeacherBean bean);

    void OnTeacherInfoNodata(String bean);

    void OnTeacherInfoFail(String msg);

    void OnTeacherSessionListSuccess(List<CourseBean> bean);

    void OnTeacherSessionListNodata(String bean);

    void OnTeacherSessionListFail(String msg);


}
