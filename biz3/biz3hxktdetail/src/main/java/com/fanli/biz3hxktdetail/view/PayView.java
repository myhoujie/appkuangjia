package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.PayBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface PayView extends IView {

    void OnPaySuccess(PayBean bean);

    void OnPayNodata(String bean);

    void OnPayFail(String msg);

}