package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.ClassSectionResponseBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface ClassSectionView  extends IView {
    void OnClassSectionBeanSuccess(ClassSectionResponseBean var1);

    void OnClassSectionBeanNodata(String var1);

    void OnClassSectionBeanFail(String var1);
}