package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.OrderOrCourseDetailResponseBean;
import com.fanli.biz3hxktdetail.view.OrderDetailView;
import com.fanli.biz3hxktdetail.view.OrderOrCourseDetailView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderOrCourseDetailPresenter extends Presenter<OrderOrCourseDetailView> {

    public void get_orderDetail(String orderId,String orderType) {
        JSONObject requestData = new JSONObject();
        requestData.put("id", orderId);
        requestData.put("orderType", orderType);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_orderDetail(requestBody).enqueue(new Callback<ResponseSlbBean<OrderOrCourseDetailResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> call, Response<ResponseSlbBean<OrderOrCourseDetailResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OrderOrCourseDetailNodata(response.body().getMsg());
                    return;
                }
                getView().OrderOrCourseDetailSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OrderOrCourseDetailFail(string);
                call.cancel();
            }
        });
    }

    public void get_buyDetail(List<String> courseIds) {
        JSONObject requestData = new JSONObject();
        requestData.put("courseIds", courseIds);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_buyDetail(requestBody).enqueue(new Callback<ResponseSlbBean<OrderOrCourseDetailResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> call, Response<ResponseSlbBean<OrderOrCourseDetailResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OrderOrCourseDetailNodata(response.body().getMsg());
                    return;
                }
                getView().OrderOrCourseDetailSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<OrderOrCourseDetailResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OrderOrCourseDetailFail(string);
                call.cancel();
            }
        });
    }

}
