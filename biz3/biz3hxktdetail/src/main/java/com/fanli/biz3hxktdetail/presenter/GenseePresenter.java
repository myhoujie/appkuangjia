package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.fanli.biz3hxktdetail.bean.GenseeBean;
import com.fanli.biz3hxktdetail.view.GenseeSdkParamsView;
import com.fanli.biz3hxktdetail.view.TaskView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenseePresenter extends Presenter<GenseeSdkParamsView> {

    public void getSdkParam(String itemId,String status) {
        JSONObject requestData = new JSONObject();
        requestData.put("itemId", itemId);
        requestData.put("status", status);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).getSdkParam(requestBody).enqueue(new Callback<ResponseSlbBean<GenseeBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<GenseeBean>> call, Response<ResponseSlbBean<GenseeBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnGenseeNodata(response.body().getMsg());
                    return;
                }
                getView().OnGenseeSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<GenseeBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnGenseeFail(string);
                call.cancel();
            }
        });

    }
}
