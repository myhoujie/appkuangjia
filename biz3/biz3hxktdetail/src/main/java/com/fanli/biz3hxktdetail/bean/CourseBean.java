package com.fanli.biz3hxktdetail.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CourseBean implements Parcelable {
    private String chargeFlag;//收费标识(0 免费'1 收费 )
    private String classAttribute;
    private String courseDetail;
    private String courseName;
    private String courseService;
    private String courseTimeRange;
    private String courseType;
    private String coverImg;
    private String firstItemStartTime;
    private String grade;
    private String hideFlag;
    private String id;
    private String initSignUpNum;
    private String itemCount;
    private String lastItemEndTime;
    private Lecturer lecturer;
    private String orders;
    private String priceMarked;
    private String pricePayable;
    private String recommendFlag;
    private String releaseStatus;
    private String releaseTime;
    private String releaseWay;
    private String remark;
    private String signUpCount;
    private String subject;
    private String techSysUserId;
    //    private Lecturer tutor;
    private String signed;//是否已报名(购买):1是 0否
    private String courseVideo;
    private String subjectName;
    private List<String> subjectNames;
    private List<Lecturer> teacherList;
    // @ApiModelProperty(required = false, value = "订单状态(待支付unpaid,已支付paid,已取消cancel,已退款refund)")
    private String orderStatus;

    //  @ApiModelProperty(required = false, value = "倒计时时间戳")
    private String countdownMills;
    private String expressFlag;//是否需要物流(0否 1是)
    private String liveOrderId;
    private String liveOrderAmount;
    private String liveOrderParentId;

    public String getLiveOrderParentId() {
        return liveOrderParentId;
    }

    public void setLiveOrderParentId(String liveOrderParentId) {
        this.liveOrderParentId = liveOrderParentId;
    }

    public String getLiveOrderAmount() {
        return liveOrderAmount;
    }

    public void setLiveOrderAmount(String liveOrderAmount) {
        this.liveOrderAmount = liveOrderAmount;
    }

    private List<CourseServiceBean> serviceTagList;

    public List<CourseServiceBean> getServiceTagList() {
        return serviceTagList;
    }

    public void setServiceTagList(List<CourseServiceBean> serviceTagList) {
        this.serviceTagList = serviceTagList;
    }

    public String getLiveOrderId() {
        return liveOrderId;
    }

    public void setLiveOrderId(String liveOrderId) {
        this.liveOrderId = liveOrderId;
    }

    public String getExpressFlag() {
        return expressFlag;
    }

    public void setExpressFlag(String expressFlag) {
        this.expressFlag = expressFlag;
    }

    public List<Lecturer> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Lecturer> teacherList) {
        this.teacherList = teacherList;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(List<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public String getCountdownMills() {
        return countdownMills;
    }

    public void setCountdownMills(String countdownMills) {
        this.countdownMills = countdownMills;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCourseVideo() {
        return courseVideo;
    }

    public void setCourseVideo(String courseVideo) {
        this.courseVideo = courseVideo;
    }

    public String getSigned() {
        return signed;
    }

    public void setSigned(String signed) {
        this.signed = signed;
    }

    public String getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(String chargeFlag) {
        this.chargeFlag = chargeFlag;
    }

    public String getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(String classAttribute) {
        this.classAttribute = classAttribute;
    }

    public String getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(String courseDetail) {
        this.courseDetail = courseDetail;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseService() {
        return courseService;
    }

    public void setCourseService(String courseService) {
        this.courseService = courseService;
    }

    public String getCourseTimeRange() {
        return courseTimeRange;
    }

    public void setCourseTimeRange(String courseTimeRange) {
        this.courseTimeRange = courseTimeRange;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getFirstItemStartTime() {
        return firstItemStartTime;
    }

    public void setFirstItemStartTime(String firstItemStartTime) {
        this.firstItemStartTime = firstItemStartTime;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getHideFlag() {
        return hideFlag;
    }

    public void setHideFlag(String hideFlag) {
        this.hideFlag = hideFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInitSignUpNum() {
        return initSignUpNum;
    }

    public void setInitSignUpNum(String initSignUpNum) {
        this.initSignUpNum = initSignUpNum;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getLastItemEndTime() {
        return lastItemEndTime;
    }

    public void setLastItemEndTime(String lastItemEndTime) {
        this.lastItemEndTime = lastItemEndTime;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getPriceMarked() {
        return priceMarked;
    }

    public void setPriceMarked(String priceMarked) {
        this.priceMarked = priceMarked;
    }

    public String getPricePayable() {
        return pricePayable;
    }

    public void setPricePayable(String pricePayable) {
        this.pricePayable = pricePayable;
    }

    public String getRecommendFlag() {
        return recommendFlag;
    }

    public void setRecommendFlag(String recommendFlag) {
        this.recommendFlag = recommendFlag;
    }

    public String getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(String releaseStatus) {
        this.releaseStatus = releaseStatus;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getReleaseWay() {
        return releaseWay;
    }

    public void setReleaseWay(String releaseWay) {
        this.releaseWay = releaseWay;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSignUpCount() {
        return signUpCount;
    }

    public void setSignUpCount(String signUpCount) {
        this.signUpCount = signUpCount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTechSysUserId() {
        return techSysUserId;
    }

    public void setTechSysUserId(String techSysUserId) {
        this.techSysUserId = techSysUserId;
    }

//    public Lecturer getTutor() {
//        return tutor;
//    }
//
//    public void setTutor(Lecturer tutor) {
//        this.tutor = tutor;
//    }

    public CourseBean() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.chargeFlag);
        dest.writeString(this.classAttribute);
        dest.writeString(this.courseDetail);
        dest.writeString(this.courseName);
        dest.writeString(this.courseService);
        dest.writeString(this.courseTimeRange);
        dest.writeString(this.courseType);
        dest.writeString(this.coverImg);
        dest.writeString(this.firstItemStartTime);
        dest.writeString(this.grade);
        dest.writeString(this.hideFlag);
        dest.writeString(this.id);
        dest.writeString(this.initSignUpNum);
        dest.writeString(this.itemCount);
        dest.writeString(this.lastItemEndTime);
        dest.writeSerializable(this.lecturer);
        dest.writeString(this.orders);
        dest.writeString(this.priceMarked);
        dest.writeString(this.pricePayable);
        dest.writeString(this.recommendFlag);
        dest.writeString(this.releaseStatus);
        dest.writeString(this.releaseTime);
        dest.writeString(this.releaseWay);
        dest.writeString(this.remark);
        dest.writeString(this.signUpCount);
        dest.writeString(this.subject);
        dest.writeString(this.techSysUserId);
        dest.writeString(this.signed);
        dest.writeString(this.courseVideo);
        dest.writeString(this.subjectName);
        dest.writeStringList(this.subjectNames);
        dest.writeList(this.teacherList);
        dest.writeString(this.orderStatus);
        dest.writeString(this.countdownMills);
        dest.writeString(this.expressFlag);
        dest.writeString(this.liveOrderId);
        dest.writeString(this.liveOrderAmount);
        dest.writeList(this.serviceTagList);
    }

    protected CourseBean(Parcel in) {
        this.chargeFlag = in.readString();
        this.classAttribute = in.readString();
        this.courseDetail = in.readString();
        this.courseName = in.readString();
        this.courseService = in.readString();
        this.courseTimeRange = in.readString();
        this.courseType = in.readString();
        this.coverImg = in.readString();
        this.firstItemStartTime = in.readString();
        this.grade = in.readString();
        this.hideFlag = in.readString();
        this.id = in.readString();
        this.initSignUpNum = in.readString();
        this.itemCount = in.readString();
        this.lastItemEndTime = in.readString();
        this.lecturer = (Lecturer) in.readSerializable();
        this.orders = in.readString();
        this.priceMarked = in.readString();
        this.pricePayable = in.readString();
        this.recommendFlag = in.readString();
        this.releaseStatus = in.readString();
        this.releaseTime = in.readString();
        this.releaseWay = in.readString();
        this.remark = in.readString();
        this.signUpCount = in.readString();
        this.subject = in.readString();
        this.techSysUserId = in.readString();
        this.signed = in.readString();
        this.courseVideo = in.readString();
        this.subjectName = in.readString();
        this.subjectNames = in.createStringArrayList();
        this.teacherList = new ArrayList<Lecturer>();
        in.readList(this.teacherList, Lecturer.class.getClassLoader());
        this.orderStatus = in.readString();
        this.countdownMills = in.readString();
        this.expressFlag = in.readString();
        this.liveOrderId = in.readString();
        this.liveOrderAmount = in.readString();
        this.serviceTagList = new ArrayList<CourseServiceBean>();
        in.readList(this.serviceTagList, CourseServiceBean.class.getClassLoader());
    }

    public static final Creator<CourseBean> CREATOR = new Creator<CourseBean>() {
        @Override
        public CourseBean createFromParcel(Parcel source) {
            return new CourseBean(source);
        }

        @Override
        public CourseBean[] newArray(int size) {
            return new CourseBean[size];
        }
    };
}
