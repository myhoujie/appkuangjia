package com.fanli.biz3hxktdetail.view;

import com.fanli.biz3hxktdetail.bean.TaskBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface TaskView extends IView {

    void OnSuccess(TaskBean bean);

    void OnNodata(String bean);

    void OnFail(String msg);

}