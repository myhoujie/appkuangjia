package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class Coupon implements Serializable {
    private String liveCouponInfoId;
    private String liveCouponId;
    private String couponName;
    private String timeStart;
    private String timeEnd;
    private String strShrink;
    private String couponTypeDesc;
    private String useRangeType;
    private String couponRangeDesc;
    private String amount;
    private String useTimeRange;
    private String collectTime;
    private String useTime;
    private String canSelected;//是否能被选用(1可勾选,0不可勾选)
    private String swUserId;
    private String couponInfoState;
    private String selected;//是否被选用(1勾选,0未勾选)

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getLiveCouponInfoId() {
        return liveCouponInfoId;
    }

    public void setLiveCouponInfoId(String liveCouponInfoId) {
        this.liveCouponInfoId = liveCouponInfoId;
    }

    public String getLiveCouponId() {
        return liveCouponId;
    }

    public void setLiveCouponId(String liveCouponId) {
        this.liveCouponId = liveCouponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getStrShrink() {
        return strShrink;
    }

    public void setStrShrink(String strShrink) {
        this.strShrink = strShrink;
    }

    public String getCouponTypeDesc() {
        return couponTypeDesc;
    }

    public void setCouponTypeDesc(String couponTypeDesc) {
        this.couponTypeDesc = couponTypeDesc;
    }

    public String getUseRangeType() {
        return useRangeType;
    }

    public void setUseRangeType(String useRangeType) {
        this.useRangeType = useRangeType;
    }

    public String getCouponRangeDesc() {
        return couponRangeDesc;
    }

    public void setCouponRangeDesc(String couponRangeDesc) {
        this.couponRangeDesc = couponRangeDesc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUseTimeRange() {
        return useTimeRange;
    }

    public void setUseTimeRange(String useTimeRange) {
        this.useTimeRange = useTimeRange;
    }

    public String getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(String collectTime) {
        this.collectTime = collectTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getCanSelected() {
        return canSelected;
    }

    public void setCanSelected(String canSelected) {
        this.canSelected = canSelected;
    }

    public String getSwUserId() {
        return swUserId;
    }

    public void setSwUserId(String swUserId) {
        this.swUserId = swUserId;
    }

    public String getCouponInfoState() {
        return couponInfoState;
    }

    public void setCouponInfoState(String couponInfoState) {
        this.couponInfoState = couponInfoState;
    }
}
