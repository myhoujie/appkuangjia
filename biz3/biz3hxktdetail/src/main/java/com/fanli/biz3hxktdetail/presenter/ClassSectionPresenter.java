package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.ClassSectionResponseBean;
import com.fanli.biz3hxktdetail.view.ClassSectionView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassSectionPresenter extends Presenter<ClassSectionView> {

    public void getClassSectionList(String courseId, String limit, String page) {
        JSONObject requestData = new JSONObject();
        requestData.put("limit", limit);
        requestData.put("page", page);
        requestData.put("courseId", courseId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_classSectionList(requestBody).enqueue(new Callback<ResponseSlbBean<ClassSectionResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<ClassSectionResponseBean>> call, Response<ResponseSlbBean<ClassSectionResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnClassSectionBeanNodata(response.body().getMsg());
                    return;
                }
                getView().OnClassSectionBeanSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<ClassSectionResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnClassSectionBeanFail(string);
                call.cancel();
            }
        });
    }
}

