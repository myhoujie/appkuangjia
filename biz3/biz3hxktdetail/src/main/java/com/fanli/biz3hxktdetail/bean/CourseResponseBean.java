package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class CourseResponseBean implements Serializable {
    private CourseBean course;
    private String orderId;
    private String orderType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public CourseBean getCourse() {
        return course;
    }

    public void setCourse(CourseBean course) {
        this.course = course;
    }
}
