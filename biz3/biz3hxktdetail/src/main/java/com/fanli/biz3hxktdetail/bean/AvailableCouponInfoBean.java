package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class AvailableCouponInfoBean implements Serializable {
    private String finalAmount;//最终金额
    private String checkedCoupouInfoId;//选中优惠券id
    private String discountAmount;//已优惠金额
    private String availableCount;//可用优惠券数目
    private String desc;//未选优惠券时的描述
    private String showCoupouBox;

    public String getShowCoupouBox() {
        return showCoupouBox;
    }

    public void setShowCoupouBox(String showCoupouBox) {
        this.showCoupouBox = showCoupouBox;
    }

    public String getCheckedCoupouInfoId() {
        return checkedCoupouInfoId;
    }

    public void setCheckedCoupouInfoId(String checkedCoupouInfoId) {
        this.checkedCoupouInfoId = checkedCoupouInfoId;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(String availableCount) {
        this.availableCount = availableCount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
    }
}
