package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class DiscountBean implements Serializable {
    private String amountDiscount;
    private String createSysUserId;
    private String discountName;
    private String discountType;
    private String id;
    private String liveCouponInfoId;
    private String liveCoursePkgId;
    private String liveOrderParentId;

    public String getAmountDiscount() {
        return amountDiscount;
    }

    public void setAmountDiscount(String amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public String getCreateSysUserId() {
        return createSysUserId;
    }

    public void setCreateSysUserId(String createSysUserId) {
        this.createSysUserId = createSysUserId;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLiveCouponInfoId() {
        return liveCouponInfoId;
    }

    public void setLiveCouponInfoId(String liveCouponInfoId) {
        this.liveCouponInfoId = liveCouponInfoId;
    }

    public String getLiveCoursePkgId() {
        return liveCoursePkgId;
    }

    public void setLiveCoursePkgId(String liveCoursePkgId) {
        this.liveCoursePkgId = liveCoursePkgId;
    }

    public String getLiveOrderParentId() {
        return liveOrderParentId;
    }

    public void setLiveOrderParentId(String liveOrderParentId) {
        this.liveOrderParentId = liveOrderParentId;
    }
}
