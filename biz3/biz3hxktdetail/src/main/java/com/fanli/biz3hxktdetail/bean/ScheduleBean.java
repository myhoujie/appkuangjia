package com.fanli.biz3hxktdetail.bean;

import java.io.Serializable;

public class ScheduleBean implements Serializable {
    private String courseItemId;
    private String itemTimeRange;
    private String itemTitle;

    public String getCourseItemId() {
        return courseItemId;
    }

    public void setCourseItemId(String courseItemId) {
        this.courseItemId = courseItemId;
    }

    public String getItemTimeRange() {
        return itemTimeRange;
    }

    public void setItemTimeRange(String itemTimeRange) {
        this.itemTimeRange = itemTimeRange;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }
}
