package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.bean.TaskBean;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.fanli.biz3hxktdetail.view.TaskView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;
import com.haier.cellarette.libutils.utilslib.app.MyLogUtil;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskPresenter extends Presenter<TaskView> {

    public void getTaskInfo(String orderId) {
        JSONObject requestData = new JSONObject();
        requestData.put("orderId", orderId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).getTaskInfo(requestBody).enqueue(new Callback<ResponseSlbBean<TaskBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<TaskBean>> call, Response<ResponseSlbBean<TaskBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnNodata(response.body().getMsg());
                    return;
                }
                getView().OnSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<TaskBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnFail(string);
                call.cancel();
            }
        });

    }
}
