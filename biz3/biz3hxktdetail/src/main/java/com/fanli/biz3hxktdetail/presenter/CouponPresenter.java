package com.fanli.biz3hxktdetail.presenter;

import com.alibaba.fastjson.JSONObject;
import com.fanli.biz3hxktdetail.api.Api;
import com.fanli.biz3hxktdetail.bean.AvailableCouponInfoBean;
import com.fanli.biz3hxktdetail.bean.CouponPopResponseBean;
import com.fanli.biz3hxktdetail.bean.CourseResponseBean;
import com.fanli.biz3hxktdetail.bean.ShareInfoBean;
import com.fanli.biz3hxktdetail.view.CouponView;
import com.fanli.biz3hxktdetail.view.CourseDetailView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.RetrofitNetNew;
import com.haier.cellarette.libretrofit.utils.BanbenUtils;
import com.haier.cellarette.libretrofit.utils.ResponseSlbBean;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponPresenter extends Presenter<CouponView> {

    public void getCouponDtlList(String checkedCoupouInfoId, List<String> liveCourseIds) {
        JSONObject requestData = new JSONObject();
        requestData.put("checkedCoupouInfoId", checkedCoupouInfoId);
        requestData.put("liveCourseIds", liveCourseIds);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).getCouponDtlList2(requestBody).enqueue(new Callback<ResponseSlbBean<CouponPopResponseBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<CouponPopResponseBean>> call, Response<ResponseSlbBean<CouponPopResponseBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnGePopCouponNodata(response.body().getMsg());
                    return;
                }
                getView().OnGetPopCouponSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<CouponPopResponseBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnGetPopCouponFail(string);
                call.cancel();
            }
        });

    }

    public void reCalOrderPrice(String checkedCoupouInfoId, String isFirstReqest, String liveCourseId, String liveOrderId) {
        JSONObject requestData = new JSONObject();
        requestData.put("checkedCoupouInfoId", checkedCoupouInfoId);
        requestData.put("isFirstReqest", isFirstReqest);
        requestData.put("liveCourseId", liveCourseId);
        requestData.put("liveOrderId", liveOrderId);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).reCalOrderPrice(requestBody).enqueue(new Callback<ResponseSlbBean<AvailableCouponInfoBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<AvailableCouponInfoBean>> call, Response<ResponseSlbBean<AvailableCouponInfoBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnReCalOrderPriceNodata(response.body().getMsg());
                    return;
                }
                getView().OnReCalOrderPriceSuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<AvailableCouponInfoBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
                System.out.println("eeeeeeeeeeeeeeeeee:" + t.toString());
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnReCalOrderPriceFail(string);
                call.cancel();
            }
        });

    }
}
