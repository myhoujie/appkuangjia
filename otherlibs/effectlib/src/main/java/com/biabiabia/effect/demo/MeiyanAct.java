package com.biabiabia.effect.demo;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.biabiabia.effect.Accelerometer;
import com.biabiabia.effect.MeiyanApp;
import com.biabiabia.effect.R;
import com.biabiabia.effect.camera.camerautil.CameraController;
import com.biabiabia.effect.camera.widget.CameraSurfaceView;
import com.biabiabia.effect.effectimp.ZZEffectConfig_v2;
import com.biabiabia.effect.face.ZZFaceManager_v2;
import com.biabiabia.effect.face.ZZFaceResult;
import com.biabiabia.effect.utils.LxLinearLayout;
import com.biabiabia.effect.utils.Meiyan2Utils;
import com.hulu.sdk.Faces;
import com.hulu.sdk.HuluUtils;
import com.meiyan2.effect.FilterAdapter;
import com.meiyan2.effect.MagicParams;
import com.meiyan2.effect.helper.MagicFilterType;

import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MeiyanAct extends AppCompatActivity implements View.OnClickListener {

    private static final String EFFECT_TUZI = "effect/tuzi";
    //private static final String EFFECT_PX = "effect/piaoxin";
    //private static final String EFFECT_FW = "effect/xinfeiwen";

    protected TextView specificTip;
    protected TextView loadTx;
    protected TextView unLoadTx;
    protected View isMeiYanView;
    protected TextView btn_switch_paizhao;
    protected TextView btnSwitchFront;
    protected TextView btn_switch_meiyan2;
    protected ImageView btn_camera_closefilter;
    protected View btnSwitchSplash;
    protected CameraSurfaceView mCameraSurfaceView;
    //    private LxLinearLayout layout_filter;
    private LxLinearLayout layout_filter_tab;
    private RecyclerView mFilterListView;
    private FilterAdapter mAdapter;
    //绘制人脸点
    protected SurfaceView mOverlap;
    private Matrix matrix = new Matrix();
    private Paint mPaint;
    private boolean isFirst = true;

    //人脸识别
    private boolean isFaceDebug = false;//绘制人脸点
    private static final int MESSAGE_DRAW_POINTS = 999;
    private boolean isTrackerPaused = false;
    private HandlerThread trackerHandlerThread;
    private Handler trackerHandler;
    private byte[] nv21;
    private byte[] tmp;
    private Accelerometer acc;

    private int realWidth;
    private int realHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meiyan);
        specificTip = findViewById(R.id.specific_tip);
        loadTx = findViewById(R.id.btn_switch_tiezhi);
        unLoadTx = findViewById(R.id.btn_switch_tiezhioff);
        isMeiYanView = findViewById(R.id.btn_switch_meiyan);
        btnSwitchFront = findViewById(R.id.btn_switch_front);
        btnSwitchSplash = findViewById(R.id.btn_switch_flash);
        btn_camera_closefilter = findViewById(R.id.btn_camera_closefilter);
        btn_switch_meiyan2 = findViewById(R.id.btn_switch_meiyan2);
        btn_switch_paizhao = findViewById(R.id.btn_switch_paizhao);
        mCameraSurfaceView = findViewById(R.id.camera);
        mOverlap = findViewById(R.id.surfaceViewOverlap);
//        layout_filter = findViewById(R.id.layout_filter);
        layout_filter_tab = findViewById(R.id.layout_filter_tab);
        mFilterListView = findViewById(R.id.filter_listView);
        layout_filter_tab.setTouch(false);
        btnSwitchFront.setOnClickListener(this);
        loadTx.setOnClickListener(this);
        unLoadTx.setOnClickListener(this);
        isMeiYanView.setOnClickListener(this);
        btn_switch_paizhao.setOnClickListener(this);
        btnSwitchSplash.setOnClickListener(this);
        btn_switch_meiyan2.setOnClickListener(this);
        btn_camera_closefilter.setOnClickListener(this);

        initCamera();

        acc = new Accelerometer(this.getApplicationContext());

        trackerHandlerThread = new HandlerThread("DrawFacePointsThread");
        trackerHandlerThread.start();
        trackerHandler = new Handler(trackerHandlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_DRAW_POINTS) {
                    faceDetect();
                }
            }
        };

        mOverlap.setZOrderOnTop(true);
        mOverlap.getHolder().setFormat(PixelFormat.TRANSLUCENT);
        mPaint = new Paint();
        mPaint.setColor(Color.rgb(57, 138, 243));
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Paint.Style.FILL);

        //
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFilterListView.setLayoutManager(linearLayoutManager);

        mAdapter = new FilterAdapter(this, MagicParams.types);
        mFilterListView.setAdapter(mAdapter);
        mAdapter.setOnFilterChangeListener(onFilterChangeListener);

    }

    private FilterAdapter.onFilterChangeListener onFilterChangeListener = new FilterAdapter.onFilterChangeListener() {

        @Override
        public void onFilterChanged(MagicFilterType filterType, int position) {
//            mCameraSurfaceView.setFilter(null, false, "", false, filterType);
            changeMeiYan2(position, filterType);
        }
    };

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_switch_flash) {//前置摄像头不可开启闪光灯
            if (CameraController.getInstance().getCameraFacing() == Camera.CameraInfo.CAMERA_FACING_BACK) {
                if (btnSwitchSplash.isSelected()) {
                    CameraController.getInstance().closeLight();
                    btnSwitchSplash.setSelected(false);
                } else {
                    CameraController.getInstance().openLight();
                    btnSwitchSplash.setSelected(true);
                }
            }
        } else if (id == R.id.btn_camera_closefilter) {
            Meiyan2Utils.hideFilters(layout_filter_tab);
        } else if (id == R.id.btn_switch_meiyan2) {
            Meiyan2Utils.showFilters(layout_filter_tab);
        } else if (id == R.id.btn_switch_front) {
            if (Camera.getNumberOfCameras() > 1) {
                switchCameraFacing();
                btnSwitchSplash.setSelected(false);
            }
        } else if (id == R.id.btn_switch_paizhao) {
            CameraController.getInstance().getCamera().takePicture(null, null, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] bytes, Camera camera) {
                    savePicture(bytes);
                    shootSound();
                }
            });
        } else if (id == R.id.btn_switch_meiyan) {
            changeMeiYan();
        } else if (id == R.id.btn_switch_tiezhi) {
            loadTx.setEnabled(false);
            String ePath = ZZEffectConfig_v2.effectConfigCopy(this.getApplicationContext(), EFFECT_TUZI);
            Log.e("geek11111", "onClick: 贴纸进来了" + ePath);
            if (ePath != null) {
                mCameraSurfaceView.changeFilter(ePath, true, "", true);
                Log.e("geek11111", "onClick: 贴纸进来了");
            } else {
                loadTx.setEnabled(true);
            }
        } else if (id == R.id.btn_switch_tiezhioff) {
            mCameraSurfaceView.changeFilter(null, false, "", false);
        }
    }


    private void initCamera() {
        Display display = this.getWindowManager().getDefaultDisplay();
        Point outSize = new Point();
        display.getRealSize(outSize);
        realWidth = outSize.x;
        realHeight = outSize.y;

        ViewGroup.LayoutParams params = mCameraSurfaceView.getLayoutParams();
        params.height = realHeight;
        params.width = realWidth;
        mCameraSurfaceView.setLayoutParams(params);

        if (Camera.getNumberOfCameras() > 1) {
            if (CameraController.getInstance().getCameraFacing() == Camera.CameraInfo.CAMERA_FACING_BACK) {
                btnSwitchFront.setSelected(true);
            } else {
                btnSwitchFront.setSelected(false);
            }
        } else {
            btnSwitchFront.setVisibility(View.INVISIBLE);
        }

        isMeiYanView.setSelected(false);
        mCameraSurfaceView.setPreviewCallback(new MyCameraPreviewCallBack(this));
//        mCameraSurfaceView.setHandler(new MyHandler());
        mCameraSurfaceView.init(this, realHeight, realWidth);
        ZZFaceManager_v2.getZZFaceManager().reset(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCameraSurfaceView.onResume();
        specificTip.setText("");

        //启动人脸识别
        isTrackerPaused = false;
        if (acc != null) {
            acc.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCameraSurfaceView.onPause();

        isTrackerPaused = true;
        trackerHandler.removeMessages(MESSAGE_DRAW_POINTS);
        if (acc != null) {
            acc.stop();
        }
    }

    private byte[] save_data;

    private static class MyCameraPreviewCallBack implements Camera.PreviewCallback {
        SoftReference<MeiyanAct> mSoftReference;

        public MyCameraPreviewCallBack(MeiyanAct activity) {
            this.mSoftReference = new SoftReference<>(activity);
        }

        MeiyanAct getMA() {
            if (mSoftReference != null) {
                return mSoftReference.get();
            } else {
                return null;
            }
        }

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            if (getMA() == null || data == null) {
                return;
            }
            CameraController.getInstance().addCallbackBuffer(data);
            if (!ZZFaceManager_v2.getZZFaceManager().canTrack) {
                return;
            }
            int l = getMA().mCameraSurfaceView.getFrameWidth() * getMA().mCameraSurfaceView.getFrameHeight() * 2;
            if (getMA().nv21 == null) {
                if (l >= data.length) {
                    getMA().nv21 = new byte[l];
                } else {
                    getMA().nv21 = new byte[data.length];
                }
            } else {
                if (getMA().nv21.length < data.length) {
                    getMA().nv21 = new byte[data.length];
                }
            }
            if (getMA().tmp == null) {
                if (l >= data.length) {
                    getMA().tmp = new byte[l];
                } else {
                    getMA().tmp = new byte[data.length];
                }
            } else {
                if (getMA().tmp.length < data.length) {
                    getMA().tmp = new byte[data.length];
                }
            }
            synchronized (getMA().nv21) {
                System.arraycopy(data, 0, getMA().nv21, 0, data.length);
            }

            if (getMA().isFirst) {
                getMA().matrix.setScale(getMA().mCameraSurfaceView.getSurfaceWidth() / (float) getMA().mCameraSurfaceView.getFrameHeight(),
                        getMA().mCameraSurfaceView.getSurfaceHeight() / (float) getMA().mCameraSurfaceView.getFrameWidth());
                getMA().isFirst = false;
            }

            getMA().trackerHandler.removeMessages(MESSAGE_DRAW_POINTS);
            getMA().trackerHandler.sendEmptyMessage(MESSAGE_DRAW_POINTS);
        }
    }

    //人脸识别
    private void faceDetect() {
        if (isTrackerPaused) {
            return;
        } else {
            if (!ZZFaceManager_v2.getZZFaceManager().canTrack) {
                return;
            }
            synchronized (nv21) {
                System.arraycopy(nv21, 0, tmp, 0, nv21.length);
            }

            /**
             * 调 实时人脸检测函数，返回当前人脸信息
             */
            boolean frontCamera = CameraController.getInstance().getCameraId() == Camera.CameraInfo.CAMERA_FACING_FRONT;
            Camera.CameraInfo info = CameraController.getInstance().getCameraInfo();

            /**
             * 获取重力传感器返回 方向
             */
            int dir = Accelerometer.getDirection();
            Log.e("geek11111", "dir: " + dir);
            //在使用后置摄像头，且传感器方向为0或2时，后置摄像头与前置orentation相反
            if (!frontCamera && dir == 0) {
                dir = 2;
            } else if (!frontCamera && dir == 2) {
                dir = 0;
            }

            /**
             * 请注意前置摄像头与后置摄像头旋转定义不同
             * 请注意不同手机摄像头旋转定义不同
             */
            if (((info.orientation == 270 && (dir & 1) == 1) ||
                    (info.orientation == 90 && (dir & 1) == 0))) {
                dir = (dir ^ 2);
            }
            float[] faces = Faces.getInstance(MeiyanApp.get()).detect(tmp, mCameraSurfaceView.getFrameWidth(), mCameraSurfaceView.getFrameHeight(), dir);
            Log.e("geek11111", "dir: " + dir);
            Log.e("geek11111", "faceDetect: " + faces.length);
            float[] bound = Faces.getInstance(MeiyanApp.get()).getRect();
            prepareCanvas(faces, bound, frontCamera);
        }
    }

    private void prepareCanvas(float[] faces, float[] boundRect, boolean frontCamera) {
        List<ZZFaceResult> faceResults = new ArrayList<>();
        if (faces.length >= 212) {
            PointF[] points = HuluUtils.getPoints(faces, mCameraSurfaceView.getFrameWidth(), mCameraSurfaceView.getFrameHeight(), frontCamera);

            ZZFaceResult faceResult = new ZZFaceResult(frontCamera, mCameraSurfaceView.getFrameWidth(), mCameraSurfaceView.getFrameHeight());
            faceResult.turn(points, realWidth, realHeight, 0);
            faceResults.add(faceResult);
        }

        ZZFaceManager_v2.getZZFaceManager().updateZZFaceResults(faceResults);

        if (isFaceDebug && faces.length >= 212) {
            if (!mOverlap.getHolder().getSurface().isValid()) {
                return;
            }
            Canvas canvas = mOverlap.getHolder().lockCanvas();
            if (canvas != null) {
                canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                canvas.setMatrix(matrix);
                HuluUtils.drawPoints(canvas, mPaint, faces, mCameraSurfaceView.getFrameWidth(), mCameraSurfaceView.getFrameHeight(), frontCamera);
                mOverlap.getHolder().unlockCanvasAndPost(canvas);
            }
        }
    }

    private void switchCameraFacing() {
        mCameraSurfaceView.change();
        btnSwitchFront.setSelected(!btnSwitchFront.isSelected());
    }

    //美颜开关操作
    private void changeMeiYan() {
        boolean isMy = mCameraSurfaceView.getMeiYan();
        mCameraSurfaceView.setMeiYan(!isMy);
        if (isMy) {
            isMeiYanView.setSelected(false);
        } else {
            isMeiYanView.setSelected(true);
        }
    }

    //美颜开关操作2
    private void changeMeiYan2(int isFirst, MagicFilterType type) {
        boolean isMy = false;
        if (isFirst == 0) {
            isMy = false;
        } else {
            isMy = true;
        }
        mCameraSurfaceView.setMeiYan2(isMy, type);
//        if (isMy) {
//            isMeiYanView.setSelected(false);
//        } else {
//            isMeiYanView.setSelected(true);
//        }
    }

    public void closeFlash() {
        CameraController.getInstance().closeLight();
        btnSwitchSplash.setSelected(false);
    }

    class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case CameraSurfaceView.CameraHandler.AFTER_INITEFFECT:
                    Bundle bundle = msg.getData();
                    String actionInfo = "";
                    if (bundle != null) {
                        actionInfo = (bundle.getString("actionInfo") == null ? "" : bundle.getString("actionInfo"));
                    }
                    specificTip.setText(actionInfo);
                    loadTx.setEnabled(true);
                    break;
                case CameraSurfaceView.CameraHandler.TRACK_FACE_ING:
                    break;
                case CameraSurfaceView.CameraHandler.WHEN_EFFECTSCROLL:
                    break;
                default:
                    break;
            }
        }
    }

    private final static String PATH = Environment.getExternalStorageDirectory().getPath();
    private final static SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.CHINESE);
    private boolean hasInit;//是否初始化过
    private boolean doTaken;//拍照标志
    private final static int quality = 100;//图片质量

    /**
     * 保存图片
     */
    private void savePicture(byte[] data) {
//        Bitmap bitmap = Bitmap.createBitmap(mCameraSurfaceView.getSurfaceWidth(), mCameraSurfaceView.getSurfaceHeight(), Bitmap.Config.ARGB_8888);
//        MYUtils.matToBitmap(frameData, bitmap);
        ContentResolver resolver = getContentResolver();
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        MediaStore.Images.Media.insertImage(resolver, bitmap, "t", "des");
//        String fileName = PATH + File.separator + dataFormat.format(new Date(System.currentTimeMillis())) + ".jpg";
//        FileOutputStream outputStream = null;
//        try {
//            outputStream = new FileOutputStream(fileName);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
//            outputStream.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (outputStream != null) {
//                try {
//                    outputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }

    /**
     * 播放系统拍照声音
     */
    private MediaPlayer mediaPlayer;

    public void shootSound() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        if (volume != 0) {
            if (mediaPlayer == null) {
                mediaPlayer = MediaPlayer.create(this, Uri.parse("file:///system/media/audio/ui/camera_click.ogg"));
            }
            if (mediaPlayer != null) {
                mediaPlayer.start();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCameraSurfaceView.onDestroy();
        if (trackerHandlerThread != null) {
            trackerHandlerThread.quitSafely();
        }
        //closeFlash();
    }
}
