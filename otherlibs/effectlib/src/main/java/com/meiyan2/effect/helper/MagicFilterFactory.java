package com.meiyan2.effect.helper;


import com.meiyan2.effect.advanced.MagicAmaroFilter;
import com.meiyan2.effect.advanced.MagicAntiqueFilter;
import com.meiyan2.effect.advanced.MagicBlackCatFilter;
import com.meiyan2.effect.advanced.MagicBrannanFilter;
import com.meiyan2.effect.advanced.MagicBrooklynFilter;
import com.meiyan2.effect.advanced.MagicCalmFilter;
import com.meiyan2.effect.advanced.MagicCoolFilter;
import com.meiyan2.effect.advanced.MagicCrayonFilter;
import com.meiyan2.effect.advanced.MagicEarlyBirdFilter;
import com.meiyan2.effect.advanced.MagicEmeraldFilter;
import com.meiyan2.effect.advanced.MagicEvergreenFilter;
import com.meiyan2.effect.advanced.MagicFairytaleFilter;
import com.meiyan2.effect.advanced.MagicFreudFilter;
import com.meiyan2.effect.advanced.MagicHealthyFilter;
import com.meiyan2.effect.advanced.MagicHefeFilter;
import com.meiyan2.effect.advanced.MagicHudsonFilter;
import com.meiyan2.effect.advanced.MagicImageAdjustFilter;
import com.meiyan2.effect.advanced.MagicInkwellFilter;
import com.meiyan2.effect.advanced.MagicKevinFilter;
import com.meiyan2.effect.advanced.MagicLatteFilter;
import com.meiyan2.effect.advanced.MagicLomoFilter;
import com.meiyan2.effect.advanced.MagicN1977Filter;
import com.meiyan2.effect.advanced.MagicNashvilleFilter;
import com.meiyan2.effect.advanced.MagicNostalgiaFilter;
import com.meiyan2.effect.advanced.MagicPixarFilter;
import com.meiyan2.effect.advanced.MagicRiseFilter;
import com.meiyan2.effect.advanced.MagicRomanceFilter;
import com.meiyan2.effect.advanced.MagicSakuraFilter;
import com.meiyan2.effect.advanced.MagicSierraFilter;
import com.meiyan2.effect.advanced.MagicSketchFilter;
import com.meiyan2.effect.advanced.MagicSkinWhitenFilter;
import com.meiyan2.effect.advanced.MagicSunriseFilter;
import com.meiyan2.effect.advanced.MagicSunsetFilter;
import com.meiyan2.effect.advanced.MagicSutroFilter;
import com.meiyan2.effect.advanced.MagicSweetsFilter;
import com.meiyan2.effect.advanced.MagicTenderFilter;
import com.meiyan2.effect.advanced.MagicToasterFilter;
import com.meiyan2.effect.advanced.MagicValenciaFilter;
import com.meiyan2.effect.advanced.MagicWaldenFilter;
import com.meiyan2.effect.advanced.MagicWarmFilter;
import com.meiyan2.effect.advanced.MagicWhiteCatFilter;
import com.meiyan2.effect.advanced.MagicXproIIFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageBrightnessFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageContrastFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageExposureFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageHueFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageSaturationFilter;
import com.meiyan2.effect.base.gpuimage.GPUImageSharpenFilter;

public class MagicFilterFactory{
	
	private static MagicFilterType filterType = MagicFilterType.NONE;
	
	public static GPUImageFilter initFilters(MagicFilterType type){
		filterType = type;
		switch (type) {
		case WHITECAT:
			return new MagicWhiteCatFilter();
		case BLACKCAT:
			return new MagicBlackCatFilter();
		case SKINWHITEN:
			return new MagicSkinWhitenFilter();
		case ROMANCE:
			return new MagicRomanceFilter();
		case SAKURA:
			return new MagicSakuraFilter();
		case AMARO:
			return new MagicAmaroFilter();
		case WALDEN:
			return new MagicWaldenFilter();
		case ANTIQUE:
			return new MagicAntiqueFilter();
		case CALM:
			return new MagicCalmFilter();
		case BRANNAN:
			return new MagicBrannanFilter();
		case BROOKLYN:
			return new MagicBrooklynFilter();
		case EARLYBIRD:
			return new MagicEarlyBirdFilter();
		case FREUD:
			return new MagicFreudFilter();
		case HEFE:
			return new MagicHefeFilter();
		case HUDSON:
			return new MagicHudsonFilter();
		case INKWELL:
			return new MagicInkwellFilter();
		case KEVIN:
			return new MagicKevinFilter();
		case LOMO:
			return new MagicLomoFilter();
		case N1977:
			return new MagicN1977Filter();
		case NASHVILLE:
			return new MagicNashvilleFilter();
		case PIXAR:
			return new MagicPixarFilter();
		case RISE:
			return new MagicRiseFilter();
		case SIERRA:
			return new MagicSierraFilter();
		case SUTRO:
			return new MagicSutroFilter();
		case TOASTER2:
			return new MagicToasterFilter();
		case VALENCIA:
			return new MagicValenciaFilter();
		case XPROII:
			return new MagicXproIIFilter();
		case EVERGREEN:
			return new MagicEvergreenFilter();
		case HEALTHY:
			return new MagicHealthyFilter();
		case COOL:
			return new MagicCoolFilter();
		case EMERALD:
			return new MagicEmeraldFilter();
		case LATTE:
			return new MagicLatteFilter();
		case WARM:
			return new MagicWarmFilter();
		case TENDER:
			return new MagicTenderFilter();
		case SWEETS:
			return new MagicSweetsFilter();
		case NOSTALGIA:
			return new MagicNostalgiaFilter();
		case FAIRYTALE:
			return new MagicFairytaleFilter();
		case SUNRISE:
			return new MagicSunriseFilter();
		case SUNSET:
			return new MagicSunsetFilter();
		case CRAYON:
			return new MagicCrayonFilter();
		case SKETCH:
			return new MagicSketchFilter();
		//image adjust
		case BRIGHTNESS:
			return new GPUImageBrightnessFilter();
		case CONTRAST:
			return new GPUImageContrastFilter();
		case EXPOSURE:
			return new GPUImageExposureFilter();
		case HUE:
			return new GPUImageHueFilter();
		case SATURATION:
			return new GPUImageSaturationFilter();
		case SHARPEN:
			return new GPUImageSharpenFilter();
		case IMAGE_ADJUST:
			return new MagicImageAdjustFilter();
		default:
			return null;
		}
	}
	
	public MagicFilterType getCurrentFilterType(){
		return filterType;
	}
}
