package com.meiyan2.effect.advanced;


import com.meiyan2.effect.base.MagicLookupFilter;

public class MagicFairytaleFilter extends MagicLookupFilter {

	public MagicFairytaleFilter() {
		super("filter/fairy_tale.png");
	}
}
